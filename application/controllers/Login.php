<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('Mlogin');
		$this->load->model('admin/Mapp_setting');

		$this->session->keep_flashdata('display');
		$this->session->keep_flashdata('error');
    }

	public function index()
	{
		$data['title'] = 'Login - Sekolah Alam Indonesia';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$data['setting'] = $this->Mapp_setting->find(1);
		
		$this->load->view('layouts/frontend/login',$data);
	}

	public function ujicoba()
	{
		$data['title'] = 'Login - Sekolah Alam Indonesia';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$data['setting'] = $this->Mapp_setting->find(1);
		
		$this->load->view('layouts/frontend/login1',$data);
	}

	function cekLogin() {
		$email = $this->input->post('email');
		$kode_unik = $this->input->post('kode_unik');

		$encrypted = script( $kode_unik, 'e' );
		$loginUser = $this->Mlogin->cekLogin($email, $encrypted);

		if (!empty($loginUser)) {
			// sukses login user
			if ($loginUser['is_kepala'] == 1) {
				$this->session->set_userdata($loginUser);
				redirect('siswa/dashboard');
			}else{
				$notif = "Maaf Anda Bukan Kepala Keluarga !";
				$tipe = "error";
				$this->session->set_flashdata($tipe, $notif);
				redirect(base_url("login/index"));
			}
		} else {
			// gagal login
			$notif = "Maaf Email/Kode Unik anda salah !";
			$tipe = "error";
			$this->session->set_flashdata($tipe, $notif);
			redirect(base_url("login/index"));
		}
	}

    function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }


}