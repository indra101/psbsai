<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('Mlogin');
		$this->load->model('admin/Mlevel');
		$this->load->model('admin/Mortu_daftar');
		$this->load->model('admin/Memail_setting');
		$this->load->model('admin/Mapp_setting');
		$this->load->model('admin/Mortu');

		$this->load->library('image_lib');
    }

	public function index()
	{
		$data['title'] = 'Daftar - Sekolah Alam Indonesia';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$data['levels'] = $this->Mlevel->getLevelOption();
		$data['ortus'] = $this->Mortu->getData(0, true);

		// print_r($data['ortus']);
		// die;

		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$this->load->view('layouts/frontend/daftar',$data);
	}

	public function konfirmasi_bayar()
	{
		$data['title'] = 'Konfirmasi Pembayaran - Sekolah Alam Indonesia';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$this->load->view('layouts/frontend/konfirmasi',$data);
	}

	public function get_kepala_by_email() {
		$email = $this->input->get('email');
		$res = ($email == null) ? null : $this->Mortu->findKepalaByEmail($email);

		echo json_encode($res);
	}

	public function daftar_baru() {
		$username = $this->session->userdata('username');
		$nama_ortu = $this->input->post('nama_ortu');
		$kelamin = $this->input->post('kelamin');
		$email = $this->input->post('email');
		$hp = $this->input->post('hp');
		$status = $this->input->post('status');
		$nama_siswa = $this->input->post('nama_siswa');
		$level = $this->input->post('level');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$pernah_daftar = $this->input->post('pernah_daftar');
		$id_ortu = $this->input->post('id_ortu');

		$setting = $this->Mapp_setting->find(1);
		$is_exist = false;

		$find = $this->Mortu_daftar->findByEmailList($email);
		$find_ortu = $this->Mortu->findByEmail($email);
		$no_daftar = "";

		// print_r(count($find));
		// die;
		if(empty($kelamin) || empty($status)) {
			$notif = "Pendaftaran gagal! Data belum lengkap, silahkan coba lagi";
			$tipe = "error";
			
		} else {

			if($pernah_daftar) {
				// if($find) {
				// 	if(count($find) > 1) {
				// 		$is_exist = true;
				// 		echo "1";
				// 	} else if(count($find) == 1) {
				// 		$is_exist = ($find['email'] == $email) ? false : true;
				// 		echo "2";
				// 	} else {
				// 		$is_exist = false;
				// 	}
				// } else {
				// 	$is_exist = false;
				// }

				if($find_ortu) {
					$is_exist1 = ($find_ortu['email'] == $email) ? false : true;
				} else {
					$is_exist1 = false;
				}

				$is_exist = $is_exist || $is_exist1;

				// echo $is_exist.", ".$is_exist1;
				// die;

			} else {
				$count_find_ortu = ($find_ortu) ? count($find_ortu) : 0;
				
				$is_exist = count($find) > 0 || $count_find_ortu > 0;
			}

			if($is_exist) {

				$notif = "Pendaftaran gagal! E-mail sudah pernah digunakan untuk mendaftar.<br>Silahkan menggunakan E-mail yang berbeda, atau hubungi bagian Admin PSB SAI ".$setting['unit'].".";
				$tipe = "error";

			} else {

				$ortu = array(	"nama" => $nama_ortu,
								"kelamin" => $kelamin,
								"email" => $email,
								"hp" => $hp,
								"status" => $status,
								"pernah_daftar" => $pernah_daftar,
								"id_ortu" => $id_ortu,
								"created_by" => $username,
								);

				$siswa = array();
				$n = 0;

				foreach($nama_siswa as $s) {
					$new = array(	"nama" => $s,
									"level" => $level[$n],
									"tgl_lahir" => $tgl_lahir[$n],
									"created_by" => $username,
								);

					array_push($siswa, $new);
					$n++;
				}

				$daftar = $this->Mortu_daftar->daftar($ortu, $siswa);

				if($daftar) {
					$kk = $this->upload_foto('foto_kk', $daftar);

					$no_daftar = "CP".substr($hp,-3).$daftar;

					$data = array("kk" => $kk, "no_daftar" => $no_daftar);

					$this->Mortu_daftar->update($daftar, $data);

					$n = 0;
					$tot_biaya = 0;
					foreach($siswa as $s) {
						$level = $this->Mlevel->getByKode($s['level']);
						$siswa[$n]['nama_level'] = $level['deskripsi'];

						$biaya = $level['biaya'];
						$siswa[$n]['biaya'] = $biaya;

						$tot_biaya += $biaya;

						$n++;
					}

					$ortu['tot_biaya'] = $tot_biaya;
					$ortu['no_daftar'] = $no_daftar;

					$this->email_daftar($email, $ortu, $siswa);

					$notif = "Pendaftaran berhasil disimpan!<br>Nomor Pendaftaran: <b>$no_daftar</b><br>Konfirmasi Pendaftaran telah dikirimkan ke e-mail Anda<br><br>Silahkan melakukan pembayaran sesuai dengan biaya pendaftaran untuk setiap jenjang sesuai informasi pada e-mail dan melakukan Konfirmasi Pembayaran.<br><br>Terima Kasih";
					$tipe = "display";
				} else {
					$notif = "Pendaftaran gagal! Silahkan coba lagi";
					$tipe = "error";
				}

			}
		}

		$this->session->set_flashdata($tipe, $notif);
		
		redirect(base_url("login"));
	}

	public function submit_konfirmasi() {
		// $email = $this->input->post('email');
		$no_daftar = $this->input->post('no_daftar');
		$no_ref = $this->input->post('no_ref');
		$tgl_transfer = $this->input->post('tgl_transfer');

		// $ortu_daftar = $this->Mortu_daftar->findByEmail($email);
		$ortu_daftar = $this->Mortu_daftar->findByNoDaftar($no_daftar);
		$siswa_daftar = $this->Mortu_daftar->findAnak($ortu_daftar['id']);
		$setting = $this->Mapp_setting->find(1);

		if(!$ortu_daftar) {

			$notif = "Nomor Pendaftaran tidak ditemukan atau salah!<br>Silahkan dicoba lagi.";
			$tipe = "error";

		} else {

			$bukti_transfer = $this->upload_foto('foto_transfer', $ortu_daftar['id']);

			$data = array(	"no_ref" => $no_ref,
							"tgl_transfer" => $tgl_transfer,
							"bukti_transfer" => $bukti_transfer,
							"bayar" => 1,
						);

			$konfirmasi = $this->Mortu_daftar->update($ortu_daftar['id'], $data);

			if($konfirmasi) {

				$n = 0;
				$tot_biaya = 0;
				foreach($siswa_daftar as $s) {
					$level = $this->Mlevel->getByKode($s['level']);
					$siswa_daftar[$n]['nama_level'] = $level['deskripsi'];

					// $biaya = $level['biaya'];
					// $siswa_daftar[$n]['biaya'] = $biaya;

					// $tot_biaya += $biaya;

					$n++;
				}

				$this->email_bayar($email, $ortu_daftar, $siswa_daftar);

				$notif = "Konfirmasi Pembayaran berhasil disimpan!<br>Mohon menunggu proses verifikasi oleh bagian Admin PSB SAI ".$setting['unit'].".<br>Proses verifikasi akan dilakukan pada hari kerja pukul 08.30 - 11.30 WIB.<br>Setelah verifikasi berhasil maka Anda akan mendapatkan Kode Unik melalui e-mail untuk dapat melanjutkan proses pendaftaran.<br><br>Terima Kasih";
				$tipe = "display";
			} else {
				$notif = "Konfirmasi Pembayaran gagal disimpan! Silahkan coba lagi";
				$tipe = "error";
			}

		}

		$this->session->set_flashdata($tipe, $notif);
		
		redirect(base_url("login"));
	}

	function upload_foto($file, $id){
        $config['upload_path']          = './uploads/daftar/';
        $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
        $config['max_size']             = '99999999'; // imb
        $this->load->library('upload', $config);
			// proses upload
		
		$time = date("dmyhis");
		$nama = substr($_FILES[$file]['name'], 0, strrpos($_FILES[$file]['name'], "."));
		$nama_new = $id.'_'.$time.'_'.$file;
		$ext = str_replace($nama, '', $_FILES[$file]['name']);
		$_FILES[$file]['name'] = $nama_new . '.' . $ext;

        $this->upload->do_upload($file);
		$upload = $this->upload->data();

		$config['source_image'] = './uploads/daftar/'.$upload['file_name'];
		$config['new_image'] = './uploads/daftar/thumb/'.$upload['file_name'];;
		//$config['create_thumb'] = TRUE;
		//$config['thumb_marker'] = '_thumb';
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 100;
		//$config['height']       = 40;

		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		
        return $upload['file_name'];
	}
	
	public function email_daftar($email, $ortu, $siswa)
    {
		$setting = $this->Mapp_setting->find(1);
		$data = array('email' => $email, 'ortu' => $ortu, 'siswa' => $siswa, 'setting' => $setting);
		$message = $this->load->view('layouts/frontend/email_daftar.php',$data,TRUE);;
		$title = "Pendaftaran Berhasil Disimpan";

		$this->kirim_email($email, $message, $title);

	}

	public function email_bayar($email, $ortu, $siswa)
    {
		$setting = $this->Mapp_setting->find(1);
		$data = array('email' => $email, 'ortu' => $ortu, 'siswa' => $siswa, 'setting' => $setting);
		$message = $this->load->view('layouts/frontend/email_bayar.php',$data,TRUE);;
		$title = "Konfirmasi Pembayaran Telah Diterima";

		$this->kirim_email($email, $message, $title);

	}

	public function kirim_email($email, $message, $title)
    {    
		$email_setting = $this->Memail_setting->find(1);
		$to = $email;
		$from = $email_setting['from'];
		$name = $email_setting['name'];

		$this->load->library('email');

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => $email_setting['host'],
			'smtp_user' => $from,  // Email gmail
			'smtp_pass'   => $email_setting['password'],  // Password email
			'smtp_crypto' => $email_setting['crypto'],
			'smtp_port'   => $email_setting['port'],
			'wordwrap' => FALSE,
			'wrapchars' => 76,
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'validate' => FALSE,
			'priority' => 1,
			'crlf' => "\r\n",
			'newline' => "\r\n",
			'bcc_batch_mode' => FALSE,
			'bcc_batch_size' => 200
		);

		$this->email->initialize($config);
		$this->email->set_newline("\r\n"); 
		
		$this->email->from($from, $name);
		$this->email->to($to);

		$this->email->subject($title);
		$this->email->message($message);

		if (! $this->email->send()) {
			// show_error($this->email->print_debugger());
			// exit;
		}else{
			//redirect(base_url("case/admin_case/setting?notif_display=1&notification=Silahkan cek email anda untuk melihat keberhasilan pengiriman email anda"));
		}
            
        
    }
}