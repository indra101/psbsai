<?php
class Mlogin extends CI_Model{
    public $table ="ortu";
    protected $primary = 'id';
    
    function cekLogin($email,$kode_unik){
        $this->db->where('email',$email);
        $this->db->where('password', $kode_unik);
        $user = $this->db->get($this->table)->row_array();
        return $user;
    }

    function cekLoginAdmin($email,$password){
        $this->db->where('email',$email);
        $this->db->where('password', $password);
        $this->db->where('is_deleted is null');
        $user = $this->db->get('user_admin')->row_array();
        return $user;
    }

}