<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">

        <div class="row mb-4">
            <div class="col-md-12" style="text-align: center;">
                Silahkan download Surat Pernyataan Orang Tua Siswa dan di-upload kembali setelah ditandatangani di atas meterai Rp 10.000
            </div>
        </div>
        
        <div class="row mb-4">
            <div class="col-md-12" style="text-align: center;">
                <a href="<?=base_url('uploads/pernyataan.pdf')?>" id="btn_download_pernyataan" target="blank" class="btn btn-primary" style="color:white">
                    <i class="fas fa-print mr-2"></i>Download Surat Pernyataan
                </a>
            </div>
        </div>

        <div class="row pt-4" style="border-top: 1px solid lightgrey; text-align: center;">
            <div class="col-md-12">
                <i class="fa fa-image" style="color: #6d7073;"></i>
                <span style="color: #6d7073;">Upload Surat Pernyataan</span>

                <div class="col text-right">
                    <span id="alert_foto" style="color: #fd7e14;display:none;font-size:8pt;">Foto Transfer belum diupload!</span>
                </div>
                <div>
                    <a href="<?=base_url("uploads/pernyataan/").$siswa['pernyataan']?>" target="_blank"><img src="<?=(!empty($siswa['pernyataan'])) ? base_url('uploads/pernyataan/thumb/'.$siswa['pernyataan']) : '';?>" class="img-fluid mx-auto d-block mt-2" id="image_preview_container" style="margin: auto; max-height: 400px; border-radius: 5px; cursor: pointer;"/></a>
                    <!-- <img id="image_preview_container" class="img-fluid mb-2 mt-2" src=""  style="margin: auto; max-height: 400px; border-radius: 5px; cursor: pointer;"> -->
                </div>
                <button type="button" id="upload_btn" class="btn btn-info" onclick="">Pilih File</button>
                <input type="file" name="foto" id="file" style="display:none;"/>
                <div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
                <div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 10 MB</i></span></div>
                <div class="spinner-border mr-2" id="spin" role="status" style="height: 17px; width: 17px; display: none;"></div>
            </div>
            
        </div>

        <!-- <div class="row">
            <div class="input-group">
                <div class="col-md-1">
                    <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="material-icons">camera</i>
                    </span>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="button" class="btn btn-success btn-block btn-sm" onclick="klik()">Upload Foto Siswa</button>
                    <input type="file" name="foto" id="file" onchange="setfilename(this.value)" style="display:none;"/>
                    <?php if(!empty($siswa['foto'])) { ?>
                        <a href="<?=base_url("uploads/siswa/").$siswa['foto']?>" target="_blank"><img src="<?=base_url('uploads/siswa/'.$siswa['foto']);?>" class="img-fluid mx-auto d-block mb-3"/></a>
                    <?php } ?>
                </div>
                <div class="col-md-8">
                    <?=($siswa['foto'])?'<a href="'.base_url("uploads/siswa/").$siswa['foto'].'" target="_blank">':'';?>
                        <input class="form-control" type="text" id="file_name" Placeholder="Foto 3x4 (png/jpg) maksimal 100mb" disabled value="<?=($siswa['foto'])?$siswa['foto']:'';?>"/>
                    <?=($siswa['foto'])?'</a>':'';?>
                </div>
            </div>
        </div> -->

        <?=form_hidden('foto_surat', $siswa['pernyataan'], 'id="foto_surat"');?>
        
        <input type="submit" id="submitBtn" style="display: none;">
        <button class="btn btn-success btn-block mt-4" onclick="return validate()">Simpan<div class="ripple-container"></div></button>
    </div>
    <div class="col-md-1"></div>
</div>

<script>

$('#upload_btn').on('click', function() {
    $('#file').click();
});

$('#btnSubmit').on('click', function() {
    $('#spin').show();
    $('#submitBtn').click();
});

$('#file').change(function(e){

    var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
    var name = this.files[0].name;
    var name_arr = name.split(".");
    var type = name_arr[name_arr.length - 1];
    var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
    var pesan_size = "\nMohon masukkan file dengan ukuran max. 10 MB";
    var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
    var cek_size = size >= 10; // max file size 10 MB
    var cek_tipe = allowed.indexOf(type) == -1;

    if(cek_size || cek_tipe) {
        var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
        if(cek_size)
            pesan += pesan_size;

        if(cek_tipe)
            pesan += pesan_tipe;

        alert(pesan);
        a.value = '';

    } else {
        
        let reader = new FileReader();
        reader.onload = (e) => { 
            $('#image_preview_container').attr('src', e.target.result); 
        }
        reader.readAsDataURL(this.files[0]); 

        $("#foto_surat").val(name);
    }
});

function validate() {
    var res = true;

    if ($("#foto_surat").val() == '') {
        $("#alert_foto").show();
        res = res && false;
    } else {
        $("#alert_foto").hide();
    }
    
    return res;
}

</script>