<div class="jumbotron background">
    <div class="content" style="padding-top:40px;">
        <div class="container-fluid">
        <center><h3 style="color:white;"><b>Surat Pernyataan</b></h3></center>
            <div class="row">
        
                <div class="col-md-8 ml-auto mr-auto">
                <h5><b><a href="<?=base_url('siswa/dashboard');?>"><font style="color:white;"><u>Dashboard</u></a> > </font><a href="<?=base_url('siswa/dashboard/datasiswa');?>"><font style="color:white;"><u>Data Anak</u></a> > </font><a href="<?=base_url('siswa/dashboard/siswa/'.$siswa['id']);?>"><font style="color:white;"><u>Menu Siswa</u></a> > Pernyataan</font></b></h5>
                <?php $this->view('siswa/notif'); ?>
                <div class="card card-signup">
                    <p class="card-title text-center" style="padding:10px;"><b>Surat Pernyataan Orang Tua Siswa</b></p>
                    <?php echo form_open_multipart('siswa/pernyataan/simpan', array('id'=>'form-submit')); ?>
                    <div class="card-body">
                        <input type="hidden" name="id" value="<?=$siswa['id']?>">

                        <?php $this->load->view('form_pernyataan'); ?>
                    
                    </div>
                    <?php echo form_close(); ?>
                </div>
                </div>

            </div>
            
        </div>
    </div>
</div>