<div class="jumbotron background">

<div class="content" style="padding-top:40px;">
      <div class="container-fluid">
      <center><h3 style="color:white;"><b>Data Anak</b></h3></center>
      <br><br>
        <div class="row d-flex justify-content-center" style="padding-left:20px;padding-right:20px;">
        <h5><b><a href="<?=base_url('siswa/dashboard');?>"><font style="color:white;"><u>Dashboard</u></a> > Data Anak</font></b></h5>

        <div class="col-md-12">
          <?php $this->view('siswa/notif'); ?>
        </div>
        
        <?php foreach($siswa as $s){ ?>
          <div class="col-lg-3 col-md-6 col-sm-6">

            <div class="card card-stats">
              <div class="card-header card-header-warning card-header-icon">
              <a href="<?=base_url('siswa/dashboard/siswa/'.$s['id'])?>" style="color:white">
                <div class="card-icon redup">
                  
                    <i class="fas fa-child"></i>
                  
                </div>
                <p class="card-category"><?=$s['deskripsi']?></p>
                <h3 class="card-title"><?=$s['nama_siswa']?></h3>
              </a>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <?php 
                    $complete = getStatusComplete($s['id'],$s['kode']);
                  if($complete == 1){ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/success.png');?>" class="img-fluid icon-status-success"/> Sudah Dilengkapi</p>
                  <?php }else{ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/failed.png');?>" class="img-fluid icon-status-failed"/> Belum Dilengkapi</p>
                  <?php } ?>
                </div>
              </div>
            </div>
            
          </div>
          
        <?php } ?>


        </div>
        
      </div>
    </div>
</div>
