<style>
  .card-stats{
    height: 180px;
  }
</style>

<div class="jumbotron background">

<div class="content" style="padding-top:40px;">
        <div class="container-fluid">
        <center><h3 style="color:white;">Menu Siswa - <b><?=$siswa['nama_siswa']?></b></h3></center>
        <h5><b><a href="<?=base_url('siswa/dashboard');?>"><font style="color:white;"><u>Dashboard</u></a> > </font><a href="<?=base_url('siswa/dashboard/datasiswa');?>"><font style="color:white;"><u>Data Anak</u></a> > Menu Siswa</font></b></h5>
        <br><br>
          <div class="row d-flex justify-content-center" style="padding-left:20px;padding-right:20px;">

            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                <a href="<?=base_url('siswa/dataSiswa/index/'.$this->uri->segment(4))?>" style="color:white">
                  <div class="card-icon redup">
                      <i class="fas fa-child"></i>
                    
                  </div>
                  <?php if($siswa['complete'] == 1){ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/success.png');?>" class="img-fluid icon-status-success"/> Sudah Dilengkapi</p>
                  <?php }else{ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/failed.png');?>" class="img-fluid icon-status-failed"/> Belum Dilengkapi</p>
                  <?php } ?>
                  <h3 class="card-title">
                    Data Siswa
                  </h3>
                </a>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">local_offer</i> 
                    Data pribadi siswa dan saudara kandung
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                <a href="<?=base_url('siswa/pindahan/index/'.$this->uri->segment(4))?>" style="color:white">
                  <div class="card-icon redup">
                      <i class="fas fa-university"></i>
                  </div>
                  <p class="card-category"><img src="<?=base_url('assets/image/success.png');?>" class="img-fluid icon-status-success"/> Tidak Wajib Diisi</p>
                  <h3 class="card-title">Pindahan</h3>
                </a>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Data sekolah asal siswa untuk siswa pindahan dari sekolah lain
                  </div>
                </div>
              </div>
            </div> -->
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                <a href="<?=base_url('siswa/pertanyaan/index/'.$this->uri->segment(4))?>" style="color:white">
                  <div class="card-icon redup">
                      <i class="fas fa-question"></i>
                  </div>
                  <?php if($pertanyaan['complete'] == 1){ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/success.png');?>" class="img-fluid icon-status-success"/> Sudah Dilengkapi</p>
                  <?php }else{ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/failed.png');?>" class="img-fluid icon-status-failed"/> Belum Dilengkapi</p>
                  <?php } ?>
                  <h3 class="card-title">Visi Misi Pendidikan</h3>
                </a>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                  Data visi dan misi pendidikan orang tua
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-primary card-header-icon">
                <a href="<?=base_url('siswa/pernyataan/index/'.$this->uri->segment(4))?>" style="color:white">
                  <div class="card-icon redup">
                    <!-- <a href="<?=base_url('uploads/pernyataan.pdf')?>" style="color:white">
                      <i class="fas fa-print"></i>
                    </a> -->

                      <i class="fas fa-print"></i>

                  </div>
                  <?php if($siswa['pernyataan']){ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/success.png');?>" class="img-fluid icon-status-success"/> Sudah Dilengkapi</p>
                  <?php }else{ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/failed.png');?>" class="img-fluid icon-status-failed"/> Belum Dilengkapi</p>
                  <?php } ?>
                  <h3 class="card-title">Surat Pernyataan</h3>
                </a>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                  Upload surat pernyataan orang tua
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
</div>
