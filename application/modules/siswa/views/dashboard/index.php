<div class="jumbotron background">

<div class="content" style="padding-top:40px;">
      <div class="container-fluid">
      <center><h3 style="color:white;"><b>Pendaftaran Siswa Baru Tahun Pelajaran <?=$setting['tahun_ajaran']?></b></h3></center>
      <center><h4 style="color:white;">Selamat Datang <b><?=($this->session->userdata('kelamin') == 'Laki-laki')? $stat = "Bapak ": $stat = "Ibu "; echo $this->session->userdata('nama_ortu'); ?></b></h4></center>
      <br>
      <br>
        <div class="row" style="padding-left:20px;padding-right:20px;">
          <div class="col-lg-3 col-md-6 col-sm-6"></div>
          <div class="col-lg-3 col-md-6 col-sm-6">

            <div class="card card-stats">
              <div class="card-header card-header-info card-header-icon">
              <a href="<?=base_url('siswa/orangtua')?>" style="color:white">
                <div class="card-icon redup">
                    <i class="fas fa-users"></i>
                </div>
                <h3 class="card-title">
                  Menu<br>Orang Tua
                </h3>
              </a>
                <div class="mt-2">
                <?php 
                    $complete = getStatusCompleteOrtu($this->session->userdata('id'));
                  if($complete == 1){ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/success.png');?>" class="img-fluid icon-status-success"/> Sudah Dilengkapi</p>
                  <?php }else{ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/failed.png');?>" class="img-fluid icon-status-failed"/> Belum Dilengkapi</p>
                  <?php } ?>
                  </div>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">local_offer</i> 
                  Menu yang berisi data Ayah, Ibu, Wali, dan Keluarga.
                </div>
                
              </div>
            </div>
            
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">

            <div class="card card-stats">
              <div class="card-header card-header-warning card-header-icon">
              <a href="<?=base_url('siswa/dashboard/datasiswa')?>" style="color:white">
                <div class="card-icon redup">
                    <i class="fas fa-child"></i>
                </div>
                <h3 class="card-title">Menu<br>Siswa</h3>
              </a>
                <div class="mt-2">
                <?php 
                    $complete = getStatusCompleteAllSiswa($this->session->userdata('id'));
                  if($complete == 1){ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/success.png');?>" class="img-fluid icon-status-success"/> Sudah Dilengkapi</p>
                  <?php }else{ ?>
                    <p class="card-category"><img src="<?=base_url('assets/image/failed.png');?>" class="img-fluid icon-status-failed"/> Belum Dilengkapi</p>
                  <?php } ?>
                  </div>
              </div>
              <div class="card-footer">
                <div class="stats">
                <i class="material-icons">local_offer</i> 
                Menu yang berisi data Siswa dan data pelengkapnya.
                </div>
              </div>
            </div>
            
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6"></div>
        </div>
        
      </div>
    </div>
</div>
