<style>
.checkbox label, .radio label, label {
    font-size: 0.775rem;
}
</style>
<div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">face</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Tanggungan... Orang</span>
                                    </div>
                                    <div class="row">
                                        <input type="number" class="form-control" placeholder="" name="tanggungan" value="<?=$keluarga['tanggungan']?>" title="Tanggungan...Orang" required>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">account_balance_wallet</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Jumlah Pengeluaran per Bulan</span>
                                    </div>
                                    <div class="row">
                                        <select class="form-control" name="pengeluaran" title="Jumlah pengeluaran per bulan" required>
                                            <option value="" disabled <?=(!$keluarga['pengeluaran'])?'selected':''?>>Jumlah pengeluaran per bulan</option>
                                            <option value="< 5.000.000" <?=($keluarga['pengeluaran'] == '< 5.000.000')?'selected':''?>>&lt; 5.000.000</option>
                                            <option value="5.000.000 - 10.000.000" <?=($keluarga['pengeluaran'] == '5.000.000 - 10.000.000')?'selected':''?>>5.000.000 - 10.000.000</option>
                                            <option value="10.000.000 - 15.000.000" <?=($keluarga['pengeluaran'] == '10.000.000 - 15.000.000')?'selected':''?>>10.000.000 - 15.000.000</option>
                                            <option value="15.000.000 - 20.000.000" <?=($keluarga['pengeluaran'] == '15.000.000 - 20.000.000')?'selected':''?>>15.000.000 - 20.000.000</option>
                                            <option value="> 20.000.000" <?=($keluarga['pengeluaran'] == '> 20.000.000')?'selected':''?>>&gt; 20.000.000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">attach_money</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Alokasi dana pendidikan per bulan...Rupiah</span>
                                    </div>
                                    <div class="row">
                                        <input type="text" class="form-control" placeholder="" name="dana_pendidikan_f" id="dana_pendidikan_f" value="<?=$keluarga['dana_pendidikan']?>" onkeyup="numFormat()" title="Alokasi dana pendidikan per bulan...Rupiah" required>
                                        <input type="hidden" name="dana_pendidikan" id="dana_pendidikan">
                                    </div>
                                </div>
                            </div>

                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">wc</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Anak tinggal bersama</span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="tinggal_bersama" title="Anak tinggal bersama" required>
                                                    <option value="" disabled <?=(!$keluarga['tinggal_bersama'])?'selected':''?>>Anak tinggal bersama</option>
                                                    <option value="kedua orang tua kandung" <?=($keluarga['tinggal_bersama'] == 'kedua orang tua kandung')?'selected':''?>>kedua orang tua kandung</option>
                                                    <option value="salah satu orang tua kandung" <?=($keluarga['tinggal_bersama'] == 'salah satu orang tua kandung')?'selected':''?>>salah satu orang tua kandung</option>
                                                    <option value="wali" <?=($keluarga['tinggal_bersama'] == 'wali')?'selected':''?>>wali</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">accessibility</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Yaitu</span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="tinggal_bersama2" value="<?=$keluarga['tinggal_bersama2']?>" title="Yaitu" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">group</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="min-height: 14px; max-height: 40px;">
                                        <span class="judul">Tuliskan semua orang yang tinggal bersama anak dalam satu rumah!</span>
                                    </div>
                                    <div class="row">
                                        <input type="text" class="form-control" placeholder="" name="penghuni" value="<?=$keluarga['penghuni']?>" title="Tuliskan semua orang yang tinggal bersama anak dalam satu rumah !" required>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">mood</i> 
                                    </span>
                                </div>
                                <div class="col-md-10" style="padding-left: 1px;">
                                    <div class="row" style="min-height: 14px; max-height: 40px;">
                                        <div class="col-md-9">
                                            <span class="judul">Orang yang terdekat dengan anak(boleh lebih dari satu) :</span>
                                        </div>
                                        
                                    </div>
                                    <div class="row" style="min-height: 14px;">
                                        <div class="col-md-9">
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="ayah" value="Ayah" <?=($keluarga['ayah'] == 'Ayah')?'checked':'unchecked'?>>Ayah</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input type="checkbox" name="ibu" value="Ibu" <?=($keluarga['ibu'] == 'Ibu')?'checked':'unchecked'?>>Ibu</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input type="checkbox" name="paman" value="Paman" <?=($keluarga['paman'] == 'Paman')?'checked':'unchecked'?>>Paman</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input type="checkbox" name="bibi" value="Bibi" <?=($keluarga['bibi'] == 'Bibi')?'checked':'unchecked'?>>Bibi</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input type="checkbox" name="kakek" value="Kakek" <?=($keluarga['kakek'] == 'Kakek')?'checked':'unchecked'?>>Kakek</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input type="checkbox" name="nenek" value="Nenek" <?=($keluarga['nenek'] == 'Nenek')?'checked':'unchecked'?>>Nenek</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <br>
                                                <label><input type="checkbox" name="lain" value="Lain" <?=($keluarga['lain'] == 'Lain')?'checked':'unchecked'?>>Lainnya, </label>
                                                <input type="text" class="form-control" style="width: 200px; margin-top: -10px !important; margin-bottom: 10px;" placeholder="Sebutkan..." name="orang_dekat" value="<?=$keluarga['orang_dekat']?>">
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">directions_bike</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Jarak dari rumah ke sekolah... km</span>
                                    </div>
                                    <div class="row">
                                        <input type="number" class="form-control" placeholder="" name="jarak" value="<?=$keluarga['jarak']?>" title="Jarak dari rumah ke sekolah...km" required>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">access_time</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Waktu tempuh ke sekolah...menit</span>
                                    </div>
                                    <div class="row">
                                        <input type="number" class="form-control" placeholder="" name="waktu" value="<?=$keluarga['waktu']?>" title="Waktu tempuh ke sekolah...menit" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">emoji_transportation</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Transportasi ke Sekolah</span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="transport" title="Transportasi ke Sekolah" required>
                                                    <option value="" disabled <?=(!$keluarga['transport'])?'selected':''?>>Transportasi ke Sekolah</option>
                                                    <option value="jalan kaki" <?=($keluarga['transport'] == 'jalan kaki')?'selected':''?>>jalan kaki</option>
                                                    <option value="kendaraan umum" <?=($keluarga['transport'] == 'kendaraan umum')?'selected':''?>>kendaraan umum</option>
                                                    <option value="kendaraan pribadi" <?=($keluarga['transport'] == 'kendaraan pribadi')?'selected':''?>>kendaraan pribadi</option>
                                                    <option value="lainnya" <?=($keluarga['transport'] == 'lainnya')?'selected':''?>>lainnya</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">question_answer</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jika Lainnya Sebutkan</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." title="Jika Lainnya Sebutkan..." name="transport2" value="<?=$keluarga['transport2']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">directions_walk</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Anak berangkat / pulang diantar oleh</span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="antar" title="Anak berangkat / pulang diantar oleh" required>
                                                    <option value="" disabled <?=(!$keluarga['antar'])?'selected':''?>>Anak berangkat / pulang diantar oleh</option>
                                                    <option value="Ayah" <?=($keluarga['antar'] == 'Ayah')?'selected':''?>>Ayah</option>
                                                    <option value="Ibu" <?=($keluarga['antar'] == 'Ibu')?'selected':''?>>Ibu</option>
                                                    <option value="supir pribadi" <?=($keluarga['antar'] == 'supir pribadi')?'selected':''?>>supir pribadi</option>
                                                    <option value="lainnya" <?=($keluarga['antar'] == 'lainnya')?'selected':''?>>lainnya</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">question_answer</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jika Lainnya Sebutkan</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." title="Jika Lainnya Sebutkan..." name="antar2" value="<?=$keluarga['antar2']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <br>
                            
                            <button type="submit" class="btn btn-success btn-block">Simpan<div class="ripple-container"></div></button>
                        </div>
                        <div class="col-md-1"></div>
                    </div>

<script>

function numFormat() {
    //alert('dsada');
    var dana = $("#dana_pendidikan_f").val().replace(/[.,\s\D]/g,'').replace(/\b0+/g, '');
    
    $("#dana_pendidikan").val(dana);        
    dana = dana.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    $("#dana_pendidikan_f").val(dana);
}

</script>