<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
<b>Data Pribadi</b>
<input type="hidden" name="id" value="<?=$siswa['id']?>">
    
    <div class="row mt-2">
        <div class="col-md-4">
            <div class="input-group">
                <div class="input-group-prepend" style="align-items: start;">
                    <span class="input-group-text">
                        <i class="material-icons">camera</i>
                    </span>
                </div>
                <div class="col-md-9">
                    <span class="judul">Foto Siswa</span>
                    <button type="button" class="btn btn-success btn-block btn-sm" onclick="klik()">Upload Foto Siswa</button>
                    <input type="file" name="foto" id="file" style="display:none;"/>
                    <?php //if(!empty($siswa['foto'])) { ?>
                        <a href="<?=base_url("uploads/siswa/").$siswa['foto']?>" target="_blank"><img src="<?=(!empty($siswa['foto'])) ? base_url('uploads/siswa/'.$thumb_url.$siswa['foto']) : '';?>" class="img-fluid mx-auto d-block mb-3 mt-2" id="image_preview_container"/></a>
                    <?php //} ?>

                    <div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
                    <div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 10 MB</i></span></div>
                </div>
                <!-- <div class="col-md-8">
                    <?=($siswa['foto'])?'<a href="'.base_url("uploads/siswa/").$siswa['foto'].'" target="_blank">':'';?>
                        <input class="form-control" type="text" id="file_name" Placeholder="Foto 3x4 (png/jpg) maksimal 100mb" disabled value="<?=($siswa['foto'])?$siswa['foto']:'';?>"/>
                    <?=($siswa['foto'])?'</a>':'';?>
                </div> -->

                <?=form_hidden('foto_siswa', $siswa['foto']);?>

            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group">
                
                <div class="input-group-prepend" style="align-items: start;">
                    <span class="input-group-text">
                        <i class="material-icons">contact_page</i>
                    </span>
                </div>
                
                <div class="col-md-9">
                    <span class="judul">Akta Kelahiran</span>
                    <button type="button" class="btn btn-success btn-block btn-sm" onclick="klik_akta()">Upload Akta Kelahiran</button>
                    <input type="file" name="akta_lahir" id="file_akta" style="display:none;"/>
                    <?php //if(!empty($siswa['foto'])) { ?>
                        <a href="<?=base_url("uploads/akta/").$siswa['akta_lahir']?>" target="_blank"><img src="<?=(!empty($siswa['akta_lahir'])) ? base_url('uploads/akta/thumb/'.$siswa['akta_lahir']) : '';?>" class="img-fluid mx-auto d-block mb-3 mt-2" id="image_preview_container_akta"/></a>
                    <?php //} ?>

                    <div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
                    <div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 10 MB</i></span></div>
                </div>

                <?=form_hidden('akta_lahir_siswa', $siswa['akta_lahir']);?>

            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group">
                <div class="input-group-prepend" style="align-items: start;">
                    <span class="input-group-text">
                        <i class="material-icons">family_restroom</i>
                    </span>
                </div>
                <div class="col-md-9">
                    <span class="judul">Kartu Keluarga</span>
                    <a href="<?=base_url("uploads/daftar/").$siswa['kk']?>" target="_blank"><img src="<?=(!empty($siswa['kk'])) ? base_url('uploads/daftar/thumb/'.$siswa['kk']) : '';?>" class="img-fluid mx-auto d-block mb-3 mt-2" id="image_preview_container_kk"/></a>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="material-icons">account_balance_wallet</i>
                    </span>
                </div>
                <div class="col-md-9">
                    <div class="row" style="height: 14px">
                        <span class="judul">NIK</span>
                    </div>
                    <div class="row">
                        <input type="number" class="form-control" placeholder="..." name="nik" value="<?=$siswa['nik']?>" title="NIK" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group" style="background-color: lightgray; border-radius: 20px;">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="material-icons">payment</i>
                    </span>
                </div>
                <div class="col-md-9">
                    <div class="row" style="height: 14px">
                        <span class="judul">No. Kartu Keluarga</span>
                    </div>
                    <div class="row">
                        <input type="number" class="form-control" placeholder="..." name="nokk" value="<?=$siswa['nokk']?>" title="No. Kartu Keluarga" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="material-icons">face</i>
                    </span>
                </div>
                <div class="col-md-9">
                    <div class="row" style="height: 14px">
                        <span class="judul">Nama Lengkap</span>
                    </div>
                    <div class="row">
                        <input type="text" class="form-control" placeholder="..." name="nama_siswa" value="<?=$siswa['nama_siswa']?>" title="Nama Lengkap" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="material-icons">call</i>
                    </span>
                </div>
                <div class="col-md-9">
                    <div class="row" style="height: 14px">
                        <span class="judul">Nama Panggilan</span>
                    </div>
                    <div class="row">
                        <input type="text" class="form-control" placeholder="..." name="panggilan" value="<?=$siswa['panggilan']?>" title="Nama Panggilan" required>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
        

        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">wc</i>
                </span>
            </div>
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Jenis Kelamin</span>
                </div>
                <div class="row">
                    <select class="form-control" name="kelamin" title="Jenis Kelamin" required>
                        <option value="" disabled <?=(!$siswa['kelamin'])?'selected':''?>>Jenis Kelamin</option>
                        <option value="Laki-laki" <?=($siswa['kelamin'] == 'Laki-laki')?'selected':''?>>Laki-laki</option>
                        <option value="Perempuan" <?=($siswa['kelamin'] == 'Perempuan')?'selected':''?>>Perempuan</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">storefront</i>
                        </span>
                    </div>
                    <div class="col-md-9">
                        <div class="row" style="height: 14px">
                            <span class="judul">Tempat Lahir</span>
                        </div>
                        <div class="row">
                            <input type="text" class="form-control" placeholder="..." name="tempat_lahir" value="<?=$siswa['tempat_lahir']?>" title="Tempat Lahir" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">date_range</i>
                        </span>
                    </div>
                    <div class="col-md-9">
                        <div class="row" style="height: 14px">
                            <span class="judul">Tanggal Lahir</span>
                        </div>
                        <div class="row">
                            <input placeholder="Selected date" type="date" id="date-picker-example" class="form-control datepicker" name="tanggal_lahir" value="<?=$siswa['tanggal_lahir']?>" title="Tanggal Lahir" required>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">home</i>
                </span>
            </div>
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Alamat</span>
                </div>
                <div class="row" style="display: block;">
                    <input type="text" class="form-control" placeholder="..." name="alamat" value="<?=$siswa['alamat']?>" title="Alamat" required>
                </div>
            </div>
        </div>

        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">group_work</i>
                </span>
            </div>
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Golongan Darah</span>
                </div>
                <div class="row">
                    <select class="form-control" name="gol_darah" title="Golongan Darah" required>
                        <option value="" disabled <?=(!$siswa['gol_darah'])?'selected':''?>>Golongan Darah</option>
                        <option value="A" <?=($siswa['gol_darah'] == 'A')?'selected':''?>>A</option>
                        <option value="B" <?=($siswa['gol_darah'] == 'B')?'selected':''?>>B</option>
                        <option value="AB" <?=($siswa['gol_darah'] == 'AB')?'selected':''?>>AB</option>
                        <option value="O" <?=($siswa['gol_darah'] == 'O')?'selected':''?>>O</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">child_care</i>
                        </span>
                    </div>
                    <div class="col-md-9">
                        <div class="row" style="height: 14px">
                            <span class="judul">Anak Ke</span>
                        </div>
                        <div class="row">
                            <input type="number" min="1" class="form-control" placeholder="..." name="anak_ke" value="<?=$siswa['anak_ke']?>" title="Anak Ke" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">toys</i>
                        </span>
                    </div>
                    <div class="col-md-9">
                        <div class="row" style="height: 14px">
                            <span class="judul">Dari...Bersaudara</span>
                        </div>
                        <div class="row">
                            <input type="number" min="1" class="form-control" placeholder="..." name="saudara" value="<?=$siswa['saudara']?>" title="Dari...Bersaudara" required>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">home_work</i>
                </span>
            </div>
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Agama</span>
                </div>
                <div class="row">
                    <select class="form-control" name="agama" title="Agama" required>
                        <option value="" disabled <?=(!$siswa['agama'])?'selected':''?>>Pilih Agama</option>
                        <option value="Islam" <?=($siswa['gol_darah'] == 'Islam')?'selected':''?>>Islam</option>
                        <option value="Kristen" <?=($siswa['gol_darah'] == 'Kristen')?'selected':''?>>Kristen</option>
                        <option value="Hindu" <?=($siswa['gol_darah'] == 'Hindu')?'selected':''?>>Hindu</option>
                        <option value="Budha" <?=($siswa['gol_darah'] == 'Budha')?'selected':''?>>Budha</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">flag</i>
                </span>
            </div>
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Kewarganegaraan</span>
                </div>
                <div class="row">
                    <input type="text" class="form-control" placeholder="..." name="warga_negara" value="<?=$siswa['warga_negara']?>" title="Kewarganegaraan" required>
                </div>
            </div>
        </div>

        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">local_florist</i>
                </span>
            </div>
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Suku</span>
                </div>
                <div class="row">
                    <input type="text" class="form-control" placeholder="..." name="suku" value="<?=$siswa['suku']?>" title="Suku" required>
                </div>
            </div>
        </div>

    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <i class="material-icons">pregnant_woman</i>
            </span>
        </div>
        <div class="col-md-10">
            <div class="row" style="height: 14px">
                <span class="judul">Bahasa Ibu</span>
            </div>
            <div class="row">
                <input type="text" class="form-control" placeholder="..." name="bahasa" value="<?=$siswa['bahasa']?>" title="Bahasa Ibu" required>
            </div>
        </div>
    </div>

    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <i class="material-icons">sports_basketball</i>
            </span>
        </div>
        <div class="col-md-10">
            <div class="row" style="height: 14px">
                <span class="judul">Bakat yang Tampak</span>
            </div>
            <div class="row">
                <input type="text" class="form-control" placeholder="..." name="bakat" value="<?=$siswa['bakat']?>" title="Bakat yang Tampak" required>
            </div>
        </div>
    </div>

    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <i class="material-icons">sports_handball</i>
            </span>
        </div>
        <div class="col-md-10">
            <div class="row" style="height: 14px">
                <span class="judul">Hobi</span>
            </div>
            <div class="row">
                <input type="text" class="form-control" placeholder="..." name="hobby" value="<?=$siswa['hobby']?>" title="Hobi" required>
            </div>
        </div>
    </div>

    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <i class="material-icons">rocket_launch</i>
            </span>
        </div>
        <div class="col-md-10">
            <div class="row" style="height: 14px">
                <span class="judul">Cita-cita</span>
            </div>
            <div class="row">
                <input type="text" class="form-control" placeholder="..." name="cita_cita" value="<?=$siswa['cita_cita']?>" title="Cita-cita" required>
            </div>
        </div>
    </div>

    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <i class="material-icons">sentiment_satisfied_alt</i>
            </span>
        </div>
        <div class="col-md-10">
            <div class="row" style="height: 14px">
                <span class="judul">Kekhususan Anak</span>
            </div>
            <div class="row">
                <input type="text" class="form-control" placeholder="..." name="khusus" value="<?=$siswa['khusus']?>" title="Kekhususan Anak" required>
            </div>
        </div>
    </div>
    <br>
    <b>Data Sekolah Asal (Wajib untuk SD1, SL7 dan Siswa Pindahan)</b>
        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">payment</i>
                        </span>
                    </div>
                    <div class="col-md-10">
                        <div class="row" style="height: 14px">
                            <span class="judul">Nomor Induk Siswa Nasional (NISN)</span>
                        </div>
                        <div class="row">
                            <input type="text" class="form-control" placeholder="..." name="nisn" value="<?=$siswa['nisn']?>" title="NISN" required>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">school</i>
                        </span>
                    </div>
                    <div class="col-md-10">
                        <div class="row" style="height: 14px">
                            <span class="judul">Asal Sekolah</span>
                        </div>
                        <div class="row">
                            <input type="text" class="form-control" placeholder="..." name="asal_sekolah" value="<?=$siswa['asal_sekolah']?>" title="Asal Sekolah" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">payment</i>
                        </span>
                    </div>
                    <div class="col-md-10">
                        <div class="row" style="height: 14px">
                            <span class="judul">NPSN Sekolah Asal</span>
                        </div>
                        <div class="row">
                            <input type="text" class="form-control" placeholder="..." name="npsn_sekolah_asal" value="<?=$siswa['npsn_sekolah_asal']?>" title="NPSN Sekolah Asal" required>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">signpost</i>
                </span>
            </div>
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Alamat Sekolah Asal</span>
                </div>
                <div class="row">
                    <input type="text" class="form-control" placeholder="..." name="alamat_sekolah_asal" value="<?=$siswa['alamat_sekolah_asal']?>" title="Alamat" >
                </div>
            </div>
        </div>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">question_answer</i>
                </span>
            </div>
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Alasan Pindah</span>
                </div>
                <div class="row">
                    <input type="text" class="form-control" placeholder="..." name="alasan_pindah" value="<?=$siswa['alasan_pindah']?>" title="Alasan Pindah" >
                </div>
            </div>
        </div>
        
    <br>
    <b>Data Saudara Kandung</b><!--&nbsp;<a href="javascript:void(0)" onclick="tambah()"><i class="material-icons" style="transform:translateY(-3px);">person_add</i></a>-->
    <?php $i=1; foreach($saudara as $s){ ?>
        <div class="row" id="tambah">
            <input type="hidden" name="s_id<?=$i?>" value="<?=$s['id']?>">
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">face</i>
                        </span>
                    </div>
                    <div class="col-md-9">
                        <div class="row" style="height: 14px;">
                            <span class="judul">Nama</span>
                        </div>
                        <div class="row" style="display: block;">
                            <input type="text" class="form-control" placeholder="..." name="s_nama<?=$i?>" value="<?=$s['nama']?>" title="Nama">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">accessibility_new</i>
                        </span>
                    </div>
                    <div class="col-md-4">
                        <div class="row" style="height: 14px">
                            <span class="judul">Umur</span>
                        </div>
                        <div class="row">
                            <input type="number" class="form-control" placeholder="..." title="Umur" name="s_umur<?=$i?>" value="<?=$s['umur']?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">school</i>
                        </span>
                    </div>
                    <div class="col-md-9">
                        <div class="row" style="height: 14px">
                            <span class="judul">Nama Sekolah</span>
                        </div>
                        <div class="row" style="display: block;">
                            <input type="text" class="form-control" placeholder="..." title="Nama Sekolah" name="s_sekolah<?=$i?>" value="<?=$s['sekolah']?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">account_balance</i>
                        </span>
                    </div>
                    <div class="col-md-5">
                        <div class="row" style="height: 14px">
                            <span class="judul">Kelas</span>
                        </div>
                        <div class="row">
                            <input type="text" class="form-control" placeholder="..." title="Kelas" name="s_kelas<?=$i?>" value="<?=$s['kelas']?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php $i++;} ?>
    <br>
    <button type="submit" class="btn btn-success btn-block">Simpan<div class="ripple-container"></div></button>
</div>
<div class="col-md-1"></div>
</div>

<script>

function klik(){
    $('#file').click();
}
    
$('#file').change(function(e){

    var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
    var name = this.files[0].name;
    var name_arr = name.split(".");
    var type = name_arr[name_arr.length - 1];
    var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
    var pesan_size = "\nMohon masukkan file dengan ukuran max. 10 MB";
    var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
    var cek_size = size >= 10; // max file size 10 MB
    var cek_tipe = allowed.indexOf(type) == -1;

    if(cek_size || cek_tipe) {
        var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
        if(cek_size)
            pesan += pesan_size;

        if(cek_tipe)
            pesan += pesan_tipe;

        alert(pesan);
        a.value = '';

    } else {
        
        let reader = new FileReader();
        reader.onload = (e) => { 
            $('#image_preview_container').attr('src', e.target.result); 
        }
        reader.readAsDataURL(this.files[0]); 

    }
});

function klik_akta(){
    $('#file_akta').click();
}

$('#file_akta').change(function(e){

var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
var name = this.files[0].name;
var name_arr = name.split(".");
var type = name_arr[name_arr.length - 1];
var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
var pesan_size = "\nMohon masukkan file dengan ukuran max. 10 MB";
var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
var cek_size = size >= 10; // max file size 10 MB
var cek_tipe = allowed.indexOf(type) == -1;

if(cek_size || cek_tipe) {
    var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
    if(cek_size)
        pesan += pesan_size;

    if(cek_tipe)
        pesan += pesan_tipe;

    alert(pesan);
    a.value = '';

} else {
    
    let reader = new FileReader();
    reader.onload = (e) => { 
        $('#image_preview_container_akta').attr('src', e.target.result); 
    }
    reader.readAsDataURL(this.files[0]); 

}
});
</script>