<div class="jumbotron background">
    <div class="content" style="padding-top:40px;">
        <div class="container-fluid">
        <center><h3 style="color:white;"><b>Data Siswa</b></h3></center>
            <div class="row">
        
                <div class="col-md-8 ml-auto mr-auto">
                <h5><b><a href="<?=base_url('siswa/dashboard');?>"><font style="color:white;"><u>Dashboard</u></a> > </font><a href="<?=base_url('siswa/dashboard/datasiswa');?>"><font style="color:white;"><u>Data Anak</u></a> > </font><a href="<?=base_url('siswa/dashboard/siswa/'.$siswa['id']);?>"><font style="color:white;"><u>Menu Siswa</u></a> > Data Siswa</font></b></h5>
                <?php $this->view('siswa/notif'); ?>
                <div class="card card-signup">
                    <p class="card-title text-center" style="padding:10px;"><b>Isi Data Siswa Dengan Benar</b></p>
                    <?php echo form_open_multipart('siswa/dataSiswa/simpan', array('id'=>'form-submit')); ?>
                    <div class="card-body">
                    
                    <?php $this->load->view('form_datasiswa'); ?>
                    
                    </div>
                    <?php echo form_close(); ?>
                </div>
                </div>

            </div>
            
        </div>
    </div>
</div>

<script>
    function tambah(){
        $("#tambah").clone().insertAfter("#tambah");
    }
</script>