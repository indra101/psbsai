<input type="hidden" name="id" value="<?=$kesehatan['id']?>">
<input type="hidden" name="id_siswa" value="<?=$kesehatan['id_siswa']?>">
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
    <b>Data Pribadi</b>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">accessibility</i>
                </span>
            </div>
            <div class="col-md-2">
                <div class="row" style="height: 14px">
                    <span class="judul">Tinggi Badan...cm</span>
                </div>
                <div class="row">
                    <input type="number" class="form-control" placeholder="..." title="Tinggi Badan...cm" name="tinggi" value="<?=$kesehatan['tinggi']?>" required>
                </div>
            </div>
        </div>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">child_care</i>
                </span>
            </div>
            <div class="col-md-2">
                <div class="row" style="height: 14px">
                    <span class="judul">Berat Badan...kg</span>
                </div>
                <div class="row">
                    <input type="number" class="form-control" placeholder="..." title="Berat Badan...kg" name="berat" value="<?=$kesehatan['berat']?>" required>
                </div>
            </div>
        </div>
        
        <br>
        <b>Penyakit yang pernah diderita</b><!--&nbsp;<a href="javascript:void(0)" onclick="tambah()"><i class="material-icons" style="transform:translateY(-3px);">plus_one</i></a>-->
        <div class="row mt-2">
            <div class="col-md-2">
                <span class="judul">Penyakit/Kondisi Fisik</span>
            </div>
            <div class="col-md-2">
                <span class="judul">Tahun Periode</span>
            </div>
            <div class="col-md-3">
                <span class="judul">Pengobatan/Penanganan</span>
            </div>
            <div class="col-md-4">
                <span class="judul">Deskripsi</span>
            </div>
            <div class="col-md-1">
                <span class="judul">Status</span>
            </div>
        </div>
        <?php $i=1; foreach($sub_kesehatan as $s){ ?>
        <div class="row" id="tambah">
            <input type="hidden" name="s_id<?=$i?>" value="<?=$s['id']?>">
            <div class="col-md-2">
                <input type="text" class="form-control" placeholder="..." title="Penyakit/Kondisi Fisik..." name="s_nama_penyakit<?=$i?>" value="<?=$s['nama_penyakit']?>">
            </div>
            <div class="col-md-2">
                <input type="text" class="form-control" placeholder="..." title="Tahun Periode..." name="s_tahun<?=$i?>" value="<?=$s['tahun']?>">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" placeholder="..." title="Pengobatan/Penanganan..." name="s_pengobatan<?=$i?>" value="<?=$s['pengobatan']?>">
            </div>
            <!-- <div class="col-md-2">
                <input type="text" class="form-control" placeholder="..." title="Pantangan..." name="s_pantangan<?=$i?>" value="<?=$s['pantangan']?>">
            </div> -->
            <div class="col-md-4">
                <input type="text" class="form-control" placeholder="..." title="Deskripsi..." name="s_deskripsi<?=$i?>" value="<?=$s['deskripsi']?>">
            </div>
            <div class="col-md-1">
                <select class="form-control" name="s_kondisi<?=$i?>" title="Status">
                    <option value="" disabled <?=(!$s['kondisi'])?'selected':''?>>Status</option>
                    <option value="Selesai" <?=($s['kondisi'] == 'Sembuh' || $s['kondisi'] == 'Selesai')?'selected':''?>>Selesai</option>
                    <option value="Lanjut" <?=($s['kondisi'] == 'Dalam Pengobatan' || $s['kondisi'] == 'Lanjut')?'selected':''?>>Lanjut</option>
                </select>
            </div>
        </div>
        <?php $i++;} ?>
        <br>
        <span class="" style="font-size: 12px;">Contoh Penyakit:<br>
        1. Skoliosis<br>
        2. Rabun dengan terapi kacamata<br>
        3. Alergi obat<br>
        4. Asma<br>
        5. Disabilitas<br>
        </span>
        <button type="submit" class="btn btn-success btn-block mt-4">Simpan<div class="ripple-container"></div></button>
    </div>
    <div class="col-md-1"></div>
</div>