<div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                        <b>Pra Kelahiran</b>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">pregnant_woman</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Kondisi kehamilan</span>
                                    </div>
                                    <div class="row">
                                        <select class="form-control" name="kondisi_hamil" title="Kondisi kehamilan" required>
                                            <option value="" disabled <?=(!$kelahiran['kondisi_hamil'])?'selected':''?>>Kondisi kehamilan</option>
                                            <option value="Cukup Bulan" <?=($kelahiran['kondisi_hamil'] == 'Cukup Bulan')?'selected':''?>>Cukup Bulan</option>
                                            <option value="Kurang Bulan" <?=($kelahiran['kondisi_hamil'] == 'Kurang Bulan')?'selected':''?>>Kurang Bulan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">accessibility_new</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Kondisi Ibu</span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="kondisi_ibu" title="Kondisi Ibu" required>
                                                    <option value="" disabled <?=(!$kelahiran['kondisi_ibu'])?'selected':''?>>Kondisi Ibu</option>
                                                    <option value="Sehat / Normal" <?=($kelahiran['kondisi_ibu'] == 'Sehat / Normal')?'selected':''?>>Sehat / Normal</option>
                                                    <option value="Bermasalah" <?=($kelahiran['kondisi_ibu'] == 'Bermasalah')?'selected':''?>>Bermasalah</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">question_answer</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jika Bermasalah Yaitu</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." title="Jika Bermasalah Yaitu..." name="kondisi_ibu2" value="<?=$kelahiran['kondisi_ibu2']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">local_florist</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Kondisi Lingkungan</span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="lingkungan" title="Kondisi Lingkungan" required>
                                                    <option value="" disabled <?=(!$kelahiran['lingkungan'])?'selected':''?>>Kondisi Lingkungan</option>
                                                    <option value="Pedesaan" <?=($kelahiran['lingkungan'] == 'Pedesaan')?'selected':''?>>Pedesaan</option>
                                                    <option value="Perkotaan" <?=($kelahiran['lingkungan'] == 'Perkotaan')?'selected':''?>>Perkotaan</option>
                                                    <option value="Lingkungan Pantai" <?=($kelahiran['lingkungan'] == 'Lingkungan Pantai')?'selected':''?>>Lingkungan Pantai</option>
                                                    <option value="Lainnya" <?=($kelahiran['lingkungan'] == 'Lainnya')?'selected':''?>>Lainnya</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">question_answer</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jika Pilih Lainnya Tuliskan</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." title="Jika Pilih Lainnya Tuliskan..." name="lingkungan2" value="<?=$kelahiran['lingkungan2']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">healing</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Selama kehamilan pernah dirawat di Rumah Sakit</span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="perawatan" title="Selama kehamilan pernah mengalami perawatan di Rumah Sakit" required>
                                                    <option value="" disabled <?=(!$kelahiran['perawatan'])?'selected':''?>>Selama kehamilan pernah mengalami perawatan di Rumah Sakit</option>
                                                    <option value="Ya" <?=($kelahiran['perawatan'] == 'Ya')?'selected':''?>>Ya</option>
                                                    <option value="Tidak" <?=($kelahiran['perawatan'] == 'Tidak')?'selected':''?>>Tidak</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">question_answer</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jika Ya Sebutkan</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." title="Jika Ya Sebutkan..." name="perawatan2" value="<?=$kelahiran['perawatan2']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <b>Kelahiran</b>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">accessibility_new</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Pekan Usia Kelahiran</span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="usia_pekan" value="<?=$kelahiran['usia_pekan']?>" title="Pekan Usia Kelahiran" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">accessibility_new</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Hari</span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="usia_hari" value="<?=$kelahiran['usia_hari']?>" title="Hari" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">question_answer</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Apakah ada kasus khusus penyebab kelahiran? Sebutkan !</span>
                                    </div>
                                    <div class="row">
                                        <input type="text" class="form-control" placeholder="..." name="kasus" value="<?=$kelahiran['kasus']?>" title="Apakah ada kasus khusus penyebab kelahiran?" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">local_florist</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Proses kelahiran</span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="proses_lahir" title="Proses kelahiran" required>
                                                    <option value="" disabled <?=(!$kelahiran['proses_lahir'])?'selected':''?>>Proses kelahiran</option>
                                                    <option value="Spontan" <?=($kelahiran['proses_lahir'] == 'Spontan')?'selected':''?>>Spontan</option>
                                                    <option value="Vacum" <?=($kelahiran['proses_lahir'] == 'Vacum')?'selected':''?>>Vacum</option>
                                                    <option value="Caesar" <?=($kelahiran['proses_lahir'] == 'Caesar')?'selected':''?>>Caesar</option>
                                                    <option value="Forceps" <?=($kelahiran['proses_lahir'] == 'Forceps')?'selected':''?>>Forceps</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">question_answer</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Karena</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="proses_lahir2" value="<?=$kelahiran['proses_lahir2']?>" title="Karena" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <b>Riwayat Perkembangan Anak</b>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">child_friendly</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Tengkurap usia</span>
                                    </div>
                                    <div class="row">
                                        <input type="text" class="form-control" placeholder="..." name="usia_tengkurap" value="<?=$kelahiran['usia_tengkurap']?>" title="Tengkurap usia..." required>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">directions_walk</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Berjalan usia</span>
                                    </div>
                                    <div class="row">
                                        <input type="text" class="form-control" placeholder="..." name="usia_berjalan" value="<?=$kelahiran['usia_berjalan']?>" title="Berjalan usia" required>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">sentiment_very_satisfied</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Bicara (2 suku kata contoh : mama, papa) usia</span>
                                    </div>
                                    <div class="row">
                                        <input type="text" class="form-control" placeholder="..." name="bicara" value="<?=$kelahiran['bicara']?>" title="Bicara (2 suku kata contoh : mama, papa) usia" required>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">airline_seat_legroom_reduced</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Merangkak usia</span>
                                    </div>
                                    <div class="row">
                                        <input type="text" class="form-control" placeholder="..." name="merangkak" value="<?=$kelahiran['merangkak']?>" title="Merangkak usia" required>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">priority_high</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Mengerti diperintah (tepuk tangan, dadah) usia</span>
                                    </div>
                                    <div class="row">
                                        <input type="text" class="form-control" placeholder="..." name="perintah" value="<?=$kelahiran['perintah']?>" title="Mengerti diperintah (tepuk tangan, dadah) usia" required>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <button type="submit" class="btn btn-success btn-block">Simpan<div class="ripple-container"></div></button>
                        </div>
                        <div class="col-md-1"></div>
                    </div>