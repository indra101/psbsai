<div class="jumbotron background">
    <div class="content" style="padding-top:40px;">
        <div class="container-fluid">
        <center><h3 style="color:white;"><b>Data Kelahiran</b></h3></center>
            <div class="row">
        
                <div class="col-md-8 ml-auto mr-auto">
                <h5><b><a href="<?=base_url('siswa/dashboard');?>"><font style="color:white;"><u>Dashboard</u></a> > </font><a href="<?=base_url('siswa/dashboard/datasiswa');?>"><font style="color:white;"><u>Data Anak</u></a> > </font><a href="<?=base_url('siswa/dashboard/siswa/'.$siswa['id']);?>"><font style="color:white;"><u>Menu Siswa</u></a> > Data Kelahiran</font></b></h5>
                <?php $this->view('siswa/notif'); ?>
                <div class="card card-signup">
                    <p class="card-title text-center" style="padding:10px;"><b>Isi Data Riwayat Kelahiran Dengan Benar</b></p>
                    <div class="card-body">
                    <?php echo form_open_multipart('siswa/kelahiran/simpan', array('id'=>'form-submit')); ?>
                        <input type="hidden" name="id" value="<?=$kelahiran['id']?>">
                        <input type="hidden" name="id_siswa" value="<?=$kelahiran['id_siswa']?>">
                    
                        <?php $this->load->view('form_kelahiran'); ?>

                    </div>
                    <?php echo form_close(); ?>
                </div>
                </div>

            </div>
            
        </div>
    </div>
</div>
