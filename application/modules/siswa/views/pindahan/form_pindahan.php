<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
<b>Siswa Pindahan</b>
    <input type="hidden" name="id" value="<?=$pindahan['id']?>">
    <input type="hidden" name="id_siswa" value="<?=$pindahan['id_siswa']?>">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <i class="material-icons">account_balance_wallet</i>
            </span>
        </div>
        <div class="col-md-10">
            <div class="row" style="height: 14px;">
                <span class="judul">NISN</span>
            </div>
            <div class="row">
                <input type="number" class="form-control" placeholder="..." title="No. Induk Siswa Nasional..." name="nisn" value="<?=$pindahan['nisn']?>">
            </div>
        </div>
    </div>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <i class="material-icons">school</i>
            </span>
        </div>
        <div class="col-md-10">
            <div class="row" style="height: 14px">
                <span class="judul">Nama Sekolah Asal</span>
            </div>
            <div class="row" style="display: block;">
                <input type="text" class="form-control" placeholder="..." title="Nama Sekolah Asal..." name="nama_sekolah" value="<?=$pindahan['nama_sekolah']?>">
            </div>
        </div>
    </div>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <i class="material-icons">home</i>
            </span>
        </div>
        <div class="col-md-10">
            <div class="row" style="height: 14px">
                <span class="judul">Alamat Sekolah Asal</span>
            </div>
            <div class="row" style="display: block;">
                <input type="text" class="form-control" placeholder="..." title="Alamat Sekolah Asal..." name="alamat_sekolah" value="<?=$pindahan['alamat_sekolah']?>">
            </div>
        </div>
    </div>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <i class="material-icons">question_answer</i>
            </span>
        </div>
        <div class="col-md-10">
            <div class="row" style="height: 14px">
                <span class="judul">Alasan Pindah</span>
            </div>
            <div class="row" style="display: block;">
                <!-- <input type="text" class="form-control" placeholder="..." title="Alasan Pindah..." name="alasan_pindah" value="<?=$pindahan['alasan_pindah']?>"> -->
                <textarea class="form-control" placeholder="..." title="Alasan Pindah..." name="alasan_pindah"><?=$pindahan['alasan_pindah']?></textarea>
            </div>
        </div>
    </div>
    
    <br>
    <b>Riwayat Sekolah </b><!--&nbsp;<a href="javascript:void(0)" onclick="tambah()"><i class="material-icons" style="transform:translateY(-3px);">plus_one</i></a>-->
    <?php $i=1; foreach($sub_pindahan as $s){ ?>
    <div class="row" id="tambah">
    <input type="hidden" name="s_id<?=$i?>" value="<?=$s['id']?>">
        <div class="col-md-2">
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Tahun</span>
                </div>
                <div class="row">
                    <input type="text" style="height: 55px" class="form-control" placeholder="..." title="Tahun..." name="s_tahun<?=$i?>" value="<?=$s['tahun']?>">
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Kelas</span>
                </div>
                <div class="row">
                    <input type="text" style="height: 55px" class="form-control" placeholder="..." title="Kelas..." name="s_kelas<?=$i?>" value="<?=$s['kelas']?>">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Nama Sekolah</span>
                </div>
                <div class="row">
                    <input type="text" style="height: 55px" class="form-control" placeholder="..." title="Nama Sekolah..." name="s_nama_sekolah<?=$i?>" value="<?=$s['nama_sekolah']?>">
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="col-md-10">
                <div class="row" style="height: 14px">
                    <span class="judul">Alasan Pindah</span>
                </div>
                <div class="row">
                    <!-- <input type="text" class="form-control" placeholder="..." title="Alasan Pindah..." name="s_alasan_pindah<?=$i?>" value="<?=$s['alasan_pindah']?>"> -->
                    <textarea class="form-control" style="height: 50px" placeholder="..." title="Alasan Pindah..." name="s_alasan_pindah<?=$i?>"><?=$s['alasan_pindah']?></textarea>
                </div>
            </div>
        </div>
    </div>
    <?php $i++;} ?>
    <br>
    <button type="submit" class="btn btn-success btn-block">Simpan<div class="ripple-container"></div></button>
</div>
<div class="col-md-1"></div>
</div>