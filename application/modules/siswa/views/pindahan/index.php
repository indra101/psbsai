<div class="jumbotron background">
    <div class="content" style="padding-top:40px;">
        <div class="container-fluid">
        <center><h4 style="color:white;"><a href="<?=base_url('siswa/dashboard/siswa/'.$siswa['id']);?>"><button type="button" class="btn btn-info">Kembali</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data Pindahan</h4></center>
            <div class="row">
        
                <div class="col-md-8 ml-auto mr-auto">
                <?php $this->view('siswa/notif'); ?>
                <div class="card card-signup">
                    <p class="card-title text-center" style="padding:10px;">Diisi jika siswa pindahan dari sekolah lain</p>
                    <div class="card-body">
                    <?php echo form_open_multipart('siswa/pindahan/simpan', array('id'=>'form-submit')); ?>
                    
                    <?php $this->load->view('form_pindahan'); ?>

                    </div>
                    <?php echo form_close(); ?>
                </div>
                </div>

            </div>
            
        </div>
    </div>
</div>

<script>
    function tambah(){
        $("#tambah").clone().insertAfter("#tambah");
    }
</script>