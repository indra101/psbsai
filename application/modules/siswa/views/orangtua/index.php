<div class="jumbotron background">
    <div class="content" style="padding-top:40px;">
        <div class="container-fluid">
        <center><h3 style="color:white;"><b>Data Orang Tua</b></h3></center>
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                <h5><b><a href="<?=base_url('siswa/dashboard');?>"><font style="color:white;"><u>Dashboard</u></a> > Data Orang Tua</font></b></h5>
                <?php $this->view('siswa/notif'); ?>
                <div class="card card-signup">
                    <p class="card-title text-center" style="padding:10px;"><b>Isi Data Orang Tua Dengan Benar</b></p>
                    <p class="card-title text-center" style="margin-top:-15px;"><span class="judul">isian dengan tanda <span style="color:red; font-weight: bold;">*</span> wajib diisi</span></p>
                    <div class="card-body">
                    
                    <?php echo form_open_multipart('siswa/orangtua/simpan', array('id'=>'form-submit')); ?>
                    
                    <?php $this->load->view('form_ortu'); ?>

                    <?php echo form_close(); ?>
                    </div>
                </div>
                </div>

            </div>
            
        </div>
    </div>
</div>

