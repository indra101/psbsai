<div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">

                        <?php if($status_kepala != 'Wali') { ?>
                            
                        <b>Ayah</b>
                            <input type="hidden" name="ayah_id" value="<?=$ayah['id']?>">
                            <?php if($ayah['status_khusus'] != 'ada' || $ayah['is_kepala'] != 1) { ?>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons" title="Status">favorite</i>
                                        </span>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row" style="height: 14px">
                                            <span class="judul">Status <span style="color:red; font-weight: bold;">*</span></span>
                                        </div>
                                        <div class="row">
                                            <select class="form-control" name="ayah_khusus" id="ayah_khusus" onchange="status_khusus()">
                                                <option value="" disabled <?=(!$ayah['status_khusus'])?'selected':''?>>Pilih Status</option>
                                                <option value="ada" <?=($ayah['status_khusus'] == 'ada')?'selected':''?>>Ada</option>
                                                <option value="meninggal" <?=($ayah['status_khusus'] == 'meninggal')?'selected':''?>>Meninggal</option>
                                                <option value="cerai" <?=($ayah['status_khusus'] == 'cerai')?'selected':''?>>Cerai</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <?php } else {
                                    echo form_hidden('ayah_khusus', $ayah['status_khusus']);
                                } ?>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="Nama Ayah">face</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Nama Lengkap <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <?php if($ayah['is_kepala'] == 1) { 
                                                echo $ayah['nama_ortu']; 
                                                echo form_hidden('ayah_nama_ortu', $ayah['nama_ortu']);
                                            } else { ?>
                                                <input type="text" class="form-control" placeholder="..." name="ayah_nama_ortu" value="<?=$ayah['nama_ortu']?>">
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="div_ayah">

                            <div class="input-group"
                                <?php if($ayah['is_kepala'] == 1 && $ayah['is_daftar'] != 1) { ?>
                                    style="background-color: lightgray; border-radius: 20px;"
                                <?php } ?>
                                >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="Email">email</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Email <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row mt-1">
                                        <?php if($ayah['is_kepala'] == 1) { 
                                                echo $ayah['email']; 
                                                echo form_hidden('ayah_email', $ayah['email']);
                                            } else { ?>
                                                <input type="email" class="form-control" placeholder="..." title="Email..." name="ayah_email" value="<?=$ayah['email']?>" 
                                                <?php if($ayah['is_kepala'] == 1 && $ayah['is_daftar'] != 1) { ?> style="background-color: lightgray;" readonly <?php } ?>
                                                >
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="Nomor Kartu Keluarga">payment</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Nomor Kartu Keluarga <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <input type="number" class="form-control" placeholder="..." title="Nomor Kartu Keluarga..." name="ayah_nkk" value="<?=$ayah['nkk']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="NIK">account_balance_wallet</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">NIK <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <input type="number" class="form-control" placeholder="..." title="NIK..." name="ayah_nik_ortu" value="<?=$ayah['nik_ortu']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Jenis Kelamin">wc</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jenis Kelamin <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="ayah_kelamin">
                                                    <option value="Laki-laki" selected>Laki-laki</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Agama">home_work</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Agama <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="ayah_agama" title="Agama">
                                                    <option value="" disabled <?=(!$ayah['agama'])?'selected':''?>>Pilih Agama</option>
                                                    <option value="Islam" <?=($ayah['agama'] == 'Islam')?'selected':''?>>Islam</option>
                                                    <option value="Kristen" <?=($ayah['agama'] == 'Kristen')?'selected':''?>>Kristen</option>
                                                    <option value="Hindu" <?=($ayah['agama'] == 'Hindu')?'selected':''?>>Hindu</option>
                                                    <option value="Budha" <?=($ayah['agama'] == 'Budha')?'selected':''?>>Budha</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Kewarganegaraan/Suku">flag</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Kewarganegaraan/Suku <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_suku" value="<?=$ayah['suku']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Tempat Lahir">storefront</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Tempat Lahir <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_tempat_lahir" value="<?=$ayah['tempat_lahir']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Tanggal Lahir">date_range</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Tanggal Lahir <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input placeholder="Selected date" type="date" id="date-picker-example" class="form-control datepicker" name="ayah_tgl_lahir" value="<?=$ayah['tgl_lahir']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="HP">phone_iphone</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Nomor Handphone <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="ayah_hp" value="<?=$ayah['hp']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Alamat Rumah">home</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Alamat Rumah <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_alamat_rumah" value="<?=$ayah['alamat_rumah']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Telepon Rumah">local_phone</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Telepon Rumah</span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="ayah_telp" value="<?=$ayah['telp']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Alamat Pekerjaan">apartment</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Alamat Pekerjaan <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_alamat_pekerjaan" value="<?=$ayah['alamat_pekerjaan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Telepon Kantor">local_phone</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Telepon Kantor</span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="ayah_telp_kantor" value="<?=$ayah['telp_kantor']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Pekerjaan">work</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Pekerjaan <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_pekerjaan" value="<?=$ayah['pekerjaan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-facebook-square pl-2" title="Akun Facebook"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Akun Facebook</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_fb" value="<?=$ayah['fb']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-twitter pl-2" title="Akun Twitter"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Akun Twitter</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_twitter" value="<?=$ayah['twitter']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-instagram pl-2" title="Akun Instagram"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Akun Instagram</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_instagram" value="<?=$ayah['instagram']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Jabatan">accessibility</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jabatan <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_jabatan" value="<?=$ayah['jabatan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Nama Instansi">account_balance</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Nama Instansi <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_instansi" value="<?=$ayah['instansi']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="Penghasilan Per-bulan">attach_money</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Penghasilan Per-bulan <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <select class="form-control" name="ayah_range_gaji">
                                            <option value="" disabled <?=(!$ayah['range_gaji'])?'selected':''?>>Pilih Nominal</option>
                                            <option value="Rp 0 - Rp 999.000" <?=($ayah['range_gaji'] == 'Rp 0 - Rp 999.000')?'selected':''?>>Rp 0 - Rp 999.000</option>
                                            <option value="Rp 1.000.000 - Rp 3.000.000" <?=($ayah['range_gaji'] == 'Rp 1.000.000 - Rp 3.000.000')?'selected':''?>>Rp 1.000.000 - Rp 3.000.000</option>
                                            <option value="Rp 3.100.000 - Rp 6.000.000" <?=($ayah['range_gaji'] == 'Rp 3.100.000 - Rp 6.000.000')?'selected':''?>>Rp 3.100.000 - Rp 6.000.000</option>
                                            <option value="Rp 6.100.000 - Rp 9.000.000" <?=($ayah['range_gaji'] == 'Rp 6.100.000 - Rp 9.000.000')?'selected':''?>>Rp 6.100.000 - Rp 9.000.000</option>
                                            <option value="Rp 9.100.000 - Rp 12.000.000" <?=($ayah['range_gaji'] == 'Rp 9.100.000 - Rp 12.000.000')?'selected':''?>>Rp 9.100.000 - Rp 12.000.000</option>
                                            <option value="Rp 12.100.000 - Rp 15.000.000" <?=($ayah['range_gaji'] == 'Rp 12.100.000 - Rp 15.000.000')?'selected':''?>>Rp 12.100.000 - Rp 15.000.000</option>
                                            <option value="Lebih dari Rp 15.000.000" <?=($ayah['range_gaji'] == 'Lebih dari Rp 15.000.000')?'selected':''?>>Lebih dari Rp 15.000.000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Jumlah Jam Kerja">access_time</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jumlah Jam Kerja (Jam) <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="ayah_jml_jam_kerja" value="<?=$ayah['jml_jam_kerja']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Waktu Untuk Keluarga">timelapse</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Waktu Untuk Keluarga (Jam) <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="ayah_waktu_keluarga" value="<?=$ayah['waktu_keluarga']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Pendidikan Terakhir">school</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Pendidikan Terakhir <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_pendidikan" value="<?=$ayah['pendidikan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Almamater">location_city</i>
                                            </span>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Almamater <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="ayah_almamater" value="<?=$ayah['almamater']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div> <!-- end div_ayah -->

                            <br>

                            <b>Ibu</b>
                            <input type="hidden" name="ibu_id" value="<?=$ibu['id']?>">
                            <?php if($ibu['status_khusus'] != 'ada' || $ibu['is_kepala'] != 1) { ?>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons" title="Status">favorite</i>
                                        </span>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row" style="height: 14px">
                                            <span class="judul">Status <span style="color:red; font-weight: bold;">*</span></span>
                                        </div>
                                        <div class="row">
                                            <select class="form-control" name="ibu_khusus" id="ibu_khusus" onchange="status_khusus()">
                                                <option value="" disabled <?=(!$ibu['status_khusus'])?'selected':''?>>Pilih Status</option>
                                                <option value="ada" <?=($ibu['status_khusus'] == 'ada')?'selected':''?>>Ada</option>
                                                <option value="meninggal" <?=($ibu['status_khusus'] == 'meninggal')?'selected':''?>>Meninggal</option>
                                                <option value="cerai" <?=($ibu['status_khusus'] == 'cerai')?'selected':''?>>Cerai</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <?php } else {
                                    echo form_hidden('ibu_khusus', $ibu['status_khusus']);
                                } ?>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="Nama">face</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Nama Lengkap <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <?php if($ibu['is_kepala'] == 1) { 
                                                echo $ibu['nama_ortu']; 
                                                echo form_hidden('ibu_nama_ortu', $ibu['nama_ortu']);
                                            } else { ?>
                                                <input type="text" class="form-control" placeholder="..." name="ibu_nama_ortu" value="<?=$ibu['nama_ortu']?>">
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                                
                            <div id="div_ibu">

                            <div class="input-group"
                                <?php if($ibu['is_kepala'] == 1 && $ibu['is_daftar'] != 1) { ?>
                                    style="background-color: lightgray; border-radius: 20px;"
                                <?php } ?>
                                >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="Email">email</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Email <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row mt-1">
                                        <input type="email" class="form-control" placeholder="..." name="ibu_email" value="<?=$ibu['email']?>"
                                        <?php if($ibu['is_kepala'] == 1 && $ibu['is_daftar'] != 1) { ?> style="background-color: lightgray;" readonly <?php } ?>
                                        >
                                    </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="Nomor Kartu Keluarga">payment</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Nomor Kartu Keluarga <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <input type="number" class="form-control" placeholder="..." name="ibu_nkk" value="<?=$ibu['nkk']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="NIK">account_balance_wallet</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">NIK <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <input type="number" class="form-control" placeholder="..." name="ibu_nik_ortu" value="<?=$ibu['nik_ortu']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Jenis Kelamin">wc</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jenis Kelamin <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="ibu_kelamin">
                                                    <option value="Perempuan" selected>Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Agama">home_work</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Agama <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="ibu_agama">
                                                    <option value="" disabled <?=(!$ibu['agama'])?'selected':''?>>Pilih Agama</option>
                                                    <option value="Islam" <?=($ibu['agama'] == 'Islam')?'selected':''?>>Islam</option>
                                                    <option value="Kristen" <?=($ibu['agama'] == 'Kristen')?'selected':''?>>Kristen</option>
                                                    <option value="Hindu" <?=($ibu['agama'] == 'Hindu')?'selected':''?>>Hindu</option>
                                                    <option value="Budha" <?=($ibu['agama'] == 'Budha')?'selected':''?>>Budha</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Kewarganegaraan/Suku">flag</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Kewarganegaraan/Suku <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_suku" value="<?=$ibu['suku']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Tempat Lahir">storefront</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Tempat Lahir <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_tempat_lahir" value="<?=$ibu['tempat_lahir']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Tanggal Lahir">date_range</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Tanggal Lahir <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input placeholder="Selected date" type="date" id="date-picker-example" class="form-control datepicker" name="ibu_tgl_lahir" value="<?=$ibu['tgl_lahir']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="HP">phone_iphone</i>
                                            </span>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Nomor Handphone <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="ibu_hp" value="<?=$ibu['hp']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Alamat Rumah">home</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Alamat Rumah <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_alamat_rumah" value="<?=$ibu['alamat_rumah']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Telepon">local_phone</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Telepon Rumah</span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="ibu_telp" value="<?=$ibu['telp']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Alamat Pekerjaan">apartment</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Alamat Pekerjaan <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_alamat_pekerjaan" value="<?=$ibu['alamat_pekerjaan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Telepon Kantor">local_phone</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Telepon Kantor</span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="ibu_telp_kantor" value="<?=$ibu['telp_kantor']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Pekerjaan">work</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Pekerjaan <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_pekerjaan" value="<?=$ibu['pekerjaan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-facebook-square pl-2" title="Akun Facebook"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Akun Facebook</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_fb" value="<?=$ibu['fb']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-twitter pl-2" title="Akun Twitter"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Akun Twitter</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_twitter" value="<?=$ibu['twitter']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-instagram pl-2" title="Akun Instagram"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Akun Instagram</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_instagram" value="<?=$ibu['instagram']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Jabatan">accessibility</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jabatan <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_jabatan" value="<?=$ibu['jabatan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Nama Instansi">account_balance</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Nama Instansi <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_instansi" value="<?=$ibu['instansi']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="Penghasilan Per-bulan">attach_money</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Penghasilan Per-bulan <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <select class="form-control" name="ibu_range_gaji">
                                            <option value="" disabled <?=(!$ibu['range_gaji'])?'selected':''?>>Pilih Nominal</option>
                                            <option value="Rp 0 - Rp 999.000" <?=($ibu['range_gaji'] == 'Rp 0 - Rp 999.000')?'selected':''?>>Rp 0 - Rp 999.000</option>
                                            <option value="Rp 1.000.000 - Rp 3.000.000" <?=($ibu['range_gaji'] == 'Rp 1.000.000 - Rp 3.000.000')?'selected':''?>>Rp 1.000.000 - Rp 3.000.000</option>
                                            <option value="Rp 3.100.000 - Rp 6.000.000" <?=($ibu['range_gaji'] == 'Rp 3.100.000 - Rp 6.000.000')?'selected':''?>>Rp 3.100.000 - Rp 6.000.000</option>
                                            <option value="Rp 6.100.000 - Rp 9.000.000" <?=($ibu['range_gaji'] == 'Rp 6.100.000 - Rp 9.000.000')?'selected':''?>>Rp 6.100.000 - Rp 9.000.000</option>
                                            <option value="Rp 9.100.000 - Rp 12.000.000" <?=($ibu['range_gaji'] == 'Rp 9.100.000 - Rp 12.000.000')?'selected':''?>>Rp 9.100.000 - Rp 12.000.000</option>
                                            <option value="Rp 12.100.000 - Rp 15.000.000" <?=($ibu['range_gaji'] == 'Rp 12.100.000 - Rp 15.000.000')?'selected':''?>>Rp 12.100.000 - Rp 15.000.000</option>
                                            <option value="Lebih dari Rp 15.000.000" <?=($ibu['range_gaji'] == 'Lebih dari Rp 15.000.000')?'selected':''?>>Lebih dari Rp 15.000.000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Jumlah Jam Kerja">access_time</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jumlah Jam Kerja <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_jml_jam_kerja" value="<?=$ibu['jml_jam_kerja']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Waktu Untuk Keluarga">timelapse</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Waktu Untuk Keluarga (Jam) <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_waktu_keluarga" value="<?=$ibu['waktu_keluarga']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Pendidikan Terakhir">school</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Pendidikan Terakhir <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_pendidikan" value="<?=$ibu['pendidikan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Almamater">location_city</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Almamater <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_almamater" value="<?=$ibu['almamater']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div> <!-- end div_ibu -->


                            <?php } else { ?>

                            <div id="div_wali">

                            <b>Wali (Jika Ada)</b>
                            <input type="hidden" name="wali_id" value="<?=$wali['id']?>">
                            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="Nomor Kartu Keluarga">payment</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Nomor Kartu Keluarga <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <input type="number" class="form-control" placeholder="..." name="wali_nkk" value="<?=$wali['nkk']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="NIK">account_balance_wallet</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">NIK <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <input type="number" class="form-control" placeholder="..." name="wali_nik_ortu" value="<?=$wali['nik_ortu']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Nama">face</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Nama Lengkap <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="wali_nama_ortu" value="<?=$wali['nama_ortu']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group"
                                        <?php if($wali['is_daftar'] != 1) { ?>
                                            style="background-color: lightgray; border-radius: 20px;"
                                        <?php } ?>
                                        >
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Email">email</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Email <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="email" class="form-control" placeholder="..." name="wali_email" value="<?=$wali['email']?>"
                                                <?php if($wali['is_daftar'] != 1) { ?> style="background-color: lightgray;" readonly <?php } ?>
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Jenis Kelamin">wc</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jenis Kelamin <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="wali_kelamin">
                                                    <option value="" disabled <?=(!$wali['kelamin'])?'selected':''?>>Pilih Jenis Kelamin</option>
                                                    <option value="Laki-laki" <?=($wali['kelamin'] == 'Laki-laki')?'selected':''?>>Laki-laki</option>
                                                    <option value="Perempuan" <?=($wali['kelamin'] == 'Perempuan')?'selected':''?>>Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Agama">home_work</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Agama <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <select class="form-control" name="wali_agama">
                                                    <option value="" disabled <?=(!$wali['agama'])?'selected':''?>>Pilih Agama</option>
                                                    <option value="Islam" <?=($wali['agama'] == 'Islam')?'selected':''?>>Islam</option>
                                                    <option value="Kristen" <?=($wali['agama'] == 'Kristen')?'selected':''?>>Kristen</option>
                                                    <option value="Hindu" <?=($wali['agama'] == 'Hindu')?'selected':''?>>Hindu</option>
                                                    <option value="Budha" <?=($wali['agama'] == 'Budha')?'selected':''?>>Budha</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Kewarganegaraan/Suku">flag</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Kewarganegaraan/Suku <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." title="Kewarganegaraan/Suku..." name="wali_suku" value="<?=$wali['suku']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Tempat Lahir">storefront</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Tempat Lahir <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." title="Tempat Lahir..." name="wali_tempat_lahir" value="<?=$wali['tempat_lahir']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Tanggal Lahir">date_range</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Tanggal Lahir <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input placeholder="Selected date" type="date" id="date-picker-example" class="form-control datepicker" name="wali_tgl_lahir" value="<?=$wali['tgl_lahir']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="HP">phone_iphone</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Nomor Handphone <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="wali_hp" value="<?=$wali['hp']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Alamat Rumah">home</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Alamat Rumah <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="wali_alamat_rumah" value="<?=$wali['alamat_rumah']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Telepon">local_phone</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Telepon Rumah</span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="wali_telp" value="<?=$wali['telp']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Alamat Pekerjaan">apartment</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Alamat Pekerjaan <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="wali_alamat_pekerjaan" value="<?=$wali['alamat_pekerjaan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Telepon Kantor">local_phone</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Telepon Kantor</span>
                                            </div>
                                            <div class="row">
                                                <input type="number" class="form-control" placeholder="..." name="wali_telp_kantor" value="<?=$wali['telp_kantor']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Pekerjaan">work</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Pekerjaan <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="wali_pekerjaan" value="<?=$wali['pekerjaan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-facebook pl-2" title="Akun Facebook"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Akun Facebook</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="wali_fb" value="<?=$wali['fb']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-twitter pl-2" title="Akun Twitter"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Akun Twitter</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="wali_twitter" value="<?=$wali['twitter']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-instagram pl-2" title="Akun Instagram"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Akun Instagram</span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="ibu_instagram" value="<?=$ibu['instagram']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Jabatan">accessibility</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jabatan <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="wali_jabatan" value="<?=$wali['jabatan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Nama Instansi">account_balance</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Nama Instansi <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="wali_instansi" value="<?=$wali['instansi']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons" title="Penghasilan Per-bulan">attach_money</i>
                                    </span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row" style="height: 14px">
                                        <span class="judul">Penghasilan Per-bulan <span style="color:red; font-weight: bold;">*</span></span>
                                    </div>
                                    <div class="row">
                                        <select class="form-control" name="wali_range_gaji">
                                            <option value="" disabled <?=(!$wali['range_gaji'])?'selected':''?>>Pilih Nominal</option>
                                            <option value="Rp 0 - Rp 999.000" <?=($wali['range_gaji'] == 'Rp 0 - Rp 999.000')?'selected':''?>>Rp 0 - Rp 999.000</option>
                                            <option value="Rp 1.000.000 - Rp 3.000.000" <?=($wali['range_gaji'] == 'Rp 1.000.000 - Rp 3.000.000')?'selected':''?>>Rp 1.000.000 - Rp 3.000.000</option>
                                            <option value="Rp 3.100.000 - Rp 6.000.000" <?=($wali['range_gaji'] == 'Rp 3.100.000 - Rp 6.000.000')?'selected':''?>>Rp 3.100.000 - Rp 6.000.000</option>
                                            <option value="Rp 6.100.000 - Rp 9.000.000" <?=($wali['range_gaji'] == 'Rp 6.100.000 - Rp 9.000.000')?'selected':''?>>Rp 6.100.000 - Rp 9.000.000</option>
                                            <option value="Rp 9.100.000 - Rp 12.000.000" <?=($wali['range_gaji'] == 'Rp 9.100.000 - Rp 12.000.000')?'selected':''?>>Rp 9.100.000 - Rp 12.000.000</option>
                                            <option value="Rp 12.100.000 - Rp 15.000.000" <?=($wali['range_gaji'] == 'Rp 12.100.000 - Rp 15.000.000')?'selected':''?>>Rp 12.100.000 - Rp 15.000.000</option>
                                            <option value="Lebih dari Rp 15.000.000" <?=($wali['range_gaji'] == 'Lebih dari Rp 15.000.000')?'selected':''?>>Lebih dari Rp 15.000.000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Jumlah Jam Kerja">access_time</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Jumlah Jam Kerja <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="wali_jml_jam_kerja" value="<?=$wali['jml_jam_kerja']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Waktu Untuk Keluarga">timelapse</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Waktu Untuk Keluarga (Jam) <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="wali_waktu_keluarga" value="<?=$wali['waktu_keluarga']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Pendidikan Terakhir">school</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Pendidikan Terakhir <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row">
                                                <input type="text" class="form-control" placeholder="..." name="wali_pendidikan" value="<?=$wali['pendidikan']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons" title="Almamater">location_city</i>
                                            </span>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row" style="height: 14px">
                                                <span class="judul">Almamater <span style="color:red; font-weight: bold;">*</span></span>
                                            </div>
                                            <div class="row" style="display: block;">
                                                <input type="text" class="form-control" placeholder="..." name="wali_almamater" value="<?=$wali['almamater']?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div> <!-- end div_wali -->

                            <?php } ?>

                            <?=form_hidden('id_kepala', $id_kepala);?>
                            <?=form_hidden('status_kepala', $status_kepala);?>

                            <br>
                            <button type="submit" class="btn btn-success btn-block">Simpan<div class="ripple-container"></div></button>
                        </div>
                        <div class="col-md-1"></div>
                    </div>

<script>

function status_khusus() {
    var ayah = $('#ayah_khusus').val();
    if(ayah == 'cerai' || ayah == 'meninggal') {
        $('#div_ayah').hide();
    } else {
        $('#div_ayah').show();
    }

    var ibu = $('#ibu_khusus').val();

    //alert(ibu);
    if(ibu == 'cerai' || ibu == 'meninggal') {
        
        $('#div_ibu').hide();
    } else {
        $('#div_ibu').show();
    }
}

</script>