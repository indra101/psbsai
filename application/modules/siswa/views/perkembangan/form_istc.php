<div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div style="padding:20px;">
                            <input type="hidden" name="id" value="<?=$perkembangan['id']?>">
                            <input type="hidden" name="id_siswa" value="<?=$perkembangan['id_siswa']?>">
                            <div id="page1_perkembangan">
                            <div id="step1">
                                <div class="radio">
                                    1. Perhatian anak hanya pada hal-hal yang bersifat luas, umum, dan tidak memperhatikan detil <br>
                                    <label><input type="radio" name="p1" value="tp" <?=($perkembangan['p1'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p1" value="jr" <?=($perkembangan['p1'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p1" value="sr" <?=($perkembangan['p1'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p1" value="sl" <?=($perkembangan['p1'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>
                                
                                <div class="radio">
                                    2. Membuat kesalahan karena ceroboh atau tidak teliti <br>
                                    <label><input type="radio" name="p2" value="tp" <?=($perkembangan['p2'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p2" value="jr" <?=($perkembangan['p2'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p2" value="sr" <?=($perkembangan['p2'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p2" value="sl" <?=($perkembangan['p2'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    3. Perhatiannya mudah teralih sehingga anak sering tidak menyelesaikan tugasnya <br>
                                    <label><input type="radio" name="p3" value="tp" <?=($perkembangan['p3'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p3" value="jr" <?=($perkembangan['p3'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p3" value="sr" <?=($perkembangan['p3'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p3" value="sl" <?=($perkembangan['p3'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    4. Mengalami kesulitan untuk mempertahankan dan memusatkan perhatian pada satu permainan sebelum beralih ke permainan lain <br>
                                    <label><input type="radio" name="p4" value="tp" <?=($perkembangan['p4'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p4" value="jr" <?=($perkembangan['p4'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p4" value="sr" <?=($perkembangan['p4'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p4" value="sl" <?=($perkembangan['p4'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    5. Seolah/tampak tidak mendengarkan ketika sedang diajak ngobrol <br>
                                    <label><input type="radio" name="p5" value="tp" <?=($perkembangan['p5'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p5" value="jr" <?=($perkembangan['p5'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p5" value="sr" <?=($perkembangan['p5'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p5" value="sl" <?=($perkembangan['p5'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    6. Menghindari atau enggan mengerjakan tugas yang membutuhkan ketekunan, konsentrasi, dan perhatian yang terus menerus <br>
                                    <label><input type="radio" name="p6" value="tp" <?=($perkembangan['p6'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p6" value="jr" <?=($perkembangan['p6'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p6" value="sr" <?=($perkembangan['p6'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p6" value="sl" <?=($perkembangan['p6'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    7. Gagal mengikuti seluruh instruksi dalam suatu permainan karena perhatian teralih sebelum seluruh instruksi diikuti <br>
                                    <label><input type="radio" name="p7" value="tp" <?=($perkembangan['p7'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p7" value="jr" <?=($perkembangan['p7'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p7" value="sr" <?=($perkembangan['p7'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p7" value="sl" <?=($perkembangan['p7'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    8. Kehilangan mainan <br>
                                    <label><input type="radio" name="p8" value="tp" <?=($perkembangan['p8'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p8" value="jr" <?=($perkembangan['p8'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p8" value="sr" <?=($perkembangan['p8'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p8" value="sl" <?=($perkembangan['p8'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    9. Perhatiannya mudah terganggu oleh suara yang bising, gerakan, atau rangsangan lain <br>
                                    <label><input type="radio" name="p9" value="tp" <?=($perkembangan['p9'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p9" value="jr" <?=($perkembangan['p9'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p9" value="sr" <?=($perkembangan['p9'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p9" value="sl" <?=($perkembangan['p9'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    10. Anak sensitive ketika disentuh atau dipeluk <br>
                                    <label><input type="radio" name="p10" value="tp" <?=($perkembangan['p10'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p10" value="jr" <?=($perkembangan['p10'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p10" value="sr" <?=($perkembangan['p10'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p10" value="sl" <?=($perkembangan['p10'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    11. Lupa dimana meletakkan mainan atau benda yang dibutuhkannya <br>
                                    <label><input type="radio" name="p11" value="tp" <?=($perkembangan['p11'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p11" value="jr" <?=($perkembangan['p11'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p11" value="sr" <?=($perkembangan['p11'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p11" value="sl" <?=($perkembangan['p11'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    12. Tidak menyelesaikan permainannya karena perhatiannya sudah beralih ke hal lain <br>
                                    <label><input type="radio" name="p12" value="tp" <?=($perkembangan['p12'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p12" value="jr" <?=($perkembangan['p12'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p12" value="sr" <?=($perkembangan['p12'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p12" value="sl" <?=($perkembangan['p12'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    13. Perhatian/daya konsentrasinya kurang dan mudah terdistraksi/teralih<br>
                                    <label><input type="radio" name="p13" value="tp" <?=($perkembangan['p13'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p13" value="jr" <?=($perkembangan['p13'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p13" value="sr" <?=($perkembangan['p13'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p13" value="sl" <?=($perkembangan['p13'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    14. Dapat menggunakan mainannya sebagaimana mestinya<br>
                                    <label><input type="radio" name="p14" value="tp" <?=($perkembangan['p14'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p14" value="jr" <?=($perkembangan['p14'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p14" value="sr" <?=($perkembangan['p14'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p14" value="sl" <?=($perkembangan['p14'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    15. Berantakan/tidak teratur ketika bermain<br>
                                    <label><input type="radio" name="p15" value="tp" <?=($perkembangan['p15'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p15" value="jr" <?=($perkembangan['p15'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p15" value="sr" <?=($perkembangan['p15'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p15" value="sl" <?=($perkembangan['p15'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    16. Terlihat gelisah, mondar-mandir<br>
                                    <label><input type="radio" name="p16" value="tp" <?=($perkembangan['p16'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p16" value="jr" <?=($perkembangan['p16'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p16" value="sr" <?=($perkembangan['p16'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p16" value="sl" <?=($perkembangan['p16'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    17. Meninggalkan tempat duduk, berlari-lari atau memanjat-manjat ketika seharusnya ia duduk dengan tenang/berada pada situasi yang tidak seharusnya<br>
                                    <label><input type="radio" name="p17" value="tp" <?=($perkembangan['p17'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p17" value="jr" <?=($perkembangan['p17'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p17" value="sr" <?=($perkembangan['p17'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p17" value="sl" <?=($perkembangan['p17'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    18. Sulit dikendalikan/diatur ketika sedang melakukan kegiatan bersama teman atau bersama saudara/orang tua di rumah<br>
                                    <label><input type="radio" name="p18" value="tp" <?=($perkembangan['p18'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p18" value="jr" <?=($perkembangan['p18'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p18" value="sr" <?=($perkembangan['p18'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p18" value="sl" <?=($perkembangan['p18'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <button type="button" class="btn btn-primary btn-block" onclick="maju()" id="next_btn">Next</button>
                                </div> <!-- end step1 -->

                                </div> <!-- end page1 -->
                                <div id="page2_perkembangan">

                                <div id="step2" style="display:none;">

                                <div class="radio">
                                    19. Mengalami kesulitan untuk bermain dengan tenang pada permainan-permainan yang santai<br>
                                    <label><input type="radio" name="p19" value="tp" <?=($perkembangan['p19'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p19" value="jr" <?=($perkembangan['p19'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p19" value="sr" <?=($perkembangan['p19'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p19" value="sl" <?=($perkembangan['p19'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    20. Terus-menerus sibuk, selalu ada yang dikerjakan, seolah tidak pernah lelah<br>
                                    <label><input type="radio" name="p20" value="tp" <?=($perkembangan['p20'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p20" value="jr" <?=($perkembangan['p20'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p20" value="sr" <?=($perkembangan['p20'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p20" value="sl" <?=($perkembangan['p20'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    21. Seperti digerakkan oleh mesin, terus berjalan kemana-mana, berlari-lari, seperti tidak perlu istirahat<br>
                                    <label><input type="radio" name="p21" value="tp" <?=($perkembangan['p21'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p21" value="jr" <?=($perkembangan['p21'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p21" value="sr" <?=($perkembangan['p21'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p21" value="sl" <?=($perkembangan['p21'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    22. Mengeluarkan kata-kata yang tidak dipahami<br>
                                    <label><input type="radio" name="p22" value="tp" <?=($perkembangan['p22'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p22" value="jr" <?=($perkembangan['p22'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p22" value="sr" <?=($perkembangan['p22'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p22" value="sl" <?=($perkembangan['p22'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    23. Gerakan tubuhnya canggung, tidak luwes, terlihat serampangan atau sembrono<br>
                                    <label><input type="radio" name="p23" value="tp" <?=($perkembangan['p23'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p23" value="jr" <?=($perkembangan['p23'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p23" value="sr" <?=($perkembangan['p23'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p23" value="sl" <?=($perkembangan['p23'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    24. Gerakan tubuhnya terlalu cepat, seperti tidak terkontrol, sehingga tanpa sengaja melukai dirinya sendiri<br>
                                    <label><input type="radio" name="p24" value="tp" <?=($perkembangan['p24'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p24" value="jr" <?=($perkembangan['p24'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p24" value="sr" <?=($perkembangan['p24'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p24" value="sl" <?=($perkembangan['p24'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    25. Saat ditanya, anak melontarkan jawaban sebelum pertanyaan selesai diberikan, seolah tanpa dipikirkan terlebih dahulu<br>
                                    <label><input type="radio" name="p25" value="tp" <?=($perkembangan['p25'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p25" value="jr" <?=($perkembangan['p25'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p25" value="sr" <?=($perkembangan['p25'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p25" value="sl" <?=($perkembangan['p25'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    26. Anak sulit menunggu giliran<br>
                                    <label><input type="radio" name="p26" value="tp" <?=($perkembangan['p26'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p26" value="jr" <?=($perkembangan['p26'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p26" value="sr" <?=($perkembangan['p26'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p26" value="sl" <?=($perkembangan['p26'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    27. Menyela/mengganggu pembicaraan orang lain<br>
                                    <label><input type="radio" name="p27" value="tp" <?=($perkembangan['p27'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p27" value="jr" <?=($perkembangan['p27'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p27" value="sr" <?=($perkembangan['p27'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p27" value="sl" <?=($perkembangan['p27'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    28. Menyela/mengganggu permainan/kegiatan orang lain<br>
                                    <label><input type="radio" name="p28" value="tp" <?=($perkembangan['p28'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p28" value="jr" <?=($perkembangan['p28'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p28" value="sr" <?=($perkembangan['p28'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p28" value="sl" <?=($perkembangan['p28'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    29. Anak tampak senang bermain atau beraktivitas sendiri<br>
                                    <label><input type="radio" name="p29" value="tp" <?=($perkembangan['p29'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p29" value="jr" <?=($perkembangan['p29'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p29" value="sr" <?=($perkembangan['p29'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p29" value="sl" <?=($perkembangan['p29'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    30. Mengalami kesulitan dalam melakukan aktivitas permainan secara berkelompok<br>
                                    <label><input type="radio" name="p30" value="tp" <?=($perkembangan['p30'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p30" value="jr" <?=($perkembangan['p30'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p30" value="sr" <?=($perkembangan['p30'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p30" value="sl" <?=($perkembangan['p30'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    31. Anak mengalami gangguan pola tidur<br>
                                    <label><input type="radio" name="p31" value="tp" <?=($perkembangan['p31'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p31" value="jr" <?=($perkembangan['p31'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p31" value="sr" <?=($perkembangan['p31'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p31" value="sl" <?=($perkembangan['p31'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    32. Anak masih mengompol pada saat tidur malam<br>
                                    <label><input type="radio" name="p32" value="tp" <?=($perkembangan['p32'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p32" value="jr" <?=($perkembangan['p32'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p32" value="sr" <?=($perkembangan['p32'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p32" value="sl" <?=($perkembangan['p32'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    33. Mengulang-ulang perkataan/membeo<br>
                                    <label><input type="radio" name="p33" value="tp" <?=($perkembangan['p33'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p33" value="jr" <?=($perkembangan['p33'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p33" value="sr" <?=($perkembangan['p33'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p33" value="sl" <?=($perkembangan['p33'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    34. Menggunakan bahasa verbal untuk mendapatkan/mengungkapkan keinginannya<br>
                                    <label><input type="radio" name="p34" value="tp" <?=($perkembangan['p34'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p34" value="jr" <?=($perkembangan['p34'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p34" value="sr" <?=($perkembangan['p34'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p34" value="sl" <?=($perkembangan['p34'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    35. Anak lebih sering menggunakan bahasa tubuhnya (menunjuk) untuk mendapatkan benda yang diinginkannya<br>
                                    <label><input type="radio" name="p35" value="tp" <?=($perkembangan['p35'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p35" value="jr" <?=($perkembangan['p35'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p35" value="sr" <?=($perkembangan['p35'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p35" value="sl" <?=($perkembangan['p35'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>

                                <div class="radio">
                                    36. Anak mengalami kesulitan dalam melakukan aktivitas permainan secara berkelompok<br>
                                    <label><input type="radio" name="p36" value="tp" <?=($perkembangan['p36'] == 'tp')?'checked':''?>>&nbsp;<span style="color:#393939;">Tidak Pernah</span></label>&nbsp;
                                    <label><input type="radio" name="p36" value="jr" <?=($perkembangan['p36'] == 'jr')?'checked':''?>>&nbsp;<span style="color:#393939;">Jarang</span></label>&nbsp;
                                    <label><input type="radio" name="p36" value="sr" <?=($perkembangan['p36'] == 'sr')?'checked':''?>>&nbsp;<span style="color:#393939;">Sering</span></label>&nbsp;
                                    <label><input type="radio" name="p36" value="sl" <?=($perkembangan['p36'] == 'sl')?'checked':''?>>&nbsp;<span style="color:#393939;">Selalu</span></label>
                                </div>
                                
                                <button type="button" class="btn btn-danger btn-block" onclick="mundur()" id="next_btn">Back</button>
                                </div> <!-- end step2 -->

                                </div> <!-- end page2 -->

                                <br>
                                <button type="submit" class="btn btn-success btn-block">Simpan<div class="ripple-container"></div></button>

                            </div>
                            
                        </div>
                        <div class="col-md-1"></div>
                    </div>