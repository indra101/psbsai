<div class="jumbotron background">
    <div class="content" style="padding-top:40px;">
        <div class="container-fluid">
        <center><h3 style="color:white;"><b>Data Perkembangan</b></h3></center>
            <div class="row">
        
                <div class="col-md-8 ml-auto mr-auto">
                <h5><b><a href="<?=base_url('siswa/dashboard');?>"><font style="color:white;"><u>Dashboard</u></a> > </font><a href="<?=base_url('siswa/dashboard/datasiswa');?>"><font style="color:white;"><u>Data Anak</u></a> > </font><a href="<?=base_url('siswa/dashboard/siswa/'.$siswa['id']);?>"><font style="color:white;"><u>Menu Siswa</u></a> > Data Perkembangan</font></b></h5>
                <?php $this->view('siswa/notif'); ?>
                <div class="card card-signup">
                    <p class="card-title text-center" style="padding:10px;">Petunjuk Pengisian :
                    Berilah tanda chek list (√) bila pernyataan di bawah ini yang muncul pada anak selama 6 bulan terakhir.</p>
                    <div class="card-body">
                    <?php echo form_open_multipart('siswa/perkembangan/simpan', array('id'=>'form-submit')); ?>
                    
                        <?php $this->load->view('form_istc'); ?>

                    </div>
                    <?php echo form_close(); ?>
                </div>
                </div>

            </div>
            
        </div>
    </div>
</div>

<script>
    var i = 1;

    function maju(){
        $('#step1').hide();
        $('#step2').show();
    }

    function mundur(){
        $('#step1').show();
        $('#step2').hide();
    }
</script>