<div class="jumbotron background">
    <div class="content" style="padding-top:40px;">
        <div class="container-fluid">

        <br>
        <br>
        <div class="row">
        <h5><b><a href="<?=base_url('siswa/dashboard');?>"><font style="color:white;"><u>Dashboard</u></a> > Tahapan</font></b></h5>
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Tahapan Pendaftaran</h4>
                  <p class="card-category">Berikut Tahapan yang sedang berlangsung pada Pendaftaran Sekolah Alam</p>
                </div>
                <div class="card-body table-responsive">
                <table id="ttahapan" class="table table-hover">
                    <thead class="text-success">
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Tahapan</th>
                    </thead>
                    <tbody>
                    <?php $i=1;
                    foreach($tahapan as $t){?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=tgl_indo($t['tanggal_mulai'])?> - <?=tgl_indo($t['tanggal_selesai'])?></td>
                        <td><?=$t['keterangan']?></td>
                      </tr>
                    <?php $i++;} ?>
                    </tbody>
                  </table>
                    </div>
                </div>
            </div>
        </div>
            
        </div>
    </div>
</div>
