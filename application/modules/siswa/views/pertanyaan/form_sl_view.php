<style>

.jawaban {
    border: 2px lightgrey solid;
    border-radius: 5px;
    padding: 5px;
    overflow-y: auto;
}

</style>

<div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                        <div class="progress md-progress" style="height: 20px">
                            <div class="progress-bar" id="theprogressbar" role="progressbar" style="width: 0%; height: 20px" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                        </div>
                        <br>
                        <div id="page1">
                            <div id="step1">
                                <b>1. Apa alasan anda melanjutkan pendidikan anak anda di Sekolah Lanjutan SAI?</b>
                                <div class="form-group purple-border">
                                    <span name="p1"><div class="jawaban"><?=nl2br($pertanyaan['p1'])?></div></span>
                                </div>
                                <b>2. Bagaimana konsep pendidikan yang ideal menurut anda?</b>
                                <div class="form-group purple-border">
                                    <span name="p2"><div class="jawaban"><?=nl2br($pertanyaan['p2'])?></div></span>
                                </div>
                                <b>3. Jelaskan konsep Sekolah Alam Indonesia yang anda ketahui!</b>
                                <div class="form-group purple-border">
                                    <span name="p3"><div class="jawaban"><?=nl2br($pertanyaan['p3'])?></div></span>
                                </div>
                                <button type="button" class="btn btn-primary btn-block" onclick="buka(2)" id="next_btn">Next</button>
                            </div>
                            <div id="step2" style="display:none;">
                                <b>4. Bagaimana pendapat anda tentang tanggung jawab pendidikan anak?</b>
                                <div class="form-group purple-border">
                                    <span name="p4"><div class="jawaban"><?=nl2br($pertanyaan['p4'])?></div></span>
                                </div>
                                <b>5. Apa yang anda harapkan untuk anak anda di Sekolah Alam Indonesia?</b>
                                <div class="form-group purple-border">
                                    <span name="p5"><div class="jawaban"><?=nl2br($pertanyaan['p5'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(1)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(3)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step3" style="display:none;">
                                <b>6. Menurut anda bagaimana kurikulum pendidikan yang ideal itu?</b>
                                <div class="form-group purple-border">
                                    <span name="p6"><div class="jawaban"><?=nl2br($pertanyaan['p6'])?></div></span>
                                </div>
                                <b>7. Dari tiga unsur berikut: akademik, akhlak dan kepemimpinan. Urutkan menurut skala prioritas, Jelaskan!</b>
                                <div class="form-group purple-border">
                                    <span name="p7"><div class="jawaban"><?=nl2br($pertanyaan['p7'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(2)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(4)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>

                            </div> <!-- end page1 -->
                            <div id="page2">

                            <div id="step4" style="display:none;">
                                <b>8. Bagaimana pendapat anda tentang pencapaian target kognitif (keilmuan) di Sekolah Lanjutan?</b>
                                <div class="form-group purple-border">
                                    <span name="p8"><div class="jawaban"><?=nl2br($pertanyaan['p8'])?></div></span>
                                </div>
                                <b>9. Dengan pemahaman konsep pendidikan yang anda yakini, jika ternyata tidak sesuai dengan konsep dan progress yang dilakukan sekolah, maka apa yang akan anda lakukan?</b>
                                <div class="form-group purple-border">
                                    <span name="p9"><div class="jawaban"><?=nl2br($pertanyaan['p9'])?></div></span>
                                </div>
                                <b>10. Bagaimana pandangan anda terhadap pacaran?</b>
                                <div class="form-group purple-border">
                                    <span name="p10"><div class="jawaban"><?=nl2br($pertanyaan['p10'])?></div></span>
                                </div>
                                <b>11. Bagaimana sikap anda jika menemukan ananda berpacaran dengan teman sekolah ataupun di luar sekolahnya?</b>
                                <div class="form-group purple-border">
                                    <span name="p35"><div class="jawaban"><?=nl2br($pertanyaan['p35'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(3)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(5)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step5" style="display:none;">
                                <b>12. Harapan Anda terhadap anak Anda di usia SL terhadap pencapaian target berikut:</b>
                                <br>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>a. Target Akhlaq...</b>
                                <div class="form-group purple-border">
                                    <span name="p11"><div class="jawaban"><?=nl2br($pertanyaan['p11'])?></div></span>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>b. Target Kognitif...</b>
                                <div class="form-group purple-border">
                                    <span name="p12"><div class="jawaban"><?=nl2br($pertanyaan['p12'])?></div></span>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>c. Target Kepemimpinan...</b>
                                <div class="form-group purple-border">
                                    <span name="p13"><div class="jawaban"><?=nl2br($pertanyaan['p13'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(4)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(6)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step6" style="display:none;">
                                <b>13. Usaha apa yang akan Anda lakukan untuk mencapai hal tersebut(no. 12)?<br></b>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>a. Target Akhlaq...</b>
                                <div class="form-group purple-border">
                                    <span name="p14"><div class="jawaban"><?=nl2br($pertanyaan['p14'])?></div></span>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>b. Target Kognitif...</b>
                                <div class="form-group purple-border">
                                    <span name="p15"><div class="jawaban"><?=nl2br($pertanyaan['p15'])?></div></span>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>c. Target Kepemimpinan...</b>
                                <div class="form-group purple-border">
                                    <span name="p16"><div class="jawaban"><?=nl2br($pertanyaan['p16'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(5)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(7)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step7" style="display:none;">
                                <b>14. Apa yang anda lakukan untuk anak anda dalam meningkatkan kemampuan membaca dan menghafal Al Qur’an, Jelaskan?</b>
                                <div class="form-group purple-border">
                                    <span name="p17"><div class="jawaban"><?=nl2br($pertanyaan['p17'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(6)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(8)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step8" style="display:none;">
                                <b>15. Tuliskan lima kata yang menggambarkan anak Anda!</b>
                                <div class="form-group purple-border">
                                    <span name="p18"><div class="jawaban"><?=nl2br($pertanyaan['p18'])?></div></span>
                                </div>
                                <b>16. Bagaimana pandangan anda tentang pendidikan anak menuju masa aqil baligh?</b>
                                <div class="form-group purple-border">
                                    <span name="p19"><div class="jawaban"><?=nl2br($pertanyaan['p19'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(7)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(9)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>

                            </div> <!-- end page2 -->
                            <div id="page3">

                            <div id="step9" style="display:none;">
                                <b>17. Apa yang anda lakukan dalam pendidikan anak menuju masa aqil baligh di tengah kondisi lingkungan yang rawan, jelaskan?</b>
                                <div class="form-group purple-border">
                                    <span name="p20"><div class="jawaban"><?=nl2br($pertanyaan['p20'])?></div></span>
                                </div>
                                <b>18. Siapakah yang bertanggung jawab terhadap pendidikan seks anak anda?</b>
                                <div class="form-group purple-border">
                                    <span name="p21"><div class="jawaban"><?=nl2br($pertanyaan['p21'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(8)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(10)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step10" style="display:none;">
                                <b>19. Seandainya Anda menemui hal berikut ini terjadi pada anak Anda, apa yang akan Anda lakukan sebagai orang tua?<br></b>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>a. Membawa buku /majalah porno</b>
                                <div class="form-group purple-border">
                                    <span name="p22"><div class="jawaban"><?=nl2br($pertanyaan['p22'])?></div></span>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>b. Memiliki tontonan /cuplikan film/ gambar porno pada HP.</b>
                                <div class="form-group purple-border">
                                    <span name="p23"><div class="jawaban"><?=nl2br($pertanyaan['p23'])?></div></span>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>c. Menonton film porno sendiri / bersama teman.</b>
                                <div class="form-group purple-border">
                                    <span name="p24"><div class="jawaban"><?=nl2br($pertanyaan['p24'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(9)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(11)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step11" style="display:none;">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>d. Menjadi korban atau pelaku bullying/pemalakan oleh/terhadap teman / kakak kelas</b>
                                <div class="form-group purple-border">
                                    <span name="p25"><div class="jawaban"><?=nl2br($pertanyaan['p25'])?></div></span>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>e. Merokok sendiri / bersama teman.</b>
                                <div class="form-group purple-border">
                                    <span name="p26"><div class="jawaban"><?=nl2br($pertanyaan['p26'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(10)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(12)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step12" style="display:none;">
                                <b>20. Pada usia anak anda sekarang, pendidikan seks apa saja yang sudah dan harus diketahuinya?</b>
                                <div class="form-group purple-border">
                                    <span name="p27"><div class="jawaban"><?=nl2br($pertanyaan['p27'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(11)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(13)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step13" style="display:none;">
                                <b>21. Bagaimana pola asuh dan komunikasi anda dengan anak di rumah? (disertai contoh)</b>
                                <div class="form-group purple-border">
                                    <span name="p28"><div class="jawaban"><?=nl2br($pertanyaan['p28'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(12)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(14)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step14" style="display:none;">
                                <b>22. Bagaimana menurut anda bentuk kerjasama yang ideal antara guru dan orang tua dalam Kegiatan Belajar Mengajar (KBM)?</b>
                                <div class="form-group purple-border">
                                    <span name="p29"><div class="jawaban"><?=nl2br($pertanyaan['p29'])?></div></span>
                                </div>
                                <b>23. Apakah anda sepakat menjadikan nilai-nilai Islam sebagai rujukan pembelajaran Akhlaq di sekolah dan di rumah? Jelaskan!</b>
                                <div class="form-group purple-border">
                                    <span name="p30"><div class="jawaban"><?=nl2br($pertanyaan['p30'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(13)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(15)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>

                            </div> <!-- end page3 -->
                            <div id="page4">

                            <div id="step15" style="display:none;">
                                <b>24. Sebagai sekolah swasta yang pembiayaan keuangannya bersifat mandiri, jika anda mengalami kesulitan keuangan sehingga mempengaruhi operasional sekolah, apa yang akan anda lakukan? </b>
                                <div class="form-group purple-border">
                                    <span name="p31"><div class="jawaban"><?=nl2br($pertanyaan['p31'])?></div></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(14)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(16)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step16" style="display:none;">
                                <b>25. Mengapa anda merasa layak diterima di Sekolah Alam Indonesia? </b>
                                <div class="form-group purple-border">
                                    <span name="p32"><div class="jawaban"><?=nl2br($pertanyaan['p32'])?></div></span>
                                </div>
                                <b>26. Kontribusi apa yang akan anda berikan kepada sekolah jika diterima di Sekolah Alam Indonesia?</b>
                                <div class="form-group purple-border">
                                    <span name="p33"><div class="jawaban"><?=nl2br($pertanyaan['p33'])?></div></span>
                                </div>
                                <b>27. Organisasi sekolah apa saja yang akan anda masuki dan aktif di dalamnya? (Tulis jawaban bisa lebih dari satu a/b/c/d)<br>
                                a. Dewan Sekolah (Komite Sekolah)<br>
                                b. Dewan Kelas (Komite Kelas)<br>
                                c. Kepanitiaan kegiatan sekolah<br>
                                d. Lainnya...</b>

                                <div class="form-group purple-border">
                                    <span name="p34"><div class="jawaban"><?=nl2br($pertanyaan['p34'])?></div></span>
                                </div>
                                
                                <button type="button" class="btn btn-danger btn-block" onclick="buka(15)" id="next_btn">Back</button>
                            </div>

                            </div> <!-- end page4 -->

                            <?=form_hidden('tipe', 0);?>

                            <br>
                            <button type="submit" class="btn btn-success btn-block" onclick="set_tipe(0);">Simpan<div class="ripple-container"></div></button>
                            
                        </div>
                        <div class="col-md-1"></div>
                    </div>

<script>
    var i = 1;

    function hide(){
        $('#step1').hide();
        $('#step2').hide();
        $('#step3').hide();
        $('#step4').hide();
        $('#step5').hide();
        $('#step6').hide();
        $('#step7').hide();
        $('#step8').hide();
        $('#step9').hide();
        $('#step10').hide();
        $('#step11').hide();
        $('#step12').hide();
        $('#step13').hide();
        $('#step14').hide();
        $('#step15').hide();
        $('#step16').hide();
    }

    function buka(step){
        hide();
        $('#step'+step).show();

        var persen = hitung();

        $('#theprogressbar').attr('aria-valuenow', persen).css('width', persen+'%');
        $('#theprogressbar').html(persen+'%');

        set_tipe(1);
        save();
    }

    $(function() {
        buka(1);
    });

    function hitung() {
        var x = 0;
        if($('textarea[name=p1]').val() != '')
            x++;
        if($('textarea[name=p2]').val() != '')
            x++;
        if($('textarea[name=p3]').val() != '')
            x++;
        if($('textarea[name=p4]').val() != '')
            x++;
        if($('textarea[name=p5]').val() != '')
            x++;
        if($('textarea[name=p6]').val() != '')
            x++;
        if($('textarea[name=p7]').val() != '')
            x++;
        if($('textarea[name=p8]').val() != '')
            x++;
        if($('textarea[name=p9]').val() != '')
            x++;
        if($('textarea[name=p10]').val() != '')
            x++;
        if($('textarea[name=p11]').val() != '')
            x++;
        if($('textarea[name=p12]').val() != '')
            x++;
        if($('textarea[name=p13]').val() != '')
            x++;
        if($('textarea[name=p14]').val() != '')
            x++;
        if($('textarea[name=p15]').val() != '')
            x++;
        if($('textarea[name=p16]').val() != '')
            x++;
        if($('textarea[name=p17]').val() != '')
            x++;
        if($('textarea[name=p18]').val() != '')
            x++;
        if($('textarea[name=p19]').val() != '')
            x++;
        if($('textarea[name=p20]').val() != '')
            x++;
        if($('textarea[name=p21]').val() != '')
            x++;
        if($('textarea[name=p22]').val() != '')
            x++;
        if($('textarea[name=p23]').val() != '')
            x++;
        if($('textarea[name=p24]').val() != '')
            x++;
        if($('textarea[name=p25]').val() != '')
            x++;
        if($('textarea[name=p26]').val() != '')
            x++;
        if($('textarea[name=p27]').val() != '')
            x++;
        if($('textarea[name=p28]').val() != '')
            x++;
        if($('textarea[name=p29]').val() != '')
            x++;
        if($('textarea[name=p30]').val() != '')
            x++;
        if($('textarea[name=p31]').val() != '')
            x++;
        if($('textarea[name=p32]').val() != '')
            x++;
        if($('textarea[name=p33]').val() != '')
            x++;
        if($('textarea[name=p34]').val() != '')
            x++;
        if($('textarea[name=p35]').val() != '')
            x++;

        var r = x/35*100;
        var res = Math.round(r);
        //alert(r);
        return res;
    }

    function save() {
        var input = $("#form-submit").serializeArray();
        //alert(input);

        $.ajax({ 
            type: "POST",   
            dataType: "json",
            url: "<?=base_url('siswa/pertanyaan/simpanSL');?>",
            data: input,
            success: function (data) {
                //alert(data.success);
            }
        });
    }

    function set_tipe(tipe) {
        $('input[name=tipe]').val(tipe);
    }

    var simpan = setInterval(save, 5*60*1000); // auto save per 5 menit

</script>