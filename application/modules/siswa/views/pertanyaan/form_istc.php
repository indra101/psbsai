<div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                        <div class="progress md-progress" style="height: 20px">
                            <div class="progress-bar" id="theprogressbar" role="progressbar" style="width: 0%; height: 20px" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                        </div>
                        <br>
                        <div id="page1">
                            <div id="step1">
                                1. Dari mana anda mendapatkan informasi tentang Sekolah Alam Indonesia?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p1"><?=$pertanyaan['p1']?></textarea>
                                </div>
                                2. Apa alasan anda memilih Sekolah Alam Indonesia? 
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p2"><?=$pertanyaan['p2']?></textarea>
                                </div>
                                3. Bagaimana konsep pendidikan yang ideal menurut anda?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p3"><?=$pertanyaan['p3']?></textarea>
                                </div>
                                <button type="button" class="btn btn-primary btn-block" onclick="buka(2)" id="next_btn">Next</button>
                            </div>
                            <div id="step2" style="display:none;">
                                4. Jelaskan konsep Sekolah Alam Indonesia yang anda ketahui!
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p4"><?=$pertanyaan['p4']?></textarea>
                                </div>
                                5. Apa yang anda ketahui tentang ISTC Sekolah Alam Indonesia?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p38"><?=$pertanyaan['p38']?></textarea>
                                </div>
                                6. Apa yang anda harapkan dari Sekolah Alam Indonesia untuk anak anda?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p5"><?=$pertanyaan['p5']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(1)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(3)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step3" style="display:none;">
                                7. Jelaskan perkembangan yang sudah dicapai oleh ananda saat ini?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p18"><?=$pertanyaan['p18']?></textarea>
                                </div>
                                8. Apa kelebihan dan kekurangan anak anda?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p29"><?=$pertanyaan['p29']?></textarea>
                                </div>
                                9. Apa target utama anda terhadap anak anda?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p30"><?=$pertanyaan['p30']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(2)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(4)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>

                            </div> <!-- end page1 -->
                            <div id="page2">

                            <div id="step4" style="display:none;">
                                10. Apa pendapat anda tentang tanggung jawab pendidikan anak, siapa yang berperan dan bagaimana seharusnya tanggung jawab itu dijalankan?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p6"><?=$pertanyaan['p6']?></textarea>
                                </div>
                                11. Menurut anda bagaimana kurikulum pendidikan yang ideal itu?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p7"><?=$pertanyaan['p7']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(3)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(5)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step5" style="display:none;">
                                12. Jenjang pendidikan apa yang anda rencanakan untuk pendidikan anak anda di Sekolah Alam Indonesia?
                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. SD 6 tahun, alasan...
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p8"><?=$pertanyaan['p8']?></textarea>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. SMP 3 tahun, alasan...
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p9"><?=$pertanyaan['p9']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(4)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(6)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step6" style="display:none;">
                                13. Bagaimana pendapat anda tentang ijazah dari Diknas?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p10"><?=$pertanyaan['p10']?></textarea>
                                </div>
                                14. Bagaimana pendapat anda, jika anak anda mendapat ijazah dari luar Sekolah Alam Indonesia?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p11"><?=$pertanyaan['p11']?></textarea>
                                </div>
                                15. Apakah anda pernah mengikuti seleksi calon siswa di Sekolah Alam Indonesia sebelumnya?<br>
                                Jika Tidak tulis Belum Pernah. Jika YA Pada saat...
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p12"><?=$pertanyaan['p12']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(5)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(7)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step7" style="display:none;">
                                16. Bagaimana menurut anda tentang sekolah berbasis komunitas?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p13"><?=$pertanyaan['p13']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(6)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(8)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>

                            </div> <!-- end page2 -->
                            <div id="page3">

                            <div id="step8" style="display:none;">
                                17. Bagaimana pendapat anda tentang belajar dan kecerdasan pada anak?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p14"><?=$pertanyaan['p14']?></textarea>
                                </div>
                                18. Dari tiga unsur berikut : akademik, akhlak dan kepemimpinan. Urutkan menurut skala prioritas, Jelaskan!
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p15"><?=$pertanyaan['p15']?></textarea>
                                </div>
                                19. Apa pendapat anda tentang pendidikan Al Quran?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p16"><?=$pertanyaan['p16']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(7)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(9)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step9" style="display:none;">
                                20. Bagaimana anda menjalankan pola pendidikan di rumah (home education)?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p17"><?=$pertanyaan['p17']?></textarea>
                                </div>
                                21.  Bagaimana harapan anda terhadap kemampuan akademik anak anda dan bagaimana upaya anda untuk mewujudkannya?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p19"><?=$pertanyaan['p19']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(8)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(10)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step10" style="display:none;">
                                22. Bagaimana menurut anda bentuk kerjasama yang ideal antara guru dan orang tua dalam kegiatan belajar mengajar?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p20"><?=$pertanyaan['p20']?></textarea>
                                </div>
                                23. Sebagai sekolah swasta yang pembiayaan keuangannya bersifat mandiri. Jika anda mengalami kesulitan keuangan sehingga mempengaruhi operasional sekolah, apa yang akan anda lakukan?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p21"><?=$pertanyaan['p21']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(9)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(11)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step11" style="display:none;">
                                24. Studi Kasus 1<br>
                                    Dalam kelas anak anda terdapat 44 siswa. Suatu hari kelas akan mengadakan kegiatan outing keluar kota, kegiatan ini membutuhkan biaya. Setelah dihitung ternyata biaya yang diperlukan sebesar Rp. 200.000,-. Ternyata dari ke-44 siswa yang ada di kelas, ada 6 siswa yang secara ekonomi hanya mampu membayar kurang dari setengah biaya yang sudah share ke-44 siswa tersebut. Bagaimana anda menyikapi hal tersebut? Apa solusi yang anda tawarkan terhadap kasus di atas?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p22"><?=$pertanyaan['p22']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(10)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(12)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>

                            </div> <!-- end page3 -->
                            <div id="page4">

                            <div id="step12" style="display:none;">
                                25. Studi Kasus 2<br>
                                Anak anda kini lebih memilih untuk tidak bersekolah. Anak anda beralasan bahwa teman-temannya tidak seru karena cengeng. Kadang anak anda juga mengatakan bahwa tementemannya nakal dan ibu guru jahat. Setiap akan masuk gerbang sekolah, tangan anak anda berkeringat dan genggamannya sangat erat memegang tangan anda. Ia kemudian menangis merengek minta pulang. Apa yang akan anda lakukan?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p23"><?=$pertanyaan['p23']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(11)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(13)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step13" style="display:none;">
                                26. Mengapa anda merasa layak diterima di Sekolah Alam Indonesia? 
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p24"><?=$pertanyaan['p24']?></textarea>
                                </div>
                                27. Jika anak anda diterima di Sekolah Alam Indonesia, kontribusi konkrit apa yang akan anda berikan?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p25"><?=$pertanyaan['p25']?></textarea>
                                </div>
                                28. Sudahkan anda mengetahui kondisi Sekolah Alam Indonesia saat ini? Jelaskan
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p26"><?=$pertanyaan['p26']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(12)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(14)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step14" style="display:none;">
                                <!-- 29. Apakah anda siap menyekolahkan anak anda dengan kondisi saat ini? Jelaskan!
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p27"><?=$pertanyaan['p27']?></textarea>
                                </div> -->
                                30. Organisasi sekolah apa saja yang akan anda masuki dan aktif di dalamnya? (Tulis Jawaban bisa lebih dari satu a/b/c/d)<br>
                                a. Dewan Sekolah (Komite Sekolah)<br>
                                b. Dewan Kelas (Komite Kelas)<br>
                                c. Kepanitiaan kegiatan sekolah<br>
                                d. Lainnya...
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p28"><?=$pertanyaan['p28']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(13)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(15)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step15" style="display:none;">
                            <b>Isian berikut khusus bagi orang tua yang memilki anak yang telah sekolah di Sekolah Alam Indonesia sebelumnya.</b><br>
                                31. Sudah berapa lama anda bergabung di komunitas SAI?  
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p31"><?=$pertanyaan['p31']?></textarea>
                                </div>
                                32. Jika anda menilai diri sendiri, sejauh mana kerjasama anda dengan guru dalam kegiatan belajar mengajar selama ini?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p32"><?=$pertanyaan['p32']?></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger btn-block" onclick="buka(14)" id="next_btn">Back</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="buka(16)" id="next_btn">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="step16" style="display:none;">
                                
                                33. Apa kontribusi/pengalaman anda dalam organisasi komunitas di Sekolah Alam Indonesia? <br>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. Dewan Sekolah (jabatan/tahun)...
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p33"><?=$pertanyaan['p33']?></textarea>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Yayasan Alamku (jabatan/tahun)...
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p34"><?=$pertanyaan['p34']?></textarea>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c. Dewan kelas (jabatan/tahun)...
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p35"><?=$pertanyaan['p35']?></textarea>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d. Kepanitian Kegiatan Sekolah, (kegiatan/tahun)
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p36"><?=$pertanyaan['p36']?></textarea>
                                </div>
                                34. Tuliskan saran dan solusi untuk SAI (komunitas, dewan guru, sarana prasarana,KBM, dll)! 
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p37"><?=$pertanyaan['p37']?></textarea>
                                </div>
                                
                                <button type="button" class="btn btn-danger btn-block" onclick="buka(15)" id="next_btn">Back</button>
                            </div>

                            </div> <!-- end page4 -->

                            <?=form_hidden('tipe', 0);?>

                            <br>
                            <button type="submit" class="btn btn-success btn-block" onclick="set_tipe(0);">Simpan<div class="ripple-container"></div></button>
                            
                        </div>
                        <div class="col-md-1"></div>
                    </div>

<script>
    var i = 1;

    function hide(){
        $('#step1').hide();
        $('#step2').hide();
        $('#step3').hide();
        $('#step4').hide();
        $('#step5').hide();
        $('#step6').hide();
        $('#step7').hide();
        $('#step8').hide();
        $('#step9').hide();
        $('#step10').hide();
        $('#step11').hide();
        $('#step12').hide();
        $('#step13').hide();
        $('#step14').hide();
        $('#step15').hide();
        $('#step16').hide();
    }

    function buka(step){
        hide();
        $('#step'+step).show();

        var persen = hitung();

        $('#theprogressbar').attr('aria-valuenow', persen).css('width', persen+'%');
        $('#theprogressbar').html(persen+'%');

        set_tipe(1);
        save();
    }

    $(function() {
        buka(1);
    });

    function hitung() {
        var x = 0;
        if($('textarea[name=p1]').val() != '')
            x++;
        if($('textarea[name=p2]').val() != '')
            x++;
        if($('textarea[name=p3]').val() != '')
            x++;
        if($('textarea[name=p4]').val() != '')
            x++;
        if($('textarea[name=p5]').val() != '')
            x++;
        if($('textarea[name=p6]').val() != '')
            x++;
        if($('textarea[name=p7]').val() != '')
            x++;
        if($('textarea[name=p8]').val() != '' || $('textarea[name=p9]').val() != '')
            x++;
        if($('textarea[name=p10]').val() != '')
            x++;
        if($('textarea[name=p11]').val() != '')
            x++;
        if($('textarea[name=p12]').val() != '')
            x++;
        if($('textarea[name=p13]').val() != '')
            x++;
        if($('textarea[name=p14]').val() != '')
            x++;
        if($('textarea[name=p15]').val() != '')
            x++;
        if($('textarea[name=p16]').val() != '')
            x++;
        if($('textarea[name=p17]').val() != '')
            x++;
        if($('textarea[name=p18]').val() != '')
            x++;
        if($('textarea[name=p19]').val() != '')
            x++;
        if($('textarea[name=p20]').val() != '')
            x++;
        if($('textarea[name=p21]').val() != '')
            x++;
        if($('textarea[name=p22]').val() != '')
            x++;
        if($('textarea[name=p23]').val() != '')
            x++;
        if($('textarea[name=p24]').val() != '')
            x++;
        if($('textarea[name=p25]').val() != '')
            x++;
        if($('textarea[name=p26]').val() != '')
            x++;
        // if($('textarea[name=p27]').val() != '')
        //     x++;
        if($('textarea[name=p28]').val() != '')
            x++;
        if($('textarea[name=p29]').val() != '')
            x++;
        if($('textarea[name=p30]').val() != '')
            x++;
        if($('textarea[name=p31]').val() != '')
            x++;
        if($('textarea[name=p32]').val() != '')
            x++;
        // if($('textarea[name=p33]').val() != '')
        //     x++;
        // if($('textarea[name=p34]').val() != '')
        //     x++;
        // if($('textarea[name=p35]').val() != '')
        //     x++;
        // if($('textarea[name=p36]').val() != '')
        //     x++;
        if($('textarea[name=p37]').val() != '')
            x++;
        if($('textarea[name=p38]').val() != '')
            x++;

        var r = x/32*100;
        var res = Math.round(r);
        //alert(r);
        return res;
    }

    function save() {
        var input = $("#form-submit").serializeArray();
        //alert(input);

        $.ajax({ 
            type: "POST",   
            dataType: "json",
            url: "<?=base_url('siswa/pertanyaan/simpanISTC');?>",
            data: input,
            success: function (data) {
                //alert(data.success);
            }
        });
    }

    function set_tipe(tipe) {
        $('input[name=tipe]').val(tipe);
    }   

    var simpan = setInterval(save, 5*60*1000); // auto save per 5 menit

</script>