<div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                        <div class="progress md-progress mb-4" style="height: 20px">
                            <div class="progress-bar" id="theprogressbar" role="progressbar" style="width: 0%; height: 20px" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                        </div>
                            <div id="step1">
                                1. Jelaskan visi Bapak/Ibu dalam mendidik anak
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p1"><?=$pertanyaan['p1']?></textarea>
                                </div>
                                2. Bagaimana peranan Bapak/Ibu sebagai orang tua dalam pembentukan masa depan anak dalam bentuk penyediaan fasilitas materi maupun non-materi (network, waktu, asset, dll) 
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p2"><?=$pertanyaan['p2']?></textarea>
                                </div>
                                3. Pada usia berapa Bapak/Ibu sebagai orang tua siap melepas/memberhentikan fasilitas yang Bapak/Ibu berikan ke anak?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p3"><?=$pertanyaan['p3']?></textarea>
                                </div>
                                4. Sebutkan langkah-langkah yang akan Bapak/Ibu lakukan untuk melepaskan ketergantungan anak terhadap orang tuanya?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p5"><?=$pertanyaan['p5']?></textarea>
                                </div>
                                5. Bagaimanakah pandangan Bapak/Ibu tentang pacaran?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p6"><?=$pertanyaan['p6']?></textarea>
                                </div>
                                6. Bagaimana sikap Bapak/Ibu jika menemukan ananda berpacaran dengan teman sekolah ataupun di luar sekolahnya?
                                <div class="form-group purple-border">
                                    <textarea class="form-control rounded p-2" rows="3" name="p7"><?=$pertanyaan['p7']?></textarea>
                                </div>
                                <button type="button" class="btn btn-primary btn-block" onclick="buka(2)" id="next_btn">Next</button>
                            </div>
                            <div id="step2" style="display:none;">
                                7. Buat skema pohon keluarga berisi Silsilah Keluarga berdasarkan Bakat, Karakter Pribadi & Jenis Pekerjaan sampai garis keturunan paling atas yang diketahui.
                                <div class="form-group purple-border">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div>
                                                <?=($pertanyaan['p4'])?'<a href="'.base_url("uploads/pertanyaan/").$pertanyaan['p4'].'" target="_blank">':'';?>
                                                <img id="image_preview_container" class="img-fluid mb-2 mt-2" src="<?=($pertanyaan['p4']) ? base_url("uploads/pertanyaan/").$pertanyaan['p4'] : '' ?>"  style="margin: auto; max-height: 400px; border-radius: 5px; cursor: pointer;">
                                                <?=($pertanyaan['p4'])?'</a>':'';?>
                                            </div>
                                            <button type="button" class="btn btn-success btn-block btn-sm" onclick="klik()">Upload Gambar</button>
                                            <input type="file" name="foto" id="file" onchange="setfilename(this.value)" value="<?=($pertanyaan['p4'])?$pertanyaan['p4']:'';?>" style="display:none;"/>

                                            <div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
                                		    <div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 10 MB</i></span></div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <button type="button" class="btn btn-danger btn-block" onclick="buka(1)" id="next_btn">Back</button>
                            </div>

                            <?=form_hidden('tipe', 0);?>
                            <?=form_hidden('p4', $pertanyaan['p4']);?>

                            <br>
                            <button type="submit" class="btn btn-success btn-block" onclick="set_tipe(0);">Simpan<div class="ripple-container"></div></button>
                            
                        </div>
                        <div class="col-md-1"></div>
                    </div>

<script>
    var i = 1;

    function hide(){
        $('#step1').hide();
        $('#step2').hide();
    }

    function buka(step){
        hide();
        $('#step'+step).show();

        var persen = hitung();

        $('#theprogressbar').attr('aria-valuenow', persen).css('width', persen+'%');
        $('#theprogressbar').html(persen+'%');

        set_tipe(1);
        save();
    }

    $(function() {
        buka(1);
    });
    
    function klik(){
        $('#file').click();
    }

    function setfilename(val){
        document.getElementById('file_name').value = val;
    }

    $('#file').change(function(e){

        var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
        var name = this.files[0].name;
        var name_arr = name.split(".");
        var type = name_arr[name_arr.length - 1];
        var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
        var pesan_size = "\nMohon masukkan file dengan ukuran max. 10 MB";
        var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
        var cek_size = size >= 10; // max file size 10 MB
        var cek_tipe = allowed.indexOf(type) == -1;

        if(cek_size || cek_tipe) {
            var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
            if(cek_size)
                pesan += pesan_size;

            if(cek_tipe)
                pesan += pesan_tipe;

            alert(pesan);
            a.value = '';

        } else {
            
            let reader = new FileReader();
            reader.onload = (e) => { 
                $('#image_preview_container').attr('src', e.target.result); 
            }
            reader.readAsDataURL(this.files[0]); 

        }
    });

    function hitung() {
        var x = 0;
        if($('textarea[name=p1]').val() != '')
            x++;
        if($('textarea[name=p2]').val() != '')
            x++;
        if($('textarea[name=p3]').val() != '')
            x++;
        //if($('#file_name').val() != '')
        //alert($('input[name=p4]').val())
        if($('input[name=p4]').val() != '')
            x++;
        if($('textarea[name=p5]').val() != '')
            x++;
        if($('textarea[name=p6]').val() != '')
            x++;
        if($('textarea[name=p7]').val() != '')
            x++;

        var r = x/7*100;
        var res = Math.round(r);
        //alert(r);
        return res;
    }

    function save() {
        var input = $("#form-submit").serializeArray();
        //alert(input);

        $.ajax({ 
            type: "POST",   
            dataType: "json",
            url: "<?=base_url('siswa/pertanyaan/simpanBless');?>",
            data: input,
            success: function (data) {
                //alert(data.success);
            }
        });
    }

    function set_tipe(tipe) {
        $('input[name=tipe]').val(tipe);
    }

    var simpan = setInterval(save, 5*60*1000); // auto save per 5 menit

</script>