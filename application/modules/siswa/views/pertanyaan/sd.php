<style>
    .purple-border textarea {
        border: 1px solid #ba68c8;
    }
    .purple-border .form-control:focus {
        border: 1px solid #ba68c8;
        box-shadow: 0 0 0 0.2rem rgba(186, 104, 200, .25);
    }

    .green-border-focus .form-control:focus {
        border: 1px solid #8bc34a;
        box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, .25);
    }
</style>
<div class="jumbotron background">
    <div class="content" style="padding-top:40px;">
        <div class="container-fluid">
        <center><h3 style="color:white;"><b>Visi Misi Pendidikan</b></h3></center>
            <div class="row">
        
                <div class="col-md-8 ml-auto mr-auto">
                <h5><b><a href="<?=base_url('siswa/dashboard');?>"><font style="color:white;"><u>Dashboard</u></a> > </font><a href="<?=base_url('siswa/dashboard/datasiswa');?>"><font style="color:white;"><u>Data Anak</u></a> > </font><a href="<?=base_url('siswa/dashboard/siswa/'.$siswa['id']);?>"><font style="color:white;"><u>Menu Siswa</u></a> > Visi Misi Pendidikan</font></b></h5>
                <?php $this->view('siswa/notif'); ?>
                <div class="card card-signup">
                    <p class="card-title text-center" style="padding:10px;"><b>Isi Pertanyaan</b></p>
                    <?php echo form_open_multipart('siswa/pertanyaan/simpanSD', array('id'=>'form-submit')); ?>
                    <div class="card-body">
                    <input type="hidden" name="id" value="<?=$pertanyaan['id']?>">
                    <input type="hidden" name="id_siswa" value="<?=$pertanyaan['id_siswa']?>">
                    
                    <?php $this->load->view('form_sd'); ?>

                    </div>
                    <?php echo form_close(); ?>
                </div>
                </div>

            </div>
            
        </div>
    </div>
</div>


