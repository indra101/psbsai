<?php

class Morangtua extends CI_Model
{
    public $table = 'ortu';
    protected $primary = 'id';
	
    public function find($id)
    {   
        return $this->db->where('id', $id)->get($this->table)->row_array();
    }

    public function findAllDaftar()
    {   
        return $this->db->where('is_daftar', 1)->get($this->table)->result_array();
    }

    public function findAyah($id, $is_kepala, $status)
    {
        if($is_kepala && $status == 'Ayah')
            $where = array("id" => $id);
        else
            $where = array("id_kepala" => $id,"status" => 'Ayah');
        return $this->db->where($where)->get($this->table)->row_array();
    }

    public function findIbu($id, $is_kepala, $status)
    {
        if($is_kepala && $status == 'Ibu')
            $where = array("id" => $id);
        else
            $where = array("id_kepala" => $id,"status" => 'Ibu');
        return $this->db->where($where)->get($this->table)->row_array();
    }

    public function findWali($id, $is_kepala, $status)
    {
        if($is_kepala && $status == 'Wali') {
            $where = array("id" => $id,"status" => 'Wali');
            return $this->db->where($where)->get($this->table)->row_array();
        } else {
            return null;
        }
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getNamaById($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get($this->table);
        return $query->row()->nama;
    }

    public function simpanOrtu($ayah, $dataAyah, $ibu, $dataIbu, $wali, $dataWali)
    {
        // use transaction
        $this->db->trans_start();
        $this->update($ayah,$dataAyah);
        $this->update($ibu,$dataIbu);
        $this->update($wali,$dataWali);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function simpanAyahIbu($id_ayah, $ayah, $id_ibu, $ibu)
    {
        // use transaction
        $this->db->trans_start();

        $this->update($id_ayah,$ayah);
        $this->update($id_ibu,$ibu);
        
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

}
