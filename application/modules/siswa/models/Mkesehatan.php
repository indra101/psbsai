<?php

class Mkesehatan extends CI_Model
{
    public $table = 'kesehatan';
    protected $primary = 'id';
	
    public function find($id)
    {   
        return $this->db->where('id', $id)->get($this->table)->row_array();
    }

    public function findSiswa($id)
    {   
        return $this->db->where('id_siswa', $id)->get($this->table)->row_array();
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getSubKesehatan($id_siswa)
    {
        $query = "select s.id,s.nama_penyakit,s.tahun,s.pengobatan,s.pantangan,s.kondisi,s.deskripsi from sub_kesehatan s where s.id_siswa=$id_siswa";
    
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function updateSubKesehatan($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update('sub_kesehatan', $data);
    }

}
