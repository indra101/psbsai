<?php

class Msiswa extends CI_Model
{
    public $table = 'siswa';
    protected $primary = 'id';
	
    public function find($id)
    {   
        // return $this->db->where('id', $id)->get($this->table)->row_array();

        $this->db->select('siswa.*,ortu.nkk as nokk', false);
        $this->db->where('siswa.id', $id);
        $this->db->join('ortu', 'ortu.id = id_ortu', 'left');
        $query = $this->db->get($this->table);

        // untuk lihat query produced
        // $q = $this->db->last_query();
        // echo $q;
        // die;

        $result = $query->row_array();

        return $result;
    }

    public function create($data)
    {
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getSaudara($id_siswa)
    {
        $query = "select s.id,s.nama,s.umur,s.sekolah,s.kelas from saudara s where s.id_siswa=$id_siswa";
    
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function updateSaudara($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update('saudara', $data);
    }

    public function get_terapi($id_siswa)
    {
        $query = "select t.id,t.jenis,t.periode,t.status,t.lembaga from terapi t where t.id_siswa=$id_siswa";
    
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function update_terapi($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update('terapi', $data);
    }

}
