<?php

class Mpindahan extends CI_Model
{
    public $table = 'pindahan';
    protected $primary = 'id';
	
    public function find($id)
    {   
        return $this->db->where('id', $id)->get($this->table)->row_array();
    }

    public function findSiswa($id)
    {   
        return $this->db->where('id_siswa', $id)->get($this->table)->row_array();
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getSubPindahan($id_siswa)
    {
        $query = "select s.id,s.tahun,s.kelas,s.nama_sekolah,s.alasan_pindah from sub_pindahan s where s.id_siswa=$id_siswa";
    
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function updateSubPindahan($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update('sub_pindahan', $data);
    }
}
