<?php

class Mkeluarga extends CI_Model
{
    public $table = 'keluarga';
    protected $primary = 'id';
	
    public function find($id)
    {   
        return $this->db->where('id', $id)->get($this->table)->row_array();
    }

    public function findSiswa($id)
    {   
        return $this->db->where('id_siswa', $id)->get($this->table)->row_array();
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

}
