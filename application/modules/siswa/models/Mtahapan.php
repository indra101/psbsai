<?php

class Mtahapan extends CI_Model
{
    public $table = 'tahapan';
    protected $primary = 'id';

    public function create($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getData(){
        $this->db->select('*', false);
        $this->db->order_by('tanggal_mulai', 'asc');
        $query = $this->db->get($this->table);
        $result = $query->result_array();
        return $result;
    }

}
