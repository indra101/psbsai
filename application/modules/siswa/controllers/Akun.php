<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
	}

	public function index()
	{
		$data['title'] = 'Akun - Sekolah Alam Indonesia';
		$this->template->build('akun/index', $data);
	}
}