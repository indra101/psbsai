<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pindahan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('Msiswa');
		$this->load->model('Mpindahan');
		$this->load->model('admin/Mapp_setting');
		
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("login"));
       }
	}

	public function index()
	{
		$data['title'] = 'Pindahan - Sekolah Alam Indonesia';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$id_siswa = $this->uri->segment(4);
		$siswa = $this->Msiswa->find($id_siswa);
		$data['id_siswa'] = $id_siswa;
		$data['siswa'] = $siswa;

		$pindahan = $this->Mpindahan->findSiswa($id_siswa);
		$data['pindahan'] = $pindahan;

		$sub_pindahan = $this->Mpindahan->getSubPindahan($id_siswa);
		$data['sub_pindahan'] = $sub_pindahan;

		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$this->template->build('pindahan/index', $data);
	}

	public function simpan()
	{
		$id = $this->input->post('id');
		$id_siswa = $this->input->post('id_siswa');
		$nisn = $this->input->post('nisn');
		$nama_sekolah = $this->input->post('nama_sekolah');
		$alamat_sekolah = $this->input->post('alamat_sekolah');
		$alasan_pindah = $this->input->post('alasan_pindah');
		
		$s_id1 = $this->input->post('s_id1');
		$s_tahun1 = $this->input->post('s_tahun1');
		$s_kelas1 = $this->input->post('s_kelas1');
		$s_nama_sekolah1 = $this->input->post('s_nama_sekolah1');
		$s_alasan_pindah1 = $this->input->post('s_alasan_pindah1');

		$s_id2 = $this->input->post('s_id2');
		$s_tahun2 = $this->input->post('s_tahun2');
		$s_kelas2 = $this->input->post('s_kelas2');
		$s_nama_sekolah2 = $this->input->post('s_nama_sekolah2');
		$s_alasan_pindah2 = $this->input->post('s_alasan_pindah2');

		$s_id3 = $this->input->post('s_id3');
		$s_tahun3 = $this->input->post('s_tahun3');
		$s_kelas3 = $this->input->post('s_kelas3');
		$s_nama_sekolah3 = $this->input->post('s_nama_sekolah3');
		$s_alasan_pindah3 = $this->input->post('s_alasan_pindah3');

		$s_id4 = $this->input->post('s_id4');
		$s_tahun4 = $this->input->post('s_tahun4');
		$s_kelas4 = $this->input->post('s_kelas4');
		$s_nama_sekolah4 = $this->input->post('s_nama_sekolah4');
		$s_alasan_pindah4 = $this->input->post('s_alasan_pindah4');

		$s_id5 = $this->input->post('s_id5');
		$s_tahun5 = $this->input->post('s_tahun5');
		$s_kelas5 = $this->input->post('s_kelas5');
		$s_nama_sekolah5 = $this->input->post('s_nama_sekolah5');
		$s_alasan_pindah5 = $this->input->post('s_alasan_pindah5');
		
		$pindahan = array( 	"nisn" =>  $nisn,
							"nama_sekolah" => $nama_sekolah,
							"alamat_sekolah" => $alamat_sekolah,
							"alasan_pindah" => $alasan_pindah
						);

		$rpindahan = $this->Mpindahan->update($id, $pindahan);

		$pindahan1 = array(	"tahun" => $s_tahun1,
							"kelas" => $s_kelas1,
							"nama_sekolah" => $s_nama_sekolah1,
							"alasan_pindah" => $s_alasan_pindah1,
						);
		
		$pindahan2 = array(	"tahun" => $s_tahun2,
							"kelas" => $s_kelas2,
							"nama_sekolah" => $s_nama_sekolah2,
							"alasan_pindah" => $s_alasan_pindah2,
						);
		
		$pindahan3 = array(	"tahun" => $s_tahun3,
							"kelas" => $s_kelas3,
							"nama_sekolah" => $s_nama_sekolah3,
							"alasan_pindah" => $s_alasan_pindah3,
						);

		$pindahan4 = array(	"tahun" => $s_tahun4,
							"kelas" => $s_kelas4,
							"nama_sekolah" => $s_nama_sekolah4,
							"alasan_pindah" => $s_alasan_pindah4,
						);

		$pindahan5 = array(	"tahun" => $s_tahun5,
							"kelas" => $s_kelas5,
							"nama_sekolah" => $s_nama_sekolah5,
							"alasan_pindah" => $s_alasan_pindah5,
						);

		$rpindahan1 = $this->Mpindahan->updateSubPindahan($s_id1,$pindahan1);
		$rpindahan2 = $this->Mpindahan->updateSubPindahan($s_id2,$pindahan2);
		$rpindahan3 = $this->Mpindahan->updateSubPindahan($s_id3,$pindahan3);
		$rpindahan4 = $this->Mpindahan->updateSubPindahan($s_id4,$pindahan4);
		$rpindahan5 = $this->Mpindahan->updateSubPindahan($s_id5,$pindahan5);

		if($rpindahan5) {
			$notif = "Data Pindahan Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Data Pindahan Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);

		//redirect(base_url("siswa/pindahan/index/$id_siswa?notif_$tipe=1&notification=$notif"));
		redirect(base_url("siswa/pindahan/index/$id_siswa"));
	}
}