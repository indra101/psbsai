<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pernyataan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('Msiswa');
		$this->load->model('admin/Mapp_setting');
		$this->load->library('image_lib');

		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("login"));
       }
	}

	public function index()
	{
		$data['title'] = 'Surat Pernyataan - Sekolah Alam Indonesia';
		$id = $this->uri->segment(4);
		$data['siswa'] = $this->Msiswa->find($id);
		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$this->template->build('pernyataan/index', $data);
	}

	public function simpan()
	{
		$id = $this->input->post('id');
		$foto_surat = $this->input->post('foto_surat');
		$pernyataan = $this->upload('foto', $id);

		if($pernyataan) {
			$foto_surat = $pernyataan;
		}
		
		$data = array("pernyataan" => $foto_surat);

		$update = $this->Msiswa->update($id, $data);

		if($update) {
			$notif = "Surat Pernyataan berhasil disimpan!";
			$tipe = "display";
		} else {
			$notif = "Surat Pernyataan gagal disimpan! Silahkan coba lagi";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		redirect(base_url("siswa/pernyataan/index/$id"));
		
	}

	function upload($file, $id){
        $config['upload_path']          = './uploads/pernyataan/';
        $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
        $config['max_size']             = '99999999'; // imb
        $this->load->library('upload', $config);
			// proses upload
			
		$nama = substr($_FILES[$file]['name'], 0, strrpos($_FILES[$file]['name'], "."));
		$nama_new = $id.'_'.$file;
		$ext = str_replace($nama, '', $_FILES[$file]['name']);
		$_FILES[$file]['name'] = $nama_new . '.' . $ext;

        $this->upload->do_upload($file);
		$upload = $this->upload->data();

		$config['source_image'] = './uploads/pernyataan/'.$upload['file_name'];
		$config['new_image'] = './uploads/pernyataan/thumb/'.$upload['file_name'];;
		//$config['create_thumb'] = TRUE;
		//$config['thumb_marker'] = '_thumb';
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 200;
		//$config['height']       = 40;

		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		
        return $upload['file_name'];
	}
}