<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orangtua extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Morangtua');
		$this->load->model('admin/Mapp_setting');
		$this->load->library('template');

		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("login"));
       }
	}

	public function index()
	{
		$data['title'] = 'Orang Tua - Sekolah Alam Indonesia';
		$id = $this->session->userdata('id');
		$ortu = $this->Morangtua->find($id);
		$data['ayah'] = $this->Morangtua->findAyah($id, $ortu['is_kepala'], $ortu['status']);
		$data['ibu'] = $this->Morangtua->findIbu($id, $ortu['is_kepala'], $ortu['status']);
		$data['wali'] = $this->Morangtua->findWali($id, $ortu['is_kepala'], $ortu['status']);

		$data['id_kepala'] = $id;
		$data['status_kepala'] = $ortu['status'];

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;
		$this->template->build('orangtua/index', $data);
	}

	public function cekLengkap() {
		$ortus = $this->Morangtua->findAllDaftar();
		$complete = 0;

		foreach($ortus as $o) {
			$id =  $o['id'];
			$status_khusus =  $o['status_khusus'];
			$nkk =  $o['nkk'];
			$nik_ortu =  $o['nik_ortu'];
			$nama_ortu =  $o['nama_ortu'];
			$email =  $o['email'];
			$kelamin =  $o['kelamin'];
			$agama =  $o['agama'];
			$suku =  $o['suku'];
			$tempat_lahir =  $o['tempat_lahir'];
			$tgl_lahir =  $o['tgl_lahir'];
			$hp =  $o['hp'];
			$alamat_rumah = $o['alamat_rumah']; 
			$alamat_pekerjaan =  $o['alamat_pekerjaan'];
			$pekerjaan =  $o['pekerjaan'];
			$jabatan =  $o['jabatan'];
			$instansi =  $o['instansi'];
			$range_gaji =  $o['range_gaji'];
			$jml_jam_kerja =  $o['jml_jam_kerja'];
			$waktu_keluarga =  $o['waktu_keluarga'];
			$pendidikan =  $o['pendidikan'];
			$almamater = $o['almamater'];

			if($status_khusus == 'meninggal' || $status_khusus == 'cerai') {
				$complete = 1;

			} else if(	
				$nkk && 
				$nik_ortu && 
				$nama_ortu && 
				$email && 
				$kelamin && 
				$agama && 
				$suku && 
				$tempat_lahir && 
				$tgl_lahir && 
				$hp && 
				$alamat_rumah && 
				$alamat_pekerjaan && 
				$pekerjaan && 
				$jabatan && 
				$instansi && 
				$range_gaji && 
				$jml_jam_kerja && 
				$waktu_keluarga && 
				$pendidikan && 
				$almamater
				){
				
				$complete = 1;
			}else{
				$complete = 0;
			}

			$o_new['complete'] = $complete;

			$this->Morangtua->update($id, $o_new);
		}

		$notif = "Data Orang Tua sudah Disimpan";
		$tipe = "display";
		
		$this->session->set_flashdata($tipe, $notif);
		
		redirect(base_url("siswa/orangtua"));
	}

	public function simpan()
	{
		$id_kepala = $this->input->post('id_kepala');
		$status_kepala = $this->input->post('status_kepala');

		if($status_kepala == 'Wali') {
			//wali
			$wali_id = $this->input->post('wali_id');
			$wali_nkk = $this->input->post('wali_nkk');
			$wali_nik_ortu = $this->input->post('wali_nik_ortu');
			$wali_nama_ortu = $this->input->post('wali_nama_ortu');
			$wali_email = $this->input->post('wali_email');
			$wali_kelamin = $this->input->post('wali_kelamin');
			$wali_agama = $this->input->post('wali_agama');
			$wali_suku = $this->input->post('wali_suku');
			$wali_tempat_lahir = $this->input->post('wali_tempat_lahir');
			$wali_tgl_lahir = $this->input->post('wali_tgl_lahir');
			$wali_tgl_lahir = ($wali_tgl_lahir)?$wali_tgl_lahir:'0000-00-00';
			$wali_hp = $this->input->post('wali_hp');
			$wali_alamat_rumah = $this->input->post('wali_alamat_rumah');
			$wali_telp = $this->input->post('wali_telp');
			$wali_alamat_pekerjaan = $this->input->post('wali_alamat_pekerjaan');
			$wali_telp_kantor = $this->input->post('wali_telp_kantor');
			$wali_pekerjaan = $this->input->post('wali_pekerjaan');
			$wali_fb = $this->input->post('wali_fb');
			$wali_twitter = $this->input->post('wali_twitter');
			$wali_instagram = $this->input->post('wali_instagram');
			$wali_jabatan = $this->input->post('wali_jabatan');
			$wali_instansi = $this->input->post('wali_instansi');
			$wali_range_gaji = $this->input->post('wali_range_gaji');
			$wali_jml_jam_kerja = $this->input->post('wali_jml_jam_kerja');
			$wali_waktu_keluarga = $this->input->post('wali_waktu_keluarga');
			$wali_pendidikan = $this->input->post('wali_pendidikan');
			$wali_almamater = $this->input->post('wali_almamater');

			if(	$wali_nkk && 
				$wali_nik_ortu && 
				$wali_nama_ortu && 
				$wali_email && 
				$wali_kelamin && 
				$wali_agama && 
				$wali_suku && 
				$wali_tempat_lahir && 
				$wali_tgl_lahir && 
				$wali_hp && 
				$wali_alamat_rumah && 
				$wali_alamat_pekerjaan && 
				$wali_pekerjaan && 
				$wali_jabatan && 
				$wali_instansi && 
				$wali_range_gaji && 
				$wali_jml_jam_kerja && 
				$wali_waktu_keluarga && 
				$wali_pendidikan && 
				$wali_almamater
				){
				
				$complete = 1;
			}else{
				$complete = 0;
			}

			//array wali
			$wali = array("nkk" =>  $wali_nkk,
			"nik_ortu" => $wali_nik_ortu,
			"nama_ortu" => $wali_nama_ortu,
			"email" => $wali_email,
			"kelamin" => $wali_kelamin,
			"agama" => $wali_agama,
			"suku" => $wali_suku,
			"tempat_lahir" => $wali_tempat_lahir,
			"tgl_lahir" => $wali_tgl_lahir,
			"hp" => $wali_hp,
			"alamat_rumah" => $wali_alamat_rumah,
			"telp" => $wali_telp,
			"alamat_pekerjaan" => $wali_alamat_pekerjaan,
			"telp_kantor" => $wali_telp_kantor,
			"pekerjaan" => $wali_pekerjaan,
			"fb" => $wali_fb,
			"twitter" => $wali_twitter,
			"instagram" => $wali_instagram,
			"jabatan" => $wali_jabatan,
			"instansi" => $wali_instansi,
			"range_gaji" => $wali_range_gaji,
			"jml_jam_kerja" => $wali_jml_jam_kerja,
			"waktu_keluarga" => $wali_waktu_keluarga,
			"pendidikan" => $wali_pendidikan,
			"almamater" => $wali_almamater,
			"complete" => $complete 
			);

			$orangtua = $this->Morangtua->update($id_kepala, $wali);

		} else {

			//ayah
			$ayah_id = $this->input->post('ayah_id');
			$ayah_khusus = $this->input->post('ayah_khusus');
			$ayah_nkk = $this->input->post('ayah_nkk');
			$ayah_nik_ortu = $this->input->post('ayah_nik_ortu');
			$ayah_nama_ortu = $this->input->post('ayah_nama_ortu');
			$ayah_email = $this->input->post('ayah_email');
			$ayah_kelamin = $this->input->post('ayah_kelamin');
			$ayah_agama = $this->input->post('ayah_agama');
			$ayah_suku = $this->input->post('ayah_suku');
			$ayah_tempat_lahir = $this->input->post('ayah_tempat_lahir');
			$ayah_tgl_lahir = $this->input->post('ayah_tgl_lahir');
			$ayah_tgl_lahir = ($ayah_tgl_lahir)?$ayah_tgl_lahir:'0000-00-00';
			$ayah_hp = $this->input->post('ayah_hp');
			$ayah_alamat_rumah = $this->input->post('ayah_alamat_rumah');
			$ayah_telp = $this->input->post('ayah_telp');
			$ayah_alamat_pekerjaan = $this->input->post('ayah_alamat_pekerjaan');
			$ayah_telp_kantor = $this->input->post('ayah_telp_kantor');
			$ayah_pekerjaan = $this->input->post('ayah_pekerjaan');
			$ayah_fb = $this->input->post('ayah_fb');
			$ayah_twitter = $this->input->post('ayah_twitter');
			$ayah_instagram = $this->input->post('ayah_instagram');
			$ayah_jabatan = $this->input->post('ayah_jabatan');
			$ayah_instansi = $this->input->post('ayah_instansi');
			$ayah_range_gaji = $this->input->post('ayah_range_gaji');
			$ayah_jml_jam_kerja = $this->input->post('ayah_jml_jam_kerja');
			$ayah_waktu_keluarga = $this->input->post('ayah_waktu_keluarga');
			$ayah_pendidikan = $this->input->post('ayah_pendidikan');
			$ayah_almamater = $this->input->post('ayah_almamater');

			//ibu
			$ibu_id = $this->input->post('ibu_id');
			$ibu_khusus = $this->input->post('ibu_khusus');
			$ibu_nkk = $this->input->post('ibu_nkk');
			$ibu_nik_ortu = $this->input->post('ibu_nik_ortu');
			$ibu_nama_ortu = $this->input->post('ibu_nama_ortu');
			$ibu_email = $this->input->post('ibu_email');
			$ibu_kelamin = $this->input->post('ibu_kelamin');
			$ibu_agama = $this->input->post('ibu_agama');
			$ibu_suku = $this->input->post('ibu_suku');
			$ibu_tempat_lahir = $this->input->post('ibu_tempat_lahir');
			$ibu_tgl_lahir = $this->input->post('ibu_tgl_lahir');
			$ibu_tgl_lahir = ($ibu_tgl_lahir)?$ibu_tgl_lahir:'0000-00-00';
			$ibu_hp = $this->input->post('ibu_hp');
			$ibu_alamat_rumah = $this->input->post('ibu_alamat_rumah');
			$ibu_telp = $this->input->post('ibu_telp');
			$ibu_alamat_pekerjaan = $this->input->post('ibu_alamat_pekerjaan');
			$ibu_telp_kantor = $this->input->post('ibu_telp_kantor');
			$ibu_pekerjaan = $this->input->post('ibu_pekerjaan');
			$ibu_fb = $this->input->post('ibu_fb');
			$ibu_twitter = $this->input->post('ibu_twitter');
			$ibu_instagram = $this->input->post('ibu_instagram');
			$ibu_jabatan = $this->input->post('ibu_jabatan');
			$ibu_instansi = $this->input->post('ibu_instansi');
			$ibu_range_gaji = $this->input->post('ibu_range_gaji');
			$ibu_jml_jam_kerja = $this->input->post('ibu_jml_jam_kerja');
			$ibu_waktu_keluarga = $this->input->post('ibu_waktu_keluarga');
			$ibu_pendidikan = $this->input->post('ibu_pendidikan');
			$ibu_almamater = $this->input->post('ibu_almamater');

			if($ayah_khusus == 'meninggal' || $ayah_khusus == 'cerai') {
				$complete_ayah = 1;

			} else if(	
				$ayah_nkk && 
				$ayah_nik_ortu && 
				$ayah_nama_ortu && 
				$ayah_email && 
				$ayah_kelamin && 
				$ayah_agama && 
				$ayah_suku && 
				$ayah_tempat_lahir && 
				$ayah_tgl_lahir && 
				$ayah_hp && 
				$ayah_alamat_rumah && 
				$ayah_alamat_pekerjaan && 
				$ayah_pekerjaan && 
				$ayah_jabatan && 
				$ayah_instansi && 
				$ayah_range_gaji && 
				$ayah_jml_jam_kerja && 
				$ayah_waktu_keluarga && 
				$ayah_pendidikan && 
				$ayah_almamater
				){
				
				$complete_ayah = 1;
			}else{
				$complete_ayah = 0;
			}

			//array ayah
			$ayah = array("status_khusus" =>  $ayah_khusus,
				"nkk" =>  $ayah_nkk,
				"nik_ortu" => $ayah_nik_ortu,
				"nama_ortu" => $ayah_nama_ortu,
				"email" => $ayah_email,
				"kelamin" => $ayah_kelamin,
				"agama" => $ayah_agama,
				"suku" => $ayah_suku,
				"tempat_lahir" => $ayah_tempat_lahir,
				"tgl_lahir" => $ayah_tgl_lahir,
				"hp" => $ayah_hp,
				"alamat_rumah" => $ayah_alamat_rumah,
				"telp" => $ayah_telp,
				"alamat_pekerjaan" => $ayah_alamat_pekerjaan,
				"telp_kantor" => $ayah_telp_kantor,
				"pekerjaan" => $ayah_pekerjaan,
				"fb" => $ayah_fb,
				"twitter" => $ayah_twitter,
				"instagram" => $ayah_instagram,
				"jabatan" => $ayah_jabatan,
				"instansi" => $ayah_instansi,
				"range_gaji" => $ayah_range_gaji,
				"jml_jam_kerja" => $ayah_jml_jam_kerja,
				"waktu_keluarga" => $ayah_waktu_keluarga,
				"pendidikan" => $ayah_pendidikan,
				"almamater" => $ayah_almamater,
				"complete" => $complete_ayah
			 );

			if($ibu_khusus == 'meninggal' || $ibu_khusus == 'cerai') {
				$complete_ibu = 1;

			} else if(	
				$ibu_nkk && 
				$ibu_nik_ortu && 
				$ibu_nama_ortu && 
				$ibu_email && 
				$ibu_kelamin && 
				$ibu_agama && 
				$ibu_suku && 
				$ibu_tempat_lahir && 
				$ibu_tgl_lahir && 
				$ibu_hp && 
				$ibu_alamat_rumah && 
				$ibu_alamat_pekerjaan && 
				$ibu_pekerjaan && 
				$ibu_jabatan && 
				$ibu_instansi && 
				$ibu_range_gaji && 
				$ibu_jml_jam_kerja && 
				$ibu_waktu_keluarga && 
				$ibu_pendidikan && 
				$ibu_almamater
				){
				
				$complete_ibu = 1;
			}else{
				$complete_ibu = 0;
			}

			//array ibu
			$ibu = array("status_khusus" =>  $ibu_khusus,
				"nkk" =>  $ibu_nkk,
				"nik_ortu" => $ibu_nik_ortu,
				"nama_ortu" => $ibu_nama_ortu,
				"email" => $ibu_email,
				"kelamin" => $ibu_kelamin,
				"agama" => $ibu_agama,
				"suku" => $ibu_suku,
				"tempat_lahir" => $ibu_tempat_lahir,
				"tgl_lahir" => $ibu_tgl_lahir,
				"hp" => $ibu_hp,
				"alamat_rumah" => $ibu_alamat_rumah,
				"telp" => $ibu_telp,
				"alamat_pekerjaan" => $ibu_alamat_pekerjaan,
				"telp_kantor" => $ibu_telp_kantor,
				"pekerjaan" => $ibu_pekerjaan,
				"fb" => $ibu_fb,
				"twitter" => $ibu_twitter,
				"instagram" => $ibu_instagram,
				"jabatan" => $ibu_jabatan,
				"instansi" => $ibu_instansi,
				"range_gaji" => $ibu_range_gaji,
				"jml_jam_kerja" => $ibu_jml_jam_kerja,
				"waktu_keluarga" => $ibu_waktu_keluarga,
				"pendidikan" => $ibu_pendidikan,
				"almamater" => $ibu_almamater,
				"complete" => $complete_ibu
			 );
			
			$orangtua = $this->Morangtua->simpanAyahIbu($ayah_id, $ayah, $ibu_id, $ibu);

		}

		if($orangtua) {
			$notif = "Data Orang Tua Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Data Orang Tua Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("siswa/orangtua/index?notif_$tipe=1&notification=$notif"));
		redirect(base_url("siswa/orangtua"));
	}
}