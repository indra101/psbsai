<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluarga extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('Mkeluarga');
		$this->load->model('admin/Mapp_setting');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("login"));
       }
	}

	public function index()
	{
		$data['title'] = 'Keluarga - Sekolah Alam Indonesia';
		$id = $this->uri->segment(4);
		$data['keluarga'] = $this->Mkeluarga->findSiswa($id);
		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$this->template->build('keluarga/index', $data);
	}

	public function simpan()
	{
		$id = $this->input->post('id');
		$id_siswa = $this->input->post('id_siswa');
		$tanggungan = $this->input->post('tanggungan');
		$pengeluaran = $this->input->post('pengeluaran');
		$dana_pendidikan = $this->input->post('dana_pendidikan');
		$tinggal_bersama = $this->input->post('tinggal_bersama');
		$tinggal_bersama2 = $this->input->post('tinggal_bersama2');
		$penghuni = $this->input->post('penghuni');
		$ayah = $this->input->post('ayah');
		$ibu = $this->input->post('ibu');
		$paman = $this->input->post('paman');
		$bibi = $this->input->post('bibi');
		$kakek = $this->input->post('kakek');
		$nenek = $this->input->post('nenek');
		$lain = $this->input->post('lain');
		$orang_dekat = $this->input->post('orang_dekat');
		$jarak = $this->input->post('jarak');
		$waktu = $this->input->post('waktu');
		$transport = $this->input->post('transport');
		$transport2 = $this->input->post('transport2');
		$antar = $this->input->post('antar');
		$antar2 = $this->input->post('antar2');

		if($tanggungan && $pengeluaran && $dana_pendidikan && $tinggal_bersama && $tinggal_bersama2 && $penghuni && ( $ayah || $ibu || $paman || $bibi || $kakek || $nenek || $lain) && $jarak && $waktu && $transport && $antar){
			$complete = array("complete" => '1');
		}else{
			$complete = array("complete" => '0');
		}
		
		$keluarga = array(  "tanggungan" =>  $tanggungan,
                    "pengeluaran" => $pengeluaran,
                    "dana_pendidikan" => $dana_pendidikan,
                    "tinggal_bersama" => $tinggal_bersama,
                    "tinggal_bersama2" => $tinggal_bersama2,
                    "penghuni" => $penghuni,
                    "ayah" => $ayah,
                    "ibu" => $ibu,
                    "paman" => $paman,
                    "bibi" => $bibi,
                    "kakek" => $kakek,
                    "nenek" => $nenek,
                    "lain" => $lain,
                    "orang_dekat" => $orang_dekat,
                    "jarak" => $jarak,
                    "waktu" => $waktu,
                    "transport" => $transport,
                    "transport2" => $transport2,
                    "antar" => $antar,
					"antar2" => $antar2);

		$keluarga = array_merge($keluarga, $complete);
					
		$family = $this->Mkeluarga->update($id, $keluarga);

		if($family) {
			$notif = "Data Keluarga Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Data Keluarga Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("siswa/keluarga/index/$id_siswa?notif_$tipe=1&notification=$notif"));
		redirect(base_url("siswa/keluarga/index/$id_siswa"));
		
	}
}