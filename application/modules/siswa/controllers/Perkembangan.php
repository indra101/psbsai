<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perkembangan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('Msiswa');
		$this->load->model('Mperkembangan');
		$this->load->model('admin/Mapp_setting');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("login"));
       }
	}

	public function index()
	{
		$data['title'] = 'Perkembangan - Sekolah Alam Indonesia';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$id_siswa = $this->uri->segment(4);
		$siswa = $this->Msiswa->find($id_siswa);
		$data['id_siswa'] = $id_siswa;
		$data['siswa'] = $siswa;

		$perkembangan = $this->Mperkembangan->findSiswa($id_siswa);
		$data['perkembangan'] = $perkembangan;
		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		if($siswa['level'] == 'kb' || $siswa['level'] == 'tka' || $siswa['level'] == 'tkb'){
			$this->template->build('perkembangan/kbtk', $data);
		}else if($siswa['level'] == 'istcsd' || $siswa['level'] == 'istcsl'){
			$this->template->build('perkembangan/istc', $data);
		}else if(preg_match("/sd/i", $siswa['level'])){
			$this->template->build('perkembangan/sd', $data);
		}else if(preg_match("/sl/i", $siswa['level'])){
			$this->template->build('perkembangan/sl', $data);
		}
	}
	
	public function simpan()
	{
		$id = $this->input->post('id');
		$id_siswa = $this->input->post('id_siswa');
		$p1 = $this->input->post('p1');
		$p2 = $this->input->post('p2');
		$p3 = $this->input->post('p3');
		$p4 = $this->input->post('p4');
		$p5 = $this->input->post('p5');
		$p6 = $this->input->post('p6');
		$p7 = $this->input->post('p7');
		$p8 = $this->input->post('p8');
		$p9 = $this->input->post('p9');
		$p10 = $this->input->post('p10');
		$p11 = $this->input->post('p11');
		$p12 = $this->input->post('p12');
		$p13 = $this->input->post('p13');
		$p14 = $this->input->post('p14');
		$p15 = $this->input->post('p15');
		$p16 = $this->input->post('p16');
		$p17 = $this->input->post('p17');
		$p18 = $this->input->post('p18');
		$p19 = $this->input->post('p19');
		$p20 = $this->input->post('p20');
		$p21 = $this->input->post('p21');
		$p22 = $this->input->post('p22');
		$p23 = $this->input->post('p23');
		$p24 = $this->input->post('p24');
		$p25 = $this->input->post('p25');
		$p26 = $this->input->post('p26');
		$p27 = $this->input->post('p27');
		$p28 = $this->input->post('p28');
		$p29 = $this->input->post('p29');
		$p30 = $this->input->post('p30');
		$p31 = $this->input->post('p31');
		$p32 = $this->input->post('p32');
		$p33 = $this->input->post('p33');
		$p34 = $this->input->post('p34');
		$p35 = $this->input->post('p35');
		$p36 = $this->input->post('p36');

		if($p1 && $p2 && $p3 && $p4 && $p5 && $p6 && $p7 && $p8 && $p9 && $p10 && $p11 && $p12 && $p13 && $p14 && $p15 && $p16 && $p17 && $p18 && $p19 && $p20 && $p21 && $p22 && $p23 && $p24 && $p25 && $p26 && $p27 && $p28 && $p29 && $p30 && $p31 && $p32 && $p33 && $p34&& $p35 && $p36){
			$complete = array("complete" => '1');
		}else{
			$complete = array("complete" => '0');
		}

		$perkembangan = array(  "p1" => $p1,
							"p2" => $p2,
							"p3" => $p3,
							"p4" => $p4,
							"p5" => $p5,
							"p6" => $p6,
							"p7" => $p7,
							"p8" => $p8,
							"p9" => $p9,
							"p10" => $p10,
							"p11" => $p11,
							"p12" => $p12,
							"p13" => $p13,
							"p14" => $p14,
							"p15" => $p15,
							"p16" => $p16,
							"p17" => $p17,
							"p18" => $p18,
							"p19" => $p19,
							"p20" => $p20,
							"p21" => $p21,
							"p22" => $p22,
							"p23" => $p23,
							"p24" => $p24,
							"p25" => $p25,
							"p26" => $p26,
							"p27" => $p27,
							"p28" => $p28,
							"p29" => $p29,
							"p30" => $p30,
							"p31" => $p31,
							"p32" => $p32,
							"p33" => $p33,
							"p34" => $p34,
							"p35" => $p35,
							"p36" => $p36
						);

		$perkembangan = array_merge($perkembangan, $complete);

		$berkembang = $this->Mperkembangan->update($id, $perkembangan);

		if($berkembang) {
			$notif = "Data Perkembangan Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Data Perkembangan Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("siswa/perkembangan/index/$id_siswa?notif_$tipe=1&notification=$notif"));
		redirect(base_url("siswa/perkembangan/index/$id_siswa"));
		
		
	}
}