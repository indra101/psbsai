<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('Mdashboard');
		$this->load->model('Msiswa');
		$this->load->model('Mkesehatan');
		$this->load->model('Mkelahiran');
		$this->load->model('Mkeluarga');
		$this->load->model('Mperkembangan');
		$this->load->model('Mpertanyaan');
		$this->load->model('admin/Mapp_setting');

		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("login"));
       }
	}

	public function index()
	{
		$data['title'] = 'Dashboard - Sekolah Alam Indonesia';

		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$this->template->build('dashboard/index', $data);
	}

	public function orangtua()
	{
		$data['title'] = 'Menu Orang Tua - Sekolah Alam Indonesia';
		$this->template->build('dashboard/orangtua', $data);
	}

	public function datasiswa()
	{
		$data['title'] = 'Menu Siswa - Sekolah Alam Indonesia';
		$id = $this->session->userdata('id');
		$data['siswa'] = $this->Mdashboard->getAnak($id);
		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;
		$this->template->build('dashboard/datasiswa', $data);
	}

	public function siswa()
	{
		$data['title'] = 'Menu Siswa - Sekolah Alam Indonesia';
		$id_siswa = $this->uri->segment(4);
		$siswa = $this->Msiswa->find($id_siswa);
		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$id = $this->session->userdata('id');

		if($id == $siswa['id_ortu']) {

			$data['id_siswa'] = $id_siswa;
			$data['siswa'] = $siswa;
			$data['kesehatan'] = $this->Mkesehatan->findSiswa($id_siswa);
			$data['kelahiran'] = $this->Mkelahiran->findSiswa($id_siswa);
			$data['keluarga'] = $this->Mkeluarga->findSiswa($id_siswa);
			$data['perkembangan'] = $this->Mperkembangan->findSiswa($id_siswa);
			$data['pertanyaan'] = $this->Mpertanyaan->findSiswa($id_siswa);
			
			if(preg_match("/bless/i", $siswa['level'])){
				$this->template->build('dashboard/bless', $data);
			}else{
				$this->template->build('dashboard/siswa', $data);
			}

		} else {

			$notif = "Anda tidak memiliki akses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);
			redirect(base_url("siswa/dashboard/datasiswa"));

		}
	}
}