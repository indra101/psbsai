<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahapan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('Mtahapan');
		$this->load->model('admin/Mapp_setting');
		
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("login"));
       }
	}

	public function index()
	{
		$data['title'] = 'Tahapan - Sekolah Alam Indonesia';

		$data['tahapan'] = $this->Mtahapan->getData();
		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$this->template->build('tahapan/index', $data);
	}
}