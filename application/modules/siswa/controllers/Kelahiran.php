<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelahiran extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('Msiswa');
		$this->load->model('Mkelahiran');
		$this->load->model('admin/Mapp_setting');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("login"));
       }
	}

	public function index()
	{
		$data['title'] = 'Kelahiran - Sekolah Alam Indonesia';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$id_siswa = $this->uri->segment(4);
		$siswa = $this->Msiswa->find($id_siswa);
		$data['id_siswa'] = $id_siswa;
		$data['siswa'] = $siswa;
		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$kelahiran = $this->Mkelahiran->findSiswa($id_siswa);
		$data['kelahiran'] = $kelahiran;
		
		$this->template->build('kelahiran/index', $data);
	}

	public function simpan()
	{
		$id = $this->input->post('id');
		$id_siswa = $this->input->post('id_siswa');
		$kondisi_hamil = $this->input->post('kondisi_hamil');
		$kondisi_ibu = $this->input->post('kondisi_ibu');
		$kondisi_ibu2 = $this->input->post('kondisi_ibu2');
		$lingkungan = $this->input->post('lingkungan');
		$lingkungan2 = $this->input->post('lingkungan2');
		$perawatan = $this->input->post('perawatan');
		$perawatan2 = $this->input->post('perawatan2');
		$usia_pekan = $this->input->post('usia_pekan');
		$usia_hari = $this->input->post('usia_hari');
		$kasus = $this->input->post('kasus');
		$proses_lahir = $this->input->post('proses_lahir');
		$proses_lahir2 = $this->input->post('proses_lahir2');
		$usia_tengkurap = $this->input->post('usia_tengkurap');
		$usia_berjalan = $this->input->post('usia_berjalan');
		$bicara = $this->input->post('bicara');
		$merangkak = $this->input->post('merangkak');
		$perintah = $this->input->post('perintah');

		if($kondisi_hamil && $kondisi_ibu && $lingkungan && $perawatan && $usia_pekan && $kasus && $proses_lahir && $proses_lahir2 && $usia_tengkurap && $usia_berjalan && $bicara && $merangkak && $perintah){
			$complete = array("complete" => '1');
		}else{
			$complete = array("complete" => '0');
		}
		
		$kelahiran = array( "kondisi_hamil" => $kondisi_hamil,
							"kondisi_ibu" => $kondisi_ibu,
							"kondisi_ibu2" => $kondisi_ibu2,
							"lingkungan" => $lingkungan,
							"lingkungan2" => $lingkungan2,
							"perawatan" => $perawatan,
							"perawatan2" => $perawatan2,
							"usia_pekan" => $usia_pekan,
							"usia_hari" => $usia_hari,
							"kasus" => $kasus,
							"proses_lahir" => $proses_lahir,
							"proses_lahir2" => $proses_lahir2,
							"usia_tengkurap" => $usia_tengkurap,
							"usia_berjalan" => $usia_berjalan,
							"bicara" => $bicara,
							"merangkak" => $merangkak,
							"perintah" => $perintah
							);

		$kelahiran = array_merge($kelahiran, $complete);

		$lahir = $this->Mkelahiran->update($id, $kelahiran);

		if($lahir) {
			$notif = "Data Kelahiran Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Data Kelahiran Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		redirect(base_url("siswa/kelahiran/index/$id_siswa"));
	}
}