<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kesehatan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('Msiswa');
		$this->load->model('Mkesehatan');
		$this->load->model('admin/Mapp_setting');

		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("login"));
       }
	}

	public function index()
	{
		$data['title'] = 'Kesehatan - Sekolah Alam Indonesia';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$id_siswa = $this->uri->segment(4);
		$siswa = $this->Msiswa->find($id_siswa);
		$data['id_siswa'] = $id_siswa;
		$data['siswa'] = $siswa;
		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$data['kesehatan'] = $this->Mkesehatan->findSiswa($id_siswa);

		$sub_kesehatan = $this->Mkesehatan->getSubKesehatan($id_siswa);
		$data['sub_kesehatan'] = $sub_kesehatan;
		
		$this->template->build('kesehatan/index', $data);
	}

	public function simpan()
	{
		$id = $this->input->post('id');
		$id_siswa = $this->input->post('id_siswa');
		$tinggi = $this->input->post('tinggi');
		$berat = $this->input->post('berat');

		if($tinggi && $berat){
			$complete = array("complete" => '1');
		}else{
			$complete = array("complete" => '0');
		}

		$s_id1 = $this->input->post('s_id1');
		$s_nama_penyakit1 = $this->input->post('s_nama_penyakit1');
		$s_tahun1 = $this->input->post('s_tahun1');
		$s_pengobatan1 = $this->input->post('s_pengobatan1');
		$s_pantangan1 = $this->input->post('s_pantangan1');
		$s_kondisi1 = $this->input->post('s_kondisi1');
		$s_deskripsi1 = $this->input->post('s_deskripsi1');

		$s_id2 = $this->input->post('s_id2');
		$s_nama_penyakit2 = $this->input->post('s_nama_penyakit2');
		$s_tahun2 = $this->input->post('s_tahun2');
		$s_pengobatan2 = $this->input->post('s_pengobatan2');
		$s_pantangan2 = $this->input->post('s_pantangan2');
		$s_kondisi2 = $this->input->post('s_kondisi2');
		$s_deskripsi2 = $this->input->post('s_deskripsi2');

		$s_id3 = $this->input->post('s_id3');
		$s_nama_penyakit3 = $this->input->post('s_nama_penyakit3');
		$s_tahun3 = $this->input->post('s_tahun3');
		$s_pengobatan3 = $this->input->post('s_pengobatan3');
		$s_pantangan3 = $this->input->post('s_pantangan3');
		$s_kondisi3 = $this->input->post('s_kondisi3');
		$s_deskripsi3 = $this->input->post('s_deskripsi3');

		$s_id4 = $this->input->post('s_id4');
		$s_nama_penyakit4 = $this->input->post('s_nama_penyakit4');
		$s_tahun4 = $this->input->post('s_tahun4');
		$s_pengobatan4 = $this->input->post('s_pengobatan4');
		$s_pantangan4 = $this->input->post('s_pantangan4');
		$s_kondisi4 = $this->input->post('s_kondisi4');
		$s_deskripsi4 = $this->input->post('s_deskripsi4');

		$s_id5 = $this->input->post('s_id5');
		$s_nama_penyakit5 = $this->input->post('s_nama_penyakit5');
		$s_tahun5 = $this->input->post('s_tahun5');
		$s_pengobatan5 = $this->input->post('s_pengobatan5');
		$s_pantangan5 = $this->input->post('s_pantangan5');
		$s_kondisi5 = $this->input->post('s_kondisi5');
		$s_deskripsi5 = $this->input->post('s_deskripsi5');


		$kesehatan = array( "tinggi" =>  $tinggi,
							"berat" => $berat
						);
		
		$kesehatan = array_merge($kesehatan, $complete);

		$rkesehatan = $this->Mkesehatan->update($id, $kesehatan);

		$kesehatan1 = array("nama_penyakit" => $s_nama_penyakit1,
							"tahun" => $s_tahun1,
							"pengobatan" => $s_pengobatan1,
							"pantangan" => $s_pantangan1,
							"kondisi" => $s_kondisi1,
							"deskripsi" => $s_deskripsi1,
						);

		$kesehatan2 = array("nama_penyakit" => $s_nama_penyakit2,
						"tahun" => $s_tahun2,
						"pengobatan" => $s_pengobatan2,
						"pantangan" => $s_pantangan2,
						"kondisi" => $s_kondisi2,
						"deskripsi" => $s_deskripsi2,
					);

		$kesehatan3 = array("nama_penyakit" => $s_nama_penyakit3,
						"tahun" => $s_tahun3,
						"pengobatan" => $s_pengobatan3,
						"pantangan" => $s_pantangan3,
						"kondisi" => $s_kondisi3,
						"deskripsi" => $s_deskripsi3,
					);

		$kesehatan4 = array("nama_penyakit" => $s_nama_penyakit4,
						"tahun" => $s_tahun4,
						"pengobatan" => $s_pengobatan4,
						"pantangan" => $s_pantangan4,
						"kondisi" => $s_kondisi4,
						"deskripsi" => $s_deskripsi4,
					);

		$kesehatan5 = array("nama_penyakit" => $s_nama_penyakit5,
						"tahun" => $s_tahun5,
						"pengobatan" => $s_pengobatan5,
						"pantangan" => $s_pantangan5,
						"kondisi" => $s_kondisi5,
						"deskripsi" => $s_deskripsi5,
					);

		$rkesehatan1 = $this->Mkesehatan->updateSubKesehatan($s_id1,$kesehatan1);
		$rkesehatan2 = $this->Mkesehatan->updateSubKesehatan($s_id2,$kesehatan2);
		$rkesehatan3 = $this->Mkesehatan->updateSubKesehatan($s_id3,$kesehatan3);
		$rkesehatan4 = $this->Mkesehatan->updateSubKesehatan($s_id4,$kesehatan4);
		$rkesehatan5 = $this->Mkesehatan->updateSubKesehatan($s_id5,$kesehatan5);

		if($rkesehatan5) {
			$notif = "Data Kesehatan Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Data Kesehatan Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);

		//redirect(base_url("siswa/kesehatan/index/$id_siswa?notif_$tipe=1&notification=$notif"));
		redirect(base_url("siswa/kesehatan/index/$id_siswa"));

	}
}