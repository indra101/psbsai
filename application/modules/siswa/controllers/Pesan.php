<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
	}

	public function index()
	{
		$data['title'] = 'Pesan - Sekolah Alam Indonesia';
		$this->template->build('pesan/index', $data);
	}
}