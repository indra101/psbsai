<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataSiswa extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->library('image_lib');
		$this->load->library('upload');
		$this->load->model('Msiswa');
		$this->load->model('admin/Mapp_setting');

		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("login"));
       }
	}

	public function index()
	{
		$data['title'] = 'Data Siswa - Sekolah Alam Indonesia';
		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;
		$id_siswa = $this->uri->segment(4);
		$siswa = $this->Msiswa->find($id_siswa);
		$data['id_siswa'] = $id_siswa;
		$data['siswa'] = $siswa;

		$saudara = $this->Msiswa->getSaudara($id_siswa);
		$data['saudara'] = $saudara;
		$terapi = $this->Msiswa->get_terapi($id_siswa);
		$data['terapi'] = $terapi;

		$data['thumb_url'] = ($siswa['before_2024']) ? '' : 'thumb/';

		if(preg_match("/bless/i", $siswa['level'])){
			$this->template->build('datasiswa/bless', $data);
		}else if(preg_match("/istc/i", $siswa['level'])){
			$this->template->build('datasiswa/istc', $data);
		}else{
			$this->template->build('datasiswa/index', $data);
		}
	}

	public function simpan()
	{
		$id = $this->input->post('id');
		$nik = $this->input->post('nik');
		$nama_siswa = $this->input->post('nama_siswa');
		$panggilan = $this->input->post('panggilan');
		$alamat = $this->input->post('alamat');
		$kd_pos = $this->input->post('kd_pos');
		$telp_rumah = $this->input->post('telp_rumah');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$tanggal_lahir = ($tanggal_lahir) ? $tanggal_lahir : '0000-00-00';
		$kelamin = $this->input->post('kelamin');
		$gol_darah = $this->input->post('gol_darah');
		$anak_ke = $this->input->post('anak_ke');
		$saudara = $this->input->post('saudara');
		$agama = $this->input->post('agama');
		$warga_negara = $this->input->post('warga_negara');
		$suku = $this->input->post('suku');
		$bahasa = $this->input->post('bahasa');
		$bakat = $this->input->post('bakat');
		$hobby = $this->input->post('hobby');
		$khusus = $this->input->post('khusus');
		$cita_cita = $this->input->post('cita_cita');
		$nisn = $this->input->post('nisn');
		$asal_sekolah = $this->input->post('asal_sekolah');
		$npsn_sekolah_asal = $this->input->post('npsn_sekolah_asal');
		$alamat_sekolah_asal = $this->input->post('alamat_sekolah_asal');
		$alasan_pindah = $this->input->post('alasan_pindah');
		$diagnosa = $this->input->post('diagnosa');

		$foto_siswa = $this->input->post('foto_siswa');
		if(!empty($_FILES['foto']['name'])) {
			$foto_siswa = $this->upload_foto('foto', $id);
		}

		$akta_siswa = $this->input->post('akta_lahir_siswa');
		if(!empty($_FILES['akta_lahir']['name'])) {
			$akta_siswa = $this->upload_akta('akta_lahir', $id);
		}

		//if($nik && $nama_siswa && $panggilan && $alamat && $kd_pos && $telp_rumah && $tempat_lahir && $tanggal_lahir && $kelamin && $gol_darah && $anak_ke && $saudara && $agama && $warga_negara && $suku && $bahasa && $bakat && $hobby && $khusus && $foto){
		if($nik && $nama_siswa && $panggilan && $alamat && $tempat_lahir && $tanggal_lahir && $kelamin && $gol_darah && $anak_ke && $saudara && $agama && $warga_negara && $suku && $bahasa && $bakat && $hobby && $khusus && $cita_cita){
			$complete = array("complete" => '1');
		}else{
			$complete = array("complete" => '0');
		}

		$s_id1 = $this->input->post('s_id1');
		$s_nama1 = $this->input->post('s_nama1');
		$s_umur1 = $this->input->post('s_umur1');
		$s_sekolah1 = $this->input->post('s_sekolah1');
		$s_kelas1 = $this->input->post('s_kelas1');

		$s_id2 = $this->input->post('s_id2');
		$s_nama2 = $this->input->post('s_nama2');
		$s_umur2 = $this->input->post('s_umur2');
		$s_sekolah2 = $this->input->post('s_sekolah2');
		$s_kelas2 = $this->input->post('s_kelas2');

		$s_id3 = $this->input->post('s_id3');
		$s_nama3 = $this->input->post('s_nama3');
		$s_umur3 = $this->input->post('s_umur3');
		$s_sekolah3 = $this->input->post('s_sekolah3');
		$s_kelas3 = $this->input->post('s_kelas3');

		$s_id4 = $this->input->post('s_id4');
		$s_nama4 = $this->input->post('s_nama4');
		$s_umur4 = $this->input->post('s_umur4');
		$s_sekolah4 = $this->input->post('s_sekolah4');
		$s_kelas4 = $this->input->post('s_kelas4');

		$s_id5 = $this->input->post('s_id5');
		$s_nama5 = $this->input->post('s_nama5');
		$s_umur5 = $this->input->post('s_umur5');
		$s_sekolah5 = $this->input->post('s_sekolah5');
		$s_kelas5 = $this->input->post('s_kelas5');

		$siswa = array( 	"nik" =>  $nik,
							"nama_siswa" => $nama_siswa,
							"panggilan" => $panggilan,
							"alamat" => $alamat,
							"kd_pos" => $kd_pos,
							"telp_rumah" => $telp_rumah,
							"tempat_lahir" => $tempat_lahir,
							"tanggal_lahir" => $tanggal_lahir,
							"kelamin" => $kelamin,
							"gol_darah" => $gol_darah,
							"anak_ke" => $anak_ke,
							"saudara" => $saudara,
							"agama" => $agama,
							"warga_negara" => $warga_negara,
							"suku" => $suku,
							"bahasa" => $bahasa,
							"bakat" => $bakat,
							"hobby" => $hobby,
							"khusus" => $khusus,
							"foto" => $foto_siswa,
							"akta_lahir" => $akta_siswa,
							"cita_cita" =>  $cita_cita,
							"nisn" =>  $nisn,
							"asal_sekolah" => $asal_sekolah,
							"npsn_sekolah_asal" => $npsn_sekolah_asal,
							"alamat_sekolah_asal" => $alamat_sekolah_asal,
							"alasan_pindah" =>  $alasan_pindah,
							"diagnosa" =>  $diagnosa,
						);

		$siswa = array_merge($siswa, $complete);

		$rsiswa = $this->Msiswa->update($id, $siswa);

		$saudara1 = array( 	"nama" =>  $s_nama1,
							"umur" => $s_umur1,
							"sekolah" => $s_sekolah1,
							"kelas" => $s_kelas1
						);
		$saudara2 = array( 	"nama" =>  $s_nama2,
							"umur" => $s_umur2,
							"sekolah" => $s_sekolah2,
							"kelas" => $s_kelas2
						);
		$saudara3 = array( 	"nama" =>  $s_nama3,
							"umur" => $s_umur3,
							"sekolah" => $s_sekolah3,
							"kelas" => $s_kelas3
						);
		$saudara4 = array( 	"nama" =>  $s_nama4,
							"umur" => $s_umur4,
							"sekolah" => $s_sekolah4,
							"kelas" => $s_kelas4
						);
		$saudara5 = array( 	"nama" =>  $s_nama5,
							"umur" => $s_umur5,
							"sekolah" => $s_sekolah5,
							"kelas" => $s_kelas5
						);

		$rsiswa1 = $this->Msiswa->updateSaudara($s_id1, $saudara1);
		$rsiswa2 = $this->Msiswa->updateSaudara($s_id2, $saudara2);
		$rsiswa3 = $this->Msiswa->updateSaudara($s_id3, $saudara3);
		$rsiswa4 = $this->Msiswa->updateSaudara($s_id4, $saudara4);
		$rsiswa5 = $this->Msiswa->updateSaudara($s_id5, $saudara5);

		$this->simpan_terapi();

		if($rsiswa5) {
			$notif = "Data Siswa Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Data Siswa Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);

		//redirect(base_url("siswa/dataSiswa/index/$id?notif_$tipe=1&notification=$notif"));
		redirect(base_url("siswa/dataSiswa/index/$id"));
	}

	public function simpan_terapi() {
		$t_id1 = $this->input->post('t_id1');
		$t_jenis1 = $this->input->post('t_jenis1');
		$t_periode1 = $this->input->post('t_periode1');
		$t_status1 = $this->input->post('t_status1');
		$t_lembaga1 = $this->input->post('t_lembaga1');

		$t_id2 = $this->input->post('t_id2');
		$t_jenis2 = $this->input->post('t_jenis2');
		$t_periode2 = $this->input->post('t_periode2');
		$t_status2 = $this->input->post('t_status2');
		$t_lembaga2 = $this->input->post('t_lembaga2');

		$t_id3 = $this->input->post('t_id3');
		$t_jenis3 = $this->input->post('t_jenis3');
		$t_periode3 = $this->input->post('t_periode3');
		$t_status3 = $this->input->post('t_status3');
		$t_lembaga3 = $this->input->post('t_lembaga3');

		$terapi1 = array( 	"jenis" =>  $t_jenis1,
							"periode" => $t_periode1,
							"status" => $t_status1,
							"lembaga" => $t_lembaga1
						);
		$terapi2 = array( 	"jenis" =>  $t_jenis2,
							"periode" => $t_periode2,
							"status" => $t_status2,
							"lembaga" => $t_lembaga2
						);
		$terapi3 = array( 	"jenis" =>  $t_jenis3,
							"periode" => $t_periode3,
							"status" => $t_status3,
							"lembaga" => $t_lembaga3
						);

		$rsiswa1 = $this->Msiswa->update_terapi($t_id1, $terapi1);
		$rsiswa2 = $this->Msiswa->update_terapi($t_id2, $terapi2);
		$rsiswa3 = $this->Msiswa->update_terapi($t_id3, $terapi3);
		
	}

	public function simpanKbtk()
	{
		$id = $this->input->post('id');
		$nik = $this->input->post('nik');
		$nama_siswa = $this->input->post('nama_siswa');
		$panggilan = $this->input->post('panggilan');
		$alamat = $this->input->post('alamat');
		$kd_pos = $this->input->post('kd_pos');
		$telp_rumah = $this->input->post('telp_rumah');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$tanggal_lahir = ($tanggal_lahir) ? $tanggal_lahir : '0000-00-00';
		$kelamin = $this->input->post('kelamin');
		$gol_darah = $this->input->post('gol_darah');
		$anak_ke = $this->input->post('anak_ke');
		$saudara = $this->input->post('saudara');
		$agama = $this->input->post('agama');
		$warga_negara = $this->input->post('warga_negara');
		$suku = $this->input->post('suku');
		$bahasa = $this->input->post('bahasa');
		$bakat = $this->input->post('bakat');
		$hobby = $this->input->post('hobby');
		$khusus = $this->input->post('khusus');
		$cita_cita = $this->input->post('cita_cita');
		$diagnosa = $this->input->post('diagnosa');

		$foto_siswa = $this->input->post('foto_siswa');
		if(!empty($_FILES['foto']['name'])) {
			$foto_siswa = $this->upload_foto('foto', $id);
		}

		$akta_siswa = $this->input->post('akta_lahir_siswa');
		if(!empty($_FILES['akta_lahir']['name'])) {
			$akta_siswa = $this->upload_akta('akta_lahir', $id);
		}

		//if($nik && $nama_siswa && $panggilan && $alamat && $kd_pos && $telp_rumah && $tempat_lahir && $tanggal_lahir && $kelamin && $gol_darah && $anak_ke && $saudara && $agama && $warga_negara && $suku && $bahasa && $bakat && $hobby && $khusus && $foto){
		if($nik && $nama_siswa && $panggilan && $alamat && $kd_pos && $telp_rumah && $tempat_lahir && $tanggal_lahir && $kelamin && $gol_darah && $anak_ke && $saudara && $agama && $warga_negara && $suku && $bahasa && $bakat && $hobby && $khusus && $cita_cita){
			$complete = array("complete" => '1');
		}else{
			$complete = array("complete" => '0');
		}

		$s_id1 = $this->input->post('s_id1');
		$s_nama1 = $this->input->post('s_nama1');
		$s_umur1 = $this->input->post('s_umur1');
		$s_sekolah1 = $this->input->post('s_sekolah1');
		$s_kelas1 = $this->input->post('s_kelas1');

		$s_id2 = $this->input->post('s_id2');
		$s_nama2 = $this->input->post('s_nama2');
		$s_umur2 = $this->input->post('s_umur2');
		$s_sekolah2 = $this->input->post('s_sekolah2');
		$s_kelas2 = $this->input->post('s_kelas2');

		$s_id3 = $this->input->post('s_id3');
		$s_nama3 = $this->input->post('s_nama3');
		$s_umur3 = $this->input->post('s_umur3');
		$s_sekolah3 = $this->input->post('s_sekolah3');
		$s_kelas3 = $this->input->post('s_kelas3');

		$s_id4 = $this->input->post('s_id4');
		$s_nama4 = $this->input->post('s_nama4');
		$s_umur4 = $this->input->post('s_umur4');
		$s_sekolah4 = $this->input->post('s_sekolah4');
		$s_kelas4 = $this->input->post('s_kelas4');

		$s_id5 = $this->input->post('s_id5');
		$s_nama5 = $this->input->post('s_nama5');
		$s_umur5 = $this->input->post('s_umur5');
		$s_sekolah5 = $this->input->post('s_sekolah5');
		$s_kelas5 = $this->input->post('s_kelas5');

		$siswa = array( 	"nik" =>  $nik,
							"nama_siswa" => $nama_siswa,
							"panggilan" => $panggilan,
							"alamat" => $alamat,
							"kd_pos" => $kd_pos,
							"telp_rumah" => $telp_rumah,
							"tempat_lahir" => $tempat_lahir,
							"tanggal_lahir" => $tanggal_lahir,
							"kelamin" => $kelamin,
							"gol_darah" => $gol_darah,
							"anak_ke" => $anak_ke,
							"saudara" => $saudara,
							"agama" => $agama,
							"warga_negara" => $warga_negara,
							"suku" => $suku,
							"bahasa" => $bahasa,
							"bakat" => $bakat,
							"hobby" => $hobby,
							"khusus" => $khusus,
							"foto" => $foto_siswa,
							"cita_cita" => $cita_cita,
							"nisn" =>  $nisn,
							"asal_sekolah" => $asal_sekolah,
							"npsn_sekolah_asal" => $npsn_sekolah_asal,
							"alamat_sekolah_asal" => $alamat_sekolah_asal,
							"alasan_pindah" =>  $alasan_pindah,
							"diagnosa" =>  $diagnosa,
							"akta_lahir" => $akta_siswa,
						);

		$siswa = array_merge($siswa, $complete);

		$rsiswa = $this->Msiswa->update($id, $siswa);

		$saudara1 = array( 	"nama" =>  $s_nama1,
							"umur" => $s_umur1,
							"sekolah" => $s_sekolah1,
							"kelas" => $s_kelas1
						);
		$saudara2 = array( 	"nama" =>  $s_nama2,
							"umur" => $s_umur2,
							"sekolah" => $s_sekolah2,
							"kelas" => $s_kelas2
						);
		$saudara3 = array( 	"nama" =>  $s_nama3,
							"umur" => $s_umur3,
							"sekolah" => $s_sekolah3,
							"kelas" => $s_kelas3
						);
		$saudara4 = array( 	"nama" =>  $s_nama4,
							"umur" => $s_umur4,
							"sekolah" => $s_sekolah4,
							"kelas" => $s_kelas4
						);
		$saudara5 = array( 	"nama" =>  $s_nama5,
							"umur" => $s_umur5,
							"sekolah" => $s_sekolah5,
							"kelas" => $s_kelas5
						);

		$rsiswa1 = $this->Msiswa->updateSaudara($s_id1, $saudara1);
		$rsiswa2 = $this->Msiswa->updateSaudara($s_id2, $saudara2);
		$rsiswa3 = $this->Msiswa->updateSaudara($s_id3, $saudara3);
		$rsiswa4 = $this->Msiswa->updateSaudara($s_id4, $saudara4);
		$rsiswa5 = $this->Msiswa->updateSaudara($s_id5, $saudara5);

		if($rsiswa5) {
			$notif = "Data Siswa Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Data Siswa Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);

		redirect(base_url("siswa/dataSiswa/index/$id"));
	}

	public function simpanBless()
	{
		$id = $this->input->post('id');
		$nik = $this->input->post('nik');
		$nama_siswa = $this->input->post('nama_siswa');
		$panggilan = $this->input->post('panggilan');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$tanggal_lahir = ($tanggal_lahir) ? $tanggal_lahir : '0000-00-00';
		$alamat = $this->input->post('alamat');
		$hp = $this->input->post('hp');
		$email = $this->input->post('email');
		$asal_sekolah = $this->input->post('asal_sekolah');
		$nisn = $this->input->post('nisn');
		$kelamin = $this->input->post('kelamin');
		$gol_darah = $this->input->post('gol_darah');
		$anak_ke = $this->input->post('anak_ke');
		$saudara = $this->input->post('saudara');
		$agama = $this->input->post('agama');
		$warga_negara = $this->input->post('warga_negara');
		$suku = $this->input->post('suku');
		$npsn_sekolah_asal = $this->input->post('npsn_sekolah_asal');
		$alamat_sekolah_asal = $this->input->post('alamat_sekolah_asal');
		$alasan_pindah = $this->input->post('alasan_pindah');

		$foto_siswa = $this->input->post('foto_siswa');
		if(!empty($_FILES['foto']['name'])) {
			$foto_siswa = $this->upload_foto('foto', $id);
		}

		$akta_siswa = $this->input->post('akta_lahir_siswa');
		if(!empty($_FILES['akta_lahir']['name'])) {
			$akta_siswa = $this->upload_akta('akta_lahir', $id);
		}

		if($foto) {
			$foto_siswa = $foto;
		}

		//if($nik && $nama_siswa && $panggilan && $tempat_lahir && $tanggal_lahir && $alamat && $hp && $email && $asal_sekolah && $nisn && $kelamin && $gol_darah && $anak_ke && $saudara && $agama && $warga_negara && $suku && $foto){
		if($nik && $nama_siswa && $panggilan && $tempat_lahir && $tanggal_lahir && $alamat && $hp && $email && $asal_sekolah && $nisn && $kelamin && $gol_darah && $anak_ke && $saudara && $agama && $warga_negara && $suku && $npsn_sekolah_asal && $alamat_sekolah_asal){
			$complete = array("complete" => '1');
		}else{
			$complete = array("complete" => '0');
		}

		$siswa = array( 	"nik" =>  $nik,
							"nama_siswa" => $nama_siswa,
							"panggilan" => $panggilan,
							"tempat_lahir" => $tempat_lahir,
							"tanggal_lahir" => $tanggal_lahir,
							"alamat" => $alamat,
							"hp" => $hp,
							"email" => $email,
							"asal_sekolah" => $asal_sekolah,
							"nisn" => $nisn,
							"kelamin" => $kelamin,
							"gol_darah" => $gol_darah,
							"anak_ke" => $anak_ke,
							"saudara" => $saudara,
							"agama" => $agama,
							"warga_negara" => $warga_negara,
							"suku" => $suku,
							"foto" => $foto_siswa,
							"npsn_sekolah_asal" => $npsn_sekolah_asal,
							"alamat_sekolah_asal" => $alamat_sekolah_asal,
							"alasan_pindah" => $alasan_pindah,
							"akta_lahir" => $akta_siswa,
						);

		$siswa = array_merge($siswa, $complete);

		$rsiswa = $this->Msiswa->update($id, $siswa);
		if($rsiswa) {
			$notif = "Data Siswa Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Data Siswa Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);

		redirect(base_url("siswa/dataSiswa/index/$id"));
	}

	function upload_foto($file, $id){
        $config['upload_path']          = './uploads/siswa/';
        $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
        $config['max_size']             = '99999999'; // imb

		$this->upload->initialize($config);
        // proses upload

		$time = date("dmyhis");
		$nama = substr($_FILES[$file]['name'], 0, strrpos($_FILES[$file]['name'], "."));
		$nama_new = $id.'_'.$time.'_'.$file;
		$ext = str_replace($nama, '', $_FILES[$file]['name']);
		$_FILES[$file]['name'] = $nama_new . '.' . $ext;

        $this->upload->do_upload($file);
        $upload = $this->upload->data();

		$config['source_image'] = './uploads/siswa/'.$upload['file_name'];
		$config['new_image'] = './uploads/siswa/thumb/'.$upload['file_name'];;
		//$config['create_thumb'] = TRUE;
		//$config['thumb_marker'] = '_thumb';
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 300;
		//$config['height']       = 40;

		$this->image_lib->initialize($config);
		$this->image_lib->resize();

        return $upload['file_name'];
    }

	function upload_akta($file, $id){
        $config['upload_path']          = './uploads/akta/';
        $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
        $config['max_size']             = '99999999'; // imb

		$this->upload->initialize($config);
        // proses upload

		$time = date("dmyhis");
		$nama = substr($_FILES[$file]['name'], 0, strrpos($_FILES[$file]['name'], "."));
		$nama_new = $id.'_'.$time.'_'.$file;
		$ext = str_replace($nama, '', $_FILES[$file]['name']);
		$_FILES[$file]['name'] = $nama_new . '.' . $ext;

        $this->upload->do_upload($file);
        $upload = $this->upload->data();

		$config['source_image'] = './uploads/akta/'.$upload['file_name'];
		$config['new_image'] = './uploads/akta/thumb/'.$upload['file_name'];;
		//$config['create_thumb'] = TRUE;
		//$config['thumb_marker'] = '_thumb';
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 300;
		//$config['height']       = 40;

		$this->image_lib->initialize($config);
		$this->image_lib->resize();

        return $upload['file_name'];
    }

}