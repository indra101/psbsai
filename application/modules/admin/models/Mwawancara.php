<?php

class Mwawancara extends CI_Model
{
    public $table = 'jadwal_interview';
    protected $primary = 'id';
	
    public function find($id)
    {
        return $this->db->where('id', $id)->get($this->table)->row_array();
    }

    public function create($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function create_multi($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getList($id_user = '') 
    {
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);

        if($id_user) {
            $this->db->where('interviewer1', $id_user);
            $this->db->or_where('interviewer2', $id_user);
            $this->db->or_where('interviewer3', $id_user);
        }
        $this->db->where('aktif', 1);
        $query = $this->db->get($this->table);

        // untuk lihat query produced
        // $q = $this->db->last_query();
        // echo $q;
        // die;

        $q = $this->db->query('SELECT FOUND_ROWS() AS `Count`');
        $jml = $q->row()->Count;

        $data = $query->result_array();

        $result = new stdClass(); 
        $result->data = $data;
        $result->jml = $jml;
        
        return $result;
    }

    public function getIdInterviewee($id)
    {
        $this->db->select('id_ortu', false);
        $this->db->where("interviewer1", $id);
        $this->db->or_where("interviewer2", $id);
        $this->db->or_where("interviewer3", $id);
        $query = $this->db->get($this->table);

        if($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                $data[] = $row->id_ortu;
            }
            return $data;
        } else {
            return array();
        }
    }

}
