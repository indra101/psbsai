<?php

class Mlevel extends CI_Model
{
    public $table = 'level';
    protected $primary = 'id';
	
    public function find($id)
    {
        return $this->db->where('id', $id)->get($this->table)->row_array();
    }

    public function create($data)
    {
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getData(){
        $this->db->select('*', false);
        $this->db->order_by('id', 'asc');
        $query = $this->db->get($this->table);
        $result = $query->result_array();
        return $result;
    }

    public function getLevelOption()
    {
        $this->db->where("aktif", 1);
        $this->db->order_by("id", "asc");
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                $data[$row->kode] = $row->deskripsi;
            }
            return $data;
        }
    }

    public function getLevelOptionAll()
    {
        $this->db->order_by("id", "asc");
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                $data[$row->kode] = $row->deskripsi;
            }
            return $data;
        }
    }

    public function getByKode($kode)
    {
        $this->db->where("kode", $kode);

        return $this->db->get($this->table)->row_array();
    }

}
