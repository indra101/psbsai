<?php

class Mortu_daftar extends CI_Model
{
    public $table = 'ortu_daftar';
    protected $primary = 'id';
	
    public function find($id)
    {
        return $this->db->where('id', $id)->get($this->table)->row_array();
    }

    public function findByEmail($email)
    {
        return $this->db->where('email', $email)
                        ->where('deleted_flag is null')
                        ->get($this->table)->row_array();
    }

    public function findByNoDaftar($no_daftar)
    {
        return $this->db->where('no_daftar', $no_daftar)
                        ->where('deleted_flag is null')
                        ->get($this->table)->row_array();
    }

    public function findByEmailList($email)
    {
        return $this->db->where('email', $email)
                        ->where('deleted_flag is null')
                        ->get($this->table)->result_array();
    }

    public function findAnak($id)
    {
        $where = array("id_ortu_daftar" => $id);
        return $this->db->where($where)->get('siswa_daftar')->result_array();
    }

    public function create($data)
    {
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();
    }

    public function daftar($ortu, $siswa)
    {
        // use transaction
        $this->db->trans_start();
        
        $id_ortu = $this->create($ortu);

        $n = 0;
        foreach($siswa as $s) {
            $s['id_ortu_daftar'] = $id_ortu;
            $this->db->insert('siswa_daftar', $s);
            //$id_siswa = $this->db->insert_id();
            $n++;
        }
        //$this->db->insert_batch('siswa', $siswa);
        $this->db->trans_complete();
        // transaction complete

        //return $this->db->trans_status();
        if($this->db->trans_status())
            $res = $id_ortu;
        else
            $res = false;

        return $res;
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function approve($id)
    {
        $data = array("verified" => 1);
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function hapus($id)
    {
        $data = array("deleted_flag" => 1);
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function getNamaById($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get($this->table);
        return $query->row()->nama_ortu;
    }

    public function getData(){
        $this->db->select('SQL_CALC_FOUND_ROWS 
                            *', false);
        $this->db->where('nama <> ""');
        $this->db->where('deleted_flag is null');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get($this->table);
        $data = $query->result_array();

        $result = new stdClass(); 
        $result->data = $data;
        return $result;
    }
    
    

}
