<?php

class Muser extends CI_Model
{
    public $table = 'user_admin';
    protected $primary = 'id';
	
    public function find($id)
    {
        $this->db->select('user_admin.*, role.nama_role');
        $this->db->where('user_admin.id', $id);
        $this->db->where('user_admin.is_deleted is null');
        $this->db->join('role', 'role.id = user_admin.id_role', 'left');
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function create($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function create_multi($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getNamaById($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get($this->table);
        return $query->row()->nama;
    }

    public function cekUsername($username)
    {
        $this->db->select('count(id) as cnt');
        $this->db->where("username", $username);
        $query = $this->db->get($this->table);

        if($query->row()->cnt) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getList($start, $length, $order, $dir, $search = '') 
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 
                            user_admin.*,
                            role.nama_role', false);
        $this->db->like('user_admin.nama', $search);
        $this->db->or_like('role.nama_role', $search);
        $this->db->where('user_admin.is_deleted is null');
        $this->db->join('role', 'role.id = user_admin.id_role', 'left');
        $this->db->order_by($order, $dir);
        $this->db->limit($length, $start);
        $query = $this->db->get($this->table);

        // untuk lihat query produced
        // $q = $this->db->last_query();
        // echo $q;
        // die;

        $q = $this->db->query('SELECT FOUND_ROWS() AS `Count`');
        $jml = $q->row()->Count;

        $data = $query->result_array();

        $result = new stdClass(); 
        $result->data = $data;
        $result->jml = $jml;
        
        return $result;
    }

    public function getData(){

        $this->db->select('SQL_CALC_FOUND_ROWS 
                            user_admin.*,
                            role.nama_role', false);
        $this->db->where('user_admin.is_deleted is null');
        $this->db->join('role', 'role.id = user_admin.id_role', 'left');
        $query = $this->db->get($this->table);

        // untuk lihat query produced
        // $q = $this->db->last_query();
        // echo $q;
        // die;

        $data = $query->result_array();

        $result = new stdClass(); 
        $result->data = $data;
        return $result;
    }

    public function userWawancara() {
        $this->db->select(' user_admin.id, 
                            user_admin.nama,
                            role.nama_role', false);
        //$this->db->where('role.id = 3 or role.id = 4');
        $this->db->where('user_admin.is_deleted is null');
        $this->db->join('role', 'role.id = user_admin.id_role', 'left');
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                $data[$row->id] = $row->nama;
            }
            return $data;
        } else {
            return array();
        }
	}

}
