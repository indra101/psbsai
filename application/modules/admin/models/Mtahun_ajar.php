<?php

class Mtahun_ajar extends CI_Model
{
    public $table = 'tahun_ajar';
    protected $primary = 'id';

    public function create($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getData(){
        $this->db->select('*', false);
        //$this->db->order_by('nama', 'asc');
        $query = $this->db->get($this->table);
        $result = $query->result_array();
        return $result;
    }

    public function getOption()
    {
        // $this->db->where("aktif", 1);
        $this->db->order_by("id", "asc");
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                $data[$row->nama] = $row->nama;
            }
            return $data;
        }
    }

}
