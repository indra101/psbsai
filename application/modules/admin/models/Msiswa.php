<?php

class Msiswa extends CI_Model
{
    public $table = 'siswa';
    protected $primary = 'id';
	
    public function find($id)
    {
        $this->db->select('siswa.*, level.deskripsi as nama_level, ortu.nkk as nokk');
        $this->db->where('siswa.id', $id);
        $this->db->join('level', 'level.kode = siswa.level', 'left');
        $this->db->join('ortu', 'ortu.id = id_ortu', 'left');
        $query = $this->db->get($this->table);
        return $query->row_array();

        $this->db->select('siswa.*,ortu.nkk as nokk', false);
        $this->db->where('siswa.id', $id);
        $this->db->join('ortu', 'ortu.id = id_ortu', 'left');
        $query = $this->db->get($this->table);

        // untuk lihat query produced
        // $q = $this->db->last_query();
        // echo $q;
        // die;

        $result = $query->row_array();

        return $result;
    }

    public function create($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function create_multi($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getNamaById($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get($this->table);
        return $query->row()->nama_siswa;
    }

    public function getByOrtu($id_ortu, $baru = 0)
    {
        $this->db->select('siswa.*, level.deskripsi as nama_level');
        $this->db->where("id_ortu", $id_ortu);
        if($baru) {
            $this->db->where('siswa.is_daftar', 1);
        }
        $this->db->join('level', 'level.kode = siswa.level', 'left');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function getSaudara($id_siswa)
    {
        $query = "select s.id,s.nama,s.umur,s.sekolah,s.kelas from saudara s where s.id_siswa=$id_siswa";
    
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function getList($start, $length, $order, $dir, $search = '') 
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 
                            siswa.id,
                            siswa.nama_siswa,
                            siswa.kelamin,
                            siswa.tempat_lahir,
                            siswa.tanggal_lahir,
                            siswa.alamat,
                            siswa.panggilan,
                            siswa.nik,
                            siswa.tahun_ajar,
                            siswa.nisn,
                            ortu.id as id_ortu,
                            ortu.nama_ortu,
                            ortu.nik_ortu,
                            ortu.tempat_lahir as tempat_lahir_ortu,
                            ortu.tgl_lahir as tgl_lahir_ortu,
                            ortu.pekerjaan,
                            ortu.nkk,
                            ortu.hp as hp_ortu,
                            ortu.status as status_ortu,
                            level.deskripsi as level', false);
        $this->db->like('siswa.nama_siswa', $search);
        $this->db->or_like('siswa.alamat', $search);
        $this->db->or_like('ortu.nama_ortu', $search);
        $this->db->join('ortu', 'ortu.id = id_ortu', 'left');
        $this->db->join('level', 'level.kode = siswa.level', 'left');
        $this->db->order_by($order, $dir);
        $this->db->limit($length, $start);
        $query = $this->db->get($this->table);

        // untuk lihat query produced
        // $q = $this->db->last_query();
        // echo $q;
        // die;

        $q = $this->db->query('SELECT FOUND_ROWS() AS `Count`');
        $jml = $q->row()->Count;

        $data = $query->result_array();

        $result = new stdClass(); 
        $result->data = $data;
        $result->jml = $jml;
        
        return $result;
    }

    public function getData($baru = 0, $lvl = ''){
        $this->db->select('SQL_CALC_FOUND_ROWS 
                            siswa.id,
                            siswa.nama_siswa,
                            siswa.kelamin,
                            siswa.tempat_lahir,
                            siswa.tanggal_lahir,
                            siswa.alamat,
                            siswa.panggilan,
                            siswa.asal_sekolah,
                            siswa.alamat_sekolah_asal,
                            siswa.npsn_sekolah_asal,
                            siswa.nik,
                            siswa.nisn,
                            siswa.cita_cita,
                            siswa.alasan_pindah,
                            siswa.diagnosa,
                            siswa.akta_lahir,
                            siswa.before_2024,
                            ortu.id as id_ortu,
                            ortu.nama_ortu,
                            ortu.nik_ortu,
                            ortu.tempat_lahir as tempat_lahir_ortu,
                            ortu.tgl_lahir as tgl_lahir_ortu,
                            ortu.pekerjaan,
                            ortu.nkk,
                            ortu.hp as hp_ortu,
                            ortu.status as status_ortu,
                            level.deskripsi as level', false);
        $this->db->join('ortu', 'ortu.id = id_ortu', 'left');
        $this->db->join('level', 'level.kode = siswa.level', 'left');
        if($baru) {
            $this->db->where('siswa.is_daftar', 1);
        }
        if($lvl) {
            $this->db->where('siswa.level', $lvl);
        }
        $query = $this->db->get($this->table);
        $result = $query->result_array();
        return $result;
    }

    public function getRiwayat($start, $length, $order, $dir, $tahun_ajar, $level) 
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 
                            siswa.id,
                            siswa.nama_siswa,
                            siswa.tempat_lahir,
                            siswa.tanggal_lahir,
                            siswa.tahun_ajar,
                            ortu.id as id_ortu,
                            ortu.nama_ortu,
                            level.deskripsi as level', false);
        if($tahun_ajar)
            $this->db->where('siswa.tahun_ajar', $tahun_ajar);
        if($level)
            $this->db->where('level.kode', $level);
        $this->db->join('ortu', 'ortu.id = id_ortu', 'left');
        $this->db->join('level', 'level.kode = siswa.level', 'left');
        $this->db->order_by($order, $dir);
        $this->db->limit($length, $start);
        $query = $this->db->get($this->table);

        // untuk lihat query produced
        // $q = $this->db->last_query();
        // echo $q;
        // die;

        $q = $this->db->query('SELECT FOUND_ROWS() AS `Count`');
        $jml = $q->row()->Count;

        $data = $query->result_array();

        $result = new stdClass(); 
        $result->data = $data;
        $result->jml = $jml;
        
        return $result;
    }

    public function getTotal($baru = 0)
    {
        $this->db->select('count(id) as cnt');
        if($baru) {
            $this->db->where('is_daftar', 1);
        }
        $query = $this->db->get($this->table);
        return $query->row()->cnt;
    }

    public function getJmlPerLevel()
    {
        $this->db->select('level, count(id) as jml');
        $this->db->where('is_daftar', 1);
        $this->db->group_by('level');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }


    public function addSiswa($id_ortu, $siswa)
    {
        // use transaction
        $this->db->trans_start();

        $siswa['id_ortu'] = $id_ortu;
        $this->db->insert('siswa', $siswa);
        $id_siswa = $this->db->insert_id();

        $this->db->insert('kelahiran', array("id_siswa" => $id_siswa));
        $this->db->insert('keluarga', array("id_siswa" => $id_siswa));
        $this->db->insert('kesehatan', array("id_siswa" => $id_siswa));
        $this->db->insert('perkembangan', array("id_siswa" => $id_siswa));
        $this->db->insert('pindahan', array("id_siswa" => $id_siswa));
        $this->db->insert('pertanyaan', array("id_siswa" => $id_siswa));
        for($i = 0; $i < 5; $i++) {
            $this->db->insert('saudara', array("id_siswa" => $id_siswa));
        }
        for($i = 0; $i < 5; $i++) {
            $this->db->insert('sub_kesehatan', array("id_siswa" => $id_siswa));
        }
        for($i = 0; $i < 5; $i++) {
            $this->db->insert('sub_pindahan', array("id_siswa" => $id_siswa));
        }
        //$this->db->insert_batch('siswa', $siswa);
        $this->db->trans_complete();
        // transaction complete

        return $this->db->trans_status();
    }

    public function get_terapi($id_siswa)
    {
        $query = "select t.id,t.jenis,t.periode,t.status,t.lembaga from terapi t where t.id_siswa=$id_siswa";
    
        $query = $this->db->query($query);
        return $query->result_array();
    }

}
