<?php

class Mortu extends CI_Model
{
    public $table = 'ortu';
    protected $primary = 'id';
	
    public function find($id)
    {
        return $this->db->where('id', $id)->get($this->table)->row_array();
    }

    public function findAyah($id, $is_kepala, $status)
    {
        if($is_kepala && $status == 'Ayah')
            $where = array("id" => $id);
        else
            $where = array("id_kepala" => $id,"status" => 'Ayah');
        return $this->db->where($where)->get($this->table)->row_array();
    }

    public function findIbu($id, $is_kepala, $status)
    {
        if($is_kepala && $status == 'Ibu')
            $where = array("id" => $id);
        else
            $where = array("id_kepala" => $id,"status" => 'Ibu');
        return $this->db->where($where)->get($this->table)->row_array();
    }

    public function findWali($id, $is_kepala, $status)
    {
        if($is_kepala && $status == 'Wali') {
            $where = array("id" => $id,"status" => 'Wali');
            return $this->db->where($where)->get($this->table)->row_array();
        } else {
            return null;
        }
    }

    public function findPasangan($id)
    {
        $where = array("id_kepala" => $id, "status_khusus" => "ada");
        return $this->db->where($where)->get($this->table)->row_array();
    }

    public function findAnak($id)
    {
        $where = array("id_ortu" => $id);
        return $this->db->where($where)->get('siswa')->result_array();
    }

    public function findKeluarga($id)
    {   
        return $this->db->where('id_ortu', $id)->get('keluarga')->row_array();
    }

    public function create($data)
    {
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();
    }

    public function daftar($ortu, $siswa, $id_daftar, $is_ortu_lama, $id_ortu_lama, $email = null, $hp = null, $kode_unik_enc = null)
    {
        // use transaction
        $this->db->trans_start();

        if($is_ortu_lama) {

            $id_ortu = $id_ortu_lama;

            // echo $id_ortu;
            // die;

            $data = array("id_ortu" => $id_ortu);
            $this->db->where('id', $id_daftar);
            $this->db->update('ortu_daftar', $data);

            $data_ortu = array(
                "email" => $email,
                "hp" => $hp,
                "password" => $kode_unik_enc,
                "is_daftar" => 1
            );
            $this->db->where('id', $id_ortu);
            $this->db->update('ortu', $data_ortu);

            // update data pasangan
            $data_pasangan = array(
                "is_daftar" => 1
            );
            $this->db->where('id_kepala', $id_ortu);
            $this->db->update('ortu', $data_pasangan);

        } else {
        
            $id_ortu = $this->create($ortu);

            if($ortu['status'] == 'Ayah') {
                $this->create(array("id_kepala" => $id_ortu, "status" => 'Ibu', "is_daftar" => 1, "created_by" => $ortu['created_by'])); // create Ibu
            } else if($ortu['status'] == 'Ibu') {
                $this->create(array("id_kepala" => $id_ortu, "status" => 'Ayah', "is_daftar" => 1, "created_by" => $ortu['created_by'])); // create Ayah
            }

        }
        
        $n = 0;
        foreach($siswa as $s) {
            $s['id_ortu'] = $id_ortu;
            $this->db->insert('siswa', $s);
            $id_siswa = $this->db->insert_id();

            $this->db->insert('kelahiran', array("id_siswa" => $id_siswa));
            $this->db->insert('keluarga', array("id_siswa" => $id_siswa));
            $this->db->insert('kesehatan', array("id_siswa" => $id_siswa));
            $this->db->insert('perkembangan', array("id_siswa" => $id_siswa));
            $this->db->insert('pindahan', array("id_siswa" => $id_siswa));
            $this->db->insert('pertanyaan', array("id_siswa" => $id_siswa));
            for($i = 0; $i < 5; $i++) {
                $this->db->insert('saudara', array("id_siswa" => $id_siswa));
            }
            for($i = 0; $i < 5; $i++) {
                $this->db->insert('sub_kesehatan', array("id_siswa" => $id_siswa));
            }
            for($i = 0; $i < 5; $i++) {
                $this->db->insert('sub_pindahan', array("id_siswa" => $id_siswa));
            }
            for($i = 0; $i < 3; $i++) {
                $this->db->insert('terapi', array("id_siswa" => $id_siswa));
            }
            
            $n++;
        }

        // update status ortu daftar
        $data = array("verified" => 1);
        $this->db->where('id', $id_daftar);
        $this->db->update('ortu_daftar', $data);

        //$this->db->insert_batch('siswa', $siswa);
        $this->db->trans_complete();
        // transaction complete

        return $this->db->trans_status();
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getNamaById($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get($this->table);
        return $query->row()->nama_ortu;
    }

    public function getList($start, $length, $order, $dir, $search = '') 
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 
                            id,
                            is_kepala,
                            id_kepala,
                            password,
                            nama_ortu,
                            status,
                            tempat_lahir,
                            tgl_lahir,
                            alamat_rumah,
                            hp,
                            email', false);
        $this->db->where('nama_ortu <> ""');
        $this->db->like('nama_ortu', $search);
        $this->db->or_like('alamat_rumah', $search);
        $this->db->order_by($order, $dir);
        $this->db->limit($length, $start);
        $query = $this->db->get($this->table);

        // untuk lihat query produced
        // $q = $this->db->last_query();
        // echo $q;
        // die;

        $q = $this->db->query('SELECT FOUND_ROWS() AS `Count`');
        $jml = $q->row()->Count;

        $data = $query->result_array();

        $result = new stdClass(); 
        $result->data = $data;
        $result->jml = $jml;
        
        return $result;
    }

    public function getData($baru = 0, $is_kepala = false){
        $this->db->select('SQL_CALC_FOUND_ROWS 
                            id,
                            is_kepala,
                            id_kepala,
                            password,
                            nama_ortu,
                            status,
                            tempat_lahir,
                            tgl_lahir,
                            alamat_rumah,
                            hp,
                            complete,
                            email,
                            nik_ortu,
                            nkk,
                            suku,
                            agama,
                            pendidikan,
                            almamater,
                            pekerjaan,
                            jabatan,
                            instansi,
                            alamat_pekerjaan,
                            telp_kantor,
                            range_gaji,
                            jml_jam_kerja,
                            kelamin,
                            waktu_keluarga', false);
        $this->db->where('nama_ortu <> ""');
        if($baru) {
            $this->db->where('is_daftar', 1);
        }
        if($is_kepala) {
            $this->db->where('is_kepala', 1);
        }
        $query = $this->db->get($this->table);
        $data = $query->result_array();

        $result = new stdClass(); 
        $result->data = $data;
        return $result;
    }

    public function findByEmail($email)
    {
        return $this->db->where('email', $email)
                        ->where('is_daftar is null')
                        ->get($this->table)->row_array();
    }

    public function findKepalaByEmail($email)
    {
        return $this->db->where('email', $email)
                        ->where('is_kepala = 1')
                        ->get($this->table)->row_array();
    }
    
    public function getTotalKepala($baru = 0)
    {
        $this->db->select('count(id) as cnt');
        $this->db->where("is_kepala", 1);
        $this->db->where('nama_ortu <> ""');
        $this->db->where('nama_ortu is not null');
        if($baru) {
            $this->db->where('is_daftar', 1);
        }
        $query = $this->db->get($this->table);
        return $query->row()->cnt;
    }

    public function listKepala($baru = 0)
    {
        $this->db->select('id, nama_ortu');
        $this->db->where("is_kepala", 1);
        $this->db->where('nama_ortu <> ""');
        $this->db->where('nama_ortu is not null');
        if($baru) {
            $this->db->where("is_daftar", 1);
        }
        $query = $this->db->get($this->table);
        
        $data = array();
        foreach($query->result() as $row)
        {
            $data[$row->id] = $row->nama_ortu;
        }
        return $data;

    }

    function getStatusCompleteOrtu($id_ortu){
        $this->db->select('complete, is_kepala, id_kepala');
        $this->db->where("id", $id_ortu);
        $ortu = $this->db->get($this->table)->row_array();
        //$ortu = $this->db->query('Select complete, is_kepala, id_kepala from ortu where id="'.$id_ortu.'"')->row_array();
    
        if($ortu['is_kepala']) {
            $this->db->select('complete');
            $this->db->where("id_kepala", $id_ortu);
            $pasangan = $this->db->get($this->table)->row_array();
            //$pasangan = $ci->db->query('Select complete from ortu where id_kepala="'.$id_ortu.'"')->row_array();
        } else {
            $this->db->select('complete');
            $this->db->where("id", $ortu['id_kepala']);
            $pasangan = $this->db->get($this->table)->row_array();
            //$pasangan = $ci->db->query('Select complete from ortu where id="'.$ortu['id_kepala'].'"')->row_array();
        }
    
        if($ortu['complete'] == 1 && $pasangan['complete'] == 1){
            return 1;
        }else{
            return 0;
        } 
    }

    // function getStatusCompleteAllSiswa($id_ortu){
    //     $res = 1;
    
    //     $this->db->select('id, level');
    //     $this->db->where('id', $id_ortu);
    //     $this->db->where('is_daftar', 1);
    //     $siswas = $this->db->get($this->table)->result_array();
    //     //$siswas = $this->db->query('Select id, level from siswa where id_ortu="'.$id_ortu.'" and is_daftar = 1')->result_array();
    
    //     foreach($siswas as $s) {
    //         $complete = getStatusComplete($s['id'], $s['level']);
    //         $res = $res && $complete;
    //     }
    
    //     return $res;
    // }

}
