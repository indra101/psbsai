<?php

class Msiswa_daftar extends CI_Model
{
    public $table = 'siswa_daftar';
    protected $primary = 'id';
	
    public function find($id)
    {
        $this->db->select('siswa_daftar.*, level.deskripsi as nama_level');
        $this->db->where('siswa_daftar.id', $id);
        $this->db->join('level', 'level.kode = siswa_daftar.level', 'left');
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function create($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function create_multi($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

    public function update($id, $data)
    {
        $this->db->where($this->primary, $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        $where = array("id" => $id);
        $this->db->where($where);

        return $this->db->delete($this->table, $where);
    }

    public function getNamaById($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get($this->table);
        return $query->row()->nama_siswa;
    }

    public function getByOrtu($id_ortu)
    {
        $this->db->select('siswa_daftar.*, level.deskripsi as nama_level');
        $this->db->where("id_ortu_daftar", $id_ortu);
        $this->db->join('level', 'level.kode = siswa_daftar.level', 'left');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function getList($start, $length, $order, $dir, $search = '') 
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 
                            siswa.id,
                            siswa.nama_siswa,
                            siswa.kelamin,
                            siswa.tempat_lahir,
                            siswa.tanggal_lahir,
                            siswa.alamat,
                            ortu.nama_ortu,
                            level.deskripsi as level', false);
        $this->db->like('siswa.nama_siswa', $search);
        $this->db->or_like('siswa.alamat', $search);
        $this->db->or_like('ortu.nama_ortu', $search);
        $this->db->join('ortu', 'ortu.id = id_ortu', 'left');
        $this->db->join('level', 'level.kode = siswa.level', 'left');
        $this->db->order_by($order, $dir);
        $this->db->limit($length, $start);
        $query = $this->db->get($this->table);

        // untuk lihat query produced
        // $q = $this->db->last_query();
        // echo $q;
        // die;

        $q = $this->db->query('SELECT FOUND_ROWS() AS `Count`');
        $jml = $q->row()->Count;

        $data = $query->result_array();

        $result = new stdClass(); 
        $result->data = $data;
        $result->jml = $jml;
        
        return $result;
    }

    public function getData(){
        $this->db->select('SQL_CALC_FOUND_ROWS 
                            siswa.id,
                            siswa.nama_siswa,
                            siswa.kelamin,
                            siswa.tempat_lahir,
                            siswa.tanggal_lahir,
                            siswa.alamat,
                            ortu.nama_ortu,
                            level.deskripsi as level', false);
        $this->db->join('ortu', 'ortu.id = id_ortu', 'left');
        $this->db->join('level', 'level.kode = siswa.level', 'left');
        $query = $this->db->get($this->table);
        $result = $query->result_array();
        return $result;
    }

    public function getTotal()
    {
        $this->db->select('count(id) as cnt');
        $query = $this->db->get($this->table);
        return $query->row()->cnt;
    }

    public function getJmlPerLevel()
    {
        $this->db->select('level, count(id) as jml');
        $this->db->group_by('level');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }


    public function addSiswa($id_ortu, $siswa)
    {
        // use transaction
        $this->db->trans_start();

        $siswa['id_ortu'] = $id_ortu;
        $this->db->insert('siswa', $siswa);
        $id_siswa = $this->db->insert_id();

        $this->db->insert('kelahiran', array("id_siswa" => $id_siswa));
        $this->db->insert('keluarga', array("id_siswa" => $id_siswa));
        $this->db->insert('kesehatan', array("id_siswa" => $id_siswa));
        $this->db->insert('perkembangan', array("id_siswa" => $id_siswa));
        $this->db->insert('pindahan', array("id_siswa" => $id_siswa));
        $this->db->insert('pertanyaan', array("id_siswa" => $id_siswa));
        for($i = 0; $i < 5; $i++) {
            $this->db->insert('saudara', array("id_siswa" => $id_siswa));
        }
        for($i = 0; $i < 5; $i++) {
            $this->db->insert('sub_kesehatan', array("id_siswa" => $id_siswa));
        }
        for($i = 0; $i < 5; $i++) {
            $this->db->insert('sub_pindahan', array("id_siswa" => $id_siswa));
        }
        //$this->db->insert_batch('siswa', $siswa);
        $this->db->trans_complete();
        // transaction complete

        return $this->db->trans_status();
    }

}
