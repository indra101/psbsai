<div class="modal fade" id="modal_edit_ortu" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Edit Data Orang Tua</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
            <?php echo form_open('admin/pendaftaran/edit_ortu'); ?>
                
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <b>Edit Data Orang Tua</b>
                        <div class="input-group mt-4">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">face</i>
                            </span>
                            </div>
                            <input name="nama_edit" id="nama_edit" type="text" class="form-control" placeholder="Nama..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">wc</i>
                            </span>
                            </div>
                            <select class="form-control" name="kelamin" id="kelamin_edit" required>
                                <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                            <input name="kelamin_edit_txt" id="kelamin_edit_txt" type="text" class="form-control" style="display: none" readonly>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">email</i>
                            </span>
                            </div>
                            <input name="email" id="email_edit" type="email" class="form-control" placeholder="Email..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">phone_iphone</i>
                            </span>
                            </div>
                            <input name="hp" id="hp_edit" type="number" class="form-control" placeholder="Telepon..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">local_offer</i>
                            </span>
                            </div>
                            <select class="form-control" name="status" id="status_edit" required>
                                <option value="" disabled selected>Pilih Status</option>
                                <option value="Ayah">Ayah</option>
                                <option value="Ibu">Ibu</option>
                                <option value="Wali">Wali</option>
                            </select>
                            <input name="status_edit_txt" id="status_edit_txt" type="text" class="form-control" style="display: none" readonly>
                        </div>
                        
                    </div>
                    <div class="col-md-2"></div>

                    <?=form_hidden('id_edit', '')?>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block" onclick="return validate_edit()">Simpan<div class="ripple-container"></div></button>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<script>

var ok_edit = true;

function validate_edit() {
    var c = confirm('Apakah data sudah benar?');
    if(c) {
        return true;
    } else {
        return false;
    }
}

</script>