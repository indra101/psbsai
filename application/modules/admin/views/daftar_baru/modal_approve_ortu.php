<style>

.spin-aja {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
  display: inline-block;
}
@keyframes sp-anime {
  100% { 
    transform: rotate(360deg); 
  }
}

</style>

<div class="modal fade" id="modal_approve" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Konfirmasi Approval</h6></p>
            </div>            
            <div class="modal-body" style="">

              <!-- <div class="row">
                <div class="col-md-12">
                  <b>Data Orang Tua</b>
                  <div class="input-group mt-4">
                      <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">face</i>
                      </span>
                      </div>
                      <span id="nama_txt"></span>
                  </div>
                  <div class="input-group">
                      <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">wc</i>
                      </span>
                      </div>
                      <span id="kelamin_txt"></span>
                  </div>
                  <div class="input-group">
                      <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">email</i>
                      </span>
                      </div>
                      <span id="email_txt"></span>
                  </div>
                  <div class="input-group">
                      <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">phone_iphone</i>
                      </span>
                      </div>
                      <span id="hp_txt"></span>
                  </div>
                  <div class="input-group">
                      <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">local_offer</i>
                      </span>
                      </div>
                      <span id="status_txt"></span>
                  </div>
                </div>
              </div> -->
              <div id="konfirmasi_ortu" class="mb-4">
                <div class="row">
                  <div class="col-md-12">
                    Apakah Orang Tua Siswa pernah melakukan pendaftaran pada tahun ajaran sebelumnya?
                  </div>
                </div>

                <?php echo form_open('admin/pendaftaran/approve', array('id'=>'form-approve')); ?>

                <input type="hidden" id="id_ortu" name="id_ortu">

                <div class="row mt-2">
                  <div class="col-md-12">
                    <input type="radio" class="mr-2" id="is_ortu_lama_no" value="0" name="is_ortu_lama" checked>Tidak
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5">
                    <input type="radio" class="mr-2" id="is_ortu_lama_ya" value="1" name="is_ortu_lama"><span>Ya, pilih data OTS</span>
                  </div>
                  <div class="col-md-7">
                    <?=form_dropdown('id_ortu_lama', array('' => 'Pilih Orang Tua Siswa') + $ortu_lama, '', 'class="form-control" width="600px;" id="id_ortu_lama"'); ?>
                  </div>
                  <div class="col">
                    <span id="alert_ortu" style="color: #fd7e14;display: none;font-size:10pt;">Data OTS harus dipilih!</span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  Apakah yakin akan approve data pendaftaran?
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <a href="#" class="btn btn-danger" id="btnBatal" data-toggle="modal" data-target="#modal_approve">Batal</a>
              <a href="#"  class="btn btn-success" id="btnApprove" onclick="return validate()"><div class="spin-aja mr-2" id="spin" role="status" style="height: 17px; width: 17px; display: none;"></div>Simpan</a>
            </div>

            <?php echo form_close(); ?>

        </div>    
    </div>
</div>