<?php $role = $this->session->userdata('id_role'); ?>
<div class="content">
        <div class="container-fluid">    
          <div class="row">
            <div class="col-md-12">
            <?php $this->view('admin/notif'); ?>
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Pendaftaran Orang Tua dan Siswa Baru</h4>
                </div>
                <div class="card-body table-responsive">
                  <table id="tortu" class="table-striped table-hover">
                    <thead class="text-success">
                      <th style="min-width: 40px">No</th>
                      <th style="min-width: 150px">No Pendaftaran</th>
                      <th style="min-width: 150px">Nama</th>
                      <th style="min-width: 100px">Status</th>
                      
                      <th style="max-width: 150px">No. HP</th>
                      <th style="min-width: 200px">E-Mail</th>
                      <th style="min-width: 250px">Anak</th>
                      <th style="min-width: 150px">Kartu Keluarga</th>
                      <th style="min-width: 150px">Data Transfer</th>
                      <!-- <th style="min-width: 150px">Nomor Referensi</th>
                      <th style="min-width: 150px">Bukti Transfer</th> -->
                      <th style="min-width: 150px">Pernah Daftar</th>
                      <th style="min-width: 150px">Tanggal Daftar</th>
                      <th style="min-width: 150px">Status Daftar</th>
                      
                      <th style="max-width: 80px">Aksi</th>
                    </thead>
                    <tbody>
                    <?php $i=1;
                    foreach($ortu as $ot){?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=$ot['no_daftar']?></td>
                        <td><?=$ot['nama']?></td>
                        <td><?=$ot['status']?></td>
                        <td><?=$ot['hp']?></td>
                        <td><?=$ot['email']?></td>
                        <td><?=$ot['anak']?></td>
                        <td><a href="<?=base_url('uploads/daftar/'.$ot['kk']);?>" target="_blank"><img src="<?=base_url('uploads/daftar/thumb/'.$ot['kk']);?>" class="img-fluid" style=""/></a></td>
                        <td>
                            <?php if($ot['bayar']) { ?>
                              <a href="<?=base_url('uploads/daftar/'.$ot['bukti_transfer']);?>" target="_blank"><img src="<?=base_url('uploads/daftar/thumb/'.$ot['bukti_transfer']);?>" class="img-fluid" style=""/></a>
                              <br>Tgl: <?=$ot['tgl_transfer']?>
                              <br>No. Ref: <?=$ot['no_ref']?>
                            <?php } else { ?>
                              Menunggu konfirmasi pembayaran
                            <?php } ?>
                        </td>
                        <td>
                          <?php if($ot['pernah_daftar']) { ?>
                            Ya
                          <?php } else { ?>
                            Tidak
                          <?php } ?>
                        </td>
                        <td><?=$ot['created_at']?></td>
                        <td>
                          <?php if($ot['verified']) { ?>
                            Approved
                          <?php } else { ?>
                            Pending
                          <?php } ?>
                        </td>
                        
                        <td>
                          <?php if(!$ot['verified']) { ?>
                            <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#modal_edit_ortu" onclick="set_ortu('<?=$ot['id']?>')">Edit</button>
                            <?php if($ot['bayar']) { ?>
                              <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#modal_approve" onclick="set_approve('<?=$ot['id']?>', '<?=$ot['id_ortu']?>')">Approve</button>
                            <?php } else { ?>
                              <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal_email" onclick="set_email('<?=$ot['id']?>')">E-mail Konfirmasi<br>Pendaftaran</button>
                            <?php } ?>
                            <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#modal_hapus" onclick="set_hapus('<?=$ot['id']?>')">Hapus Data</button>
                            <!-- <a href="pendaftaran/kirim_email" class="btn btn-warning btn-block">Email</a> -->
                          <?php } ?>
                        </td>
                      </tr>
                    <?php $i++;} ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modal_hapus" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Konfirmasi Hapus Data</h6></p>
            </div>            

            <?php echo form_open('admin/pendaftaran/hapus', array('id'=>'form-hapus')); ?>

            <input type="hidden" id="id_ortu_hapus" name="id_ortu_hapus">

            <div class="modal-body" style="text-align: center;">
                Apakah yakin akan hapus data pendaftaran?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#modal_hapus">Batal<div class="ripple-container"></div></button>
              <button type="button" id="btnHapus" class="btn btn-danger btn-block">Hapus<div class="ripple-container"></div></button>
            </div>

            <?php echo form_close(); ?>
            
        </div>    
    </div>
</div>

<div class="modal fade" id="modal_email" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Kirim E-mail Kode Unik</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                Konfirmasi Pendaftaran akan dikirim ulang ke e-mail Orang Tua Siswa,
                <br>Apakah akan dilanjutkan?
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#modal_email">Batal<div class="ripple-container"></div></button>
            <button type="button" id="btnKirim" class="btn btn-success btn-block">Kirim<div class="ripple-container"></div></button>
            </div>
        </div>    
    </div>
</div>

<?php echo form_open('admin/pendaftaran/email_daftar', array('id'=>'form-email')); ?>

<input type="hidden" id="id_ortu_daftar" name="id_ortu_daftar">

<?php echo form_close(); ?>

<?php $this->view('admin/daftar_baru/modal_edit_ortu'); ?>
<?php $this->view('admin/daftar_baru/modal_approve_ortu'); ?>

<script>

$("#id_ortu_lama").select2({
    dropdownParent: $("#modal_approve"),
    width: '100%'
});

$(function() {
    $('#tortu').dataTable( {
      dom:  "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'f><'col-sm-12 col-md-4'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      buttons: [
        {
          extend: 'excelHtml5',
          filename: 'Data_Orang_Tua',
          title: 'Data Orang Tua',
          messageTop: '',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8]
          }
        },
      ],
      searching: true
    } );

    $('.buttons-excel').css('background-color', '#49a54d').css('color', 'white');
    $('.dt-buttons').css('float', 'right');
});

function set_approve(id, id_ortu_lama = null) {
  $.ajax({ 
      type: "GET",   
      dataType: "json",
      url: "<?=base_url('admin/pendaftaran/set_ortu_edit');?>",
      data: 'id='+id,
      success: function (data) {
        $('#nama_txt').html(data.nama);
        $('#kelamin_txt').html(data.kelamin);
        $('#hp_txt').html(data.hp);
        $('#email_txt').html(data.email);
        $('#status_txt').html(data.status);
        // $('#is_ortu_lama').val(data.pernah_daftar);
        // alert(data.pernah_daftar)
        if(data.pernah_daftar == 1) {
          $("#is_ortu_lama_ya").prop("checked", true);
          $("#is_ortu_lama_no").prop("checked", false);
        }
      }
    });

  $('#id_ortu').val(id);
  $('#id_ortu_lama').val(id_ortu_lama);
  $('#id_ortu_lama').trigger('change');

  if(id_ortu_lama == null || id_ortu_lama == '' || id_ortu_lama == 0)
    $('#konfirmasi_ortu').show();
  else 
    $('#konfirmasi_ortu').hide();
}

function set_hapus(id) {
  $('#id_ortu_hapus').val(id);
}

$('#btnApprove').on('click', function() {
  var valid = validate();

  if(valid) {
    $('#spin').show();
    $('#form-approve').submit();
    $('#btnApprove').prop("disabled",true);
  }
});

$('#btnHapus').on('click', function() {
  $('#form-hapus').submit();
});

function validate() {
  var res = true;

  if($("#ya").is(':checked')) {

    if ($("#id_ortu_lama").val() == '') {
      $("#alert_ortu").show();
      res = res && false;
    } else {
      $("#alert_ortu").hide();
    }

  }
  
  return res;
}

function set_email(id_ortu) {
  $('#id_ortu_daftar').val(id_ortu);
}

$('#btnKirim').click(function() {
  $('#form-email').submit();
});

function set_ortu(id) {
  $.ajax({ 
      type: "GET",   
      dataType: "json",
      url: "<?=base_url('admin/pendaftaran/set_ortu_edit');?>",
      data: 'id='+id,
      success: function (data) {
        $('#nama_edit').val(data.nama);
        $('#kelamin_edit').val(data.kelamin);
        $('#kelamin_edit_txt').val(data.kelamin);
        $('#email_edit').val(data.email);
        $('#hp_edit').val(data.hp);
        $('#status_edit').val(data.status);
        $('#status_edit_txt').val(data.status);
        $('input[name=id_edit]').val(data.id);

        if(data.id_ortu != null)
          disable_data();
        else 
          enable_data();
      }
  });
}

function disable_data() {
  $('#nama_edit').prop('readonly', true);
  $('#kelamin_edit').hide();
  $('#kelamin_edit_txt').show();
  // $('#hp_edit').prop('readonly', true);
  $('#status_edit').hide();
  $('#status_edit_txt').show();
}

function enable_data() {
  $('#nama_edit').prop('readonly', false);
  $('#kelamin_edit').show();
  $('#kelamin_edit_txt').hide();
  // $('#hp_edit').prop('readonly', false);
  $('#status_edit').show();
  $('#status_edit_txt').hide();
}

</script>