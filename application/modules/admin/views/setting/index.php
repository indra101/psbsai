<div class="content">
        <div class="container-fluid">    
          <div class="row">
            <div class="col-md-12">
            <?php $this->view('admin/notif'); ?>
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Pengaturan Data</h4>
                </div>
                <div class="card-body table-responsive">

                <?php echo form_open_multipart('admin/setting/simpan', array('id'=>'form-submit')); ?>
                  <!-- <div class="row">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">description</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">Nama Unit</span>
                                  </div>
                                  <div class="row">
                                      <input type="text" class="form-control" placeholder="..." name="unit" value="<?=$setting['unit']?>" title="Nama Unit" style="width: 300px;" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row mt-3">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">description</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">Alamat Website Unit</span>
                                  </div>
                                  <div class="row">
                                      <input type="text" class="form-control" placeholder="..." name="unit_url" value="<?=$setting['unit_url']?>" title="Alamat Website Unit" style="width: 300px;" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row mt-3">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">description</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">Alamat Website PSB</span>
                                  </div>
                                  <div class="row">
                                      <input type="text" class="form-control" placeholder="..." name="unit_url_psb" value="<?=$setting['unit_url_psb']?>" title="Alamat Website PSB" style="width: 300px;" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div> -->
                  <div class="row mt-3">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">face</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">Nama Admin</span>
                                  </div>
                                  <div class="row">
                                      <input type="text" class="form-control" placeholder="..." name="admin_nama" value="<?=$setting['admin_nama']?>" title="Nama Admin" style="width: 300px;" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row mt-3">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">phone_iphone</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">No HP Admin</span>
                                  </div>
                                  <div class="row">
                                      <input type="text" class="form-control" placeholder="..." name="admin_hp" value="<?=$setting['admin_hp']?>" title="No HP Admin" style="width: 300px;" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row mt-3">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">call</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">No WA Admin</span>
                                  </div>
                                  <div class="row">
                                      <input type="text" class="form-control" placeholder="..." name="admin_wa" value="<?=$setting['admin_wa']?>" title="No WA Admin" style="width: 300px;" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row mt-3">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">account_balance</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">Nama Bank</span>
                                  </div>
                                  <div class="row">
                                      <input type="text" class="form-control" placeholder="..." name="rekening_bank" value="<?=$setting['rekening_bank']?>" title="Nama Bank" style="width: 300px;" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row mt-3">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">payment</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">No Rekening Bank</span>
                                  </div>
                                  <div class="row">
                                      <input type="text" class="form-control" placeholder="..." name="rekening_no" value="<?=$setting['rekening_no']?>" title="No Rekening Bank" style="width: 300px;" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row mt-3">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">description</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">Nama Rekening</span>
                                  </div>
                                  <div class="row">
                                      <input type="text" class="form-control" placeholder="..." name="rekening_nama" value="<?=$setting['rekening_nama']?>" title="Nama Rekening" style="width: 300px;" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row mt-3">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">today</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">Tahun Ajaran</span>
                                  </div>
                                  <div class="row">
                                    <?=form_dropdown('tahun_ajaran', array('' => 'Pilih Tahun Ajaran') + $tahun_ajars, $setting['tahun_ajaran'], 'class="form-control" id="tahun_ajaran" style="width: 300px"'); ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row mt-3">
                      <div class="col-md-12">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="material-icons">app_registration</i>
                                  </span>
                              </div>
                              <div class="col-md-9">
                                  <div class="row" style="height: 18px">
                                      <span class="judul" style="font-size:15px">Status Pendaftaran</span>
                                  </div>
                                  <div class="row">
                                    <?=form_dropdown('stat_pendaftaran', $stat_pendaftarans, $setting['stat_pendaftaran'], 'class="form-control" id="stat_pendaftaran" style="width: 300px"'); ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <?php echo form_close(); ?>

                  <a href="#" class="btn btn-success mt-5" onclick="konfirmasi()">Simpan<div class="ripple-container"></div></a>

                  

                </div>
              </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_konfirm" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Konfirmasi Simpan Data</h6></p>
            </div>            
            <div class="modal-body">
                Apakah semua data telah sesuai?
            </div>
            <div class="modal-footer text-center">
                <a href="#" class="btn btn-danger" id="btnBatal" onclick="konfirmasi()">Batal</a>
                <a href="#"  class="btn btn-success" id="btnSubmit" onclick="return simpan()"><div class="spinner-border mr-2" id="spin" role="status" style="height: 17px; width: 17px; display: none;"></div>Simpan</a>
            </div>
        </div>    
    </div>
</div>

<script>

function konfirmasi() {
    $('#modal_konfirm').modal('toggle');
}

function simpan() {
    $('#form-submit').submit();
}

function closeNotif() {
    $('.alert').fadeOut();
}

</script>