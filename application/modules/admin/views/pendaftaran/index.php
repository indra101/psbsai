<div class="content">
    <div class="container-fluid">
        <div class="row">
        
        
        
          <div class="col-md-10 ml-auto mr-auto">

            <?php $this->view('admin/notif'); ?>

            <div class="card card-signup">
              <h2 class="card-title text-center" style="padding:10px;">Pendaftaran Orang Tua dan Siswa Baru</h2>
              <div class="card-body">

              <?php echo form_open('admin/pendaftaran/daftar_baru'); ?>
                
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                    <b>Biodata Orang Tua</b>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">face</i>
                            </span>
                            </div>
                            <input name="nama_ortu" type="text" class="form-control" placeholder="Nama..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">wc</i>
                            </span>
                            </div>
                            <select class="form-control" name="kelamin" required>
                                <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">email</i>
                            </span>
                            </div>
                            <input name="email" type="email" class="form-control" placeholder="Email..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">phone_iphone</i>
                            </span>
                            </div>
                            <input name="hp" type="number" class="form-control" placeholder="Telepon..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">local_offer</i>
                            </span>
                            </div>
                            <select class="form-control" name="status" required>
                                <option value="" disabled selected>Pilih Status</option>
                                <option value="Ayah">Ayah</option>
                                <option value="Ibu">Ibu</option>
                                <option value="Wali">Wali</option>
                            </select>
                        </div>
                        <br>
                        <b>Biodata Anak</b>&nbsp;<a href="javascript:void(0)" onclick="tambah()"><i class="material-icons" style="transform:translateY(-3px);">person_add</i></a>
                        <div class="row">
                            <div class="col-md-12" id="tambah">
                                <div class="input-group" id="div_anak">
                                    <i class="material-icons mr-2" id="logo_anak">face</i><input name="nama_siswa[]" id="nama_siswa" type="text" class="form-control mr-2" placeholder="Nama..." required>
                                    <i class="material-icons mr-2" id="logo_lvl">school</i>
                                    <?=form_dropdown('level[]', array('' => 'Pilih Jenjang') + $levels, '', 'class="form-control" id="level" required'); ?>
                                    <i class="material-icons mr-2 pt-2">lock</i>
                                </div>
                            </div>

                        </div>
                        <br>
                        <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#modal_konfirm">Proses<div class="ripple-container"></div></button>
                    </div>
                    <div class="col-md-2"></div>

                    <div class="modal fade" id="modal_konfirm" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                                    <p class="form-row-content mb-0"><h6>Konfirmasi Pendaftaran</h6></p>
                                </div>            
                                <div class="modal-body">
                                    Apakah semua data sudah benar?
                                </div>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Submit" onclick="validate()"/>
                                </div>
                            </div>    
                        </div>
                    </div>

                    <?php echo form_close(); ?>

                </div>
              </div>
            </div>
          </div>

        </div>
    </div>
</div>

<script>
    var num = 1;

    function tambah(){
        
        $("#tambah").append("<div class='input-group' id='div_anak"+num+"'></div>");
        var new_logo_anak = $("#logo_anak").clone().prop('id', 'logo_anak'+num );
        var new_anak = $("#nama_siswa").clone().prop('id', 'nama_siswa'+num ).val('');
        var new_logo_lvl = $("#logo_lvl").clone().prop('id', 'logo_lvl'+num );
        var new_lvl = $("#level").clone().prop('id', 'level'+num );
        
        $("#div_anak"+num).append(new_logo_anak);
        $("#div_anak"+num).append(new_anak);
        $("#div_anak"+num).append(new_logo_lvl);
        $("#div_anak"+num).append(new_lvl).append('<i class="rem material-icons mr-2 pt-2" data-no="' + num + '"  style="cursor: pointer; color: mediumvioletred;">delete</i>');
        num++;
    }

    function validate() {
        //alert("E-Mail: indra@abc.com\nKode Unik: 1111111")
        $('#modal_konfirm').modal('toggle');
    }

    $(document).on('click', '.rem', function() {
        let no = $(this).data('no');
        $("#div_anak"+no).remove();
    });  

</script>