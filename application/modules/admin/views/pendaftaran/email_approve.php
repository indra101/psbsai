<img src="<?=$setting['unit_url_psb']?>/assets/image/logo.png"/>
<br/>
<br/>
<h2>Pendaftaran Telah Diverifikasi</h2><br/>
<div style='font-size: medium'>
    <strong>Assalamu'alaikum Wr Wb</strong>
    <br>
    <br>
    Alhamdulillah pendaftaran siswa telah berhasil diverifikasi.<br>
    <br>
    Bapak/Ibu dimohon dapat melengkapi data-data yang diperlukan dengan melakukan login pada web PSB Sekolah Alam Indonesia <?=$setting['unit']?> dengan detil sebagai berikut:<br>
    <br>
    Email: <?=$email?><br>
    Kode Unik: <?=$kode_unik?><br>
    <br>
    <br>
    Apabila terdapat informasi yang belum jelas Bapak/Ibu dapat menghubungi bagian Admin PSB SAI <?=$setting['unit']?> dengan <?=$setting['admin_nama']?> melalui nomor <?=$setting['admin_hp']?> pada hari kerja pukul 08.30 - 11.30 WIB.
    <br>
    <br>
    Atas perhatian Bapak/Ibu kami mengucapkan terima kasih.
    <br/>
    <br/>
    <br/>
    <strong>Wassalamu'alaikum Wr Wb</strong>
    <br/>
    <br/>
    <br/>
    Admin PSB
    <br>
    Sekolah Alam Indonesia <?=$setting['unit']?>
</div>