<img src="<?=$setting['unit_url_psb']?>/assets/image/logo.png"/>
<br/>
<br/>
<h2>Konfirmasi Pendaftaran Siswa Baru</h2><br/>
<div style='font-size: medium'>
    <strong>Assalamu'alaikum Wr Wb</strong>
    <br>
    <br>
    <?php $panggil = ($ortu['kelamin'] == 'Laki-laki') ? 'Bapak' : 'Ibu'; ?> 
    <?=$panggil?> <?=$ortu['nama']?>
    <br>
    Yang kami hormati,
    <br>
    <br>
    Pendaftaran siswa baru telah berhasil disimpan dengan rincian sebagai berikut: 
    <br>
    <br>
    <?php foreach($siswa as $s) { ?>
    Nama Siswa: <?=$s['nama']?>
    Jenjang: <?=$s['level']?>
    <?php } ?>
    <br>
    <br>
    Kami akan melakukan verifikasi data pendaftaran yang sudah Bapak/Ibu masukkan untuk dapat masuk ke proses berikutnya.
    <br>
    Apabila data pendaftaran telah berhasil diverifikasi maka pemberitahuan akan dikirimkan melalaui e-mail.
    <br>
    <br>
    Apabila terdapat informasi yang belum jelas, <?=$panggil?> dapat menghubungi bagian Admin PSB SAI <?=$setting['unit']?> dengan <?=$setting['admin_nama']?> melalui nomor <?=$setting['admin_hp']?> pada hari kerja pukul 08.30 - 11.30 WIB.
    <br>
    <br>
    Terima kasih atas kesediaan <?=$panggil?> mendaftarkan putra/i <?=$panggil?> di Sekolah Alam Indonesia <?=$setting['unit']?>.
    <br/>
    <br/>
    <br/>
    <strong>Wassalamu'alaikum Wr Wb</strong>
    <br/>
    <br/>
    <br/>
    Admin PSB
    <br>
    Sekolah Alam Indonesia <?=$setting['unit']?>
    <br/>
    <br/>
    <!-- <img src="http://psb.sai-cipedak.sch.id/assets/image/bukit_bawah.png"/> -->
</div>