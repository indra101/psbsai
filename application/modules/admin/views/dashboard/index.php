<div class="content">
        <div class="container-fluid">

        <?php $this->view('admin/notif'); ?>

          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <h3><b>Tahun Ajaran <?=$tahun_ajaran?></b></h3>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa')?>">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon redup">
                      <i class="fas fa-child"></i>
                  </div>
                  <p class="card-category">Data Siswa</p>
                  <h3 class="card-title">
                    <?=$total_siswa?> Orang
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">local_offer</i> 
                    Jumlah Siswa Mendaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/orangTua')?>">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon redup">
                      <i class="fas fa-users"></i>
                  </div>
                  <p class="card-category">Orang Tua</p>
                  <h3 class="card-title"><?=$total_ortu?> Orang</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Orang Tua atau Wali.
                  </div>
                </div>
              </div>
              </a>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=kb')?>">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">KB</span>
                  </div>
                  <p class="card-category">KB</p>
                  <h3 class="card-title"><?=(empty($lvl['kb'])) ? 0 : $lvl['kb']?> Siswa</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=tka')?>">
              <div class="card card-stats">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">TK A</span>
                  </div>
                  <p class="card-category">TK A</p>
                  <h3 class="card-title"><?=(empty($lvl['tka'])) ? 0 : $lvl['tka']?> Siswa
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=tkb')?>">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">TK B</span>
                  </div>
                  <p class="card-category">TK B</p>
                  <h3 class="card-title"><?=(empty($lvl['tkb'])) ? 0 : $lvl['tkb']?> Siswa</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=sd1')?>">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">SD 1</span>
                  </div>
                  <p class="card-category">SD 1</p>
                  <h3 class="card-title"><?=(empty($lvl['sd1'])) ? 0 : $lvl['sd1']?> Siswa</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=sd2')?>">
              <div class="card card-stats">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">SD 2</span>
                  </div>
                  <p class="card-category">SD 2</p>
                  <h3 class="card-title"><?=(empty($lvl['sd2'])) ? 0 : $lvl['sd2']?> Siswa
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=sd3')?>">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">SD 3</span>
                  </div>
                  <p class="card-category">SD 3</p>
                  <h3 class="card-title"><?=(empty($lvl['sd3'])) ? 0 : $lvl['sd3']?> Siswa</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=sd4')?>">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">SD 4</span>
                  </div>
                  <p class="card-category">SD 4</p>
                  <h3 class="card-title"><?=(empty($lvl['sd4'])) ? 0 : $lvl['sd4']?> Siswa</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=sd5')?>">
              <div class="card card-stats">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">SD 5</span>
                  </div>
                  <p class="card-category">SD 5</p>
                  <h3 class="card-title"><?=(empty($lvl['sd5'])) ? 0 : $lvl['sd5']?> Siswa
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=sd6')?>">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">SD 6</span>
                  </div>
                  <p class="card-category">SD 6</p>
                  <h3 class="card-title"><?=(empty($lvl['sd6'])) ? 0 : $lvl['sd6']?> Siswa</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=sl7')?>">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">SL 7</span>
                  </div>
                  <p class="card-category">SL 7</p>
                  <h3 class="card-title"><?=(empty($lvl['sl7'])) ? 0 : $lvl['sl7']?> Siswa</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=sl8')?>">
              <div class="card card-stats">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">SL 8</span>
                  </div>
                  <p class="card-category">SL 8</p>
                  <h3 class="card-title"><?=(empty($lvl['sl8'])) ? 0 : $lvl['sl8']?> Siswa
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=sl9')?>">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">SL 9</span>
                  </div>
                  <p class="card-category">SL 9</p>
                  <h3 class="card-title"><?=(empty($lvl['sl9'])) ? 0 : $lvl['sl9']?> Siswa</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=bless')?>">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">BLESS</span>
                  </div>
                  <p class="card-category">BLESS</p>
                  <h3 class="card-title"><?=(empty($lvl['bless'])) ? 0 : $lvl['bless']?> Siswa</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=istcsd')?>">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">ISTC SD</span>
                  </div>
                  <p class="card-category">ISTC SD</p>
                  <h3 class="card-title"><?=(empty($lvl['istcsd'])) ? 0 : $lvl['istcsd']?> Siswa</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <a href="<?=base_url('admin/siswa?lvl=istcsl')?>">
              <div class="card card-stats">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon redup">
                    <span style="font-size: xx-large; font-weight: bold;">ISTC SL</span>
                  </div>
                  <p class="card-category">ISTC SL</p>
                  <h3 class="card-title"><?=(empty($lvl['istcsl'])) ? 0 : $lvl['istcsl']?> Siswa
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">local_offer</i> 
                    Jumlah Data Siswa terdaftar.
                  </div>
                </div>
              </div>
              </a>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Tahapan Pendaftaran</h4>
                  <p class="card-category">Berikut Tahapan yang sedang berlangsung pada Pendaftaran Sekolah Alam</p>
                </div>
                <div class="card-body table-responsive">
                <table class="table table-hover">
                    <thead class="text-success">
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Tahapan</th>
                    </thead>
                    <tbody>
                    <?php $i=1;
                    foreach($tahapan as $t){?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=tgl_indo($t['tanggal_mulai'])?> - <?=tgl_indo($t['tanggal_selesai'])?></td>
                        <td><?=$t['keterangan']?></td>
                      </tr>
                    <?php $i++;} ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

<script>

function closeNotif() {
    $('.alert').fadeOut();
}

</script>