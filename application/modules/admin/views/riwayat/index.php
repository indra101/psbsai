<?php $role = $this->session->userdata('id_role'); ?>
<div class="content">
        <div class="container-fluid">    
            <div class="row">
            <div class="col-md-12">
            <?php $this->view('admin/notif'); ?>
              <div class="card">
                <div class="card-header card-header-info">
                  <span class="card-title" style="font-size: x-large;">Riwayat Pendaftaran</span>
                  <!-- <div class="nav-tabs-navigation float-right">
                      <div class="nav-tabs-wrapper">
                          <ul class="nav nav-tabs" data-tabs="tabs">
                              <li class="nav-item">
                                  <a class="nav-link active" href="/admin/siswa"><i class="material-icons">face</i>Lihat Siswa Baru</a>
                              </li>
                          </ul>
                      </div>
                  </div> -->
                </div>

                <br>
                <div class="row">
                  <div class="col-md-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                              <i class="material-icons" id="logo_lvl">event</i>
                          </span>
                          <div class="col-md-10">
                            <div class="row" style="height: 14px">
                                <span class="judul">Tahun Ajaran</span>
                            </div>
                            <div class="row">
                              <?=form_dropdown('tahun_ajar', array('' => 'Semua Tahun Ajar') + $tahun_ajars, '', 'class="form-control" id="tahun_ajar"'); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-md-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                              <i class="material-icons" id="logo_lvl">school</i>
                          </span>
                          <div class="col-md-10">
                            <div class="row" style="height: 14px">
                                <span class="judul">Jenjang</span>
                            </div>
                            <div class="row">
                              <?=form_dropdown('level', array('' => 'Semua Jenjang') + $levels, '', 'class="form-control" id="level"'); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                    <button id="cari" type="button" class="btn btn-primary ml-4">Cari<div class="ripple-container"></div></button>
                  </div>
                </div>

                <div class="card-body table-responsive">
                  <table id="tsiswa" class="table-striped table-hover">
                    <thead class="text-success">
                      <th style="min-width: 40px">No</th>
                      <th style="min-width: 200px">Nama</th>
                      <th style="min-width: 100px">Jenjang</th>
                      <!-- <th style="min-width: 200px">Tempat/Tanggal Lahir</th> -->
                      <th style="min-width: 200px">Orang Tua</th>
                      <th style="min-width: 200px">Tahun Ajaran</th>
                      <th>Aksi</th>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

$(function() {

    var noms = 0;
    var table = $('#tsiswa').DataTable({
        processing: true,
        serverSide: true,
        ajax: "<?=base_url('admin/riwayat/list')?>",            
        columns: [
            { data: 'nom', name: 'nom',  sortable: false},
            { data: 'nama_siswa', name: 'nama' , searchable: true},
            { data: 'level', name: 'level' , searchable: true},
            // { data: null, name: 'ttl', render: function ( data, type, row ) {
            //     var tempat_lahir = (data.tempat_lahir == null) ? '' : data.tempat_lahir;
            //     var tgl_lahir = (data.tanggal_lahir == null) ? '' : data.tanggal_lahir;
            //     return tempat_lahir + '/' + tgl_lahir;
            // } },
            { data: 'nama_ortu', name: 'nama_ortu' , searchable: true},
            { data: 'tahun_ajar', name: 'tahun_ajar' , searchable: true},
            { data: null, name: 'aksi', render: function ( data, type, row ) {
                return '<a class="btn btn-info btn-block" href="siswa/view/' + data.id + '" style="">View</a>';
            } },
            
        ],
        //order: [[ 9, "desc" ]],
    });
    
    $('.buttons-excel').css('background-color', '#49a54d').css('color', 'white');
    $('.dt-buttons').css('float', 'right');
    $('.dataTables_filter').hide();

    $("#cari").click(function() {

        var tahun_ajar = $("#tahun_ajar").val();
        var level = $("#level").val();

        table.ajax.url("<?php echo base_url('admin/riwayat/list?tahun_ajar=')?>" + tahun_ajar + "&level=" + level);
        table.ajax.reload();
    });
    
});

// $('#nocase3').keypress(function(e){
//     if(e.keyCode==13)
//     $('#cari3').click();
// });

</script>