<?php $role = $this->session->userdata('id_role'); ?>
<div class="content">
        <div class="container-fluid">    
          <div class="row">
            <div class="col-md-12">
            <?php $this->view('admin/notif'); ?>
              <div class="card">
                <div class="card-header card-header-info">
                  <span class="card-title" style="font-size: x-large;">Orang Tua Siswa Lama</span>
                  <div class="nav-tabs-navigation float-right">
                            <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="/admin/orangTua"><i class="material-icons">face</i>Lihat Orang Tua Baru</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </div>
                
                <div class="card-body table-responsive">
                  <table id="tortu" class="table-striped table-hover">
                    <thead class="text-success">
                      <th style="min-width: 40px">No</th>
                      <th style="min-width: 150px">Nama</th>
                      <th style="min-width: 100px">Status</th>
                      <th style="min-width: 150px">Tempat/Tanggal Lahir</th>
                      <th style="min-width: 350px">Alamat</th>
                      <th style="max-width: 150px">No. HP</th>
                      <th style="max-width: 100px">E-Mail</th>
                      <th style="min-width: 200px">NIK</th>
                      <th style="min-width: 200px">No. KK</th>
                      <th style="min-width: 200px">Suku</th>
                      <th style="min-width: 200px">Agama</th>
                      <th style="min-width: 200px">Pendidikan</th>
                      <th style="min-width: 200px">Almamater</th>
                      <th style="min-width: 200px">Pekerjaan</th>
                      <th style="min-width: 200px">Jabatan</th>
                      <th style="min-width: 200px">Instansi</th>
                      <th style="min-width: 200px">Alamat Pekerjaan</th>
                      <th style="min-width: 200px">Telp Kantor</th>
                      <th style="min-width: 200px">Range Gaji</th>
                      <th style="min-width: 200px">Jml Jam Kerja</th>
                      <th style="min-width: 200px">Waktu Keluarga</th>
                      <th style="min-width: 200px">Anak</th>
                      <th style="min-width: 100px">Status Form</th>
                      <th style="max-width: 80px">Aksi</th>
                    </thead>
                    <tbody>
                    <?php $i=1;
                    foreach($ortu as $ot){?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=$ot['nama_ortu']?></td>
                        <td><?=$ot['status']?></td>
                        <td><?=$ot['tempat_lahir']?>/<?=$ot['tgl_lahir']?></td>
                        <td><?=$ot['alamat_rumah']?></td>
                        <td><?=$ot['hp']?></td>
                        <td><?=$ot['email']?></td>
                        <td><?=$ot['nik_ortu']?></td>
                        <td><?=$ot['nkk']?></td>
                        <td><?=$ot['suku']?></td>
                        <td><?=$ot['agama']?></td>
                        <td><?=$ot['pendidikan']?></td>
                        <td><?=$ot['almamater']?></td>
                        <td><?=$ot['pekerjaan']?></td>
                        <td><?=$ot['jabatan']?></td>
                        <td><?=$ot['instansi']?></td>
                        <td><?=$ot['alamat_pekerjaan']?></td>
                        <td><?=$ot['telp_kantor']?></td>
                        <td><?=$ot['range_gaji']?></td>
                        <td><?=$ot['jml_jam_kerja']?></td>
                        <td><?=$ot['waktu_keluarga']?></td>
                        <td><?=$ot['anak']?></td>
                        <td><?=$ot['lengkap']?></td>
                        <td>
                          <a class="btn btn-info btn-block" href="view/<?=$ot["id"]?>">View</a>
                          <?php if($ot['is_kepala'] && ($role == 1 || $role == 2)){ ?>
                            <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#modal_edit_ortu" onclick="set_ortu('<?=$ot['id']?>')">Edit</button>
                            <button type="button" class="btn btn-success btn-block" onclick="lihat_kode('<?=$ot['nama_ortu'] ?>','<?=$ot['kode_unik']?>')" >Lihat Kode Unik</button>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php $i++;} ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_kode" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Kode Unik</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                <div id="nama_modal" class="mb-4" style="font-size: x-large;"></div>
                <div id="kode_modal" style="font-size: xx-large;font-weight: bold;"></div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#modal_kode">Tutup<div class="ripple-container"></div></button>
            </div>
        </div>    
    </div>
</div>

<?php $this->view('admin/orangtua/modal_edit_ortu'); ?>

<script>

$(function() {
  /*
    var noms = 0;
    var table = $('#dtables').DataTable({
        processing: true,
        serverSide: true,
        //bFilter: false,
        ajax: "<?=base_url('admin/orangTua/list')?>",            
        columns: [
            { data: 'nom', name: 'nom',  sortable: false},
            { data: 'nama_ortu', name: 'nama' , searchable: true},
            { data: 'status', name: 'status' , searchable: true},
            { data: null, name: 'ttl', render: function ( data, type, row ) {
              var tempat_lahir = (data.tempat_lahir == null) ? '' : data.tempat_lahir;
              var tgl_lahir = (data.tgl_lahir == null) ? '' : data.tgl_lahir;
                return tempat_lahir + '/' + tgl_lahir;
            } },
            { data: 'alamat_rumah', name: 'alamat_rumah' , searchable: true},
            { data: 'hp', name: 'status' , searchable: true},
            { data: 'email', name: 'status' , searchable: true},
            { data: 'anak', name: 'anak', searchable: true},
            { data: 'lengkap', name: 'lengkap', searchable: true},
            { data: null, name: 'aksi', render: function ( data, type, row ) {
                return '<a class="btn btn-info btn-block" href="orangTua/view/' + data.id + '" style="">View</a><a class="btn btn-success btn-block" href="#" onclick="lihat_kode(\''+data.nama_ortu+'\',\''+data.kode_unik+'\')" style="">Lihat Kode Unik</a>';
            } },
            
        ],
        //order: [[ 9, "desc" ]],
    });
    */
    $('#tortu').dataTable( {
      dom:  "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'f><'col-sm-12 col-md-4'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      buttons: [
        {
          extend: 'excelHtml5',
          filename: 'Data_Orang_Tua',
          title: 'Data Orang Tua',
          messageTop: '',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]
          }
        },
      ],
      searching: true
    } );

    $('.buttons-excel').css('background-color', '#49a54d').css('color', 'white');
    $('.dt-buttons').css('float', 'right');
});

function lihat_kode(nama, kode){
  //alert(kode)
  $('#nama_modal').html(nama);
  $('#kode_modal').html(kode);
  $('#modal_kode').modal('toggle');
}

function set_ortu(id) {
  $.ajax({ 
      type: "GET",   
      dataType: "json",
      url: "<?=base_url('admin/orangTua/set_ortu_edit');?>",
      data: 'id='+id,
      success: function (data) {
        //alert(data);
        $('#nama_edit').val(data.nama_ortu);
        $('#kelamin_edit').val(data.kelamin);
        $('#email_edit').val(data.email);
        $('#hp_edit').val(data.hp);
        $('#status_edit').val(data.status);
        $('input[name=id_edit]').val(data.id);
      }
  });
}

</script>