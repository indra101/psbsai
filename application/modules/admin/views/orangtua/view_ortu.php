<style>

.siswa_daftar {
  background-color: coral;
  border-radius: 10px;
}

</style>

<div class="content">
        <div class="container-fluid">    
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Data Orang Tua - <?=$nama?></h4>
                </div>
                <div class="card-body">
                  
                  <div class="card card-nav-tabs card-plain">
                    <div class="card-header card-header-danger">
                        <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item mr-2 mb-2">
                                        <a class="nav-link active" href="#home" data-toggle="tab"><i class="material-icons">face</i>Orang Tua</a>
                                    </li>
                                    <?php foreach($anak as $a) { ?>
                                    <li class="nav-item mr-2 mb-2<?php if($a['is_daftar']) { ?> siswa_daftar <?php } ?> ">
                                        <a class="nav-link" href="<?=base_url("admin/siswa/view/".$a['id'])?>"><i class="material-icons">accessibility</i><?=$a['nama_siswa'].' - '.$a['nama_level']?></a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div><div class="card-body ">
                        <div class="tab-content">
                            <div class="tab-pane active" id="home">

                            <?php $this->load->view('siswa/orangtua/form_ortu'); ?>

                            </div>
                            <div class="tab-pane" id="updates">
                                
                              <?php //$this->load->view('form_keluarga'); ?>

                            </div>
                        </div>
                    </div>
                  </div>

                  
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

<script>

$(function($) {

  $(".btn-success").remove();
  //$(".btn-success").prop('disabled', 1);
  $("input").prop('disabled', 1);
  $("textarea").prop('disabled', 1);
  $("select").prop('disabled', 1);

});

</script>