<?php $role = $this->session->userdata('id_role'); ?>
<div class="content">
        <div class="container-fluid">    
            <div class="row">
            <div class="col-md-12">
              <?php $this->view('admin/notif'); ?>
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Informasi Jenjang</h4>
                  <p class="card-category">Berikut informasi status dan biaya masing-masing jenjang</p>
                </div>

                <div class="card-body table-responsive">
                <table class="table table-hover">
                    <thead class="text-success">
                      <th>No</th>
                      <th>Jenjang</th>
                      <th>Biaya</th>
                      <th>Status</th>
                      <?php if($role == 1 || $role == 2){ ?>
                      <th>Aksi</th>
                      <?php } ?>
                    </thead>
                    <tbody>
                    <?php $i=1;
                    foreach($level as $lvl){?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=$lvl['deskripsi']?></td>
                        <td><span class="number"><?=$lvl['biaya']?></span></td>
                        <td><?=($lvl['aktif']) ? 'Aktif' : 'Tidak Aktif'?></td>
                        <?php if($role == 1 || $role == 2){ ?>
                        <td><?php echo '<button class="btn btn-warning btn-block" onclick="update('."'".$lvl['id']."','".$lvl['aktif']."','".$lvl['biaya']."'".')">Edit</button>'; ?></td>
                        <?php } ?>
                      </tr>
                    <?php $i++;} ?>
                    </tbody>
                  </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_update" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Edit Jenjang</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
            <?php echo form_open('admin/level/update'); ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <b>Edit Jenjang</b>
                        <input type="hidden" name="id" id="id_edit"/>
                        <div class="row mt-2">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons mr-2">money</i> Biaya &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>
                                </div>
                                <input type="text" class="form-control rounded p-2" rows="5" id="biaya" name="biaya" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons mr-2">toggle_on</i> Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>
                                </div>
                                <select class="form-control" name="aktif" id="aktif" required>
                                    <option value="" disabled selected>Pilih Status</option>
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-danger btn-block">Batal<div class="ripple-container"></div></button>
                    <button type="submit" class="btn btn-success btn-block">Simpan<div class="ripple-container"></div></button>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<script>
  $(function() {
    $('#ttahapan').dataTable( {
      "searching": true
    } );

    $('#biaya').number(true, 0, ',', '.');
    $('span.number').number(true, 0, ',', '.');
  });

  function update(id, aktif, biaya){
    $("#modal_update").modal({backdrop: 'static', keyboard: false});
    $("#id_edit").val(id);
    $("#aktif").val(aktif);
    $("#biaya").val(biaya);
  }


</script>
