<style>
.select2-container {
width: 250px !important;
padding: 0;
}
</style>

<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Ubah Jadwal Wawancara</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
            <?php echo form_open('admin/wawancara/edit_jadwal'); ?>
                
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <b>Jadwal Wawancara</b>
                        <div class="input-group mt-4">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">date_range</i>
                            </span>
                            </div>
                            <input name="tanggal" id="tanggal_edit" type="date" class="form-control" placeholder="Tanggal..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons" title="Waktu Mulai">schedule</i>
                                </span>
                            </div>
                            <input style="" type="time" name="waktu_mulai" id="waktu_mulai_edit" value="08:00:00" class="form-control" required/>
                            &nbsp;&nbsp;s/d&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="time" name="waktu_selesai" id="waktu_selesai_edit" value="08:00:00" class="form-control" required/>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">face</i>
                            </span>
                            <?=form_dropdown('id_ortu', array('' => 'Pilih Orang Tua') + $ortus, '', 'class="form-control" width="400px;" id="id_ortu_edit" required'); ?>
                            </div>
                            
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">sentiment_satisfied_alt</i>
                            </span>
                            <?=form_dropdown('interviewer1', array('' => 'Pilih Pewawancara 1') + $users, '', 'class="form-control" id="interviewer1_edit" required'); ?>
                            </div>
                            
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">sentiment_satisfied_alt</i>
                            </span>
                            <?=form_dropdown('interviewer2', array('' => 'Pilih Pewawancara 2') + $users, '', 'class="form-control" id="interviewer2_edit" required'); ?>
                            </div>
                            
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">sentiment_satisfied_alt</i>
                            </span>
                            <?=form_dropdown('interviewer3', array('' => 'Pilih Pewawancara 3') + $users, '', 'class="form-control" id="interviewer3_edit" required'); ?>
                            </div>
                        </div>
                        
                        <?=form_hidden('id_edit', '');?>
                        
                    </div>
                    <div class="col-md-2"></div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block" onclick="return validate()">Simpan<div class="ripple-container"></div></button>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<script>

function validate() {
    var c = confirm('Apakah data sudah benar?');
    if(c) {
        return true;
    } else {
        return false;
    }
}

$(function() {
    $("#id_ortu_edit").select2({
        dropdownParent: $("#modal_edit")
    });
    $("#interviewer1_edit").select2({
        dropdownParent: $("#modal_edit")
    });
    $("#interviewer2_edit").select2({
        dropdownParent: $("#modal_edit")
    });
    $("#interviewer3_edit").select2({
        dropdownParent: $("#modal_edit")
    });
});
</script>