<div class="content">
        <div class="container-fluid">    
            <div class="row">
            <div class="col-md-12">
            <?php $this->view('admin/notif'); ?>
              <div class="card">
                <div class="card-header card-header-success">
                <span class="card-title" style="font-size: x-large;">Jadwal Wawancara</span>
                  <div class="nav-tabs-navigation float-right">
                      <div class="nav-tabs-wrapper">
                          <ul class="nav nav-tabs" data-tabs="tabs">
                              <li class="nav-item">
                                  <a class="nav-link active" href="/admin/wawancara/riwayat"><i class="material-icons">history</i>Lihat Riwayat Wawancara</a>
                              </li>
                          </ul>
                      </div>
                  </div>
                </div>
                <?php $role = $this->session->userdata('id_role');
                if($role == 1 || $role == 2) { ?>
                <br>
                <div class="row">
                  <div class="col-md-2">
                    <div style="padding-left:15px;padding-right:15px;">
                      <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal_tambah">Tambah Jadwal<div class="ripple-container"></div></button>
                    </div>
                  </div>
                </div>
                <?php } ?>
                <div class="card-body table-responsive">
                  <table id="dtables" class="table-striped table-hover">
                    <thead class="text-success">
                      <th style="min-width: 40px">No</th>
                      <th style="min-width: 100px">Tanggal</th>
                      <th style="min-width: 150px">Waktu</th>
                      <th style="min-width: 150px">Orang Tua</th>
                      <th style="min-width: 150px">Siswa</th>
                      <th style="min-width: 150px">Pewawancara 1</th>
                      <th style="min-width: 150px">Pewawancara 2</th>
                      <th style="min-width: 150px">Pewawancara 3</th>
                      <th style="min-width: 150px">Catatan Meja 1</th>
                      <th style="min-width: 150px">Catatan Meja 2</th>
                      <th style="min-width: 150px">Catatan Meja 3</th>
                      <th style="min-width: 150px; display: none;">Catatan Meja 1</th>
                      <th style="min-width: 150px; display: none;">Catatan Meja 2</th>
                      <th style="min-width: 150px; display: none;">Catatan Meja 3</th>
                      <th>Aksi</th>
                    </thead>
                    <tbody>
                    <?php $i=1;
                    foreach($jadwal as $u){
                    ?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=$u['tanggal']?></td>
                        <td><?=$u['waktu_mulai'].' s/d '.$u['waktu_selesai']?></td>
                        <td><?=$u['nama_ortu'].'<br>'.$u['nama_pasangan']?></td>
                        <td><?=$u['anak']?></td>
                        <td><?=$u['nama_interviewer1']?></td>
                        <td><?=$u['nama_interviewer2']?></td>
                        <td><?=$u['nama_interviewer3']?></td>
                        <td>
                        <?php if($role == 1 || $role == 2 || in_array($u['id_ortu'], $interviewee)) { ?>
                          <a class="btn btn-info btn-info" href="#" onclick="set_catatan(1, '<?=$u['id']?>')" data-toggle="modal" data-target="#modal_catatan">Isi Catatan</a>
                        <?php } ?>
                        </td>
                        <td>
                        <?php if($role == 1 || $role == 2 || in_array($u['id_ortu'], $interviewee)) { ?>
                          <a class="btn btn-info btn-info" href="#" onclick="set_catatan(2, '<?=$u['id']?>')" data-toggle="modal" data-target="#modal_catatan">Isi Catatan</a>
                        <?php } ?>
                        </td>
                        <td>
                        <?php if($role == 1 || $role == 2 || in_array($u['id_ortu'], $interviewee)) { ?>
                          <a class="btn btn-info btn-info" href="#" onclick="set_catatan(3, '<?=$u['id']?>')" data-toggle="modal" data-target="#modal_catatan">Isi Catatan</a>
                        <?php } ?>
                        </td>
                        <td style="display: none;"><?=$u['notes1']?></td>
                        <td style="display: none;"><?=$u['notes2']?></td>
                        <td style="display: none;"><?=$u['notes3']?></td>
                        <td>
                        <?php if($role == 1 || $role == 2 || in_array($u['id_ortu'], $interviewee)) { ?>
                          <a class="btn btn-info btn-block" href="orangTua/view/<?=$u['id_ortu']?>">Lihat Data OTS</a>
                        <?php } ?>
                        <?php if($role == 1 || $role == 2) { ?>
                          <?='<button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#modal_edit" onclick="set_user('.$u['id'].')" style="">Edit</button>'; ?>
                          <?='<button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#modal_delete" onclick="set_delete('.$u['id'].')" style="">Hapus</button>'; ?>
                        <?php } ?>
                        </td>
                      </tr>
                    <?php $i++;} ?>
                    </tbody>
                  </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Hapus Jadwal</h6></p>
            </div>

            <?=form_open('admin/wawancara/delete'); ?>

            <div class="modal-body" style="text-align: center;">
                <div id="nama_modal" class="mb-4" style="font-size: x-large;">Yakin hapus jadwal?</div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success btn-block">Submit<div class="ripple-container"></div></button>
            </div>

            <?=form_hidden('id_delete', '')?>

            <?=form_close() ?>

        </div>    
    </div>
</div>

<?php $this->view('admin/wawancara/modal_tambah'); ?>
<?php $this->view('admin/wawancara/modal_edit'); ?>
<?php $this->view('admin/wawancara/modal_catatan'); ?>

<script>

$(function() {

  $('#dtables').dataTable( {
    dom:  "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'f><'col-sm-12 col-md-4'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      buttons: [
        {
          extend: 'excelHtml5',
          filename: 'Jadwal_Wawancara',
          title: 'Jadwal Wawancara',
          messageTop: '',
          exportOptions: {
            columns: [0,1,2,3,4,5,6,7,11,12,13]
          }
        },
      ],
      searching: true
  } );

  $('.buttons-excel').css('background-color', '#49a54d').css('color', 'white');
  $('.dt-buttons').css('float', 'right');
    
});

function set_user(id) {
  $.ajax({ 
      type: "GET",   
      dataType: "json",
      url: "<?=base_url('admin/wawancara/set_user_edit');?>",
      data: 'id='+id,
      success: function (data) {
        $('#tanggal_edit').val(data.tanggal);
        $('#waktu_mulai_edit').val(data.waktu_mulai + ':00');
        $('#waktu_selesai_edit').val(data.waktu_selesai + ':00');
        $('#id_ortu_edit').val(data.id_ortu);
        $('#interviewer1_edit').val(data.interviewer1);
        $('#interviewer2_edit').val(data.interviewer2);
        $('#interviewer3_edit').val(data.interviewer3);
        $('input[name=id_edit]').val(data.id);

        $('#id_ortu_edit').trigger('change.select2');
        $('#interviewer1_edit').trigger('change.select2');
        $('#interviewer2_edit').trigger('change.select2');
        $('#interviewer3_edit').trigger('change.select2');
      }
  });
}

function set_catatan(ruang_no, id) {
  $.ajax({ 
      type: "GET",   
      dataType: "json",
      url: "<?=base_url('admin/wawancara/set_user_edit');?>",
      data: 'id='+id,
      success: function (data) {
        
        var catatan = '';
        var interviewer = 0;
        
        if(ruang_no == 1) {
          catatan = data.notes1;
          interviewer = data.nama_interviewer1
        } else if(ruang_no == 2) {
          catatan = data.notes2;
          interviewer = data.nama_interviewer2
        } else {
          catatan = data.notes3;
          interviewer = data.nama_interviewer3
        }

        $('#notes').val(catatan);
        $('#ruang_no').html(ruang_no);
        $('#interviewer').html(interviewer);
        $('input[name=id_edit_catatan]').val(id);
        $('input[name=no_ruang]').val(ruang_no);
      }
  });
}

function set_delete(id) {
  $('input[name=id_delete]').val(id);
}

</script>