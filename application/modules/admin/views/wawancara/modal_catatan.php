<style>
.select2-container {
width: 250px !important;
padding: 0;
}
</style>

<div class="modal fade" id="modal_catatan" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Isi Catatan Meja</h6></p>
            </div>            
            <div class="modal-body" style="text-align: left;">
                
            <?php echo form_open('admin/wawancara/simpan_catatan'); ?>
                
                <div class="row">
                    <!-- <div class="col-md-2"></div> -->
                    <div class="col-md-12">
                        <span class="ml-4"><b>Meja <span id="ruang_no"></span></b></span><br><br>
                        <span class="ml-4"><b>Pewawancara</b></span>
                        <div class="input-group mt-2 mb-4">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">person</i>
                            </span>
                            <span id="interviewer"></span>
                            </div>
                        </div>
                        <span class="ml-4"><b>Catatan</b></span>
                        <div class="input-group mt-2" style="overflow-x: auto;">
                            <div class="input-group-prepend ml-4">
                                <?=form_textarea('notes', '', 'id="notes" style="border-radius: 10px; width: 700px; height: 500px !important; padding: 10px;"')?>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-2"></div> -->

                    <?=form_hidden('id_edit_catatan', '');?>
                    <?=form_hidden('no_ruang', '');?>

                </div>
                <div class="modal-footer">
                    <div id="edit_buttons">
                        <button type="button" class="btn btn-danger btn-block"  data-toggle="modal" data-target="#modal_catatan">Batal</button>
                        <button type="submit" class="btn btn-success btn-block" onclick="return validate()">Simpan</button>
                    </div>
                    <div id="riwayat_buttons" style="display: none;">
                        <button type="button" class="btn btn-danger btn-block"  data-toggle="modal" data-target="#modal_catatan">Tutup</button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<script>

function validate() {
    var c = confirm('Apakah data sudah benar?');
    if(c) {
        return true;
    } else {
        return false;
    }
}

$(function() {
    $("#id_ortu").select2({
        dropdownParent: $("#modal_tambah")
    });
    $("#interviewer1").select2({
        dropdownParent: $("#modal_tambah")
    });
    $("#interviewer2").select2({
        dropdownParent: $("#modal_tambah")
    });
    $("#interviewer3").select2({
        dropdownParent: $("#modal_tambah")
    });
});

</script>