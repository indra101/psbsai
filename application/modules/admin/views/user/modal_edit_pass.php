<div class="modal fade" id="modal_pass" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Ganti Password</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
            <?=form_open('admin/user/ganti_pass'); ?>
                
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <b>Ganti Password</b>
                        
                        <div class="input-group pt-4">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">lock</i>
                            </span>
                            </div>
                            <input name="password_lama" type="password" class="form-control" placeholder="Password Lama..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">lock</i>
                            </span>
                            </div>
                            <input name="password_baru" type="password" class="form-control" placeholder="Password Baru..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">lock</i>
                            </span>
                            </div>
                            <input name="password_konfirm" type="password" class="form-control" placeholder="Konfirmasi Password Baru..." required>
                        </div>
                        
                        
                    </div>
                    <div class="col-md-2"></div>

                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#modal_konfirm">Simpan<div class="ripple-container"></div></button> -->
                    <button type="submit" class="btn btn-success btn-block" onclick="return validate_pass()">Simpan<div class="ripple-container"></div></button>
                </div>

                <?=form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<script>

function validate_pass() {
    // var c = confirm('Apakah data sudah benar?');
    // //$('#modal_konfirm').modal('hide');
    // if(c) {
    //     if(!ok){
    //         //$('#modal_konfirm').modal('toggle');
    //         return false;
    //     } else {
    //         return ok;
    //     }
    // } else {
    //     return false;
    // }
}

</script>