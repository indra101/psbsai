<div class="modal fade" id="modal_create" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Buat User Baru</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
            <?php echo form_open('admin/user/create_user'); ?>
                
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <b>Data User Baru</b>
                        <div class="input-group mt-4">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">face</i>
                            </span>
                            </div>
                            <input name="nama" type="text" class="form-control" placeholder="Nama..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">person</i>
                                </span>
                            </div>
                            <input name="username" id="username" type="text" class="form-control pr-2" placeholder="Username..." onkeyup="cek_username();" required>
                            <i id="oke" class="material-icons pt-2" style="display: none; color: green;">done</i>
                            <i id="no_oke" class="material-icons pt-2" style="display: none; color: red;">clear</i>
                        </div>
                        <!-- <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">lock</i>
                            </span>
                            </div>
                            <input name="password" type="password" class="form-control" placeholder="Password..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">lock</i>
                            </span>
                            </div>
                            <input name="password_konfirm" type="password" class="form-control" placeholder="Konfirmasi Password..." required>
                        </div> -->
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">email</i>
                            </span>
                            </div>
                            <input name="email" type="email" class="form-control" placeholder="Email..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">phone_iphone</i>
                            </span>
                            </div>
                            <input name="hp" type="number" class="form-control" placeholder="Telepon..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">local_offer</i>
                            </span>
                            </div>
                            <select class="form-control" name="status" required>
                                <option value="" disabled selected>Pilih Status</option>
                                <option value="Administrator">Administrator</option>
                                <option value="Admin PPSB">Admin PPSB</option>
                                <option value="Guru">Guru</option>
                                <option value="Orang Tua">Orang Tua</option>
                            </select>
                        </div>
                        
                    </div>
                    <div class="col-md-2"></div>

                    <div class="modal fade" id="modal_konfirm" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                                    <p class="form-row-content mb-0"><h6>Konfirmasi Pendaftaran</h6></p>
                                </div>            
                                <div class="modal-body">
                                    Apakah semua data sudah benar?
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit" onsubmit="validate()" data-toggle="modal" data-target="#modal_konfirm">Submit</button>
                                </div>
                            </div>    
                        </div>
                    </div>

                    

                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#modal_konfirm">Simpan<div class="ripple-container"></div></button> -->
                    <button type="submit" class="btn btn-success btn-block" onclick="return validate()">Simpan<div class="ripple-container"></div></button>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<script>

var ok = false;

function cek_username() {
    //alert('sdada');
    var username = $('#username').val();

    $.ajax({ 
        type: "GET",   
        //dataType: "json",
        url: "<?=base_url('admin/user/cek_username');?>",
        data: 'username='+username,
        success: function (data) {
            //alert(data);
            if(username == '') {
                $('#no_oke').hide();
                $('#oke').hide();
                ok = false;
            } else if(data == 1) {
                $('#no_oke').hide();
                $('#oke').show();
                ok = true;
            } else {
                $('#oke').hide();
                $('#no_oke').show();
                ok = false;
            }
        
        }
    });
}

function validate() {
    var c = confirm('Apakah data sudah benar?');
    //$('#modal_konfirm').modal('hide');
    if(c) {
        if(!ok){
            //$('#modal_konfirm').modal('toggle');
            return false;
        } else {
            return ok;
        }
    } else {
        return false;
    }
}

</script>