<div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Hapus User</h6></p>
            </div>

            <?=form_open('admin/user/delete'); ?>

            <div class="modal-body" style="text-align: center;">
                <div id="nama_modal" class="mb-4" style="font-size: x-large;">Yakin hapus user?</div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success btn-block">Submit<div class="ripple-container"></div></button>
            </div>

            <?=form_hidden('id_delete', '')?>

            <?=form_close() ?>

        </div>    
    </div>
</div>