<div class="content">
        <div class="container-fluid">    
            <div class="row">
            <div class="col-md-12">
              <?php $this->view('admin/notif'); ?>
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Manajemen User</h4>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-2">
                    <div style="padding-left:15px;padding-right:15px;">
                      <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal_create">Tambah User<div class="ripple-container"></div></button>
                    </div>
                  </div>
                </div>
                <div class="card-body table-responsive">
                <table id="tuser" class="table table-hover">
                    <thead class="text-success">
                      <th>No</th>
                      <th>Nama</th>
                      <th>Status</th>
                      <th>No. HP</th>
                      <th>E-Mail</th>
                      <th>Peran</th>
                      <th>Aktif</th>
                      <th>Aksi</th>
                    </thead>
                    <tbody>
                    <?php $i=1;
                    foreach($user as $u){?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=$u['nama']?></td>
                        <td><?=$u['status']?></td>
                        <td><?=$u['hp']?></td>
                        <td><?=$u['email']?></td>
                        <td><?=$u['nama_role']?></td>
                        <td><?=($u['aktif'])?'Aktif':'Tidak Aktif';?></td>
                        <td>
                          <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#modal_edit" onclick="set_user('<?=$u['id']?>')" style="">Edit</button>
                          <button type="button" class="btn btn-success btn-block" onclick="set_id('<?=$u['id']?>')" style="">Reset Password</button>
                          <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#modal_delete" onclick="set_delete('<?=$u['id']?>')" style="">Hapus</button>
                          <button type="button" class="btn btn-success btn-block" onclick="lihat_pass('<?=$u['nama']?>','<?=$u['password']?>')" style="display: none;">Lihat Password</button>
                        </td>
                      </tr>
                    <?php $i++;} ?>
                    </tbody>
                  </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_pass" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Kode Unik</h6></p>
            </div>
            <div class="modal-body" style="text-align: center;">
                <div id="nama_modal" class="mb-4" style="font-size: x-large;"></div>
                <div id="pass_modal" style="font-size: xx-large;font-weight: bold;"></div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#modal_pass">Tutup<div class="ripple-container"></div></button>
            </div>
        </div>    
    </div>
</div>

<div class="modal fade" id="modal_reset" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Reset Password</h6></p>
            </div>

            <?=form_open('admin/user/reset_pass'); ?>

            <div class="modal-body" style="text-align: center;">
                <div id="nama_modal" class="mb-4" style="font-size: x-large;">Yakin Reset Password?</div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success btn-block">Submit<div class="ripple-container"></div></button>
            </div>

            <?=form_hidden('id', '')?>

            <?=form_close() ?>

        </div>    
    </div>
</div>

<?php $this->view('admin/user/create_user'); ?>
<?php $this->view('admin/user/edit_user'); ?>
<?php $this->view('admin/user/delete_user'); ?>

<script>

$(function() {
  /*
    var noms = 0;
    var table = $('#dtables').DataTable({
        processing: true,
        serverSide: true,
        //bFilter: false,
        ajax: "<?=base_url('admin/user/list')?>",            
        columns: [
            { data: 'nom', name: 'nom',  sortable: false},
            { data: 'nama', name: 'nama' , searchable: true},
            { data: 'status', name: 'status' , searchable: true},
            { data: 'hp', name: 'status' , searchable: true},
            { data: 'email', name: 'status' , searchable: true},
            { data: 'nama_role', name: 'nama_role', searchable: true},
            { data: null, name: 'aktif', render: function ( data, type, row ) {
              if(data.aktif)
                return 'Aktif';
              else
                return 'Tidak Aktif';
            } },
            { data: null, name: 'aksi', render: function ( data, type, row ) {
                return '<button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#modal_edit" onclick="set_user(' + data.id + ')" style="">Edit</button><a class="btn btn-success btn-block" href="#" onclick="set_id('+data.id+')" style="">Reset Password</a><a class="btn btn-success btn-block" href="#" onclick="lihat_pass(\''+data.nama+'\',\''+data.password+'\')" style="display: none;">Lihat Password</a>';
            } },
            
        ],
        //order: [[ 9, "desc" ]],
    });
    */
});

$(function() {
    $('#tuser').dataTable( {
      "searching": true
    } );
  });

function lihat_pass(nama, password){
  //alert(kode)
  $('#nama_modal').html(nama);
  $('#pass_modal').html(password);
  $('#modal_pass').modal('toggle');
}

function set_id(id) {
  $('input[name=id]').val(id);
  //alert($('input[name=id]').val());
  $('#modal_reset').modal('toggle');
}

function set_user(id) {
  //var username = $('#username').val();

  $.ajax({ 
      type: "GET",   
      dataType: "json",
      url: "<?=base_url('admin/user/set_user_edit');?>",
      data: 'id='+id,
      success: function (data) {
        //alert(data);
        $('#nama_edit').val(data.nama);
        $('#username_edit').val(data.username);
        $('#email_edit').val(data.email);
        $('#hp_edit').val(data.hp);
        $('#status_edit').val(data.status);
        $('#aktif_edit').val(data.aktif);
        $('input[name=id_edit]').val(data.id);
      }
  });
}

function set_delete(id) {
  $('input[name=id_delete]').val(id);
}

</script>