<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Edit User</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
            <?php echo form_open('admin/user/edit_user'); ?>
                
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <b>Edit Data User</b>
                        <div class="input-group mt-4">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">face</i>
                            </span>
                            </div>
                            <input name="nama_edit" id="nama_edit" type="text" class="form-control" placeholder="Nama..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">person</i>
                                </span>
                            </div>
                            <input name="username_edit" id="username_edit" type="text" class="form-control pr-2" placeholder="Username..." onkeyup="cek_username_edit();" required>
                            <i id="oke_edit" class="material-icons pt-2" style="display: none; color: green;">done</i>
                            <i id="no_oke_edit" class="material-icons pt-2" style="display: none; color: red;">clear</i>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">email</i>
                            </span>
                            </div>
                            <input name="email_edit" id="email_edit" type="email" class="form-control" placeholder="Email..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">phone_iphone</i>
                            </span>
                            </div>
                            <input name="hp_edit" id="hp_edit" type="number" class="form-control" placeholder="Telepon..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">local_offer</i>
                            </span>
                            </div>
                            <select class="form-control" name="status_edit" id="status_edit" required>
                                <option value="" disabled selected>Pilih Status</option>
                                <option value="Administrator">Administrator</option>
                                <option value="Admin PPSB">Admin PPSB</option>
                                <option value="Guru">Guru</option>
                                <option value="Orang Tua">Orang Tua</option>
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">local_offer</i>
                            </span>
                            </div>
                            <select class="form-control" name="aktif_edit" id="aktif_edit" required>
                                <option value="1">Aktif</option>
                                <option value="0 PPSB">Tidak Aktif</option>
                            </select>
                        </div>
                        
                    </div>
                    <div class="col-md-2"></div>

                    <?=form_hidden('id_edit', '')?>

                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#modal_konfirm">Simpan<div class="ripple-container"></div></button> -->
                    <button type="submit" class="btn btn-success btn-block" onclick="return validate_edit()">Simpan<div class="ripple-container"></div></button>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<script>

var ok_edit = true;

function cek_username_edit() {
    //alert('sdada');
    var username = $('#username_edit').val();

    $.ajax({ 
        type: "GET",   
        //dataType: "json",
        url: "<?=base_url('admin/user/cek_username');?>",
        data: 'username='+username,
        success: function (data) {
            //alert(data);
            if(username == '') {
                $('#no_oke_edit').hide();
                $('#oke_edit').hide();
                ok_edit = false;
            } else if(data == 1) {
                $('#no_oke_edit').hide();
                $('#oke_edit').show();
                ok_edit = true;
            } else {
                $('#oke_edit').hide();
                $('#no_oke_edit').show();
                ok_edit = false;
            }
        }
    });
}

function validate_edit() {
    var c = confirm('Apakah data sudah benar?');
    //$('#modal_konfirm').modal('hide');
    if(c) {
        if(!ok_edit){
            //$('#modal_konfirm').modal('toggle');
            return false;
        } else {
            return ok_edit;
        }
    } else {
        return false;
    }
}

</script>