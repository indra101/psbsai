
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
<div class="container">
<div class="navbar-wrapper">
<span class="navbar-brand"> <img src="<?=base_url('assets/image/logosai.png'); ?>" style="width:15px;height:15px;transform: translateY(-3px);"/> Sekolah Alam Indonesia</span>
</div>

</div>
</nav>
<!-- End Navbar -->
<div class="wrapper wrapper-full-page">
<div class="page-header login-page header-filter" filter-color="black" style="background-image: url('<?=base_url("assets/image/bg.jpg"); ?>'); background-size: cover; background-position: top center;">
<!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
<div class="container">
<div class="website">
    <br><br><br><br><br>
</div>
<div class="mobile">
    <br><br>
</div>
<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">

    <?=form_open('admin/login/cekLoginAdmin', array('id'=>'form-submit')); ?>

    <?php $this->view('layouts/notif'); ?>

        <div class="card card-login">
        <div class="card-header card-header-success text-center">
            
            <div class="social-line">
                <img src="<?=base_url('assets/image/logosai.png'); ?>" style="width:100px;height:100px;"/> 
            </div>
            <h4 class="card-title">Login Administrator</h4>
        </div>
        <div class="card-body ">
            <p class="card-description text-center">Masukkan Email dan Password</p>
            
            </span>
            <span class="bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend col-md-2">
                    <span class="input-group-text" style="transform: translate(-13px, 0px);">
                        <i class="material-icons">email</i>
                    </span>
                </div>
                <div class="col-md-10">
                    <input id="email" name="email" type="email" class="form-control" placeholder="E-Mail...">
                </div>
                <div class="col text-right">
                    <span id="alert_email" style="color: #fd7e14;display:none;font-size:8pt;">E-Mail harus diisi!</span>
                </div>
            </div>
            </span>
            <span class="bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend col-md-2">
                    <span class="input-group-text" style="transform: translate(-13px, 0px);">
                        <i class="material-icons">lock_outline</i>
                    </span>
                </div>
                <div class="col-md-10">
                    <input id="password" name="password" type="password" class="form-control" placeholder="Password...">
                </div>
                <div class="col text-right">
                    <span id="alert_password" style="color: #fd7e14;display:none;font-size:8pt;">Password harus diisi!</span>
                </div>
            </div>
            </span>
        </div>
        <div class="card-footer justify-content-center">
            <button id="btn_submit" type="button" class="btn btn-success btn-block" onclick="login()">Masuk</button>
        </div>

        <?=form_hidden('admin', 1);?>

        <?php echo form_close(); ?>

        </div>
    </form>
    </div>
</div>
</div>

<script>

function login(){
    if(validasi()){
        $('#form-submit').submit();
    }
}

function validasi(){
    var cek = true;
    if($('#email').val()){
        $('#alert_email').hide();
        cek = true;
    }else{
        $('#alert_email').show();
        cek = false;
    }

    if($('#password').val()){
        $('#alert_password').hide();
        cek = true;
    }else{
        $('#alert_password').show();
        cek = false;
    }
    return cek;
}

$("#email").on( "keydown", function(event) {
    if(event.which == 13) 
    $("#btn_submit").click();
});

$("#password").on( "keydown", function(event) {
    if(event.which == 13) 
    $("#btn_submit").click();
});

</script>