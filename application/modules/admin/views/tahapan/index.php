<?php $role = $this->session->userdata('id_role'); ?>
<div class="content">
        <div class="container-fluid">    
            <div class="row">
            <div class="col-md-12">
              <?php $this->view('admin/notif'); ?>
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Tahapan Pendaftaran</h4>
                  <p class="card-category">Berikut Tahapan yang sedang berlangsung pada Pendaftaran Sekolah Alam</p>
                </div>

                <?php if($role == 1 || $role == 2){ ?>
                <br>
                <div class="row">
                  <div class="col-md-2">
                    <div style="padding-left:15px;padding-right:15px;">
                      <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal_create">Tambah Tahapan<div class="ripple-container"></div></button>
                    </div>
                  </div>
                </div>
                <?php } ?>

                <div class="card-body table-responsive">
                <table <?php if($role == 1 || $role == 2){ ?>id="ttahapan"<?php } ?> class="table table-hover">
                    <thead class="text-success">
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Tahapan</th>
                      <?php if($role == 1 || $role == 2){ ?>
                      <th>Aksi</th>
                      <?php } ?>
                    </thead>
                    <tbody>
                    <?php $i=1;
                    foreach($tahapan as $t){?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=tgl_indo($t['tanggal_mulai'])?> - <?=tgl_indo($t['tanggal_selesai'])?></td>
                        <td><?=$t['keterangan']?></td>
                        <?php if($role == 1 || $role == 2){ ?>
                        <td><?php echo '<button class="btn btn-info btn-block" onclick="update('."'".$t['id']."','".$t['tanggal_mulai']."','".$t['tanggal_selesai']."','".$t['keterangan']."'".')">Edit</button><button class="btn btn-danger btn-block" onclick="hapus('."'".$t['id']."','".$t['keterangan']."'".')">Hapus</button>'; ?></td>
                        <?php } ?>
                      </tr>
                    <?php $i++;} ?>
                    </tbody>
                  </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_create" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Buat Tahapan Baru</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
            <?php echo form_open('admin/tahapan/create'); ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <b>Tambah Tahapan</b>
                        <div class="input-group mt-4">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">date_range</i> Tanggal Mulai &nbsp;&nbsp;
                            </span>
                            </div>
                            <input name="tanggal_mulai" type="date" class="form-control" placeholder="Tanggal Mulai..." required>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">date_range</i> Tanggal Selesai
                                </span>
                            </div>
                            <input name="tanggal_selesai" type="date" class="form-control" placeholder="Tanggal Selesai..." required>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">sports_handball</i> Keterangan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </span>
                            </div>
                            <textarea class="form-control rounded p-2" rows="5" name="keterangan" required></textarea>
                        </div>
                        
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Simpan<div class="ripple-container"></div></button>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<div class="modal fade" id="modal_update" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Edit Tahapan</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
            <?php echo form_open('admin/tahapan/update'); ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <b>Edit Tahapan</b>
                        <input type="hidden" name="id" id="id_edit"/>
                        <div class="input-group mt-4">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">date_range</i> Tanggal Mulai &nbsp;&nbsp;
                            </span>
                            </div>
                            <input name="tanggal_mulai" id="edit_tanggal_mulai" type="date" class="form-control" placeholder="Tanggal Mulai..." required>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">date_range</i> Tanggal Selesai
                                </span>
                            </div>
                            <input name="tanggal_selesai" id="edit_tanggal_selesai" type="date" class="form-control" placeholder="Tanggal Selesai..." required>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">sports_handball</i> Keterangan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </span>
                            </div>
                            <textarea class="form-control rounded p-2" rows="5" id="edit_keterangan" name="keterangan" required></textarea>
                        </div>
                        
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block">Simpan<div class="ripple-container"></div></button>
                    <button type="button" data-dismiss="modal" class="btn btn-info btn-block">Tidak<div class="ripple-container"></div></button>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Konfirmasi</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
            <?php echo form_open('admin/tahapan/delete'); ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <b>Anda Yakin Menghapus Tahapan</b><br>
                        <pre style="color:green;" id="hapus_keterangan"></pre>
                        <input type="hidden" name="id" id="id_hapus"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-block">Ya<div class="ripple-container"></div></button>
                    <button type="button" data-dismiss="modal" class="btn btn-success btn-block">Tidak<div class="ripple-container"></div></button>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<script>
  $(function() {
    $('#ttahapan').dataTable( {
      "searching": true
    } );
  });

  function update(id,mulai,selesai,keterangan){
    $("#modal_update").modal({backdrop: 'static', keyboard: false});
    $("#id_edit").val(id);
    $("#edit_tanggal_mulai").val(mulai);
    $("#edit_tanggal_selesai").val(selesai);
    $("#edit_keterangan").val(keterangan);
  }

  function hapus(id,keterangan){
    $("#modal_delete").modal({backdrop: 'static', keyboard: false});
    $("#id_hapus").val(id);
    $("#hapus_keterangan").html(keterangan);
  }

</script>
