<style>

#overlay{	
  position: fixed;
  top: 0;
  z-index: 100;
  width: inherit;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;  
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% { 
    transform: rotate(360deg); 
  }
}
.is-hide{
  display:none;
}

#konten_view .card {
  box-shadow: none !important;
}

#konten_view .card .card-header {
  box-shadow: none !important;
}

</style>

<script src='<?php echo base_url('assets'); ?>/print/jspdf.min.js' type="text/javascript"></script>
<script src='<?php echo base_url('assets'); ?>/print/html2canvas.js' type="text/javascript"></script>

<div class="content">
        <div class="container-fluid">    
          <div class="row">
            <div class="col-md-12" id='konten_view'>
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Data Siswa - <b><?=$nama?> (<?=$siswa['nama_level']?>)</b></h4>
                </div>
                <div style="padding-left:15px;padding-right:15px;">
                  <button type="button" id="btn_print" class="btn btn-warning"><i class="material-icons mr-2">print</i>Cetak<div class="ripple-container"></div></button>
                </div>
                <div class="card-body">
                  <div class="card card-nav-tabs card-plain">
                    <div class="card-header card-header-danger">
                        <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#datasiswa" id="nav-datasiswa" data-toggle="tab"><i class="material-icons">face</i>Data Siswa</a>
                                    </li>

                                    <?php if($siswa['level'] != 'bless') { ?>

                                    <li class="nav-item">
                                        <a class="nav-link" href="#keluarga" id="nav-keluarga" data-toggle="tab"><i class="material-icons">group</i>Keluarga</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#kelahiran" id="nav-kelahiran" data-toggle="tab"><i class="material-icons">favorite</i>Kelahiran</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#kesehatan" id="nav-kesehatan" data-toggle="tab"><i class="material-icons">add_box</i>Kesehatan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#perkembangan" id="nav-perkembangan" data-toggle="tab"><i class="material-icons">ballot</i>Perkembangan</a>
                                    </li>

                                    <?php } ?>

                                    <!-- <li class="nav-item">
                                        <a class="nav-link" href="#pindahan" id="nav-pindahan" data-toggle="tab"><i class="material-icons">flight</i>Pindahan</a>
                                    </li> -->
                                    <li class="nav-item">
                                        <a class="nav-link" href="#pertanyaan" id="nav-pertanyaan" data-toggle="tab"><i class="material-icons">public</i>Visi Misi Pendidikan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#pernyataan" data-toggle="tab"><i class="material-icons">public</i>Surat Pernyataan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?=base_url("admin/orangTua/view/".$siswa['id_ortu'])?>"><i class="material-icons">wc</i>Data Orang Tua</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div><div class="card-body ">
                        <div class="tab-content">
                            <div class="tab-pane active" id="datasiswa">
                              <?php 
                                if(preg_match("/bless/i", $siswa['level'])) { 
                                  $this->load->view('siswa/datasiswa/form_bless'); 
                                } else if(preg_match("/istc/i", $siswa['level'])) { 
                                  $this->load->view('siswa/datasiswa/form_istc'); 
                                } else {
                                  $this->load->view('siswa/datasiswa/form_datasiswa'); 
                                } ?>
                            </div>

                            <?php if($siswa['level'] != 'bless') { ?>

                            <div class="tab-pane" id="keluarga">
                              <?php $this->load->view('siswa/keluarga/form_keluarga'); ?>
                            </div>
                            <div class="tab-pane" id="kelahiran">
                              <?php $this->load->view('siswa/kelahiran/form_kelahiran'); ?>
                            </div>
                            <div class="tab-pane" id="kesehatan">
                              <?php $this->load->view('siswa/kesehatan/form_kesehatan'); ?>
                            </div>
                            <div class="tab-pane" id="perkembangan">
                              <?php $this->load->view('siswa/perkembangan/form_'.$grup_lvl); ?>
                            </div>

                            <?php } ?>
                            
                            <!-- <div class="tab-pane" id="pindahan">
                              <?php //$this->load->view('siswa/pindahan/form_pindahan'); ?>
                            </div> -->
                            <div class="tab-pane" id="pertanyaan">
                              <?php $this->load->view('siswa/pertanyaan/form_'.$grup_lvl.'_view'); ?>
                            </div>
                            <div class="tab-pane" id="pernyataan">
                              <?php $this->load->view('siswa/pernyataan/form_pernyataan'); ?>
                            </div>
                        </div>
                    </div>
                  </div>

                  
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

<div id="overlay">
  <div class="cv-spinner">
    <span class="spinner"></span>
  </div>
</div>

<script>

$(function($) {
  
  $(".btn-success").remove();
  $("#upload_btn").remove();
  $("#btn_download_pernyataan").remove();
  //$(".btn-success").prop('disabled', 1);
  $("input").prop('disabled', 1);
  $("textarea").prop('disabled', 1);
  $("select").prop('disabled', 1);
  $(".progress").remove();

  $('[id^=step]').show();
  $('[id^=next]').remove();

});

$('#btn_print2').click(function() {
  $("#overlay").fadeIn(300);
});

$('#btn_print').click(function() {

  $("#overlay").fadeIn();
  $(window).scrollTop(0);
  $('#btn_print').hide();

  var width = 200;
  var height = 256.02;
  var orientation = 'p';
  var pdf = new jsPDF(orientation, 'mm', [height + 20, width + 20]);

  $('#nav-datasiswa').click();
  $('#btn_print').hide();

  setTimeout(function(){
    html2canvas(document.getElementById("konten_view")).then(function(canvas) {
      var img = canvas.toDataURL('image/jpeg', 1.0);
      var divHeight = $('#konten_view').height();
      var divWidth = $('#konten_view').width();
      var ratio = divHeight / divWidth;
      var width = 200;
      var height = ratio * width;
      pdf.addImage(img, 'JPEG', 10, 10, width, height);
        
    }).then(function(canvas) {
      $('#nav-keluarga').click();
      $('#btn_print').hide();
      pdf.addPage();

      html2canvas(document.getElementById("konten_view")).then(function(canvas) {
        var img = canvas.toDataURL('image/jpeg', 1.0);
        var divHeight = $('#konten_view').height();
        var divWidth = $('#konten_view').width();
        var ratio = divHeight / divWidth;
        var width = 200;
        var height = ratio * width;
        pdf.addImage(img, 'JPEG', 10, 10, width, height);

      }).then(function(canvas) {
        $('#nav-kelahiran').click();
        $('#btn_print').hide();
        pdf.addPage();

        html2canvas(document.getElementById("konten_view")).then(function(canvas) {
          var img = canvas.toDataURL('image/jpeg', 1.0);
          var divHeight = $('#konten_view').height();
          var divWidth = $('#konten_view').width();
          var ratio = divHeight / divWidth;
          var width = 200;
          var height = ratio * width;
          pdf.addImage(img, 'JPEG', 10, 10, width, height);

        }).then(function(canvas) {
          $('#nav-kesehatan').click();
          $('#btn_print').hide();
          pdf.addPage();

          html2canvas(document.getElementById("konten_view")).then(function(canvas) {
            var img = canvas.toDataURL('image/jpeg', 1.0);
            var divHeight = $('#konten_view').height();
            var divWidth = $('#konten_view').width();
            var ratio = divHeight / divWidth;
            var width = 200;
            var height = ratio * width;
            pdf.addImage(img, 'JPEG', 10, 10, width, height);

          }).then(function(canvas) {
            $('#nav-perkembangan').click();
            $('#btn_print').hide();
            $('#page2_perkembangan').hide();
            pdf.addPage();

            html2canvas(document.getElementById("konten_view")).then(function(canvas) {
              var img = canvas.toDataURL('image/jpeg', 1.0);
              var divHeight = $('#konten_view').height();
              var divWidth = $('#konten_view').width();
              var ratio = divHeight / divWidth;
              var width = 200;
              var height = ratio * width;
              pdf.addImage(img, 'JPEG', 10, 10, width, height);

            }).then(function(canvas) {

              $('#page1_perkembangan').hide();
              $('#page2_perkembangan').show();
              
              pdf.addPage();

              html2canvas(document.getElementById("konten_view")).then(function(canvas) {
                var img = canvas.toDataURL('image/jpeg', 1.0);
                var divHeight = $('#konten_view').height();
                var divWidth = $('#konten_view').width();
                var ratio = divHeight / divWidth;
                var width = 200;
                var height = ratio * width;
                pdf.addImage(img, 'JPEG', 10, 10, width, height);

                $('#page1_perkembangan').show();

              }).then(function(canvas) {
                
                // $('#nav-pindahan').click();
                // $('#btn_print').hide();
                // pdf.addPage();

                // html2canvas(document.getElementById("konten_view")).then(function(canvas) {
                //   var img = canvas.toDataURL('image/jpeg', 1.0);
                //   var divHeight = $('#konten_view').height();
                //   var divWidth = $('#konten_view').width();
                //   var ratio = divHeight / divWidth;
                //   var width = 200;
                //   var height = ratio * width;
                //   pdf.addImage(img, 'JPEG', 10, 10, width, height);

                // }).then(function(canvas) {
                  $('#nav-pertanyaan').click();
                  $('#btn_print').hide();
                  $('#page2').hide();
                  $('#page3').hide();
                  $('#page4').hide();
                  
                  pdf.addPage();

                  html2canvas(document.getElementById("konten_view")).then(function(canvas) {
                    var img = canvas.toDataURL('image/jpeg', 1.0);
                    var divHeight = $('#konten_view').height();
                    var divWidth = $('#konten_view').width();
                    var ratio = divHeight / divWidth;
                    var width = 200;
                    var height = ratio * width;
                    pdf.addImage(img, 'JPEG', 10, 10, width, height);

                  }).then(function(canvas) {
                    $('#page1').hide();
                    $('#page2').show();
                    
                    pdf.addPage();

                    html2canvas(document.getElementById("konten_view")).then(function(canvas) {
                      var img = canvas.toDataURL('image/jpeg', 1.0);
                      var divHeight = $('#konten_view').height();
                      var divWidth = $('#konten_view').width();
                      var ratio = divHeight / divWidth;
                      var width = 200;
                      var height = ratio * width;
                      pdf.addImage(img, 'JPEG', 10, 10, width, height);

                    }).then(function(canvas) {
                      $('#page2').hide();
                      $('#page3').show();
                      
                      pdf.addPage();

                      html2canvas(document.getElementById("konten_view")).then(function(canvas) {
                        var img = canvas.toDataURL('image/jpeg', 1.0);
                        var divHeight = $('#konten_view').height();
                        var divWidth = $('#konten_view').width();
                        var ratio = divHeight / divWidth;
                        var width = 200;
                        var height = ratio * width;
                        pdf.addImage(img, 'JPEG', 10, 10, width, height);

                      }).then(function(canvas) {
                        $('#page3').hide();
                        $('#page4').show();
                        
                        pdf.addPage();

                        html2canvas(document.getElementById("konten_view")).then(function(canvas) {
                          var img = canvas.toDataURL('image/jpeg', 1.0);
                          var divHeight = $('#konten_view').height();
                          var divWidth = $('#konten_view').width();
                          var ratio = divHeight / divWidth;
                          var width = 200;
                          var height = ratio * width;
                          pdf.addImage(img, 'JPEG', 10, 10, width, height);
                          pdf.save('<?=$nama?> (<?=$siswa['nama_level']?>).pdf');   

                          $('#page1').show();
                          $('#page2').show();
                          $('#page3').show();
                          $('#nav-datasiswa').click();
                          $('#btn_print').show();

                          setTimeout(function(){
                            $("#overlay").fadeOut(300);
                          },500);
                          
                        });
                      });
                    });
                  });
                // });
              });
            });
          });
        });
      });  
    });
  },500);
});

</script>