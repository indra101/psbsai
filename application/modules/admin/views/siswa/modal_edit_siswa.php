<div class="modal fade" id="modal_edit_siswa" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Edit Data Siswa</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
            <?php echo form_open('admin/siswa/edit_siswa'); ?>
                
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <b>Edit Data Siswa</b>
                        <div class="input-group mt-4">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">face</i>
                            </span>
                            </div>
                            <input name="nama" id="nama_edit" type="text" class="form-control" placeholder="Nama..." required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">wc</i>
                            </span>
                            </div>
                            <select class="form-control" name="kelamin" id="kelamin_edit" required>
                                <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">school</i>
                            </span>
                            </div>
                            <?=form_dropdown('level', array('' => 'Pilih Jenjang') + $levels, '', 'class="form-control" id="level_edit" '); ?>
                        </div>
                        
                    </div>
                    <div class="col-md-2"></div>

                    <?=form_hidden('id_edit', '')?>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-block" onclick="return validate_edit()">Simpan<div class="ripple-container"></div></button>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>    
    </div>
</div>

<script>

var ok_edit = true;

function validate_edit() {
    var c = confirm('Apakah data sudah benar?');
    if(c) {
        return true;
    } else {
        return false;
    }
}

</script>