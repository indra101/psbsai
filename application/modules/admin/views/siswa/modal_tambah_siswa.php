<style>
.select2-container {
padding: 0;
width: 390px !important;
}
</style>

<div class="modal fade" id="modal_create" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
                <p class="form-row-content mb-0"><h6>Tambah Siswa</h6></p>
            </div>            
            <div class="modal-body" style="text-align: center;">
                
                <?php echo form_open('admin/siswa/addSiswa'); ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">face</i>
                            </span>
                            <?=form_dropdown('id_ortu', array('' => 'Pilih Orang Tua') + $ortus, '', 'class="form-control" id="id_ortu" required'); ?>
                            </div>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons" id="logo_anak">face</i>
                            </span>
                            <input name="nama_siswa" id="nama_siswa" type="text" class="form-control" style="width: 390px !important;" placeholder="Nama..." required>
                            </div>
                        </div>
                        
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons" id="logo_lvl">school</i>
                            </span>
                            <?=form_dropdown('level', array('' => 'Pilih Jenjang') + $levels, '', 'class="form-control" id="level" required'); ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-block" onclick="return validate()">Simpan<div class="ripple-container"></div></button>
            </div>
            <?php echo form_close(); ?>
        </div>    
    </div>
</div>

<script>

function validate() {
    var c = confirm('Apakah data sudah benar?');
    if(c) {
        return true;
    } else {
        return false;
    }
}

$(function() {
    $("#id_ortu").select2({
        dropdownParent: $("#modal_create")
    });
    $("#level").select2({
        dropdownParent: $("#modal_create")
    });
});

</script>