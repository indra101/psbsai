<?php $role = $this->session->userdata('id_role'); ?>
<div class="content">
        <div class="container-fluid">    
            <div class="row">
            <div class="col-md-12">
            <?php $this->view('admin/notif'); ?>
              <div class="card">
                <div class="card-header card-header-success">
                  <span class="card-title" style="font-size: x-large;">Siswa Baru</span>
                  <div class="nav-tabs-navigation float-right">
                      <div class="nav-tabs-wrapper">
                          <ul class="nav nav-tabs" data-tabs="tabs">
                              <li class="nav-item">
                                  <a class="nav-link active" href="/admin/siswa/siswa_lama"><i class="material-icons">face</i>Lihat Siswa Lama</a>
                              </li>
                          </ul>
                      </div>
                  </div>
                </div>

                <?php if($role == 1 || $role == 2){ ?>
                <br>
                <div class="row">
                  <div class="col-md-2">
                    <div style="padding-left:15px;padding-right:15px;">
                      <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal_create">Tambah Siswa<div class="ripple-container"></div></button>
                    </div>
                  </div>
                </div>
                <?php } ?>

                <div class="row">
                  <div class="col-md-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                              <i class="material-icons" id="logo_lvl">event</i>
                          </span>
                          <div class="col-md-10">
                            <div class="row" style="height: 14px">
                                <span class="judul">Jenjang</span>
                            </div>
                            <div class="row">
                              <?=form_dropdown('level', array('' => 'Semua Jenjang') + $levels_all, $level_default, 'class="form-control" id="level_all"'); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                    <button id="cari" type="button" class="btn btn-primary ml-4">Cari<div class="ripple-container"></div></button>
                  </div>
                </div>

                <div class="card-body table-responsive">
                  <table id="tsiswa" class="table-striped table-hover">
                    <thead class="text-success">
                      <th style="min-width: 40px">No</th>
                      <th style="min-width: 200px">Nama</th>
                      <th style="min-width: 200px">Panggilan</th>
                      <th style="min-width: 100px">Jenis Kelamin</th>
                      <th style="min-width: 100px">Jenjang</th>
                      <th style="min-width: 200px">Tempat/Tanggal Lahir</th>
                      <th style="min-width: 450px">Alamat</th>
                      <th style="min-width: 200px">Orang Tua</th>
                      <th style="min-width: 200px">Nomor KK</th>
                      <th style="min-width: 450px;">Nama Ayah</th>
                      <th style="min-width: 450px;">NIK Ayah</th>
                      <th style="min-width: 450px;">Tempat/Tgl Lahir Ayah</th>
                      <th style="min-width: 450px;">Pekerjaan Ayah</th>
                      <th style="min-width: 450px;">HP Ayah</th>
                      <th style="min-width: 450px;">Nama Ibu</th>
                      <th style="min-width: 450px;">NIK Ibu</th>
                      <th style="min-width: 450px;">Tempat/Tgl Lahir Ibu</th>
                      <th style="min-width: 450px;">Pekerjaan Ibu</th>
                      <th style="min-width: 450px;">HP Ibu</th>
                      <th style="min-width: 450px;">Nama Wali</th>
                      <th style="min-width: 450px;">NIK Wali</th>
                      <th style="min-width: 450px;">Tempat/Tgl Lahir Wali</th>
                      <th style="min-width: 450px;">Pekerjaan Wali</th>
                      <th style="min-width: 450px;">HP Wali</th>
                      <th style="min-width: 450px;">NISN</th>
                      <th style="min-width: 450px;">Asal Sekolah</th>
                      <th style="min-width: 450px;">Alamat Sekolah Asal</th>
                      <th style="min-width: 450px;">NPSN Sekolah Asal</th>
                      <th>Aksi</th>
                    </thead>
                    <tbody>
                    <?php $i=1;
                    //print_r($siswa);
                    foreach($siswa as $s){?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=$s['nama_siswa']?></td>
                        <td><?=$s['panggilan']?></td>
                        <td><?=$s['kelamin']?></td>
                        <td><?=$s['level']?></td>
                        <td><?=$s['tempat_lahir']?>/<?=$s['tanggal_lahir']?></td>
                        <td><?=$s['alamat']?></td>
                        <td><?=$s['nama_ortu']?></td>
                        <td><?=$s['nkk']?></td>
                        <td><?=$s['nama_ayah']?></td>
                        <td><?=$s['nik_ayah']?></td>
                        <td><?=$s['tempat_lahir_ayah']?>/<?=$s['tgl_lahir_ayah']?></td>
                        <td><?=$s['pekerjaan_ayah']?></td>
                        <td><?=$s['hp_ayah']?></td>
                        <td><?=$s['nama_ibu']?></td>
                        <td><?=$s['nik_ibu']?></td>
                        <td><?=$s['tempat_lahir_ibu']?>/<?=$s['tgl_lahir_ibu']?></td>
                        <td><?=$s['pekerjaan_ibu']?></td>
                        <td><?=$s['hp_ibu']?></td>
                        <td><?=$s['nama_wali']?></td>
                        <td><?=$s['nik_wali']?></td>
                        <td><?=$s['tempat_lahir_wali']?>/<?=$s['tgl_lahir_wali']?></td>
                        <td><?=$s['pekerjaan_wali']?></td>
                        <td><?=$s['hp_wali']?></td>
                        <td><?=$s['nisn']?></td>
                        <td><?=$s['asal_sekolah']?></td>
                        <td><?=$s['alamat_sekolah_asal']?></td>
                        <td><?=$s['npsn_sekolah_asal']?></td>
                        <td>
                          <a class="btn btn-info btn-block" href="siswa/view/<?=$s['id']?>" style="">View</a>
                          <?php if($role == 1 || $role == 2){ ?>
                            <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#modal_edit_siswa" onclick="set_siswa('<?=$s['id']?>')">Edit</button>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php $i++;} ?>
                    </tbody>
                  </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->view('admin/siswa/modal_tambah_siswa'); ?>
<?php $this->view('admin/siswa/modal_edit_siswa'); ?>

<script>

$(function() {
  /*
    var noms = 0;
    var table = $('#dtables').DataTable({
        processing: true,
        serverSide: true,
        //bFilter: false,
        ajax: "<?=base_url('admin/siswa/list')?>",            
        columns: [
            { data: 'nom', name: 'nom',  sortable: false},
            { data: 'nama_siswa', name: 'nama' , searchable: true},
            { data: 'kelamin', name: 'status' , searchable: true},
            { data: 'level', name: 'level' , searchable: true},
            { data: null, name: 'ttl', render: function ( data, type, row ) {
                var tempat_lahir = (data.tempat_lahir == null) ? '' : data.tempat_lahir;
                var tgl_lahir = (data.tanggal_lahir == null) ? '' : data.tanggal_lahir;
                return tempat_lahir + '/' + tgl_lahir;
            } },
            { data: 'alamat', name: 'alamat' , searchable: true},
            { data: 'nama_ortu', name: 'nama_ortu' , searchable: true},
            { data: null, name: 'aksi', render: function ( data, type, row ) {
                return '<a class="btn btn-info btn-block" href="siswa/view/' + data.id + '" style="">View</a>';
            } },
            
        ],
        //order: [[ 9, "desc" ]],
    });
    */

    $('#tsiswa').dataTable( {
      dom:  "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'f><'col-sm-12 col-md-4'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      buttons: [
        {
          extend: 'excelHtml5',
          filename: 'Data_Siswa',
          title: 'Data Siswa',
          messageTop: '',
          exportOptions: {
            columns: [0,1,2,3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
          }
        },
      ],
      columnDefs: [
        {targets: 8, visible: 0},
        {targets: 9, visible: 0},
        {targets: 10, visible: 0},
        {targets: 11, visible: 0},
        {targets: 12, visible: 0},
        {targets: 13, visible: 0},
        {targets: 14, visible: 0},
        {targets: 15, visible: 0},
        {targets: 16, visible: 0},
        {targets: 17, visible: 0},
        {targets: 18, visible: 0},
        {targets: 19, visible: 0},
        {targets: 20, visible: 0},
        {targets: 21, visible: 0},
        {targets: 22, visible: 0},
        {targets: 23, visible: 0},
        {targets: 24, visible: 0},
        {targets: 25, visible: 0},
        {targets: 26, visible: 0},
        {targets: 27, visible: 0},
      ],
      searching: true
    } );

    $('.buttons-excel').css('background-color', '#49a54d').css('color', 'white');
    $('.dt-buttons').css('float', 'right');
    
});

function set_siswa(id) {
  $.ajax({ 
      type: "GET",   
      dataType: "json",
      url: "<?=base_url('admin/siswa/set_siswa_edit');?>",
      data: 'id='+id,
      success: function (data) {
        $('#nama_edit').val(data.nama_siswa);
        $('#kelamin_edit').val(data.kelamin);
        $('#level_edit').val(data.level);
        $('input[name=id_edit]').val(data.id);
      }
  });
}

$("#cari").click(function() {

  var lvl = $("#level_all").val();
  window.location.href = "<?php echo base_url('admin/siswa?lvl=')?>" + lvl;

});

</script>