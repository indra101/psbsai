<?php if($this->session->flashdata('display')) { ?>
            
    <div>                        
        <div class="alert alert-info alert-dismissible" style="padding-top: 20px; display: block; background-color: #007ec7;">
            <a href="#" class="close" aria-label="close" onclick="closeNotif()"><i class="fa fa-times"></i></a>
            <div style="text-align: center; font-size: large; font-weight: bold;"><?=$this->session->flashdata('display')?></div>
        </div>
    </div>

<?php } ?>
<?php if($this->session->flashdata('error')) {  ?>
    
    <div>
        <div class="alert alert-danger alert-dismissible" style="padding-top: 10px; display: block;">
            <a href="#" class="close" aria-label="close" onclick="closeNotif()"><i class="fa fa-times"></i></a>
            <div style="text-align: center; font-size: large; font-weight: bold;"><?=$this->session->flashdata('error') ?></div>
        </div>
    </div>
    
<?php } ?>



<script>

function closeNotif() {
    $('.alert').fadeOut();
}

</script>