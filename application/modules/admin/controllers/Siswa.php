<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";
			redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
		}
		
		$this->load->model('Msiswa');
		$this->load->model('Mortu');
		$this->load->model('Mlevel');
		$this->load->model('siswa/Mkeluarga');
		$this->load->model('siswa/Mkelahiran');
		$this->load->model('siswa/Mkesehatan');
		$this->load->model('siswa/Mperkembangan');
		$this->load->model('siswa/Mpertanyaan');
		$this->load->model('siswa/Mpindahan');
	}

	public function index()
	{
		$role = $this->session->userdata('id_role');
		
		if($role != 1 && $role != 2) {
			$notif = "Anda tidak memiliki akses ke halaman ini!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("admin/dashboard?notif_$tipe=1&notification=$notif"));
			redirect(base_url("admin/dashboard"));
		}
		
        $data['title'] = 'Siswa - Sekolah Alam Indonesia';
		$data['judul'] = 'Siswa';

		$lvl = $this->input->get('lvl');
		$siswa = $this->Msiswa->getData(1, $lvl);

		$i = 0;
		foreach($siswa as $a) {
			//$a["nom"] = $start + $i;

			$a["nama_ayah"] = "";
			$a["nik_ayah"] = "";
			$a["tempat_lahir_ayah"] = "";
			$a["tgl_lahir_ayah"] = "";
			$a["pekerjaan_ayah"] = "";
			$a["hp_ayah"] = "";

			$a["nama_ibu"] = "";
			$a["nik_ibu"] = "";
			$a["tempat_lahir_ibu"] = "";
			$a["tgl_lahir_ibu"] = "";
			$a["pekerjaan_ibu"] = "";
			$a["hp_ibu"] = "";

			$a["nama_wali"] = "";
			$a["nik_wali"] = "";
			$a["tempat_lahir_wali"] = "";
			$a["tgl_lahir_wali"] = "";
			$a["pekerjaan_wali"] = "";
			$a["hp_wali"] = "";

			if($a["status_ortu"] == 'Ayah') {
				$a["nama_ayah"] = $a["nama_ortu"];
				$a["nik_ayah"] = $a["nik_ortu"];
				$a["tempat_lahir_ayah"] = $a["tempat_lahir_ortu"];
				$a["tgl_lahir_ayah"] = $a["tgl_lahir_ortu"];
				$a["pekerjaan_ayah"] = $a["pekerjaan"];
				$a["hp_ayah"] = $a["hp_ortu"];

				$ibu = $this->Mortu->findPasangan($a["id_ortu"]);

				if($ibu) {
					$a["nama_ibu"] = $ibu["nama_ortu"];
					$a["nik_ibu"] = $ibu["nik_ortu"];
					$a["tempat_lahir_ibu"] = $ibu["tempat_lahir"];
					$a["tgl_lahir_ibu"] = $ibu["tgl_lahir"];
					$a["pekerjaan_ibu"] = $ibu["pekerjaan"];
					$a["hp_ibu"] = $ibu["hp"];
				}

			} else if($a["status_ortu"] == 'Ibu') {
				$a["nama_ibu"] = $a["nama_ortu"];
				$a["nik_ibu"] = $a["nik_ortu"];
				$a["tempat_lahir_ibu"] = $a["tempat_lahir_ortu"];
				$a["tgl_lahir_ibu"] = $a["tgl_lahir_ortu"];
				$a["pekerjaan_ibu"] = $a["pekerjaan"];
				$a["hp_ibu"] = $a["hp_ortu"];

				$ayah = $this->Mortu->findPasangan($a["id_ortu"]);

				if($ayah) {
					$a["nama_ayah"] = $ayah["nama_ortu"];
					$a["nik_ayah"] = $ayah["nik_ortu"];
					$a["tempat_lahir_ayah"] = $ayah["tempat_lahir"];
					$a["tgl_lahir_ayah"] = $ayah["tgl_lahir"];
					$a["pekerjaan_ayah"] = $ayah["pekerjaan"];
					$a["hp_ayah"] = $ayah["hp"];
				}

			} else if($a["status_ortu"] == 'Wali') {
				$a["nama_wali"] = $a["nama_ortu"];
				$a["nik_wali"] = $a["nik_ortu"];
				$a["tempat_lahir_wali"] = $a["tempat_lahir_ortu"];
				$a["tgl_lahir_wali"] = $a["tgl_lahir_ortu"];
				$a["pekerjaan_wali"] = $a["pekerjaan"];
				$a["hp_wali"] = $a["hp_ortu"];
			}

			$siswa[$i] = $a;
			$i++;
		}

		$data['siswa'] = $siswa;
		$data['levels'] = $this->Mlevel->getLevelOption();
		$data['levels_all'] = $this->Mlevel->getLevelOptionAll();

		$lvl = $this->input->get('lvl');
		$data['level_default'] = ($lvl) ? $lvl : '';
		$data['ortus'] = $this->Mortu->listKepala();
		// $data['thumb_url'] = ($siswa['before_2024']) ? '' : 'thumb/';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$this->template->build('siswa/index', $data);
	}

	public function siswa_lama()
	{
		$role = $this->session->userdata('id_role');
		
		if($role != 1 && $role != 2) {
			$notif = "Anda tidak memiliki akses ke halaman ini!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("admin/dashboard?notif_$tipe=1&notification=$notif"));
			redirect(base_url("admin/dashboard"));
		}
		
        $data['title'] = 'Siswa - Sekolah Alam Indonesia';
		$data['judul'] = 'Siswa';

		$siswa = $this->Msiswa->getData();
		$data['siswa'] = $siswa;

		$data['levels'] = $this->Mlevel->getLevelOption();
		$data['ortus'] = $this->Mortu->listKepala();

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$this->template->build('siswa/siswa_lama', $data);
	}

	public function view($id) {
        $data['title'] = 'Siswa - Sekolah Alam Indonesia';
		$data['judul'] = 'Siswa';
		
		$siswa = $this->Msiswa->find($id);
		$data['nama'] = $siswa['nama_siswa'];
		$data['siswa'] = $siswa;
		$data['saudara'] = $this->Msiswa->getSaudara($id);
		$data['keluarga'] = $this->Mkeluarga->findSiswa($id);
		$data['kelahiran'] = $this->Mkelahiran->findSiswa($id);
		$data['kesehatan'] = $this->Mkesehatan->findSiswa($id);
		$sub_kesehatan = $this->Mkesehatan->getSubKesehatan($id);
		$data['sub_kesehatan'] = $sub_kesehatan;
		$data['perkembangan'] = $this->Mperkembangan->findSiswa($id);
		$data['pertanyaan'] = $this->Mpertanyaan->findSiswa($id);
		$data['pindahan'] = $this->Mpindahan->findSiswa($id);
		$sub_pindahan = $this->Mpindahan->getSubPindahan($id);
		$data['sub_pindahan'] = $sub_pindahan;
		$terapi = $this->Msiswa->get_terapi($id);
		$data['terapi'] = $terapi;
		$level = $siswa['level'];
		$grup_lvl = '';
		if($level == 'kb' || $level == 'tka' || $level == 'tkb') {
			$grup_lvl = 'kbtk';
		} else if($level == 'sd1' || $level == 'sd2' || $level == 'sd3' || $level == 'sd4' || $level == 'sd5' || $level == 'sd6') {
			$grup_lvl = 'sd';
		} else if($level == 'sl7' || $level == 'sl8' || $level == 'sl9') {
			$grup_lvl = 'sl';
		} else if($level == 'bless') {
			$grup_lvl = 'bless';
		} else if($level == 'istcsd' || $level == 'istcsl') {
			$grup_lvl = 'istc';
		} 
		$data['grup_lvl'] = $grup_lvl;
		$data['thumb_url'] = ($siswa['before_2024']) ? '' : 'thumb/';
		
		$this->template->build('siswa/view_siswa', $data);
	}

	public function set_siswa_edit() {
		$id = $this->input->get('id');
		$res = $this->Msiswa->find($id);

		echo json_encode($res);
	}

	public function edit_siswa() {
		$id = $this->input->post('id_edit');
		$nama_siswa = $this->input->post('nama');
		$kelamin = $this->input->post('kelamin');
		$level = $this->input->post('level');
		
		$siswa = array(	"nama_siswa" => $nama_siswa,
						"kelamin" => $kelamin,
						"level" => $level,
					);

		$res = $this->Msiswa->update($id, $siswa);

		if($res) {
			$notif = "Data Siswa berhasil diubah!";
			$tipe = "display";
		} else {
			$notif = "Data Siswa gagal diubah!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/siswa/index?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/siswa"));
	}

	public function list() {
		$start = (int)$this->input->get('start') + 1;
        $length = (int)$this->input->get('length');
        $order = $this->input->get('order[0][column]');
		$dir = $this->input->get('order[0][dir]');
		$search = $this->input->get('search[value]');
		
		$siswa = $this->Msiswa->getList($start-1, $length, $order, $dir, $search);

		$i = 0;
		foreach($siswa->data as $a) {
			$a["nom"] = $start + $i;

			$a["nama_ayah"] = "";
			$a["nik_ayah"] = "";
			$a["tempat_lahir_ayah"] = "";
			$a["tgl_lahir_ayah"] = "";
			$a["pekerjaan_ayah"] = "";
			$a["hp_ayah"] = "";

			$a["nama_ibu"] = "";
			$a["nik_ibu"] = "";
			$a["tempat_lahir_ibu"] = "";
			$a["tgl_lahir_ibu"] = "";
			$a["pekerjaan_ibu"] = "";
			$a["hp_ibu"] = "";

			$a["nama_wali"] = "";
			$a["nik_wali"] = "";
			$a["tempat_lahir_wali"] = "";
			$a["tgl_lahir_wali"] = "";
			$a["pekerjaan_wali"] = "";
			$a["hp_wali"] = "";

			if($a["status_ortu"] == 'Ayah') {
				$a["nama_ayah"] = $a["nama_ortu"];
				$a["nik_ayah"] = $a["nik_ortu"];
				$a["tempat_lahir_ayah"] = $a["tempat_lahir_ortu"];
				$a["tgl_lahir_ayah"] = $a["tgl_lahir_ortu"];
				$a["pekerjaan_ayah"] = $a["pekerjaan"];
				$a["hp_ayah"] = $a["hp_ortu"];

				$ibu = $this->Mortu->findPasangan($a["id_ortu"]);

				if($ibu) {
					$a["nama_ibu"] = $ibu["nama_ortu"];
					$a["nik_ibu"] = $ibu["nik"];
					$a["tempat_lahir_ibu"] = $ibu["tempat_lahir"];
					$a["tgl_lahir_ibu"] = $ibu["tgl_lahir"];
					$a["pekerjaan_ibu"] = $ibu["pekerjaan"];
					$a["hp_ibu"] = $ibu["hp"];
				}

			} else if($a["status_ortu"] == 'Ibu') {
				$a["nama_ibu"] = $a["nama_ortu"];
				$a["nik_ibu"] = $a["nik_ortu"];
				$a["tempat_lahir_ibu"] = $a["tempat_lahir_ortu"];
				$a["tgl_lahir_ibu"] = $a["tgl_lahir_ortu"];
				$a["pekerjaan_ibu"] = $a["pekerjaan"];
				$a["hp_ibu"] = $a["hp_ortu"];

				$ayah = $this->Mortu->findPasangan($a["id_ortu"]);

				if($ayah) {
					$a["nama_ayah"] = $ayah["nama_ortu"];
					$a["nik_ayah"] = $ayah["nik"];
					$a["tempat_lahir_ayah"] = $ayah["tempat_lahir"];
					$a["tgl_lahir_ayah"] = $ayah["tgl_lahir"];
					$a["pekerjaan_ayah"] = $ayah["pekerjaan"];
					$a["hp_ayah"] = $ayah["hp"];
				}

			} else if($a["status_ortu"] == 'Wali') {
				$a["nama_wali"] = $a["nama_ortu"];
				$a["nik_wali"] = $a["nik_ortu"];
				$a["tempat_lahir_wali"] = $a["tempat_lahir_ortu"];
				$a["tgl_lahir_wali"] = $a["tgl_lahir_ortu"];
				$a["pekerjaan_wali"] = $a["pekerjaan"];
				$a["hp_wali"] = $a["hp_ortu"];
			}

			$siswa->data[$i] = $a;
			$i++;
		}

		$data = $siswa->data;
        $total = $siswa->jml;

        $tableContent = array (
            "draw" => $this->input->get('draw'), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => $total, // total number of records
            "recordsFiltered" => $total, // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
            "start" => $start,
            "length" => $length
        );

        echo json_encode($tableContent, JSON_PRETTY_PRINT);
	}

	public function addSiswa(){

		$id_ortu = $this->input->post('id_ortu');
		$nama_siswa = $this->input->post('nama_siswa');
		$level = $this->input->post('level');
		$username = $this->session->userdata('username');

		$siswa = array(	"nama_siswa" => $nama_siswa,
						"level" 	 => $level,
						"created_by" => $username,
					  );

		$tambah = $this->Msiswa->addSiswa($id_ortu, $siswa);

		if($tambah) {
			$notif = "Penambahan Siswa berhasil disimpan !";
			$tipe = "display";
		} else {
			$notif = "Pendaftaran Siswa Gagal! Silahkan coba lagi";
			$tipe = "error";
		}
		
		$this->session->set_flashdata($tipe, $notif);

		//redirect(base_url("admin/siswa/index?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/siswa"));
	}
}