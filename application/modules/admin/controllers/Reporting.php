<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporting extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("admin/login"));
       	}
	}

	public function index()
	{
        $data['title'] = 'Reporting - Sekolah Alam Indonesia';
        $data['judul'] = 'Reporting';
		$this->template->build('reporting/index', $data);
	}
}