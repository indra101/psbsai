<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->library('template');
		$this->template->set_layout('backend/login');
		
		$this->load->model('Mlogin');
	}

	public function index()
	{
		$data['title'] = 'Administrator - Sekolah Alam Indonesia';
		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$this->template->build('login/index', $data);
	}

	function cekLoginAdmin() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$encrypted = script($password, 'e');
		$loginUser = $this->Mlogin->cekLoginAdmin($email, $encrypted);

		if (!empty($loginUser)) {
			// sukses login user
			$this->session->set_userdata($loginUser);
			redirect('admin/dashboard');
		} else {
			// gagal login
			$notif = "Maaf Email/Password Anda salah!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);
			redirect(base_url("admin/login/index"));
		}
    }
    

    function logout() {
        $this->session->sess_destroy();
        redirect('admin/login');
	}
	
}