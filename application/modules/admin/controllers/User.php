<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("admin/login"));
		}

		$role = $this->session->userdata('id_role');

		if($role != 1) {
			$notif = "Anda tidak memiliki akses ke halaman ini!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("admin/dashboard?notif_$tipe=1&notification=$notif"));
			redirect(base_url("admin/dashboard"));
		}
		
		$this->load->model('Muser');
		$this->load->model('Mlogin');
		//$this->load->model('Mrole');
	}

	public function index()
	{
        $data['title'] = 'User - Sekolah Alam Indonesia';
		$data['judul'] = 'User';

		$user = $this->Muser->getData();

		$i = 0;
		foreach($user->data as $a) {
			$a["password"] = script($a['password'], 'd' );
			$user->data[$i] = $a;
			$i++;
		}

		$user = $user->data;

		$data['user'] = $user;
		
		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$this->template->build('user/index', $data);
	}

	public function cek_username() {
		$username = $this->input->get('username');
		$res = $this->Muser->cekUsername($username);

		echo $res;
	}

	public function set_user_edit() {
		$id = $this->input->get('id');
		$res = $this->Muser->find($id);

		echo json_encode($res);
	}

	public function edit_user() {
		$id = $this->input->post('id_edit');
		$nama = $this->input->post('nama_edit');
		$username = $this->input->post('username_edit');
		$email = $this->input->post('email_edit');
		$hp = $this->input->post('hp_edit');
		$aktif = $this->input->post('aktif_edit');
		$status = $this->input->post('status_edit');

		if($status == 'Administrator') {
			$role = 1;
		} else if($status == 'Admin PPSB') {
			$role = 2;
		} else if($status == 'Guru') {
			$role = 3;
		} else if($status == 'Orang Tua') {
			$role = 4;
		}

		$user = array(	"nama" => $nama,
						"email" => $email,
						"hp" => $hp,
						"status" => $status,
						"id_role" => $role,
						"aktif" => $aktif,
						);

		$res = $this->Muser->update($id, $user);

		if($res) {
			$notif = "Data user berhasil disimpan!";
			$tipe = "display";
		} else {
			$notif = "Gagal simpan data user!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/user?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/user"));
	}

	public function create_user() {
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$hp = $this->input->post('hp');
		$status = $this->input->post('status');
		//$password = $this->input->post('level');
		$password_enc = script('123456', 'e' ); // password standar

		if($status == 'Administrator') {
			$role = 1;
		} else if($status == 'Admin PPSB') {
			$role = 2;
		} else if($status == 'Guru') {
			$role = 3;
		} else if($status == 'Orang Tua') {
			$role = 4;
		}

		$user = array(	"nama" => $nama,
						"password" => $password_enc,
						"username" => $username,
						"email" => $email,
						"hp" => $hp,
						"status" => $status,
						"id_role" => $role,
						"aktif" => 1,
						);

		$res = $this->Muser->create($user);

		if($res) {
			$notif = "User baru berhasil disimpan!<br>Password: 123456";
			$tipe = "display";
		} else {
			$notif = "Gagal simpan user baru!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/user?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/user"));
	}

	public function reset_pass() {
		$id = $this->input->post('id');
		$password_enc = script('123456', 'e' ); 

		$user = array("password" => $password_enc);

		$res = $this->Muser->update($id, $user);

		if($res) {
			$notif = "Password berhasil diubah menjadi 123456";
			$tipe = "display";
		} else {
			$notif = "Gagal reset password!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/user?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/user"));
		
	}

	public function ganti_pass() {
		$id = $this->session->userdata('id');
		$email = $this->session->userdata('email');
		$password_lama = $this->input->post('password_lama');
		$password_baru = $this->input->post('password_baru');
		$password_enc = script($password_baru, 'e' ); 

		$encrypted = script($password_lama, 'e');
		$loginUser = $this->Mlogin->cekLoginAdmin($email, $encrypted);

		if (!empty($loginUser)) {
			// sukses login user
			$user = array("password" => $password_enc);

			$res = $this->Muser->update($id, $user);

			if($res) {
				$notif = "Password berhasil diubah!";
				$tipe = "display";
			} else {
				$notif = "Gagal ubah password!";
				$tipe = "error";
			}
			
			
		} else {
			// gagal login
			$notif = "Maaf password yang Anda masukkan salah!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);

		//redirect(base_url("admin/dashboard?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/dashboard"));
		
	}

	public function list() {
		$start = (int)$this->input->get('start') + 1;
        $length = (int)$this->input->get('length');
        $order = $this->input->get('order[0][column]');
		$dir = $this->input->get('order[0][dir]');
		$search = $this->input->get('search[value]');
		
		$user = $this->Muser->getList($start-1, $length, $order, $dir, $search);

		$i = 0;
		foreach($user->data as $a) {
			$a["nom"] = $start + $i;
			$a["password"] = script($a['password'], 'd' );
			$user->data[$i] = $a;
			$i++;
		}

		$data = $user->data;
        $total = $user->jml;

        $tableContent = array (
            "draw" => $this->input->get('draw'), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => $total, // total number of records
            "recordsFiltered" => $total, // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
            "start" => $start,
            "length" => $length
        );

        echo json_encode($tableContent, JSON_PRETTY_PRINT);
	}

	public function delete() {
		$id = $this->input->post('id_delete');
		// $res = $this->Muser->delete($id);

		$data = array("is_deleted" => 1);
		$res = $this->Muser->update($id, $data);

		if($res) {
			$notif = "User berhasil dihapus!";
			$tipe = "display";
		} else {
			$notif = "Gagal hapus user!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/user?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/user"));
	}
}