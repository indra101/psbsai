<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";
			redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
	   }
	   
	   $this->load->model('Mortu');
	   $this->load->model('Msiswa');
	   $this->load->model('Mtahapan');
	   $this->load->model('Mapp_setting');
	}

	public function index()
	{
		$data['title'] = 'Dashboard - Sekolah Alam Indonesia';
		$data['judul'] = 'Dashboard';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$data['total_ortu'] = $this->Mortu->getTotalKepala(1);
		$data['total_siswa'] = $this->Msiswa->getTotal(1);

		$siswa = $this->Msiswa->getJmlPerLevel();
		$lvl = array();
		$i = 0;
		foreach($siswa as $s) {
			$lvl[$s['level']] = $s['jml'];
		}

		$data['lvl'] = $lvl;

		$data['tahapan'] = $this->Mtahapan->getData();

		$setting = $this->Mapp_setting->find(1);
		$data['tahun_ajaran'] = $setting['tahun_ajaran'];
		
		$this->template->build('dashboard/index', $data);
	}
}