<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Riwayat extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";
			redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
		}
		
		$this->load->model('Msiswa');
		$this->load->model('Mortu');
		$this->load->model('Mlevel');
		$this->load->model('Mtahun_ajar');
	}

	public function index()
	{
		$role = $this->session->userdata('id_role');
		
		if($role != 1 && $role != 2 && $role != 3) {
			$notif = "Anda tidak memiliki akses ke halaman ini!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			redirect(base_url("admin/dashboard"));
		}
		
        $data['title'] = 'Riwayat Pendaftaran - Sekolah Alam Indonesia';
		$data['judul'] = 'Riwayat Pendaftaran';
		
		//$siswa = $this->Msiswa->getData(1);
		$data['levels'] = $this->Mlevel->getLevelOptionAll();
		$data['tahun_ajars'] = $this->Mtahun_ajar->getOption();

		$this->template->build('riwayat/index', $data);
	}

	public function list() {
		$start = (int)$this->input->get('start') + 1;
        $length = (int)$this->input->get('length');
        $order = $this->input->get('order[0][column]');
		$dir = $this->input->get('order[0][dir]');
		$search = $this->input->get('search[value]');

		$tahun_ajar = $this->input->get('tahun_ajar');
		$level = $this->input->get('level');
		
		$siswa = $this->Msiswa->getRiwayat($start-1, $length, $order, $dir, $tahun_ajar, $level);

		$i = 0;
		foreach($siswa->data as $a) {
			$a["nom"] = $start + $i;

			$siswa->data[$i] = $a;
			$i++;
		}

		$data = $siswa->data;
        $total = $siswa->jml;

        $tableContent = array (
            "draw" => $this->input->get('draw'), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => $total, // total number of records
            "recordsFiltered" => $total, // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
            "start" => $start,
            "length" => $length
        );

        echo json_encode($tableContent, JSON_PRETTY_PRINT);
	}

}