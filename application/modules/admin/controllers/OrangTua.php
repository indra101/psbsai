<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrangTua extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";
			redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
		}

		$this->load->model('Mortu');
		$this->load->model('Msiswa');
	}

	public function index() {
		$role = $this->session->userdata('id_role');

		if($role != 1 && $role != 2) {
			$notif = "Anda tidak memiliki akses ke halaman ini!";
			$tipe = "error";
			redirect(base_url("admin/dashboard?notif_$tipe=1&notification=$notif"));
		}
		
        $data['title'] = 'Orang Tua - Sekolah Alam Indonesia';
		$data['judul'] = 'Orang Tua';
		
		$ortu = $this->Mortu->getData(1);
		$i = 0;

		foreach($ortu->data as $a) {
			$lengkap = true;
			$pernyataan = true;
			$anak = $this->Msiswa->getByOrtu($a["id"], 1);
			$n = 1;

			if(!$a["is_kepala"])
				$a["id"] = $a["id_kepala"];

			$a["anak"] = "";

			foreach($anak as $b) {
				$a["anak"] .= "$n. ".$b['nama_siswa']." - ".$b['nama_level']."<br>";
				// $complete = getStatusComplete($b['id'],$b['level']);
				// $lengkap = $lengkap && $complete;

				$pernyataan = $pernyataan && !empty($b['pernyataan']);
				
				$n++;
			}

			$a["kode_unik"] = script($a['password'], 'd' );
			
			if($a["is_kepala"]) {
				$lengkap = getStatusCompleteOrtu($a['id']);
				$stat_lengkap = ($lengkap) ? "Lengkap" : "Belum Lengkap";

				$lengkap_anak = getStatusCompleteAllSiswa($a['id']);
				$stat_lengkap_anak = ($lengkap_anak) ? "Lengkap" : "Belum Lengkap";

				$stat_pernyataan = ($pernyataan) ? "Sudah" : "Belum";
			} else {
				$stat_lengkap = '-';
				$stat_lengkap_anak = '-';
				$stat_pernyataan = '-';
			}

			$a["lengkap"] = $stat_lengkap;
			$a["lengkap_anak"] = $stat_lengkap_anak;
			$a["pernyataan"] = $stat_pernyataan;

			$ortu->data[$i] = $a;
			$i++;
		}

		$data['ortu'] = $ortu->data;

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$this->template->build('orangtua/index', $data);
	}

	public function ortu_lama() {
		$role = $this->session->userdata('id_role');

		if($role != 1 && $role != 2) {
			$notif = "Anda tidak memiliki akses ke halaman ini!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("admin/dashboard?notif_$tipe=1&notification=$notif"));
			redirect(base_url("admin/dashboard"));
		}
		
        $data['title'] = 'Orang Tua - Sekolah Alam Indonesia';
		$data['judul'] = 'Orang Tua';
		
		$ortu = $this->Mortu->getData();
		$i = 0;
		foreach($ortu->data as $a) {
			$lengkap = true;
			$anak = $this->Msiswa->getByOrtu($a["id"]);
			$n = 1;
			if(!$a["is_kepala"])
				$a["id"] = $a["id_kepala"];
			$a["anak"] = "";
			foreach($anak as $b) {
				$a["anak"] .= "$n. ".$b['nama_siswa']." - ".$b['nama_level']."<br>";
				$complete = getStatusComplete($b['id'],$b['level']);
				$lengkap = $lengkap && $complete;
				$n++;
			}
			$a["kode_unik"] = script($a['password'], 'd' );
			
			if($a["is_kepala"]) {
				$stat_lengkap = ($lengkap) ? "Lengkap" : "Belum Lengkap";
			} else {
				$stat_lengkap = '-';
			}
			$a["lengkap"] = $stat_lengkap;

			$ortu->data[$i] = $a;
			$i++;
		}

		$data['ortu'] = $ortu->data;

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$this->template->build('orangtua/ortu_lama', $data);
	}

	public function view($id) {
        $data['title'] = 'Orang Tua - Sekolah Alam Indonesia';
		$data['judul'] = 'Orang Tua';
		
		$ortu = $this->Mortu->find($id);
		$data['nama'] = $this->Mortu->getNamaById($id);
		$data['ayah'] = $this->Mortu->findAyah($id, $ortu['is_kepala'], $ortu['status']);
		$data['ibu'] = $this->Mortu->findIbu($id, $ortu['is_kepala'], $ortu['status']);
		$data['wali'] = $this->Mortu->findWali($id, $ortu['is_kepala'], $ortu['status']);
		$data['anak'] = $this->Msiswa->getByOrtu($id);

		$data['id_kepala'] = $id;
		$data['status_kepala'] = $ortu['status'];

		$this->template->build('orangtua/view_ortu', $data);
	}

	public function set_ortu_edit() {
		$id = $this->input->get('id');
		$res = $this->Mortu->find($id);

		echo json_encode($res);
	}

	public function edit_ortu() {
		$id = $this->input->post('id_edit');
		$nama_ortu = $this->input->post('nama_edit');
		$kelamin = $this->input->post('kelamin');
		$email = $this->input->post('email');
		$hp = $this->input->post('hp');
		$status = $this->input->post('status');

		$ortu = array(	"nama_ortu" => $nama_ortu,
						"kelamin" => $kelamin,
						"email" => $email,
						"hp" => $hp,
						"status" => $status,
					);

		$res = $this->Mortu->update($id, $ortu);

		if($res) {
			$notif = "Data Orang Tua berhasil diubah!";
			$tipe = "display";
		} else {
			$notif = "Data Orang Tua gagal diubah!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/orangTua/index?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/orangTua"));
	}

	public function list() {
		$start = (int)$this->input->get('start') + 1;
        $length = (int)$this->input->get('length');
        $order = $this->input->get('order[0][column]');
		$dir = $this->input->get('order[0][dir]');
		$search = $this->input->get('search[value]');
		
		$ortu = $this->Mortu->getList($start-1, $length, $order, $dir, $search);

		$i = 0;
		foreach($ortu->data as $a) {
			$a["nom"] = $start + $i;
			$lengkap = true;
			$anak = $this->Msiswa->getByOrtu($a["id"]);
			$n = 1;
			if(!$a["is_kepala"])
				$a["id"] = $a["id_kepala"];
			$a["anak"] = "";
			foreach($anak as $b) {
				$a["anak"] .= "$n. ".$b['nama_siswa']." - ".$b['nama_level']."<br>";
				$complete = getStatusComplete($b['id'],$b['level']);
				$lengkap = $lengkap && $complete;
				$n++;
			}
			$a["kode_unik"] = script($a['password'], 'd' );

			if($a["is_kepala"]) {
				$stat_lengkap = ($lengkap) ? "Lengkap" : "Belum Lengkap";
			} else {
				$stat_lengkap = '-';
			}
			$a["lengkap"] = $stat_lengkap;

			$ortu->data[$i] = $a;
			$i++;
		}

		// print_r($ortu->data);
		// die;

		$data = $ortu->data;
        $total = $ortu->jml;

        $tableContent = array (
            "draw" => $this->input->get('draw'), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => $total, // total number of records
            "recordsFiltered" => $total, // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
            "start" => $start,
            "length" => $length
        );

        echo json_encode($tableContent, JSON_PRETTY_PRINT);
	}
}