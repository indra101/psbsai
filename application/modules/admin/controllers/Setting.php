<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";
			redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
	   }
	   
	   $this->load->model('Mapp_setting');
	   $this->load->model('Mtahun_ajar');
	}

	public function index()
	{
		$data['title'] = 'Pengaturan - Sekolah Alam Indonesia';
		$data['judul'] = 'Pengaturan';

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$setting = $this->Mapp_setting->find(1);
		$data['setting'] = $setting;

		$data['tahun_ajars'] = $this->Mtahun_ajar->getOption();

		$data['stat_pendaftarans'] = array(
					"open" => "Open",
					"close" => "Close",
					"coming" => "Coming Soon",
		);
		
		$this->template->build('setting/index', $data);
	}

	public function simpan() {

		$setting = $this->Mapp_setting->find(1);

		// $unit = $this->input->post('unit');
		// $unit_url = $this->input->post('unit_url');
		// $unit_url_psb = $this->input->post('unit_url_psb');
		$admin_nama = $this->input->post('admin_nama');
		$admin_hp = $this->input->post('admin_hp');
		$admin_wa = $this->input->post('admin_wa');
		$rekening_bank = $this->input->post('rekening_bank');
		$rekening_no = $this->input->post('rekening_no');
		$rekening_nama = $this->input->post('rekening_nama');
		$tahun_ajaran = $this->input->post('tahun_ajaran');
		$stat_pendaftaran = $this->input->post('stat_pendaftaran');

		$data = array(
					// "unit" 			=> $unit,
					// "unit_url" 		=> $unit_url,
					// "unit_url_psb" 	=> $unit_url_psb,
					"admin_nama" 	=> $admin_nama,
					"admin_hp" 		=> $admin_hp,
					"admin_wa" 		=> $admin_wa,
					"rekening_bank" => $rekening_bank,
					"rekening_no" 	=> $rekening_no,
					"rekening_nama" => $rekening_nama,
					"tahun_ajaran" 	=> $tahun_ajaran,
					"stat_pendaftaran" 	=> $stat_pendaftaran,
		);

		$res = $this->Mapp_setting->update(1, $data);

		if($res) {
			$notif = "Data Pengaturan Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Data Pengaturan Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);

		redirect(base_url("admin/setting"));
	}
}