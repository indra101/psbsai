<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("admin/login"));
		}
		
		$this->load->model('Mlevel');
	}

	public function index()
	{
        $data['title'] = 'Level - Sekolah Alam Indonesia';
		$data['judul'] = 'Level';
		
		$data['level'] = $this->Mlevel->getData();

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$this->template->build('level/index', $data);
	}

	public function create(){
		$aktif = $this->input->post('aktif');
		$biaya = $this->input->post('biaya');

		$level = array(	"aktif" => $aktif,
						"biaya" => $biaya,
					  );

		$rlevel = $this->Mlevel->create($level);

		if($rlevel) {
			$notif = "Jenjang Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Jenjang Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/level/index?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/level"));
	}

	public function update(){
		$id = $this->input->post('id');
		$aktif = $this->input->post('aktif');
		$biaya = str_replace('.', '', $this->input->post('biaya'));

		$level = array(	"aktif" => $aktif,
						"biaya" => $biaya,
					  );

		$rlevel = $this->Mlevel->update($id,$level);

		if($rlevel) {
			$notif = "Data Berhasil Diubah";
			$tipe = "display";
		} else {
			$notif = "Data Gagal Diubah";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/level/index?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/level"));
	}

	public function delete(){
		$id = $this->input->post('id');
		$rlevel = $this->Mlevel->delete($id);

		if($rlevel) {
			$notif = "Jenjang Berhasil Dihapus";
			$tipe = "display";
		} else {
			$notif = "Jenjang Gagal Dihapus";
			$tipe = "error";
		}
		
		$this->session->set_flashdata($tipe, $notif);

		//redirect(base_url("admin/level/index?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/level"));
	}
}