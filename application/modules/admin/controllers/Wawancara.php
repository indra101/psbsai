<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wawancara extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("admin/login"));
		}
		   
		$this->load->model('Mortu');
		$this->load->model('Msiswa');
		$this->load->model('Muser');
		$this->load->model('Mwawancara');
	}

	public function index()
	{
        $data['title'] = 'Wawancara - Sekolah Alam Indonesia';
		$data['judul'] = 'Wawancara';
		
		$data['ortus'] = $this->Mortu->listKepala(1);
		$data['users'] = $this->Muser->userWawancara();
		$data['jadwal'] = $this->list_jadwal();
		$id = $this->session->userdata('id');
		$data['interviewee'] = $this->Mwawancara->getIdInterviewee($id);
		// print_r($data['interviewee']);
		// die;

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$this->template->build('wawancara/index', $data);
	}

	public function riwayat()
	{
        $data['title'] = 'Wawancara - Sekolah Alam Indonesia';
		$data['judul'] = 'Wawancara';
		
		$data['ortus'] = $this->Mortu->listKepala(1);
		$data['users'] = $this->Muser->userWawancara();
		$data['jadwal'] = $this->list_jadwal();
		$id = $this->session->userdata('id');
		$data['interviewee'] = $this->Mwawancara->getIdInterviewee($id);
		// print_r($data['interviewee']);
		// die;

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$this->template->build('wawancara/riwayat', $data);
	}

	public function tambah_jadwal() {
		$tanggal = $this->input->post('tanggal');
		$waktu_mulai = $this->input->post('waktu_mulai');
		$waktu_selesai = $this->input->post('waktu_selesai');
		$id_ortu = $this->input->post('id_ortu');
		$interviewer1 = $this->input->post('interviewer1');
		$interviewer2 = $this->input->post('interviewer2');
		$interviewer3 = $this->input->post('interviewer3');

		$jadwal = array("tanggal" => $tanggal,
						"waktu_mulai" => $waktu_mulai,
						"waktu_selesai" => $waktu_selesai,
						"id_ortu" => $id_ortu,
						"interviewer1" => $interviewer1,
						"interviewer2" => $interviewer2,
						"interviewer3" => $interviewer3,
						"aktif" => 1,
						);

		$res = $this->Mwawancara->create($jadwal);

		if($res) {
			$notif = "Jadwal berhasil disimpan!";
			$tipe = "display";
		} else {
			$notif = "Gagal simpan jadwal!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/wawancara?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/wawancara"));
	}

	public function edit_jadwal() {
		$id = $this->input->post('id_edit');
		$tanggal = $this->input->post('tanggal');
		$waktu_mulai = $this->input->post('waktu_mulai');
		$waktu_selesai = $this->input->post('waktu_selesai');
		$id_ortu = $this->input->post('id_ortu');
		$interviewer1 = $this->input->post('interviewer1');
		$interviewer2 = $this->input->post('interviewer2');
		$interviewer3 = $this->input->post('interviewer3');

		$jadwal = array("tanggal" => $tanggal,
						"waktu_mulai" => $waktu_mulai,
						"waktu_selesai" => $waktu_selesai,
						"id_ortu" => $id_ortu,
						"interviewer1" => $interviewer1,
						"interviewer2" => $interviewer2,
						"interviewer3" => $interviewer3,
						"aktif" => 1,
						);

		$res = $this->Mwawancara->update($id, $jadwal);

		if($res) {
			$notif = "Jadwal berhasil disimpan!";
			$tipe = "display";
		} else {
			$notif = "Gagal simpan jadwal!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/wawancara?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/wawancara"));
	}

	public function simpan_catatan() {
		$id = $this->input->post('id_edit_catatan');
		$notes = $this->input->post('notes');
		$no_ruang = $this->input->post('no_ruang');
		
		$jadwal = array("notes$no_ruang" => $notes);

		$res = $this->Mwawancara->update($id, $jadwal);

		if($res) {
			$notif = "Catatan berhasil disimpan!";
			$tipe = "display";
		} else {
			$notif = "Gagal simpan catatan!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/wawancara?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/wawancara"));
	}

	public function list() {
		$i = 0;
		foreach($wawancara->data as $a) {
			$a["nom"] = $i + 1;
			$a["nama_ortu"] = $this->Mortu->getNamaById($a["id_ortu"]);
			$a["nama_pasangan"] = $this->Mortu->findPasangan($a["id_ortu"])['nama_ortu'];
			$siswa = $this->Msiswa->getByOrtu($a["id_ortu"], 1);
			$a["anak"] = "";
			$n = 1;
			foreach($siswa as $b) {
				$a["anak"] .= "$n. ".$b['nama_siswa']." - ".$b['nama_level']."<br>";
				$n++;
			}
			$a["nama_interviewer1"] = $this->Muser->getNamaById($a["interviewer1"]);
			$a["nama_interviewer2"] = $this->Muser->getNamaById($a["interviewer2"]);
			$a["nama_interviewer3"] = $this->Muser->getNamaById($a["interviewer3"]);
			$wawancara->data[$i] = $a;
			$i++;
		}

		$data = $wawancara->data;
        $total = $wawancara->jml;

        $tableContent = array (
            "draw" => $this->input->get('draw'), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => $total, // total number of records
            "recordsFiltered" => $total, // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
        );

        echo json_encode($tableContent, JSON_PRETTY_PRINT);
	}

	public function list_jadwal() {
		$role = $this->session->userdata('id_role');

		if($role == 1 || $role == 2) {
			$wawancara = $this->Mwawancara->getList();
		} else {
			$id = $this->session->userdata('id');
			$wawancara = $this->Mwawancara->getList($id);
		}

		$i = 0;
		foreach($wawancara->data as $a) {
			$a["nama_ortu"] = $this->Mortu->getNamaById($a["id_ortu"]);
			$pasangan = $this->Mortu->findPasangan($a["id_ortu"]);
			$a["nama_pasangan"] = (!empty($pasangan)) ? $pasangan['nama_ortu'] : null;
			$siswa = $this->Msiswa->getByOrtu($a["id_ortu"], 1);
			$a["anak"] = "";
			$n = 1;
			foreach($siswa as $b) {
				$a["anak"] .= "$n. ".$b['nama_siswa']." - ".$b['nama_level']."<br>";
				$n++;
			}
			$a["nama_interviewer1"] = $this->Muser->getNamaById($a["interviewer1"]);
			$a["nama_interviewer2"] = $this->Muser->getNamaById($a["interviewer2"]);
			$a["nama_interviewer3"] = $this->Muser->getNamaById($a["interviewer3"]);
			$wawancara->data[$i] = $a;
			$i++;
		}

		$data = $wawancara->data;
		
		return $data;
	}

	public function set_user_edit() {
		$id = $this->input->get('id');
		$res = $this->Mwawancara->find($id);

		$res['nama_interviewer1'] = $this->Muser->getNamaById($res['interviewer1']);
		$res['nama_interviewer2'] = $this->Muser->getNamaById($res['interviewer2']);
		$res['nama_interviewer3'] = $this->Muser->getNamaById($res['interviewer3']);

		echo json_encode($res);
	}

	public function delete() {
		$id = $this->input->post('id_delete');
		$res = $this->Mwawancara->delete($id);

		if($res) {
			$notif = "Jadwal berhasil dihapus!";
			$tipe = "display";
		} else {
			$notif = "Gagal hapus jadwal!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/wawancara?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/wawancara"));
	}
}