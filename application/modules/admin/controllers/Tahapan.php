<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahapan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		if(!$this->session->userdata('email')){
            $notif = "Silahkan login untuk mengakses!";
			$tipe = "error";

			$this->session->set_flashdata($tipe, $notif);

			//redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
			redirect(base_url("admin/login"));
		}
		
		$this->load->model('Mtahapan');
	}

	public function index()
	{
        $data['title'] = 'Tahapan - Sekolah Alam Indonesia';
		$data['judul'] = 'Tahapan';
		
		$data['tahapan'] = $this->Mtahapan->getData();

		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');
		$this->template->build('tahapan/index', $data);
	}

	public function create(){
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$tanggal_selesai = $this->input->post('tanggal_selesai');
		$keterangan = $this->input->post('keterangan');

		$tahapan = array("tanggal_mulai" =>  $tanggal_mulai,
						 "tanggal_selesai" => $tanggal_selesai,
						 "keterangan" => $keterangan
						);

		$rtahapan = $this->Mtahapan->create($tahapan);

		if($rtahapan) {
			$notif = "Tahapan Berhasil Disimpan";
			$tipe = "display";
		} else {
			$notif = "Tahapan Gagal Disimpan";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/tahapan/index?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/tahapan"));
	}

	public function update(){
		$id = $this->input->post('id');
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$tanggal_selesai = $this->input->post('tanggal_selesai');
		$keterangan = $this->input->post('keterangan');

		$tahapan = array("tanggal_mulai" =>  $tanggal_mulai,
						 "tanggal_selesai" => $tanggal_selesai,
						 "keterangan" => $keterangan
						);

		$rtahapan = $this->Mtahapan->update($id,$tahapan);

		if($rtahapan) {
			$notif = "Tahapan Berhasil Diubah";
			$tipe = "display";
		} else {
			$notif = "Tahapan Gagal Diubah";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		//redirect(base_url("admin/tahapan/index?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/tahapan"));
	}

	public function delete(){
		$id = $this->input->post('id');
		$rtahapan = $this->Mtahapan->delete($id);

		if($rtahapan) {
			$notif = "Tahapan Berhasil Dihapus";
			$tipe = "display";
		} else {
			$notif = "Tahapan Gagal Dihapus";
			$tipe = "error";
		}
		
		$this->session->set_flashdata($tipe, $notif);

		//redirect(base_url("admin/tahapan/index?notif_$tipe=1&notification=$notif"));
		redirect(base_url("admin/tahapan"));
	}
}