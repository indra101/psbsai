<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->template->set_layout('backend/default');
		// if(!$this->session->userdata('email')){
        //     $notif = "Silahkan login untuk mengakses!";
		// 	$tipe = "error";
		// 	redirect(base_url("admin/login/index?notif_$tipe=1&notification=$notif"));
		// }
		
		$role = $this->session->userdata('id_role');

		if($role != 1 && $role != 2) {
			$notif = "Anda tidak memiliki akses ke halaman ini!";
			$tipe = "error";
			redirect(base_url("admin/dashboard?notif_$tipe=1&notification=$notif"));
		}

		$this->load->model('Mortu');
		$this->load->model('Mortu_daftar');
		$this->load->model('Msiswa');
		$this->load->model('Msiswa_daftar');
		$this->load->model('Mlevel');
		$this->load->model('Memail_setting');
		$this->load->model('Mapp_setting');

		$this->session->keep_flashdata('display');
		$this->session->keep_flashdata('error');
	}

	public function index_old()
	{
        $data['title'] = 'Pendaftaran - Sekolah Alam Indonesia';
		$data['judul'] = 'Pendaftaran';
		
		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$data['levels'] = $this->Mlevel->getLevelOption();
		
		$this->template->build('pendaftaran/index', $data);
	}

	public function index()
	{
        $data['title'] = 'Pendaftaran - Sekolah Alam Indonesia';
		$data['judul'] = 'Pendaftaran';

		$ortu = $this->Mortu_daftar->getData();
		$i = 0;
		foreach($ortu->data as $a) {
			$lengkap = true;
			$anak = $this->Msiswa_daftar->getByOrtu($a["id"]);
			$n = 1;
			
			$a["anak"] = "";
			foreach($anak as $b) {
				$a["anak"] .= "$n. ".$b['nama']." - ".$b['tgl_lahir']." - ".$b['nama_level']."<br>";
				$n++;
			}

			$ortu->data[$i] = $a;
			$i++;
		}

		$data['ortu'] = $ortu->data;
		$data['ortu_lama'] = $this->Mortu->listKepala();
		
		$data['notif_display'] = $this->input->get('notif_display');
		$data['notif_error'] = $this->input->get('notif_error');
		$data['notification'] = $this->input->get('notification');

		$data['levels'] = $this->Mlevel->getLevelOption();
		
		$this->template->build('daftar_baru/index', $data);
	}

	public function approve() {

		$setting = $this->Mapp_setting->find(1);
		$username = $this->session->userdata('username');
		$id_ortu = $this->input->post('id_ortu');
		$is_ortu_lama = $this->input->post('is_ortu_lama');
		$id_ortu_lama = $this->input->post('id_ortu_lama');
		$kode_unik = rand(100000,999999);
		$kode_unik_enc = script($kode_unik, 'e' );

		$ortu_dft = $this->Mortu_daftar->find($id_ortu);
		$anak_dft = $this->Msiswa_daftar->getByOrtu($id_ortu);

		if(!$ortu_dft['verified']) {

			$ortu = array(	"nama_ortu" => $ortu_dft['nama'],
							"kelamin" => $ortu_dft['kelamin'],
							"password" => $kode_unik_enc,
							"email" => $ortu_dft['email'],
							"hp" => $ortu_dft['hp'],
							"status" => $ortu_dft['status'],
							"status_khusus" => "ada",
							"is_kepala" => 1,
							"is_daftar" => 1,
							"created_by" => $username,
							);

			$siswa = array();
			$n = 0;
			foreach($anak_dft as $s) {
				$new = array(	"nama_siswa" => $s['nama'],
								"level" => $s['level'],
								"tahun_ajar" => $setting['tahun_ajaran'],
								"kk" => $ortu_dft['kk'],
								"is_daftar" => 1,
								"created_by" => $username,
							);

				array_push($siswa, $new);
				$n++;
			}

			$daftar = $this->Mortu->daftar($ortu, $siswa, $id_ortu, $is_ortu_lama, $id_ortu_lama, $ortu_dft['email'], $ortu_dft['hp'], $kode_unik_enc);

			if($daftar) {

				$is_email = $this->kirim_email($ortu_dft['email'], $kode_unik);

				if($is_email) {
					$notif = "Pendaftaran berhasil disimpan!<br><br>Kode Unik telah dikirim melalui email Orang Tua Siswa";
				} else {
					$notif = "Pendaftaran berhasil disimpan!<br><br>E-mail notifikasi Kode Unik gagal dikirim, silahkan kirim ulang e-mail pada menu Orang Tua";
				}
				
				$tipe = "display";
			} else {
				$notif = "Pendaftaran gagal! Silahkan coba lagi";
				$tipe = "error";
			}

		} else {
			$notif = "Status pendaftaran sudah approved";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		redirect(base_url("admin/pendaftaran"));
	}

	public function hapus() {

		$id_ortu = $this->input->post('id_ortu_hapus');
		$res = $this->Mortu_daftar->hapus($id_ortu);

		if($res) {
			$notif = "Data Pendaftaran berhasil dihapus!";
			$tipe = "display";
		} else {
			$notif = "Gagal menghapus Data Pendaftaran! Silahkan coba lagi";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		redirect(base_url("admin/pendaftaran"));
	}

	public function set_ortu_edit() {
		$id = $this->input->get('id');
		$res = $this->Mortu_daftar->find($id);

		echo json_encode($res);
	}

	public function edit_ortu() {
		$id = $this->input->post('id_edit');
		$nama_ortu = $this->input->post('nama_edit');
		$kelamin = $this->input->post('kelamin');
		$email = $this->input->post('email');
		$hp = $this->input->post('hp');
		$status = $this->input->post('status');

		$ortu = array(	"nama" => $nama_ortu,
						"kelamin" => $kelamin,
						"email" => $email,
						"hp" => $hp,
						"status" => $status,
					);

		$res = $this->Mortu_daftar->update($id, $ortu);

		if($res) {
			$notif = "Data Orang Tua berhasil diubah!";
			$tipe = "display";
		} else {
			$notif = "Data Orang Tua gagal diubah!";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		redirect(base_url("admin/pendaftaran"));
	}

	public function email_ulang()
    {   
		$email = $this->input->post('email');
		$kode_unik = $this->input->post('kode_unik');

		$is_email = $this->kirim_email($email, $kode_unik);

		if($is_email) {
			$notif = "E-mail notifikasi Kode Unik telah dikirim melalui email Orang Tua Siswa";
			$tipe = "display";
		} else {
			$notif = "E-mail notifikasi Kode Unik gagal dikirim, silahkan kirim ulang e-mail pada menu Orang Tua";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		redirect(base_url("admin/orangTua"));
	}

	public function email_daftar()
    {
		$setting = $this->Mapp_setting->find(1);
		$id_ortu = $this->input->post('id_ortu_daftar');
		$ortu = $this->Mortu_daftar->find($id_ortu);
		$siswa = $this->Msiswa_daftar->getByOrtu($id_ortu);
		$email = $ortu['email'];

		$n = 0;
		$tot_biaya = 0;
		foreach($siswa as $s) {
			$level = $this->Mlevel->getByKode($s['level']);
			$siswa[$n]['nama_level'] = $level['deskripsi'];

			$biaya = $level['biaya'];
			$siswa[$n]['biaya'] = $biaya;

			$tot_biaya += $biaya;

			$n++;
		}

		$ortu['tot_biaya'] = $tot_biaya;

		$data = array('email' => $email, 'ortu' => $ortu, 'siswa' => $siswa, 'setting' => $setting);
		$message = $this->load->view('layouts/frontend/email_daftar.php',$data,TRUE);;
		$title = "Pendaftaran Berhasil Disimpan";

		$is_email = $this->kirim_email_daftar($email, $message, $title);

		if($is_email) {
			$notif = "Konfirmasi pendaftaran telah dikirim melalui email Orang Tua Siswa";
			$tipe = "display";
		} else {
			$notif = "E-mail konfirmasi pendaftaran gagal dikirim";
			$tipe = "error";
		}

		$this->session->set_flashdata($tipe, $notif);
		
		redirect(base_url("admin/pendaftaran"));

	}

	public function kirim_email($email, $kode_unik)
    {    
		$email_setting = $this->Memail_setting->find(1);
		$app_setting = $this->Mapp_setting->find(1); 

		$title = "Pendaftaran Berhasil Diverifikasi";
		// $from = $setting['email_address'];
		$to = $email;
		// $from = 'admin-psb@sai-cipedak.sch.id';
		// $name = 'Admin PSB SAI Cipedak';
		//$to = 'indras101@yahoo.com';

		$from = $email_setting['from'];
		$name = $email_setting['name'];

		$data = array('email' => $email, 'kode_unik' => $kode_unik, 'setting' => $app_setting);
		$message = $this->load->view('pendaftaran/email_approve.php',$data,TRUE);;

		$this->load->library('email');

		$config = Array(
			'protocol' => 'smtp',
			//'smtp_host' => 'smtp.hostinger.co.id',
			'smtp_host' => $email_setting['host'],
			'smtp_user' => $from,  // Email gmail
			//'smtp_pass'   => 'Saipsb123',  // Password email
			'smtp_pass'   => $email_setting['password'],  // Password email
			//'smtp_crypto' => 'ssl',
			'smtp_crypto' => $email_setting['crypto'],
			//'smtp_port'   => 587,
			'smtp_port'   => $email_setting['port'],
			'wordwrap' => FALSE,
			'wrapchars' => 76,
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'validate' => FALSE,
			'priority' => 1,
			'crlf' => "\r\n",
			'newline' => "\r\n",
			'bcc_batch_mode' => FALSE,
			'bcc_batch_size' => 200
		);

		$this->email->initialize($config);
		$this->email->set_newline("\r\n"); 
		
		$this->email->from($from, $name);
		$this->email->to($to);

		$this->email->subject($title);
		$this->email->message($message);

		$is_email = 1;

		if (! $this->email->send()) {
			$is_email = 0;
			//show_error($this->email->print_debugger());
			//exit;
		}else{
			//redirect(base_url("case/admin_case/setting?notif_display=1&notification=Silahkan cek email anda untuk melihat keberhasilan pengiriman email anda"));
		}

		return $is_email;
    }

	public function kirim_email_daftar($email, $message, $title)
    {    
		$email_setting = $this->Memail_setting->find(1);
		$to = $email;
		$from = $email_setting['from'];
		$name = $email_setting['name'];

		$this->load->library('email');

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => $email_setting['host'],
			'smtp_user' => $from,  // Email gmail
			'smtp_pass'   => $email_setting['password'],  // Password email
			'smtp_crypto' => $email_setting['crypto'],
			'smtp_port'   => $email_setting['port'],
			'wordwrap' => FALSE,
			'wrapchars' => 76,
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'validate' => FALSE,
			'priority' => 1,
			'crlf' => "\r\n",
			'newline' => "\r\n",
			'bcc_batch_mode' => FALSE,
			'bcc_batch_size' => 200
		);

		$this->email->initialize($config);
		$this->email->set_newline("\r\n"); 
		
		$this->email->from($from, $name);
		$this->email->to($to);

		$this->email->subject($title);
		$this->email->message($message);

		$is_email = 1;

		if (! $this->email->send()) {
			$is_email = 0;
			//show_error($this->email->print_debugger());
			//exit;
		}else{
			//redirect(base_url("case/admin_case/setting?notif_display=1&notification=Silahkan cek email anda untuk melihat keberhasilan pengiriman email anda"));
		}

		return $is_email;
    }

	public function tes_email()
    {    
		$email_setting = $this->Memail_setting->find(1);
		$app_setting = $this->Mapp_setting->find(1); 

		$title = "Pendaftaran Berhasil Diverifikasi";
		// $from = $setting['email_address'];
		$to = 'indra101@gmail.com';
		// $from = 'admin-psb@sai-cipedak.sch.id';
		// $name = 'Admin PSB SAI Cipedak';
		//$to = 'indras101@yahoo.com';

		$from = $email_setting['from'];
		$name = $email_setting['name'];

		$data = array('email' => 'indra101@gmail.com', 'kode_unik' => '2231312', 'setting' => $app_setting);
		$message = $this->load->view('pendaftaran/email_approve.php',$data,TRUE);;

		$this->load->library('email');

		$config = Array(
			'protocol' => 'smtp',
			//'smtp_host' => 'smtp.hostinger.co.id',
			'smtp_host' => $email_setting['host'],
			'smtp_user' => $from,  // Email gmail
			//'smtp_pass'   => 'Saipsb123',  // Password email
			'smtp_pass'   => $email_setting['password'],  // Password email
			//'smtp_crypto' => 'ssl',
			'smtp_crypto' => $email_setting['crypto'],
			//'smtp_port'   => 587,
			'smtp_port'   => $email_setting['port'],
			'wordwrap' => FALSE,
			'wrapchars' => 76,
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'validate' => FALSE,
			'priority' => 1,
			'crlf' => "\r\n",
			'newline' => "\r\n",
			'bcc_batch_mode' => FALSE,
			'bcc_batch_size' => 200
		);

		$this->email->initialize($config);
		$this->email->set_newline("\r\n"); 
		
		$this->email->from($from, $name);
		$this->email->to($to);

		$this->email->subject($title);
		$this->email->message($message);

		$is_email = 1;

		if (! $this->email->send()) {
			$is_email = 0;
			show_error($this->email->print_debugger());
			exit;
		}else{
			//redirect(base_url("case/admin_case/setting?notif_display=1&notification=Silahkan cek email anda untuk melihat keberhasilan pengiriman email anda"));
		}

		return $is_email;
    }
}