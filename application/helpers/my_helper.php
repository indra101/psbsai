<?php
function script( $string, $action = null ) {
        // you may change these values to your own
        $secret_key = 'sekolah_alam';
        $secret_iv = 'sekolah_alam';
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
     
        return $output;
}

function getStatusComplete($id_siswa,$jenjang){
    $ci = & get_instance();

    if($jenjang == 'bless'){
        $siswa = $ci->db->query('Select complete from siswa where id="'.$id_siswa.'"')->row_array();
        $pertanyaan = $ci->db->query('Select complete from pertanyaan where id_siswa="'.$id_siswa.'"')->row_array();
        if($siswa['complete'] == 1 && $pertanyaan['complete'] == 1){
            return 1;
        }else{
            return 0;
        } 
    }else{
        $siswa = $ci->db->query('Select complete, pernyataan from siswa where id="'.$id_siswa.'"')->row_array();
        $kesehatan = $ci->db->query('Select complete from kesehatan where id_siswa="'.$id_siswa.'"')->row_array();
        $kelahiran = $ci->db->query('Select complete from kelahiran where id_siswa="'.$id_siswa.'"')->row_array();
        $keluarga = $ci->db->query('Select complete from keluarga where id_siswa="'.$id_siswa.'"')->row_array();
        $perkembangan = $ci->db->query('Select complete from perkembangan where id_siswa="'.$id_siswa.'"')->row_array();
        $pertanyaan = $ci->db->query('Select complete from pertanyaan where id_siswa="'.$id_siswa.'"')->row_array();

        if($siswa['complete'] == 1 && $kesehatan['complete'] == 1 && $kelahiran['complete'] == 1 && $keluarga['complete'] == 1 && $perkembangan['complete'] == 1 && $pertanyaan['complete'] == 1 && !empty($siswa['pernyataan'])){
            return 1;
        }else{
            return 0;
        } 
    }
}

function getStatusCompleteOrtu($id_ortu){
    $ci = & get_instance();

    $ortu = $ci->db->query('Select complete, is_kepala, id_kepala from ortu where id="'.$id_ortu.'"')->row_array();

    if($ortu['is_kepala'])
        $pasangan = $ci->db->query('Select complete from ortu where id_kepala="'.$id_ortu.'"')->row_array();
    else {
        $pasangan = $ci->db->query('Select complete from ortu where id="'.$ortu['id_kepala'].'"')->row_array();
    }

    if(empty($pasangan))
        $pas = 1;
    else 
        $pas = $pasangan['complete'];

    if($ortu['complete'] == 1 && $pas == 1){
        return 1;
    }else{
        return 0;
    } 
}

function getStatusCompleteAllSiswa($id_ortu){
    $ci = & get_instance();
    $res = 1;

    $siswas = $ci->db->query('Select id, level from siswa where id_ortu="'.$id_ortu.'" and is_daftar = 1')->result_array();

    foreach($siswas as $s) {
        $complete = getStatusComplete($s['id'], $s['level']);
        $res = $res && $complete;
    }

    return $res;
}

function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}