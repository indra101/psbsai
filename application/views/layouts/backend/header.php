<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title><?=$title?></title>
  <link rel="shortcut icon" type="image/png" href="<?=base_url('assets/image/logo.png');?>"/>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?=base_url('assets/material_admin/assets/css/material-dashboard.css?v=2.1.1'); ?>" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?=base_url('assets/material_admin/assets/demo/demo.css'); ?>" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <link href="<?=base_url('assets/select2/css/select2.css'); ?>" rel="stylesheet" />
  <link href="<?=base_url('assets/buttons/css/buttons.dataTables.css'); ?>" rel="stylesheet" />

  <script src="<?=base_url('assets/material_admin/assets/js/core/jquery.min.js'); ?>"></script>
  <script src="<?=base_url('assets/select2/js/select2.min.js'); ?>"></script>
  <script src="<?=base_url('assets/buttons/js/buttons.dataTables.min.js'); ?>"></script>
  <script src="<?=base_url('assets/jquery-number-master/jquery.number.min.js'); ?>"></script>
  
  <style type="text/css">
    .redup:hover{
        background-color: #000;
        opacity: 0.8;
    }
    @media (max-width: 991px) {
        .website{
            display:none;
        }
    }
    @media (min-width: 992px) {
        .mobile{
            display:none;
        }
    }
    .judul {
        font-family: "Trebuchet MS", Helvetica, sans-serif;
        font-size: 12px;
        color: #a1a1a1;
    }
	</style>
    
</head>

<body class="">