<div class="wrapper ">
  <div class="sidebar" data-color="green" data-background-color="black" data-image="<?=base_url('assets/material_admin/assets/img/sidebar-1.jpg'); ?>">
    <div class="logo">
      <a href="<?=base_url('admin/dashboard')?>" class="simple-text logo-normal">
        <img src="<?=base_url('assets/image/logo.png');?>" class="img-fluid mx-auto d-block"/>
      </a>
    </div>

    <?php $role = $this->session->userdata('id_role'); ?>

    <div class="sidebar-wrapper" style="height: calc(100vh - 150px);">
      <ul class="nav">
        <li class="nav-item <?=($judul == 'Dashboard')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/dashboard')?>">
            <i class="material-icons">dashboard</i>
            <p>Dashboard</p>
          </a>
        </li>
        <?php if($role == 1 || $role == 2) { //Administrator & Admin PPSB ?>
        <li class="nav-item <?=($judul == 'Pendaftaran')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/pendaftaran')?>">
            <i class="material-icons">library_books</i>
            <p>Pendaftaran</p>
          </a>
        </li>
        <li class="nav-item <?=($judul == 'Orang Tua')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/orangTua')?>">
            <i class="material-icons">wc</i>
            <p>Orang Tua</p>
          </a>
        </li>
        <li class="nav-item <?=($judul == 'Siswa')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/siswa')?>">
            <i class="material-icons">child_care</i>
            <p>Siswa</p>
          </a>
        </li>
        <li class="nav-item <?=($judul == 'Jenjang')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/level')?>">
            <i class="material-icons">assessment</i>
            <p>Jenjang</p>
          </a>
        </li>
        <li class="nav-item <?=($judul == 'Riwayat Pendaftaran')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/riwayat')?>">
            <i class="material-icons">history</i>
            <p>Riwayat Pendaftaran</p>
          </a>
        </li>
        <?php } ?>
        <li class="nav-item <?=($judul == 'Wawancara')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/wawancara')?>">
            <i class="material-icons">comment</i>
            <p>Wawancara</p>
          </a>
        </li>
        <li class="nav-item <?=($judul == 'Tahapan')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/tahapan')?>">
            <i class="material-icons">unarchive</i>
            <p>Tahapan</p>
          </a>
        </li>
        <!-- <li class="nav-item <?=($judul == 'Reporting')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/reporting')?>">
            <i class="material-icons">content_paste</i>
            <p>Reporting</p>
          </a>
        </li> -->
        <?php if($role == 1) { //Administrator ?>
        <li class="nav-item <?=($judul == 'User')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/user')?>">
            <i class="material-icons">person</i>
            <p>User</p>
          </a>
        </li>
        <li class="nav-item <?=($judul == 'Pengaturan')?'active':''?>">
          <a class="nav-link" href="<?=base_url('admin/setting')?>">
            <i class="material-icons">settings</i>
            <p>Pengaturan</p>
          </a>
        </li>
        <?php } ?>
        
      </ul>
      <ul></ul>
    </div>
  </div>
  <div class="main-panel">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <a class="navbar-brand" href="#pablo"><?=$judul?></a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon icon-bar"></span>
          <span class="navbar-toggler-icon icon-bar"></span>
          <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
          <!--
          <form class="navbar-form">
            <div class="input-group no-border">
              <input type="text" value="" class="form-control" placeholder="Search...">
              <button type="submit" class="btn btn-white btn-round btn-just-icon">
                <i class="material-icons">search</i>
                <div class="ripple-container"></div>
              </button>
            </div>
          </form>we
          -->
          <ul class="navbar-nav">
            <!--
            <li class="nav-item dropdown">
              <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">notifications</i>
                <span class="notification">5</span>
                <p class="d-lg-none d-md-block">
                  Some Actions
                </p>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Mike John responded to your email</a>
                <a class="dropdown-item" href="#">You have 5 new tasks</a>
                <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                <a class="dropdown-item" href="#">Another Notification</a>
                <a class="dropdown-item" href="#">Another One</a>
              </div>
            </li>
            -->

            <?=$this->session->userdata('nama')?>

            <li class="nav-item">
              <a class="nav-link" href="<?=base_url('admin/dashboard')?>">
                <i class="material-icons">dashboard</i>
                <p class="d-lg-none d-md-block">
                  Stats
                </p>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">person</i>
                <p class="d-lg-none d-md-block">
                  Account
                </p>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                <!--
                <a class="dropdown-item" href="#">Profile</a>
                <a class="dropdown-item" href="#">Settings</a>
                <div class="dropdown-divider"></div>
                -->
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal_pass">Ganti Password</a>
                <a class="dropdown-item" href="<?=base_url('admin/login/logout')?>">Keluar</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <?=$this->load->view('admin/user/modal_edit_pass'); ?>
    <!-- End Navbar -->
