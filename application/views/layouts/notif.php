<?php if($this->session->flashdata('display')) { ?>
            
    <div>                        
        <div class="alert rounded" style="background: teal;color:white;padding-top: 10px; display: block;">
            <a href="#" class="close" aria-label="close" onclick="closeNotif()"><i class="fa fa-times"></i></a>
            <div style="text-align: center;"><?=$this->session->flashdata('display')?></div>
        </div>
    </div>

<?php } ?>
<?php if($this->session->flashdata('error')) {  ?>
    
    <div>
        <div class="alert rounded" style="background: #be0000;color:white;padding-top: 10px; display: block;">
            <a href="#" class="close" aria-label="close" onclick="closeNotif()"><i class="fa fa-times"></i></a>
            <div style="text-align: center;"><?=$this->session->flashdata('error') ?></div>
        </div>
    </div>
    
<?php } ?>



<script>

function closeNotif() {
    $('.alert').fadeOut();
}

</script>