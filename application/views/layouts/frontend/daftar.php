<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS 
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap-grid.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap-reboot.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/font-awesome/css/font-awesome.min.css'); ?>">
	-->
	
	<!-- Material Bootstrap CSS -->
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<link href="<?=base_url('assets/material_bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?=base_url('assets/material_bootstrap/css/mdb.min.css'); ?>" rel="stylesheet">
	<!-- <link href="<?=base_url('assets/material_admin/assets/css/material-dashboard.css?v=2.1.1'); ?>" rel="stylesheet" /> -->

	<script src="<?=base_url('assets/material_admin/assets/js/plugins/moment.min.js'); ?>"></script>

	<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/jquery-3.4.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/material_admin/assets/js/plugins/bootstrap-datetimepicker.min.js'); ?>"></script>

	<link rel="shortcut icon" type="image/png" href="<?=base_url('assets/image/logo.png');?>"/>
	<link href="<?=base_url('assets/buttons/css/buttons.dataTables.css'); ?>" rel="stylesheet" />
	

    <title><?=$title?></title>

	<style>
		.paral {
			min-height:100vh;
			background-attachment: fixed;
			background-size: cover;
			background-position: 50% 50%;
		}
		.paralsec {
			//background-color: #000;
			//background-image: linear-gradient(180deg, #51DDDD 0%, #2C66B9 100%, #2C66B9 100%);
			//background-image:url('http://imansulaiman.com/wp-content/uploads/2019/04/25db546b-4135-4c2e-9be2-2409dbfa4b36.jpg');
			background-image: url("<?=base_url('assets/image/bg.png'); ?>");
			background-color: rgba(255, 255, 255, 0.8);
		}
		.jumbotron{
			margin-bottom: 0;
		}
		.footer {
			position: fixed;
			left:0;
			bottom: 0;
			width: 100%;
			background-color: #242423!important;
		}
	</style>
</head>
<body>
	
	<div class="jumbotron paral paralsec overlay">
		
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="card shadow bg-white" style="border: 0px !important; border-radius: 20px;">
					<div class="card-body">
					<?php $this->view('layouts/notif'); ?>
					<div class="row">
						<div class="col-md" style="padding-left:20px; padding-right:20px;">
							<img src="<?=base_url('assets/image/logo.png');?>" class="img-fluid mx-auto d-block" style="width:30%;padding-bottom: 10px;"/>

							<h5 class="text-center mb-4">Form Pendaftaran Siswa Baru</h5>

							<!-- <p class="text-center" style="font-size:8pt;padding-bottom: 10px;">Pastikan telah memiliki Kode Unik yang di dapatkan dari Sekolah. Jika belum memiliki silahkan menghubungi bagian Administrasi Sekolah Alam Indonesia.</p> -->

							<?php echo form_open_multipart('daftar/daftar_baru', array('id'=>'form-submit')); ?>

							<div class="row">

								<div class="col-md-6" style="padding:20px; border-right: 1px solid lightgrey;">

									<h6 class="mb-4">Data Orang Tua</h6>

									<div class="form-group mt-4" style="font-size:13pt;margin-bottom: 0.7rem;">
										<div class="row">
											<div class="col-md-12">
												<span style="color: #6d7073;">Apakah Anda pernah mendaftar pada periode tahun 2021 hingga sekarang?</span>
											</div>
											<div class="col text-right">
												<span id="alert_daftar" style="color: #fd7e14;display:none;font-size:8pt;">Jawaban harus dipilih!</span>
											</div>
										</div>
										<input type="radio" class="mr-2" id="ya" name="pernah_daftar" value="1" onclick="show_cari()"><label for="ya">Ya</label>
										<input type="radio" class="mr-2 ml-4" id="tidak" name="pernah_daftar" value="0" onclick="hide_cari()"><label for="tidak">Tidak</label>
									</div>


									<div id="div_cari" style="display: none; margin-bottom: 20px">
										<div class="form-group" style="font-size:12pt;">
											<div class="row">
												<div class="col">
												<span style="color: #6d7073;">Masukkan Email yang pernah didaftarkan</span>
												</div>
											</div>
											<input type="text" class="form-control" id="email_cari" placeholder="e.g. email@alamat.com" style="font-size:15px;">
										</div>

										<a href="#" id="btn_cari" class="btn btn-info" onclick="search_ortu()">Cari Data</a>

										<div style="margin-top: 5px"><span id="alert_cari" style="color: red;display:none;font-size:10pt;">Data tidak ditemukan</span></div>
										
									</div>

									<div id="data_ortu" style="display: none;">

										<div class="form-group mt-4" style="font-size:13pt;margin-bottom: 0.7rem;" id="teks_input">
											<div class="row">
												<div class="col-md-12">
													<span style="color: #6d7073;">Silahkan melengkapi data orang tua</span>
												</div>
											</div>
										</div>

										<input type="hidden" id="id_ortu" name="id_ortu">

										<div class="form-group" style="font-size:10pt;margin-bottom: 0.7rem;">
											<div class="row">
												<div class="col">
													<i class="fa fa-user" style="color: #6d7073;"></i>
													<span style="color: #6d7073;">Nama</span>
												</div>
												<div class="col text-right">
													<span id="alert_nama" style="color: #fd7e14;display:none;font-size:8pt;">Nama harus diisi!</span>
												</div>
											</div>
											<input type="text" class="form-control" id="nama_ortu" name="nama_ortu" style="font-size:15px;" required>
										</div>

										<div class="form-group" style="font-size:10pt;margin-bottom: 0.7rem;">
											<div class="row">
												<div class="col">
													<i class="fa fa-heart" style="color: #6d7073;"></i>
													<span style="color: #6d7073;">Jenis Kelamin</span>
												</div>
												<div class="col text-right">
													<span id="alert_kelamin" style="color: #fd7e14;display:none;font-size:8pt;">Jenis Kelamin wajib diisi!</span>
												</div>
											</div>
											<select class="form-control" name="kelamin" id="kelamin" required>
												<option value="" disabled selected>Pilih</option>
												<option value="Laki-laki">Laki-laki</option>
												<option value="Perempuan">Perempuan</option>
											</select>
											<input type="text" class="form-control" id="kelamin_txt" style="font-size:15px;" readonly>
										</div>

										<div class="form-group" style="font-size:10pt;margin-bottom: 0.7rem;">
											<div class="row">
												<div class="col">
													<i class="fa fa-envelope" style="color: #6d7073;"></i>
													<span style="color: #6d7073;">Email</span>
												</div>
												<div class="col text-right">
													<span id="alert_email" style="color: #fd7e14;display:none;font-size:8pt;">Email wajib diisi!</span>
												</div>
											</div>
											<input type="text" class="form-control" id="email" name="email" style="font-size:15px;" required>
										</div>

										<div class="form-group" style="font-size:10pt;margin-bottom: 0.7rem;">
											<div class="row">
												<div class="col">
													<i class="fa fa-mobile-alt" style="color: #6d7073;"></i>
													<span style="color: #6d7073;">Nomor HP</span>
												</div>
												<div class="col text-right">
													<span id="alert_hp" style="color: #fd7e14;display:none;font-size:8pt;">Nomor HP wajib diisi!</span>
												</div>
											</div>
											<input type="number" class="form-control" id="hp" name="hp" style="font-size:15px;" required>
										</div>

										<div class="form-group" style="font-size:10pt;margin-bottom: 0.7rem;">
											<div class="row">
												<div class="col">
													<i class="fa fa-clipboard" style="color: #6d7073;"></i>
													<span style="color: #6d7073;">Status</span>
												</div>
												<div class="col text-right">
													<span id="alert_status" style="color: #fd7e14;display:none;font-size:8pt;">Status wajib diisi!</span>
												</div>
											</div>
											<select class="form-control" name="status" id="status" required>
												<option value="" disabled selected>Pilih</option>
												<option value="Ayah">Ayah</option>
												<option value="Ibu">Ibu</option>
												<option value="Wali">Wali</option>
											</select>
											<input type="text" class="form-control" id="status_txt" style="font-size:15px;" readonly>
										</div>

										<div class="form-group" style="font-size:10pt;margin-bottom: 0.7rem;">
											<div class="row">
												<div class="col-md-12">
													<i class="fa fa-address-card" style="color: #6d7073;"></i>
													<span style="color: #6d7073;">Upload Kartu Keluarga</span>
												</div>
												<div class="col text-right">
													<span id="alert_foto_kk" style="color: #fd7e14;display:none;font-size:8pt;">Foto Kartu Keluarga belum diupload!</span>
												</div>
											</div>
											<div>
												<img id="image_preview_container_kk" class="img-fluid mb-2 mt-2" src=""  style="margin: auto; max-height: 400px; border-radius: 5px; cursor: pointer;">
											</div>
											<button type="button" id="upload_kk_btn" class="btn btn-primary btn-block btn-sm" onclick="">Pilih File</button>
											<input type="file" name="foto_kk" id="file_kk" onchange="setfilename(this.value)" style="display:none;"  required/>
											<div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
											<div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 10 MB</i></span></div>
										</div>

									</div>
									
								</div>


								<div class="col-md-6" id="tambah" style="padding:20px;">

									<h6 class="">Data Anak<a href="javascript:void(0)" onclick="tambah()" title="Tambah Data Anak"><i class="fa fa-user-plus ml-2" ></i></a></h6>

									<div class="mb-2">
										<span class="style" style="font-size: x-small">*Untuk mendaftarkan lebih dari 1 anak silahkan tekan tombol (+) di atas</span>
									</div>
									
									<!-- <div class="mb-2">Anak ke-1</div> -->
									<div id="div_anak"> <!---- start div anak -->
									<div class="form-group" style="font-size:10pt;margin-bottom: 0.7rem;">
										<div class="row">
											<div class="col">
												<i class="fa fa-smile" style="color: #6d7073;"></i>
												<span style="color: #6d7073;">Nama</span>
											</div>
											<div class="col text-right">
												<span id="alert_nama_siswa" style="color: #fd7e14;display:none;font-size:8pt;">Nama Siswa harus diisi!</span>
											</div>
										</div>
										<input type="text" class="form-control" id="nama_siswa" name="nama_siswa[]" style="font-size:15px;" required>
									</div>

									<div class="form-group" style="font-size:10pt;margin-bottom: 0.7rem;">
										<div class="row">
											<div class="col">
												<i class="fa fa-calendar-alt" style="color: #6d7073;"></i>
												<span style="color: #6d7073;">Tanggal Lahir</span>
											</div>
											<div class="col text-right">
												<span id="alert_tgl_lahir" style="color: #fd7e14;display:none;font-size:8pt;">Tanggal Lahir harus diisi!</span>
											</div>
										</div>
										<input placeholder="Selected date" type="date" id="tgl_lahir" class="form-control datepicker" name="tgl_lahir[]" value="" required>
									</div>

									<div class="form-group" style="font-size:10pt;margin-bottom: 0.7rem;">
										<div class="row">
											<div class="col">
												<i class="fa fa-graduation-cap" style="color: #6d7073;"></i>
												<span style="color: #6d7073;">Jenjang</span>
											</div>
											<div class="col text-right">
												<span id="alert_level" style="color: #fd7e14;display:none;font-size:8pt;">Jenjang wajib diisi!</span>
											</div>
										</div>
										<?=form_dropdown('level[]', array('' => 'Pilih Jenjang') + $levels, '', 'class="form-control" id="level" required'); ?>
									</div>

									</div> <!---- end div anak -->
									
								</div>

								

							</div>

							<div class="text-center mt-4">
								<a href="#" id="btn_submit" class="btn btn-success" onclick="konfirmasi()">Daftar</a>
								<input type="submit" id="submitBtn" style="display: none;">
								<?php echo form_close(); ?>

								<div style="padding-top:8px;">
									<p class="text-center"><a href="login">Kembali ke Beranda</a></p>
									<p class="text-center">Butuh Bantuan? <a href="https://wa.me/<?=$setting['admin_wa']?>">Hubungi Administrasi PSB</a></p>
								</div>
								
							</div>
							
						</div>

						
					</div>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>

		<div class="modal fade" id="modal_konfirm" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
						<p class="form-row-content mb-0"><h6>Konfirmasi Pendaftaran Siswa Baru</h6></p>
					</div>            
					<div class="modal-body">
						Dengan ini Saya menyatakan bahwa data pendaftaran telah sesuai dan dapat dipertanggungjawabkan.<br><br>Apakah Anda akan melanjutkan?
					</div>
					<div class="modal-footer text-center">
						<a href="#" class="btn btn-danger" id="btnBatal" data-toggle="modal" data-target="#modal_konfirm">Batal</a>
						<a href="#"  class="btn btn-success" id="btnSubmit" onclick="return validate()"><div class="spinner-border mr-2" id="spin" role="status" style="height: 17px; width: 17px; display: none;"></div>Simpan</a>
					</div>
				</div>    
			</div>
		</div>

		<!-- <div class="modal fade" id="modal_ortu" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
			<div class="modal-dialog"  style="max-width: 1000px">
				<div class="modal-content">
					<div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
						<p class="form-row-content mb-0"><h6>Pencarian Data Orang Tua</h6></p>
					</div>            
					<div class="modal-body">
					<div class="card">
                
                <div class="card-body table-responsive">
                  <table id="tortu" class="table-striped table-hover">
                    <thead class="text-success">
                      <th style="min-width: 40px">No</th>
                      <th style="min-width: 150px">Nama</th>
					  <th style="min-width: 150px">Email</th>
                      <th style="min-width: 100px">Status</th>
                      <th style="min-width: 350px">Alamat</th>
                      <th style="max-width: 80px">Aksi</th>
                    </thead>
                    <tbody>
                    <?php $i=1;
					// print_r($ortus->data);
                    foreach($ortus->data as $ot){?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=$ot['nama_ortu']?></td>
						<td><?=$ot['email']?></td>
                        <td><?=$ot['status']?></td>
                        <td><?=$ot['alamat_rumah']?></td>
                        <td>
                          <a class="btn btn-info btn-block" onclick="set_ortu(<?=$ot["id"]?>, '<?=$ot["nama_ortu"]?>', '<?=$ot["email"]?>', '<?=$ot["kelamin"]?>', '<?=$ot["hp"]?>', '<?=$ot["status"]?>'); open_ortu()">Pilih</a>
                        </td>
                      </tr>
                    <?php $i++;} ?>
                    </tbody>
                  </table>
                </div>
              </div>
					</div>
					<div class="modal-footer text-center">
						<a href="#" onclick="open_ortu()" class="btn btn-danger" id="btnBatal">Tutup</a>
					</div>
				</div>    
			</div>
		</div> -->

	</div>

<!-- Footer -->
<footer class="page-footer font-small blue pt-0 footer">
	<!-- Copyright -->
	<div class="footer-copyright text-center py-3">© 2024 Copyright
		<a href="<?=$setting['unit_url']?>"> Sekolah Alam Indonesia <?=$setting['unit']?></a>
	</div>
</footer>


<!-- Javascript 
<script src="<?=base_url('assets/bootstrap/js/bootstrap.bundle.js'); ?>"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap.js'); ?>"></script>
-->


<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/popper.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/mdb.min.js'); ?>"></script>


<script>

	var biaya = new Map([['kb', 500000], ['tka', 100000], ['tkb', 500000]]);
	
    var num = 1;

    function tambah(){
        
        $("#tambah").append("<div id='div_anak"+num+"'></div>");
        var new_anak = $("#div_anak").clone().prop('id', 'new_anak'+num ).append('<i class="fa fa-trash mr-2 pt-2" data-no="' + num + '"  style="cursor: pointer; color: mediumvioletred;" title="Hapus Data Anak"></i>');

		$("#div_anak"+num).append('<div style="margin-top: 20px; margin-bottom: 20px; border-bottom: 1px solid #aa66cc;"></div>');
		//$("#div_anak"+num).append("<div class='mb-2'>Anak ke-"+(num+1)+"</div>");
        $("#div_anak"+num).append(new_anak);
        num++;
    }

    function konfirmasi() {
		if(validate()) {
			$('#modal_konfirm').modal('toggle');
		}
    }

	function open_ortu() {
		$('#modal_ortu').modal('toggle');
    }

	function hide_cari() {
		$('#div_cari').hide();
		clear_ortu();

		$("#nama_ortu").prop('readonly', false);
		$("#email").prop('readonly', false);
		$("#kelamin").show();
		$("#kelamin_txt").hide();
		$("#hp").prop('readonly', false);
		$("#status").show();
		$("#status_txt").hide();

		$('#data_ortu').show();
		$('#teks_input').show();
	}

	function show_cari() {
		$('#div_cari').show();
		clear_ortu();

		$("#nama_ortu").prop('readonly', true);
		// $("#email").prop('readonly', true);
		$("#kelamin").hide();
		$("#kelamin_txt").show();
		// $("#hp").prop('readonly', true);
		$("#status").hide();
		$("#status_txt").show();

		$('#data_ortu').show();
		$('#teks_input').hide();
	}

	function search_ortu() {
		var email = $("#email_cari").val();
		$.ajax({ 
			type: "GET",   
			dataType: "json",
			url: "<?=base_url('daftar/get_kepala_by_email');?>",
			data: 'email='+email,
			success: function (data) {

				if(data == null) {
					clear_ortu();
					$('#alert_cari').show();
				} else {
					$("#id_ortu").val(data.id);
					$("#nama_ortu").val(data.nama_ortu);
					$("#email").val(data.email);
					$("#kelamin").val(data.kelamin);
					$("#kelamin_txt").val(data.kelamin);
					$("#hp").val(data.hp);
					$("#status").val(data.status);
					$("#status_txt").val(data.status);
					$('#alert_cari').hide();
				}
			}
		});
	}

	$(function() {
		$('#tortu').dataTable( {
		dom:  "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'f><'col-sm-12 col-md-4'B>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
		buttons: [
			{
			// extend: 'excelHtml5',
			filename: 'Data_Orang_Tua',
			title: 'Data Orang Tua',
			messageTop: '',
			
			},
		],
		searching: true
		} );

		$('.dt-buttons').hide();
	});

    $(document).on('click', '.fa-trash', function() {
        let no = $(this).data('no');
        $("#div_anak"+no).remove();
    });  

	$('#upload_btn').on('click', function() {
		$('#file').click();
	});

	$('#upload_kk_btn').on('click', function() {
		$('#file_kk').click();
	});

	$('#btnSubmit').on('click', function() {
		$('#spin').show();
		$('#submitBtn').click();
		$('#btnSubmit').prop("disabled",true);
	});

	$('#btnBatal').on('click', function() {
		$('#modal_konfirm').modal('toggle');
	});

	$('#file_kk').change(function(e){

		var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
		var name = this.files[0].name;
		var name_arr = name.split(".");
		var type = name_arr[name_arr.length - 1];
		var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
		var pesan_size = "\nMohon masukkan file dengan ukuran max. 10 MB";
		var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
		var cek_size = size >= 10; // max file size 10 MB
		var cek_tipe = allowed.indexOf(type) == -1;

		if(cek_size || cek_tipe) {
			var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
			if(cek_size)
				pesan += pesan_size;

			if(cek_tipe)
				pesan += pesan_tipe;

			alert(pesan);
			a.value = '';

		} else {
			
			let reader = new FileReader();
			reader.onload = (e) => { 
				$('#image_preview_container_kk').attr('src', e.target.result); 
			}
			reader.readAsDataURL(this.files[0]); 

		}
	});

	function set_ortu(id, nama, email, kelamin, hp, status) {
		$("#id_ortu").val(id);
		$("#nama_ortu").val(nama);
		$("#email").val(email);
		$("#kelamin").val(kelamin);
		$("#kelamin_txt").val(kelamin);
		$("#hp").val(hp);
		$("#status").val(status);
		$("#status_txt").val(status);
	}

	function clear_ortu() {
		$("#id_ortu").val('');
		$("#nama_ortu").val('');
		$("#email").val('');
		$("#kelamin").val('');
		$("#kelamin_txt").val('');
		$("#hp").val('');
		$("#status").val('');
		$("#status_txt").val('');
	}

	function validate() {
		var res = true;

		if ($("#nama_ortu").val() == '') {
			$("#alert_nama").show();
			res = res && false;
		} else {
			$("#alert_nama").hide();
		}

		if ($("#kelamin").val() == null) {
			$("#alert_kelamin").show();
			res = res && false;
		} else {
			$("#alert_kelamin").hide();
		}

		if ($("#email").val() == '') {
			$("#alert_email").show();
			res = res && false;
		} else {
			$("#alert_email").hide();
		}

		if ($("#hp").val() == '') {
			$("#alert_hp").show();
			res = res && false;
		} else {
			$("#alert_hp").hide();
		}

		if ($("#status").val() == null) {
			$("#alert_status").show();
			res = res && false;
		} else {
			$("#alert_status").hide();
		}

		if (!$("#ya").is(':checked') && !$("#tidak").is(':checked')) {
			$("#alert_daftar").show();
			res = res && false;
		} else {
			$("#alert_daftar").hide();
		}

		if ($("#file_kk").val() == '') {
			$("#alert_foto_kk").show();
			res = res && false;
		} else {
			$("#alert_foto_kk").hide();
		}

		if ($("#nama_siswa").val() == '') {
			$("#alert_nama_siswa").show();
			res = res && false;
		} else {
			$("#alert_nama_siswa").hide();
		}

		if ($("#tgl_lahir").val() == '') {
			$("#alert_tgl_lahir").show();
			res = res && false;
		} else {
			$("#alert_tgl_lahir").hide();
		}

		if ($("#level").val() == null) {
			$("#alert_level").show();
			res = res && false;
		} else {
			$("#alert_level").hide();
		}
		
		return res;
	}

	
</script>
<script src="<?=base_url('assets/material_admin/assets/js/plugins/jquery.dataTables.min.js'); ?>"></script>
</body>
</html>