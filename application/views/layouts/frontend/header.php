<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS 
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap-grid.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap-reboot.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/font-awesome/css/font-awesome.min.css'); ?>">
	-->
	
	<!-- Material Bootstrap CSS -->
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<link href="<?=base_url('assets/material_bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?=base_url('assets/material_bootstrap/css/mdb.min.css'); ?>" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="<?=base_url('assets/dashboard/material-dashboard.css?v=2.1.1'); ?>" rel="stylesheet" />
    <link href="<?=base_url('assets/material_admin/assets/css/material.css?v=2.1.1'); ?>" rel="stylesheet" />
    <script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/jquery-3.4.1.min.js'); ?>"></script>
    <title><?=$title?></title>
    <link rel="shortcut icon" type="image/png" href="<?=base_url('assets/image/logo.png');?>"/>

	<style type="text/css">
		.jumbotron{
			margin-bottom: 0;
		}
		.btn-warning{
			background-color: #5dc953!important;
        }

        @media (max-width: 991px) {
            .website{
                display:none;
            }
            .mobile-jumbotron{
                background-size: 105% 30%;
                min-height:100vh;
                background-position: 50% -35%;
                background-repeat:no-repeat;
                background-image: url("<?=base_url('assets/image/bg-home.png'); ?>");
            }
            .mobile-dashboard{
                color:white!important;
                text-align:center;
                font-weight: bold;
                padding-top:30px;
            }
            .tombol:hover{
                color:orange;
            }
            .tombol-aktif{
                color:orange;
            }
            .footer {
                position: fixed;
                z-index: 100;
                left:0;
                bottom: 0;
                width: 100%;
                background-color: #242423!important;
            }
            .background {
                min-height:100vh;
                background-attachment: fixed;
                background-position: 50% -250%;
                background-repeat:no-repeat;
                background-image: url("<?=base_url('assets/image/bg-home2.png'); ?>");
                background-size: 109% 85%;
            }
            .icon-status-failed{
                width:26px;
                transform: translate(5px,1px);
            }
            .redup:hover{
                background-color: #000;
                opacity: 0.8;
            }
        }
        @media (min-width: 992px) {
            .mobile{
                display:none;
            }
            .website-dashboard{
                color:white!important;
                transform: translateX(-25%)!important;
            }
            .website-akun{
                transform: translateX(-50%)!important;
            }
            .tombol-putih{
                color:white!important;
                border: 2px solid white!important;
            }
            .tombol-putih:hover{
                color:orange!important;
                border: 2px solid orange!important;
            }
            .paddingnol{
                padding-right: 0!important;
                padding-left: 0!important;
            }
            .btn-outline-default.active, .btn-outline-default:active, .btn-outline-default:active:focus, .btn-outline-default:focus, .btn-outline-default:hover {
                color: orange!important;
                background-color: transparent!important;
                border-color: orange!important;
            }
            .footer {
                position: fixed;
                left:0;
                bottom: 0;
                width: 100%;
                background-color: #242423!important;
            }
            .background {
                min-height:100vh;
                background-attachment: fixed;
                background-position: 50% -250%;
                background-repeat:no-repeat;
                background-image: url("<?=base_url('assets/image/bg-home.png'); ?>");
                background-size: 109% 85%;
            }
            .padding-nav{
                padding-right:8%!important;
            }
            .size-icon{
                font-size: 1em!important;
            }
            .icon-status-success{
                width:15px;
                transform: translateY(-2px);
            }
            .icon-status-failed{
                width:23px;
                transform: translate(5px,1px);
            }
            .redup:hover{
                background-color: #000;
                opacity: 0.8;
            }
        }

        .judul {
            font-family: "Trebuchet MS", Helvetica, sans-serif;
            font-size: 12px;
            color: #a1a1a1;
            overflow-wrap: break-word;
            word-wrap: break-word;
        }
        
	</style>
</head>
<body>