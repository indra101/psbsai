<!--Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top website padding-nav" style="height: 65px!important;">
  <a class="navbar-brand text-center" href="<?=base_url('siswa/dashboard')?>" style="width: 300px;"><img src="<?=base_url('assets/image/logo.png');?>" class="img-fluid" style="width:30%;"/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item website-dashboard">
        Dashboard Pendaftaran Siswa Baru Tahun <?=$setting['tahun_ajaran']?>
      </li>
      <li class="nav-item dropdown">
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <!--
      <li class="nav-item">
        <button type="button" class="btn btn-outline-default waves-effect">Tahapan</button>
      </li>
      <li class="nav-item">
        <button type="button" class="btn btn-outline-default waves-effect">Pesan</button>
      </li>
      <li class="nav-item">
        <button type="button" class="btn btn-outline-default waves-effect">Keluar</button>
      </li>
      -->
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light paddingnol" href="<?=base_url('siswa/tahapan')?>">
          <button type="button" class="btn btn-outline-default waves-effect tombol-putih">Tahapan</button>
        </a>
      </li>
      <!--
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light paddingnol" data-toggle="dropdown">
          <button type="button" class="btn btn-outline-default waves-effect tombol-putih">Pesan</button>
        </a>
        
        <div class="dropdown-menu dropdown-menu-right dropdown-default website-akun"
          aria-labelledby="navbarDropdownMenuLink-333" style="width: 300px;">
          <p style="padding-right:15px;padding-left:15px;font-size:10pt;">Anda Memiliki 5 Pesan yang belum di Baca</p>
          <hr style="border: 1px solid green;">
          <div style="padding-right:15px;padding-left:15px;font-size:8pt;">
            <a class="dropdown-item" href="#"><i class="fas fa-envelope"></i> Pesan 1</a>
            <a class="dropdown-item" href="#"><i class="fas fa-envelope"></i> Pesan 2</a>
            <a class="dropdown-item" href="#"><i class="fas fa-envelope"></i> Pesan 3</a>
            <a class="dropdown-item" href="#"><i class="fas fa-envelope"></i> Pesan 4</a>
            <a class="dropdown-item" href="#"><i class="fas fa-envelope"></i> Pesan 5</a>
          </div>
        </div>
      </li>
      -->
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light paddingnol" data-toggle="dropdown">
          <button type="button" class="btn btn-outline-default waves-effect tombol-putih">Akun</button>
        </a>
        
        <div class="dropdown-menu dropdown-menu-right dropdown-default website-akun"
          aria-labelledby="navbarDropdownMenuLink-333">
          <!--
            <a class="dropdown-item" href="<?=base_url('siswa/akun')?>">Pengaturan</a>
           -->
            <a class="dropdown-item" href="<?=base_url('login/logout')?>">Keluar</a>
        </div>
      </li>
      
    </ul>
  </div>
</nav>
<!--/.Navbar -->