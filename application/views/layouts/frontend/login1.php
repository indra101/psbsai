<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS 
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap-grid.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap-reboot.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/font-awesome/css/font-awesome.min.css'); ?>">
	-->
	
	<!-- Material Bootstrap CSS -->
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<link href="<?=base_url('assets/material_bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?=base_url('assets/material_bootstrap/css/mdb.min.css'); ?>" rel="stylesheet">

	<link rel="shortcut icon" type="image/png" href="<?=base_url('assets/image/favicon.png');?>"/>
	

    <title><?=$title?></title>

	<style>
		.paral {
			min-height:100vh;
			background-attachment: fixed;
			background-size: cover;
			background-position: 50% 50%;
		}
		.paralsec {
			/* background-color: #000;
			background-image: linear-gradient(180deg, #51DDDD 0%, #2C66B9 100%, #2C66B9 100%);
			background-image:url('http://imansulaiman.com/wp-content/uploads/2019/04/25db546b-4135-4c2e-9be2-2409dbfa4b36.jpg'); */
			background-image: url("<?=base_url('assets/image/bg.png'); ?>");
			background-color: rgba(255, 255, 255, 0.8);
		}
		.jumbotron{
			margin-bottom: 0;
		}
		.footer {
			position: fixed;
			left:0;
			bottom: 0;
			width: 100%;
			background-color: #242423!important;
		}

		.numberCircle {
			border-radius: 50%;
			width: 32px;
			height: 32px;
			/* padding: 8px; */

			background: #fff;
			border: 2px solid #666;
			/* color: #666; */
			text-align: center;
			margin: auto;
			font-size: 20px;
		}
	</style>
</head>
<body>
	
	<div class="jumbotron paral paralsec overlay">
		
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="card shadow bg-white" style="border: 0px !important;">
					<div class="card-body">
					<?php $this->view('layouts/notif'); ?>
					<div class="row">
						<div class="col-md" style="padding:20px;">
							<img src="<?=base_url('assets/image/logo.png');?>" class="img-fluid mx-auto d-block" style="width:70%;padding-bottom: 30px;"/>

							<div class="text-center justify-content-center mb-3">
								<div class="numberCircle justify-content-center" style="color: white; border-color: #4285f4; background-color: #4285f4">1</div>
							</div>
							<h4 class="text-center">Pendaftaran Siswa Baru</h4>
							<h4 class="text-center">Tahun Ajaran <?=$setting['tahun_ajaran']?></h4>

							<div style="padding:10px;">
								<a href="/daftar" class="btn btn-primary btn-block" >Daftar Siswa Baru</a>
							</div>

							<p class="text-center mt-4" style="font-size:8pt;padding-bottom: 10px;">-------------------------------------------------------------------------------------------------------</p>

							<div class="text-center justify-content-center mb-3">
								<div class="numberCircle justify-content-center" style="color: white; border-color: #f37021; background-color: #f37021">2</div>
							</div>
							<h4 class="text-center">Konfirmasi Pembayaran</h4>
							<h4 class="text-center">Biaya Pendaftaran Siswa Baru</h4>

							<div style="padding:10px;">
								<a href="/daftar/konfirmasi_bayar" class="btn btn-warning btn-block" style="background-color: #f37021!important;">Konfirmasi Pembayaran</a>
							</div>

							<p class="text-center mt-4" style="font-size:8pt;padding-bottom: 10px;">-------------------------------------------------------------------------------------------------------</p>

							
							<div class="text-center justify-content-center mb-3">
								<div class="numberCircle justify-content-center" style="color: white; border-color: #00c851; background-color: #00c851">3</div>
							</div>
							<h4 class="text-center">Pengisian Form Kelengkapan</h4>
							<h4 class="text-center">Pendaftaran Siswa Baru</h4>

							<p class="text-center" style="font-size:10pt;padding-bottom: 10px;">Pastikan telah memiliki Kode Unik yang di dapatkan melalui e-mail. Jika belum memiliki silahkan menghubungi bagian Administrasi PSB Sekolah Alam Indonesia.</p>
							<?php echo form_open_multipart('login/cekLogin', array('id'=>'form-submit')); ?>
							<div style="padding:10px;">
								<div class="form-group" style="font-size:12pt;margin-bottom: 0.7rem;">
									<div class="row">
										<div class="col">
											<i class="fa fa-user" style="color: #6d7073;"></i>
											<span style="color: #6d7073;">Email</span>
										</div>
										<div class="col text-right">
											<span id="alert_email" style="color: #fd7e14;display:none;font-size:8pt;">Email Wajib Diisi!</span>
										</div>
									</div>
									<input type="text" class="form-control" id="email" name="email" style="font-size:15px;">
								</div>
								<div class="form-group" style="font-size:12pt;margin-bottom: 0.7rem;">
									<div class="row">
										<div class="col">
											<i class="fa fa-lock" style="color: #6d7073;"></i>
											<span style="color: #6d7073;">Kode Unik</span>
										</div>
										<div class="col text-right">
											<span id="alert_kode_unik" style="color: #fd7e14;display:none;font-size:8pt;">Kode Unik Wajib Diisi!</span>
										</div>
									</div>
									<input type="password" class="form-control" id="kode_unik" name="kode_unik" style="font-size:15px;">
								</div>
								<button id="btn_submit" type="button" class="btn btn-success btn-block" onclick="login()">Masuk</button>
								<?php echo form_close(); ?>
								<div style="padding-top:8px;"></div>
								
								<br>
								<p class="text-center">Butuh Bantuan? <a href="https://wa.me/<?=$setting['admin_wa']?>">Hubungi Administrasi PSB</a></p>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>

	</div>

<!-- Footer -->
<footer class="page-footer font-small blue pt-0 footer">
	<!-- Copyright -->
	<div class="footer-copyright text-center py-3">© 2020 Copyright:
		<a href="<?=$setting['unit_url']?>"> Sekolah Alam Indonesia <?=$setting['unit']?></a>
	</div>
</footer>


<!-- Javascript 
<script src="<?=base_url('assets/bootstrap/js/bootstrap.bundle.js'); ?>"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap.js'); ?>"></script>
-->

<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/jquery-3.4.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/popper.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/mdb.min.js'); ?>"></script>
<script type="text/javascript">
	function login(){
		if(validasi()){
			$('#form-submit').submit();
		}
	}

	function validasi(){
		var cek = true;
		if($('#email').val()){
			$('#alert_email').hide();
			cek = true;
		}else{
			$('#alert_email').show();
			cek = false;
		}

		if($('#kode_unik').val()){
			$('#alert_kode_unik').hide();
			cek = true;
		}else{
			$('#alert_kode_unik').show();
			cek = false;
		}
		return cek;
	}

	$("#email").on( "keydown", function(event) {
      if(event.which == 13) 
	  	$("#btn_submit").click();
    });

	$("#kode_unik").on( "keydown", function(event) {
      if(event.which == 13) 
	  	$("#btn_submit").click();
    });

</script>
</body>
</html>