<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS 
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap-grid.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap-reboot.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/font-awesome/css/font-awesome.min.css'); ?>">
	-->
	
	<!-- Material Bootstrap CSS -->
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<link href="<?=base_url('assets/material_bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?=base_url('assets/material_bootstrap/css/mdb.min.css'); ?>" rel="stylesheet">
	<!-- <link href="<?=base_url('assets/material_admin/assets/css/material-dashboard.css?v=2.1.1'); ?>" rel="stylesheet" /> -->

	<script src="<?=base_url('assets/material_admin/assets/js/plugins/moment.min.js'); ?>"></script>

	<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/jquery-3.4.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/material_admin/assets/js/plugins/bootstrap-datetimepicker.min.js'); ?>"></script>

	<link rel="shortcut icon" type="image/png" href="<?=base_url('assets/image/logo.png');?>"/>
	

    <title><?=$title?></title>

	<style>
		.paral {
			min-height:100vh;
			background-attachment: fixed;
			background-size: cover;
			background-position: 50% 50%;
		}
		.paralsec {
			//background-color: #000;
			//background-image: linear-gradient(180deg, #51DDDD 0%, #2C66B9 100%, #2C66B9 100%);
			//background-image:url('http://imansulaiman.com/wp-content/uploads/2019/04/25db546b-4135-4c2e-9be2-2409dbfa4b36.jpg');
			background-image: url("<?=base_url('assets/image/bg.png'); ?>");
			background-color: rgba(255, 255, 255, 0.8);
		}
		.jumbotron{
			margin-bottom: 0;
		}
		.footer {
			position: fixed;
			left:0;
			bottom: 0;
			width: 100%;
			background-color: #242423!important;
		}
	</style>
</head>
<body>
	
	<div class="jumbotron paral paralsec overlay">
		
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="card shadow bg-white" style="border: 0px !important;">
					<div class="card-body">
					<?php $this->view('layouts/notif'); ?>
					<div class="row">
						<div class="col-md" style="padding-left:40px; padding-right:40px;">
							<img src="<?=base_url('assets/image/logo.png');?>" class="img-fluid mx-auto d-block" style="width:50%;padding-bottom: 10px;"/>

							<h5 class="text-center mb-4">Konfirmasi Pembayaran <br>Biaya Pendaftaran Siswa Baru</h5>

							<!-- <p class="text-center" style="font-size:8pt;padding-bottom: 10px;">Pastikan telah memiliki Kode Unik yang di dapatkan dari Sekolah. Jika belum memiliki silahkan menghubungi bagian Administrasi Sekolah Alam Indonesia.</p> -->

							<?php echo form_open_multipart('daftar/submit_konfirmasi', array('id'=>'form-submit')); ?>

							<div class="row">

								<div class="col-md-12" style="padding:10px;">

									<!-- <h6 class="">Pembayaran Biaya Pendaftaran</h6> -->
									<span class="mb-4" style="font-size: x-small">*Pastikan nominal transfer telah sesuai dengan daftar biaya untuk masing-masing jenjang</span>

									<div class="form-group mt-2" style="font-size:10pt;margin-bottom: 0.7rem;">
										<div class="row">
											<div class="col">
												<i class="fa fa-id-card mr-1" style="color: #6d7073;"></i>
												<span style="color: #6d7073;">Nomor Pendaftaran</span>
											</div>
											<div class="col-md-12 text-right">
												<span id="alert_no_daftar" style="color: #fd7e14;display:none;font-size:8pt;">Nomor Pendaftaran harus diisi!</span>
											</div>
										</div>
										<input type="text" class="form-control" id="no_daftar" name="no_daftar" style="font-size:15px;" required>
									</div>

									<div class="form-group mt-2" style="font-size:10pt;margin-bottom: 0.7rem;">
										<div class="row">
											<div class="col">
												<i class="fa fa-calendar-alt mr-1" style="color: #6d7073;"></i>
												<span style="color: #6d7073;">Tanggal Transfer</span>
											</div>
											<div class="col text-right">
												<span id="alert_tgl_transfer" style="color: #fd7e14;display:none;font-size:8pt;">Tanggal Transfer harus diisi!</span>
											</div>
										</div>
										<input placeholder="Selected date" type="date" id="tgl_transfer" class="form-control datepicker" name="tgl_transfer" value="" required>
									</div>

									<div class="form-group mt-2" style="font-size:10pt;margin-bottom: 0.7rem;">
										<div class="row">
											<div class="col">
												<i class="fa fa-sticky-note mr-1" style="color: #6d7073;"></i>
												<span style="color: #6d7073;">Nomor Transaksi / Referensi / Struk ATM</span>
											</div>
											<div class="col-md-12 text-right">
												<span id="alert_ref" style="color: #fd7e14;display:none;font-size:8pt;">Nomor Referensi harus diisi!</span>
											</div>
										</div>
										<input type="text" class="form-control" id="no_ref" name="no_ref" style="font-size:15px;" required>
									</div>

									<div class="form-group mt-2" style="font-size:10pt;margin-bottom: 0.7rem;">
										<div class="row">
											<div class="col-md-12">
												<i class="fa fa-image mr-1" style="color: #6d7073;"></i>
												<span style="color: #6d7073;">Upload Bukti Transfer</span>

												<div class="col text-right">
													<span id="alert_foto_transfer" style="color: #fd7e14;display:none;font-size:8pt;">Foto Transfer belum diupload!</span>
												</div>
												<div>
													<img id="image_preview_container" class="img-fluid mb-2 mt-2" src=""  style="margin: auto; max-height: 400px; border-radius: 5px; cursor: pointer;">
												</div>
												<button type="button" id="upload_btn" class="btn btn-primary btn-block btn-sm" onclick="">Pilih File</button>
                								<input type="file" name="foto_transfer" id="file" onchange="setfilename(this.value)" style="display:none;" required/>
												<div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
												<div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 10 MB</i></span></div>
											</div>
											
										</div>
										
									</div>

									
								</div>

							</div>

							<div class="text-center mt-4">
								<a href="#" id="btn_submit" class="btn btn-success" onclick="konfirmasi()">Simpan</a>
								<input type="submit" id="submitBtn" style="display: none;">
								<?php echo form_close(); ?>

								<div style="padding-top:8px;">
									<p class="text-center"><a href="/login">Kembali ke Beranda</a></p>
									<p class="text-center">Butuh Bantuan? <a href="https://wa.me/<?=$setting['admin_wa']?>">Hubungi Administrasi PSB</a></p>
								</div>
								
							</div>
							
						</div>

						
					</div>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>

		<div class="modal fade" id="modal_konfirm" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header p-3 mb-4" style="justify-content: flex-start;background-color: #f1f2f3;border-radius: 10px 10px 0 0;">
						<p class="form-row-content mb-0"><h6>Konfirmasi Pembayaran</h6></p>
					</div>            
					<div class="modal-body">
						Apakah semua data telah sesuai?
					</div>
					<div class="modal-footer text-center">
						<a href="#" class="btn btn-danger" id="btnBatal" data-toggle="modal" data-target="#modal_konfirm">Batal</a>
						<a href="#"  class="btn btn-success" id="btnSubmit" onclick="return validate()"><div class="spinner-border mr-2" id="spin" role="status" style="height: 17px; width: 17px; display: none;"></div>Simpan</a>
					</div>
				</div>    
			</div>
		</div>

	</div>

<!-- Footer -->
<footer class="page-footer font-small blue pt-0 footer">
	<!-- Copyright -->
	<div class="footer-copyright text-center py-3">© 2024 Copyright
		<a href="<?=$setting['unit_url']?>"> Sekolah Alam Indonesia <?=$setting['unit']?></a>
	</div>
</footer>


<!-- Javascript 
<script src="<?=base_url('assets/bootstrap/js/bootstrap.bundle.js'); ?>"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap.js'); ?>"></script>
-->


<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/popper.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/mdb.min.js'); ?>"></script>


<script>

    function konfirmasi() {
		if(validate()) {
			$('#modal_konfirm').modal('toggle');
		}
    }

	$('#upload_btn').on('click', function() {
		$('#file').click();
	});

	$('#btnSubmit').on('click', function() {
		$('#spin').show();
		$('#submitBtn').click();
		$('#btnSubmit').prop("disabled",true);
	});

	$('#btnBatal').on('click', function() {
		$('#modal_konfirm').modal('toggle');
	});

	$('#file').change(function(e){

		var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
		var name = this.files[0].name;
		var name_arr = name.split(".");
		var type = name_arr[name_arr.length - 1];
		var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
		var pesan_size = "\nMohon masukkan file dengan ukuran max. 10 MB";
		var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
		var cek_size = size >= 10; // max file size 10 MB
		var cek_tipe = allowed.indexOf(type) == -1;

		if(cek_size || cek_tipe) {
			var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
			if(cek_size)
				pesan += pesan_size;

			if(cek_tipe)
				pesan += pesan_tipe;

			alert(pesan);
			a.value = '';

		} else {
			
			let reader = new FileReader();
			reader.onload = (e) => { 
				$('#image_preview_container').attr('src', e.target.result); 
			}
			reader.readAsDataURL(this.files[0]); 

		}
	});

	function validate() {
		var res = true;

		// if ($("#email").val() == '') {
		// 	$("#alert_email").show();
		// 	res = res && false;
		// } else {
		// 	$("#alert_email").hide();
		// }

		if ($("#no_daftar").val() == '') {
			$("#alert_no_daftar").show();
			res = res && false;
		} else {
			$("#alert_no_daftar").hide();
		}

		if ($("#tgl_transfer").val() == '') {
			$("#alert_tgl_transfer").show();
			res = res && false;
		} else {
			$("#alert_tgl_transfer").hide();
		}

		if ($("#no_ref").val() == '') {
			$("#alert_ref").show();
			res = res && false;
		} else {
			$("#alert_ref").hide();
		}

		if ($("#file").val() == '') {
			$("#alert_foto_transfer").show();
			res = res && false;
		} else {
			$("#alert_foto_transfer").hide();
		}
		
		return res;
	}

</script>

</script>
</body>
</html>