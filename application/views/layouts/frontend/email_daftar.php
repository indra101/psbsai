<img src="<?=$setting['unit_url_psb']?>/assets/image/logo.png"/>
<br/>
<br/>
<h2>Konfirmasi Pendaftaran Siswa Baru Tahun Ajaran <?=$setting['tahun_ajaran']?></h2><br/>
<div style='font-size: medium'>
    <strong>Assalamu'alaikum Wr Wb</strong>
    <br>
    <br>
    <?php $panggil = ($ortu['kelamin'] == 'Laki-laki') ? 'Bapak' : 'Ibu'; ?> 
    <?=$panggil?> <?=$ortu['nama']?>
    <br>
    Yang kami hormati,
    <br>
    <br>
    Pendaftaran siswa baru telah berhasil disimpan dengan rincian sebagai berikut: 
    <br>
    <br>
    Nomor Pendaftaran: <b><?=$ortu['no_daftar']?></b>
    <br>
    <br>
    <table style="border: 1px solid grey; border-collapse: collapse;">
        <thead>
            <th style="border: 1px solid grey; border-collapse: collapse; padding: 15px;">No.</th>
            <th style="border: 1px solid grey; border-collapse: collapse; padding: 15px;">Nama</th>
            <th style="border: 1px solid grey; border-collapse: collapse; padding: 15px;">Jenjang</th>
            <th style="border: 1px solid grey; border-collapse: collapse; padding: 15px;">Biaya Pendaftaran</th>
        </thead>
        <tbody>
            <?php $n = 1; foreach($siswa as $s) { ?>
                <tr>
                    <td style="border: 1px solid grey; border-collapse: collapse; padding: 15px;"><?=$n.'.'?></td>
                    <td style="border: 1px solid grey; border-collapse: collapse; padding: 15px;"><?=$s['nama']?></td>
                    <td style="border: 1px solid grey; border-collapse: collapse; padding: 15px;"><?=$s['nama_level']?></td>
                    <td style="border: 1px solid grey; border-collapse: collapse; padding: 15px;">Rp <?=number_format($s['biaya'],0,",",".")?></td>
                <tr>
            <?php $n++; } ?>
        </tbody>
    </table>
    <br>
    <br>
    Silahkan melakukan pembayaran biaya pendaftaran dengan nilai total <b>Rp <?=number_format($ortu['tot_biaya'],0,",",".")?></b> ke rekening <?=$setting['rekening_bank']?> nomor <?=$setting['rekening_no']?> atas nama <?=$setting['rekening_nama']?> 
    <br>dengan menyertakan nama calon siswa dan jenjangnya pada kolom keterangan, <b>contoh: "Biaya Pendaftaran Budi SD 1"</b>.
    <br>
    Setelah melakukan pembayaran harap menyimpan bukti pembayaran dan melakukan konfirmasi pembayaran melalui menu Konfirmasi Pembayaran dengan memasukkan Nomor Pendaftaran.
    <br>
    <br>
    Apabila terdapat informasi yang belum jelas, <?=$panggil?> dapat menghubungi bagian Admin PSB SAI <?=$setting['unit']?> dengan <?=$setting['admin_nama']?> melalui nomor <?=$setting['admin_hp']?> 
    <br>pada hari kerja pukul 08.30 - 11.30 WIB.
    <br>
    <br>
    Terima kasih atas kesediaan <?=$panggil?> mendaftarkan putra/i-nya di Sekolah Alam Indonesia <?=$setting['unit']?>.
    <br/>
    <br/>
    <br/>
    <strong>Wassalamu'alaikum Wr Wb</strong>
    <br/>
    <br/>
    <br/>
    Admin PSB
    <br>
    Sekolah Alam Indonesia <?=$setting['unit']?>
    <br/>
    <br/>
    <!-- <img src="<?=$setting['unit_url_psb']?>/assets/image/bukit_bawah.png"/> -->
</div>