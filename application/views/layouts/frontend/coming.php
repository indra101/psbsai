<!DOCTYPE html>
<html lang="id">

<head>
    <title>Coming Soon</title>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta content="Halaman default" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.11/css/mdb.min.css" rel="stylesheet">
	<link rel="shortcut icon" type="image/png" href="<?=base_url('assets/image/favicon.png');?>"/>
    <style>
        .paral {
			min-height:100vh;
			background-attachment: fixed;
			background-size: cover;
			background-position: 50% 50%;
		}
		.paralsec {
			background-image: url("<?=base_url('assets/image/bg.png'); ?>");
			background-color: rgba(255, 255, 255, 0.8);
		}
		.jumbotron{
			margin-bottom: 0;
		}
		.footer {
			position: fixed;
			left:0;
			bottom: 0;
			width: 100%;
			background-color: #242423!important;
		}
    </style>
</head>

<body>
<div class="jumbotron paral paralsec overlay">
		
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="card shadow bg-white" style="border: 0px !important; border-radius: 20px;">
					<div class="card-body">
					<div class="row">
						<div class="col-md" style="padding:20px;">
							<img src="<?=base_url('assets/image/logo.png');?>" class="img-fluid mx-auto d-block" style="padding-bottom: 30px;"/>
							<h1 class="text-center">Pendaftaran Siswa Baru</h1>
							<h2 class="text-center">Tahun Ajaran 2024/2025</h2>

							<img src="<?=base_url('assets/image/coming-soon.png');?>" class="img-fluid mx-auto d-block mt-4" style="padding-bottom: 30px;"/>

							
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>

	</div>

<!-- Footer -->
<footer class="page-footer font-small blue pt-0 footer">
	<!-- Copyright -->
	<div class="footer-copyright text-center py-3">© 2020 Copyright:
		<a href="https://sai-cipedak.sch.id"> Sekolah Alam Indonesia</a>
	</div>
</footer>
</body>

</html>
