<br><br>
<!-- Footer -->
<footer class="page-footer font-small blue pt-0 footer website">
	<!-- Copyright -->
	<div class="footer-copyright text-center py-3">© 2019 Copyright:
		<a href="https://sekolahalamindonesia.org/"> Sekolah Alam Indonesia</a>
	</div>
</footer>
<footer class="page-footer font-small blue pt-0 footer mobile">
	<!-- Copyright -->
	<div class="row text-center py-3">
      <div class="col tombol tombol-aktif"><a href="<?=base_url('siswa/dashboard')?>"><i class="fas fa-home"></i><br>Home</a></div>
      <div class="col tombol"><a href="<?=base_url('siswa/tahapan')?>"><i class="fas fa-list-alt"></i><br>Tahapan</a></div>
      <!--<div class="col tombol"><a href="<?=base_url('siswa/pesan')?>"><i class="fas fa-envelope"></i><br>Pesan</a></div>-->
      <div class="col tombol"><a href="<?=base_url('siswa/akun')?>"><i class="fas fa-user"></i><br>Akun</a></div>
	</div>
</footer>
<!-- Javascript 
<script src="<?=base_url('assets/bootstrap/js/bootstrap.bundle.js'); ?>"></script>
<script src="<?=base_url('assets/bootstrap/js/bootstrap.js'); ?>"></script>
-->
<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/popper.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('assets/material_bootstrap/js/mdb.min.js'); ?>"></script>

</body>
</html>