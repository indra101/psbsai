<img src="<?=$setting['unit_url_psb']?>/assets/image/logo.png"/>
<br/>
<br/>
<h2>Konfirmasi Pembayaran Biaya Pendaftaran Siswa Baru Telah Diterima</h2><br/>
<div style='font-size: medium'>
    <strong>Assalamu'alaikum Wr Wb</strong>
    <br>
    <br>
    <?php $panggil = ($ortu['kelamin'] == 'Laki-laki') ? 'Bapak' : 'Ibu'; ?> 
    <?=$panggil?> <?=$ortu['nama']?>
    <br>
    Yang kami hormati,
    <br>
    <br>
    Terima kasih atas konfirmasi pembayaran yang <?=$panggil?> lakukan atas pendaftaran siswa baru dengan rincian sebagai berikut: 
    <br>
    <br>
    <table style="border: 1px solid grey; border-collapse: collapse;">
        <thead>
            <th style="border: 1px solid grey; border-collapse: collapse; padding: 15px;">No.</th>
            <th style="border: 1px solid grey; border-collapse: collapse; padding: 15px;">Nama</th>
            <th style="border: 1px solid grey; border-collapse: collapse; padding: 15px;">Jenjang</th>
        </thead>
        <tbody>
            <?php $n = 1; foreach($siswa as $s) { ?>
                <tr>
                    <td style="border: 1px solid grey; border-collapse: collapse; padding: 15px;"><?=$n.'.'?></td>
                    <td style="border: 1px solid grey; border-collapse: collapse; padding: 15px;"><?=$s['nama']?></td>
                    <td style="border: 1px solid grey; border-collapse: collapse; padding: 15px;"><?=$s['nama_level']?></td>
                <tr>
            <?php $n++; } ?>
        </tbody>
    </table>
    <br>
    <br>
    Kami akan melakukan verifikasi data pendaftaran dan konfirmasi pembayaran yang sudah <?=$panggil?> lakukan.
    <br>
    Apabila data verifikasi data telah berhasil maka Anda akan mendapatkan kode unik melalui e-mail untuk dapat melakukan pengisian form kelengkapan data pendaftaran.
    <br>
    <br>
    Apabila terdapat informasi yang belum jelas, <?=$panggil?> dapat menghubungi bagian Admin PSB SAI <?=$setting['unit']?> dengan <?=$setting['admin_nama']?> melalui nomor <?=$setting['admin_hp']?> pada hari kerja pukul 08.30 - 11.30 WIB.
    <br>
    <br>
    Terima kasih atas perhatian <?=$panggil?>.
    <br/>
    <br/>
    <br/>
    <strong>Wassalamu'alaikum Wr Wb</strong>
    <br/>
    <br/>
    <br/>
    Admin PSB
    <br>
    Sekolah Alam Indonesia <?=$setting['unit']?>
    <br/>
    <br/>
    <!-- <img src="<?=$setting['unit_url_psb']?>/assets/image/bukit_bawah.png"/> -->
</div>